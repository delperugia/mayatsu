// Copyright 2018 Pierre del Perugia
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

//
// Miscellaneous functions
//

#ifndef thoe_misc_h
#define thoe_misc_h

#include <string>

namespace thoe {

namespace xml2json {

//-------------------------------------------------------------------------
// To be called once when program starts
void init ( void );

//-------------------------------------------------------------------------
// Converts an XML document into a JSON object
// If a node has no attribute and contains only a single text,
// the corresponding JSON object property will be a string.
// If not, each node is converted to a JSON objects where:
//  - each attribute is added as a property (prefixed with '@')
//  - each sub-node is added as a property
//  - the child texts are name #text
//  - nodes with same name are grouped into array
// E.g.:
//  <alpha id="9">                  {
//      lorem                         "alpha": {
//      <beta/>                         "#text": [
//      <gamma></gamma>                   "\n    lorem\n    ",
//      <delta>d</delta>                  "\n    ipsum\n    ",
//      <epsilon a="2" b="3" />           "\n    dolor\n"
//      <zeta c="4">z</zeta>            ],
//      ipsum                           "@id": "9",
//      <eta>one</eta>                  "delta": "d",
//      <eta>two</eta>                  "epsilon": { "@a": "2", "@b": "3" },
//      <eta d="1">three</eta>          "eta":     [ "one",
//                                                   "two"
//  </alpha>                                         { "#text": "three", "@d": "1" } ],
//                                      "zeta":    { "#text": "z", "@c": "4" }
//                                    }
//                                  }
//
// This must be very close to the description found at:
//   https://www.xml.com/pub/a/2006/05/31/converting-between-xml-and-json.html
//
bool convert ( const std::string &  p_xml,
               std::string &        p_json );

} // namespace xml2json

} // namespace thoe

#endif
