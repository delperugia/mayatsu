// Copyright 2018 Pierre del Perugia
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

//
// Convenient functions working on Spidermonkey native API
//

#ifndef thoe_javascript_h
#define thoe_javascript_h

#include <string>

#include "thoe_mozjs.h"
#include "thoe_script_executor.h"

namespace thoe {

//-------------------------------------------------------------------------

// Log an error at the service level, eventually adding last error
void js_service_error_log( const std::string & p_message, ThreadFrame * p_frame );

//-------------------------------------------------------------------------
// Function adding global objects, functions and classes

// Define the require function in the global object
bool js_add_blob_class ( JSContext *                                p_context,
                         JS::HandleObject                           p_global );

// Define the crypto object in the global object
bool js_add_crypto_object ( JSContext *                             p_context,
                            JS::HandleObject                        p_global );

// Define the MySQL class in the global object
bool js_add_mysql_class ( JSContext *                               p_context,
                          JS::HandleObject                          p_global );

// Define HTTP functions http_request and jresp_request
bool js_add_http_object ( JSContext *                               p_context,
                          const std::string &                       p_document,
                          const masnae::Fastcpipp_Environment &     p_environment,
                          JS::HandleObject                          p_global );

// Define the Money global object
bool js_add_money_class ( JSContext *                               p_context,
                          JS::HandleObject                          p_global );

// Define the Request global object
bool js_add_request_object ( JSContext *                            p_context,
                             const std::string &                    p_document,
                             const masnae::Fastcpipp_Environment &  p_environment,
                             const std::string &                    p_security_json,
                             JS::HandleObject                       p_global );
                            
// Define the require function in the global object
bool js_add_require_function ( JSContext *                          p_context,
                               JS::HandleObject                     p_global );

// Define the system functions in the global object
bool js_add_system_object ( JSContext *                             p_context,
                            JS::HandleObject                        p_global );

//-------------------------------------------------------------------------
// Blob related function

// Instantiate a new Blob object, initialized with data
JSObject * js_new_blob ( JSContext *                                p_context,
                         JS::HandleObject                           p_global,
                         const uint8_t *                            p_data,
                         size_t                                     p_size );

// Check if an object is an instance of Blob
bool js_is_blob ( JS::HandleValue                                   p_value );

// Move or copy the data from the Blob instance to the given Data
void js_get_blob_data ( JS::HandleValue                             p_value, 
                        masnae::Data &                              p_data,
                        bool                                        p_move );

} // namespace thoe

#endif
