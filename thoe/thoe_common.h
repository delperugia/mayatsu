// Copyright 2018 Pierre del Perugia
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

//
// Definitions of global elements
//

#ifndef thoe_common_h
#define thoe_common_h

#include <fastcgi++/manager.hpp>

#include "masnae_bus.h"
#include "masnae_config.h"
#include "masnae_http.h"
#include "thoe_script_executor.h"
#include "thoe_script_loader.h"
#include "thoe_request.h"

//-------------------------------------------------------------------------
namespace thoe {
namespace constants {

constexpr size_t    k_post_max_size           = 1024 * 1024;
constexpr unsigned  k_threads_per_cpu         = 2;
constexpr char      k_env_var[]               = "THOE_CONFIG";
constexpr char      k_default_config_file[]   = "/var/www/cgi-config/thoe.json";
constexpr char      k_default_script_folder[] = "/var/www/cgi-scripts/";
constexpr char      k_default_log_folder[]    = "/var/log/thoe/";

} // namespace constants
} // namespace thoe

//-------------------------------------------------------------------------
// Global variables
extern thoe::ScriptExecutor   g_ScriptExecutor;
extern thoe::ScriptLoader     g_ScriptLoader;

#endif
