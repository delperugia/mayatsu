// Copyright 2018 Pierre del Perugia
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

//
// Auto-test and console mode
//

#ifndef thoe_validate_h
#define thoe_validate_h

#include <string>

namespace thoe {

//-------------------------------------------------------------------------
// Interactive console session to test url
void console ( void );

//-------------------------------------------------------------------------
// The test program must return a JSend result with data:
//      { x: 42, y: "forty-two", z: [ 2, 3, 7 ] }
// To be launched with:
//      ./thoe.fcgi -t thoe_validate.js -f `pwd`/thoe_config_validate.json
void validate ( const std::string &  p_test_url, bool *  p_succeeded );

} // namespace thoe

#endif
