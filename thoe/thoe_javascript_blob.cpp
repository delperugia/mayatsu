// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

// Copyright 2018 Pierre del Perugia
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <adawat/adawat_encoding.h>

#include "masnae_misc.h"
#include "thoe_mozjs.h"
#include "thoe_common.h"
#include "thoe_javascript.h"
#include "thoe_js_tools.h"

namespace thoe {

#pragma GCC diagnostic ignored "-Wmissing-field-initializers"

//-------------------------------------------------------------------------
// The private data attached to a Blob object
typedef struct {
    masnae::Data data;   // the data itself
} Blob;

//-------------------------------------------------------------------------
// Blob class declaration
static
bool js_blob_constructor( JSContext * p_context, unsigned p_argc, JS::Value * p_argv );

static
void js_blob_finalize( JSFreeOp * p_free_op, JSObject * p_object );

static
JSClassOps js_blob_class_ops = {                      
    nullptr,                // addProperty
    nullptr,                // delProperty
#if MOZJS_MAJOR_VERSION < 60
    nullptr,                // getProperty
    nullptr,                // setProperty
#endif
    nullptr,                // enumerate
#if MOZJS_MAJOR_VERSION >= 60
    nullptr,                // newEnumerate
#endif
    nullptr,                // resolve
    nullptr,                // mayResolve
    js_blob_finalize,       // finalize
    nullptr,                // call
    nullptr,                // hasInstance
    js_blob_constructor,    // construct
    nullptr                 // trace
};

static
JSClass js_blob_class = {
    "Blob",
    JSCLASS_HAS_PRIVATE | JSCLASS_FOREGROUND_FINALIZE,
    & js_blob_class_ops
};

//-------------------------------------------------------------------------
static
bool js_blob_constructor( JSContext * p_context, unsigned p_argc, JS::Value * p_argv )
{
    JS::CallArgs args = JS::CallArgsFromVp( p_argc, p_argv );
    //
    // Build the object
    JSObject * object = JS_NewObjectForConstructor( p_context, & js_blob_class, args );
    if ( object == nullptr ) {
        JS_ReportOutOfMemory( p_context );
        return false;
    }
    //
    // Allocate private data
    auto blob = std::unique_ptr< Blob >( new (std::nothrow) Blob() );
    if ( blob == nullptr ) {
        JS_ReportOutOfMemory( p_context );
        return false;
    }
    //
    JS_SetPrivate( object, blob.release() );
    //
    // Return the new object
    args.rval().setObject( * object );
    return true;
}

//-------------------------------------------------------------------------
// Note that finalize is called twice, 2nd time private is nullptr
static
void js_blob_finalize( JSFreeOp * p_free_op, JSObject * p_object )
{
    auto blob = static_cast< Blob* >( JS_GetPrivate( p_object ) );
    //
    if ( blob != nullptr )
        delete blob;
}

//-------------------------------------------------------------------------
// Returns an element of the Blob
static
bool js_blob_at( JSContext * p_context, unsigned p_argc, JS::Value * p_argv )
{
    JS::CallArgs args  = JS::CallArgsFromVp( p_argc, p_argv );
    //
    if ( ! args.thisv().isObject() || 
         JS_GetClass( & args.thisv().toObject() ) != & js_blob_class )
    {
        JS_ReportErrorUTF8( p_context, "Blob.at: called on non-object" );
        return false;
    }
    //
    if ( ! js_tools_check_argument( p_context, "Blob.at", args, "I" ) )
        return false;
    //
    //-------
    // Retrieve parameters
    int32_t index = args[0].toInt32();
    //
    // Retrieve the Blob
    Blob * blob = static_cast< Blob* >( JS_GetPrivate( & args.thisv().toObject() ) );
    //
    if ( index < 0 ||
         static_cast< uint32_t >( index ) > blob->data.size() )
    {
        JS_ReportErrorUTF8( p_context, "Blob.at: invalid index" );
        return false;
    }
    //
    args.rval().setInt32( blob->data[ static_cast< uint32_t >( index ) ] );
    //
    return true;
}

//-------------------------------------------------------------------------
// Set the blob with the content of file
static
bool js_blob_from_file( JSContext * p_context, unsigned p_argc, JS::Value * p_argv )
{
    JS::CallArgs args  = JS::CallArgsFromVp( p_argc, p_argv );
    //
    if ( ! args.thisv().isObject() || 
         JS_GetClass( & args.thisv().toObject() ) != & js_blob_class )
    {
        JS_ReportErrorUTF8( p_context, "Blob.fromFile: called on non-object" );
        return false;
    }
    //
    //-------
    if ( ! js_tools_check_argument( p_context, "Blob.fromFile", args, "S" ) )
        return false;
    //
    // Get file name and open it
    std::string file_name = js_tools_convert( p_context, args[0].toString() );
    FILE *      file      = fopen( file_name.c_str(), "rb" );
    if ( file == nullptr ) {
        JS_ReportErrorUTF8( p_context, "Blob.fromFile: cannot open file" );
        return false;
    }
    //
    // Get file size
    size_t file_size;
    //
    {
        long int position  = -1;
        //
        if ( fseek( file, 0, SEEK_END)     != 0  ||
             ( position = ftell( file ) )   < 0  ||
             fseek( file, 0, SEEK_SET)     != 0 )
        {
            fclose( file );
            JS_ReportErrorUTF8( p_context, "Blob.fromFile: cannot get file size" );
            return false;
        }
        //
        file_size = static_cast< size_t >( position );  // position >=0
    }
    //
    // Retrieve blob and resize it
    Blob * blob  = static_cast< Blob* >( JS_GetPrivate( & args.thisv().toObject() ) );
    //
    blob->data.resize( file_size);
    //
    // Read file
    bool ok = fread( blob->data.data(), 1, file_size, file ) == file_size;
    fclose( file );
    //
    if ( ! ok ) {
        JS_ReportErrorUTF8( p_context, "Blob.fromFile: cannot read file" );
        return false;
    }
    //
    // Update the size property with the new size
    JS::RootedObject object( p_context, & args.thisv().toObject() );
    JS::RootedValue  size  ( p_context, JS::Int32Value( static_cast< int32_t >( blob->data.size() ) ) );
    //
    JS_SetProperty( p_context, object, "size", size );
    //
    // Returns the object to allow method chaining
    args.rval().setObject( args.thisv().toObject() );
    return true;
}

//-------------------------------------------------------------------------
// Set the blob with the given string
// 2nd optional argument specify the encoding of the string: 
//    none,base16,base32,base32hex,base64,base64url,base64_xx
static
bool js_blob_set( JSContext * p_context, unsigned p_argc, JS::Value * p_argv )
{
    JS::CallArgs args  = JS::CallArgsFromVp( p_argc, p_argv );
    //
    if ( ! args.thisv().isObject() || 
         JS_GetClass( & args.thisv().toObject() ) != & js_blob_class )
    {
        JS_ReportErrorUTF8( p_context, "Blob.set: called on non-object" );
        return false;
    }
    //
    if ( ! js_tools_check_argument( p_context, "Blob.set", args, "S/S" ) )
        return false;
    //
    //-------
    // Retrieve parameters
    std::string string = js_tools_convert( p_context, args[0].toString() );
    std::string encoding;
    //
    if ( p_argc > 1 )
        encoding = js_tools_convert( p_context, args[1].toString() );
    else
        encoding = "none";
    //
    // Create a new Blob
    Blob *  blob = static_cast< Blob * >( JS_GetPrivate( & args.thisv().toObject() ) );
    //
    //-------
    // Decode string and copy into blob
    if ( encoding == "none" )
    {
        blob->data.assign( string.begin(), string.end() );  // nothing to do in that case
    }
    else
    {
        bool          ok;
        adawat::Data  data;
        //
             if ( encoding == "base16" )
                ok = adawat::encoding_base16_decode( string, data );
        //
        else if ( encoding == "base32" )
                ok = adawat::encoding_base32_decode( string, data, false );
        else if ( encoding == "base32hex" )
                ok = adawat::encoding_base32_decode( string, data, true );
        //
        else if ( encoding == "base64" )
                ok = adawat::encoding_base64_decode( string, data, adawat::base64_type::standard );
        else if ( encoding == "base64url" )
                ok = adawat::encoding_base64_decode( string, data, adawat::base64_type::url );
        else if ( encoding == "base64_xx" )
                ok = adawat::encoding_base64_decode( string, data, adawat::base64_type::xxencoding );
        //
        else {
            JS_ReportErrorUTF8( p_context, "Blob.set: invalid encoding" );
            return false;
        }
        //
        if ( ! ok ) {
            JS_ReportErrorUTF8( p_context, "Blob.set: cannot decode string" );
            return false;
        }
        //
        blob->data.assign( data.begin(), data.end() );
    }
    //
    //-------
    // Cleanup space
    blob->data.shrink_to_fit();
    //
    // Update the size property with the new size
    JS::RootedObject object( p_context, & args.thisv().toObject() );
    JS::RootedValue  size  ( p_context, JS::Int32Value( static_cast< int32_t >( blob->data.size() ) ) );
    JS_SetProperty( p_context, object, "size", size );
    //
    // Returns the object to allow method chaining
    args.rval().setObject( args.thisv().toObject() );
    return true;
}

//-------------------------------------------------------------------------
// Returns the Blob as a Uint8Array
static
bool js_blob_to_arraybuffer( JSContext * p_context, unsigned p_argc, JS::Value * p_argv )
{
    JS::CallArgs args  = JS::CallArgsFromVp( p_argc, p_argv );
    //
    if ( ! args.thisv().isObject() || 
         JS_GetClass( & args.thisv().toObject() ) != & js_blob_class )
    {
        JS_ReportErrorUTF8( p_context, "Blob.toArrayBuffer: called on non-object" );
        return false;
    }
    //
    //-------
    // Retrieve the Blob
    Blob * blob = static_cast< Blob* >( JS_GetPrivate( & args.thisv().toObject() ) );
    //
    uint8_t * buffer = static_cast< uint8_t * >( malloc( blob->data.size() ) );
    if ( buffer == nullptr ) {
        JS_ReportOutOfMemory( p_context );
        return false;
    }
    //
    memcpy( buffer, blob->data.data(), blob->data.size() );
    //
    JS::RootedObject array( p_context,
                            JS_NewArrayBufferWithContents(
                                p_context,
                                blob->data.size(),
                                buffer ) );
    //
    JS::RootedObject obj( p_context,
                          JS_NewUint8ArrayWithBuffer( p_context, array, 0, -1 ) );
    //
    args.rval().setObject( * obj );
    //
    return true;
}

//-------------------------------------------------------------------------
// Save the content of a blob to a file
// Receives the file name as parameter
static
bool js_blob_to_file( JSContext * p_context, unsigned p_argc, JS::Value * p_argv )
{
    JS::CallArgs args  = JS::CallArgsFromVp( p_argc, p_argv );
    //
    if ( ! args.thisv().isObject() || 
         JS_GetClass( & args.thisv().toObject() ) != & js_blob_class )
    {
        JS_ReportErrorUTF8( p_context, "Blob.toFile: called on non-object" );
        return false;
    }
    //
    //-------
    if ( ! js_tools_check_argument( p_context, "Blob.toFile", args, "S" ) )
        return false;
    //
    // Get file name and open file
    std::string file_name = js_tools_convert( p_context, args[0].toString() );
    FILE *      file      = fopen( file_name.c_str(), "wb" );
    if ( file == nullptr ) {
        JS_ReportErrorUTF8( p_context, "Blob.toFile: cannot open file" );
        return false;
    }
    //
    // Retrieve the Blob
    Blob * blob = static_cast< Blob* >( JS_GetPrivate( & args.thisv().toObject() ) );
    //
    // Write it to file
    bool ok = fwrite( blob->data.data(), 1, blob->data.size(), file ) == blob->data.size();
    fclose( file );
    //
    if ( ! ok ) {
        JS_ReportErrorUTF8( p_context, "Blob.toFile: cannot write file" );
        return false;
    }
    //
    return true;
}

//-------------------------------------------------------------------------
// Returns the Blob as a string
// 1st optional argument specify the encoding to use: 
//    none,base16,base32,base32hex,base64,base64url,base64_xx
// 2nd optional argument specify if padding must be used
static
bool js_blob_to_string( JSContext * p_context, unsigned p_argc, JS::Value * p_argv )
{
    JS::CallArgs args  = JS::CallArgsFromVp( p_argc, p_argv );
    //
    if ( ! args.thisv().isObject() || 
         JS_GetClass( & args.thisv().toObject() ) != & js_blob_class )
    {
        JS_ReportErrorUTF8( p_context, "Blob.toString: called on non-object" );
        return false;
    }
    //
    if ( ! js_tools_check_argument( p_context, "Blob.to_string", args, "/SB" ) )
        return false;
    //
    //-------
    // Retrieve parameters
    std::string encoding;
    bool        padding;
    //
    if ( p_argc > 0 )
        encoding = js_tools_convert( p_context, args[0].toString() );
    else
        encoding = "none";
    //
    if ( p_argc > 1 )
        padding  = args[1].toBoolean();
    else
        padding = true;
    //
    // Retrieve the Blob
    Blob * blob = static_cast< Blob* >( JS_GetPrivate( & args.thisv().toObject() ) );
    //
    if ( encoding == "none" )
    {
        // It must be an UTF-8 string
        if ( ! masnae::is_utf8( blob->data.data(), blob->data.size() ) ) {
            JS_ReportErrorUTF8( p_context, "Blob.toString: invalid UTF-8 string detected" );
            return false;
        }
        //
        // Build a new string
        //
        // JS_NewStringCopyUTF8Z doesn't use the given length but instead
        // look for a terminating 0. Temporary add one (but pass the original
        // length).
        size_t original_length = blob->data.size();
        //
        blob->data.push_back(0);
        //
        JS::ConstUTF8CharsZ utf8_string( reinterpret_cast<char*>( blob->data.data() ),
                                         original_length );  // no string copy here
        JS::RootedString string( p_context,
                                 JS_NewStringCopyUTF8Z( p_context,
                                                        utf8_string ) );
        // And remove terminating 0
        blob->data.pop_back();
        //
        args.rval().setString( string );
    }
    else
    {
        std::string  text;
        //
        // Converts the Blob's data using the given encoding
        //
             if ( encoding == "base16" )
                text = adawat::encoding_base16_encode( blob->data );
        //
        else if ( encoding == "base32" )
                text = adawat::encoding_base32_encode( blob->data, false, padding );
        else if ( encoding == "base32hex" )
                text = adawat::encoding_base32_encode( blob->data, true,  padding );
        //
        else if ( encoding == "base64" )
                text = adawat::encoding_base64_encode( blob->data, adawat::base64_type::standard,   padding );
        else if ( encoding == "base64url" )
                text = adawat::encoding_base64_encode( blob->data, adawat::base64_type::url,        padding );
        else if ( encoding == "base64_xx" )
                text = adawat::encoding_base64_encode( blob->data, adawat::base64_type::xxencoding, padding );
        //
        else {
            JS_ReportErrorUTF8( p_context, "Blob.toString: invalid encoding" );
            return false;
        }
        //
        // Return result
        JS::RootedString string( p_context,
                                 JS_NewStringCopyZ( p_context,
                                                    text.c_str() ) );
        //
        args.rval().setString( string );
    }
    //
    return true;
}

//-------------------------------------------------------------------------
static
const JSPropertySpec js_blob_properties[] = {
    JS_INT32_PS( "size", 0, JSPROP_ENUMERATE | JSPROP_PERMANENT ),  // not JSPROP_READONLY because set by js_blob_set
    JS_PS_END
};

static
const JSFunctionSpec js_blob_methods[] = 
{
    //     name              function                      nargs flags
    JS_FN("at",              js_blob_at,                   1,    0),
    JS_FN("fromFile",        js_blob_from_file,            1,    0),
    JS_FN("set",             js_blob_set,                  1,    0),
    JS_FN("toArrayBuffer",   js_blob_to_arraybuffer,       0,    0),
    JS_FN("toFile",          js_blob_to_file,              1,    0),
    JS_FN("toString",        js_blob_to_string,            0,    0),
    JS_FS_END,
};

//-------------------------------------------------------------------------
// Define the Blob class in the global object
// Called only once by ScriptExecutor::execute_script if global
// is reused (m_optz_clear_global = false).
bool js_add_blob_class( JSContext *       p_context,
                        JS::HandleObject  p_global )
{
    if ( ! JS_InitClass( p_context,
                         p_global,
                         nullptr,               // parent_proto
                         & js_blob_class,
                         js_blob_constructor,
                         0,                     // nb constructor args
                         js_blob_properties,
                         js_blob_methods,
                         nullptr,               // static_ps
                         nullptr ) )            // static_fs
    {
        js_service_error_log( "JavaScript: Failed to define blob class", nullptr );
        return false;
    }
    //
    return true;
}

//-------------------------------------------------------------------------
// Instantiate a new Blob object, initialized with data
JSObject * js_new_blob( JSContext *       p_context,
                        JS::HandleObject  p_global,
                        const uint8_t *   p_data,
                        size_t            p_size )
{
    // Retrieve the Blob class constructor
    JS::RootedValue value( p_context );
    //
    if ( ! JS_GetProperty( p_context, p_global, "Blob", & value) ) {
        JS_ReportErrorUTF8(p_context, "Blob: class not found");
        return nullptr;
    }
    //
    if ( ! value.isObject() ) {
        JS_ReportErrorUTF8(p_context, "Blob: class is not an object");
        return nullptr;
    }
    //
    JS::RootedObject constructor( p_context,
                                  & value.toObject() );
    //
    // Create a new object with this constructor
    JS::RootedObject object( p_context,
                             JS_New( p_context,
                                     constructor,
                                     JS::HandleValueArray::empty() ) );
    //
    if ( object == nullptr ) {
        JS_ReportErrorUTF8(p_context, "Blob: new blob is not an object");
        return nullptr;
    }
    //
    // Copy the data in the Blob
    Blob * blob = static_cast< Blob* >( JS_GetPrivate( object ) );
    //
    blob->data.assign( p_data, p_data + p_size );
    blob->data.shrink_to_fit();
    //
    JS::RootedValue size( p_context, JS::Int32Value( static_cast< int32_t >( p_size ) ) );
    JS_SetProperty( p_context, object, "size", size );
    //
    return object;
}

//-------------------------------------------------------------------------
// Check if an object is an instance of Blob
bool js_is_blob( JS::HandleValue p_value )
{
    return p_value.isObject() &&
           JS_GetClass( & p_value.toObject() ) == & js_blob_class;
}

//-------------------------------------------------------------------------
// Move or copy the data from the Blob instance to the given Data
void js_get_blob_data( JS::HandleValue  p_value, 
                       masnae::Data &   p_data,
                       bool             p_move )
{
    if ( js_is_blob( p_value ) ) {
        Blob * blob = static_cast< Blob* >( JS_GetPrivate( & p_value.toObject() ) );
        //
        if ( p_move )
            p_data = std::move( blob->data );
        else
            p_data = blob->data;
    }
}

} // namespace thoe
