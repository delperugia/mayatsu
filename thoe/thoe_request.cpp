// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

// Copyright 2018 Pierre del Perugia
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <libxml/xmlversion.h>
#include <rapidjson/document.h>

#include "masnae_global.h"
#include "masnae_misc.h"
#include "thoe_common.h"
#include "thoe_request.h"
#include "thoe_js_tools.h"

namespace thoe {

//-------------------------------------------------------------------------
JsRequestHandler::JsRequestHandler() :
    masnae::RequestHandler    ( thoe::constants::k_post_max_size )
{
}

//-------------------------------------------------------------------------
// Returns true if the peer is allowed to call this web service
bool JsRequestHandler::check_security ( const std::string &           p_module,
                                        masnae::Fastcpipp_Path &      p_document,
                                        masnae::ProcessingResponse &  p_result )
{
    // When executed in simulation mode (auto-test or console) use
    // the following security context:
    if ( m_security_simulate_uriana )
    {
        m_security = {
            .user        = "111",
            .profile     = "profile_test",
            .role        = "role_test",
            .scopes      = "refresh profile",
            .permissions = {
                { "perm_1",  "one" },
                { "perm_2",  "two" },
                { "thoe.ws", ""    }
            }
        };
    }
    else // call Uriana to validate the received AccessToken
    {
        std::string accesstoken;
        //
        // Retrieve AccessToken
        if ( p_module == "cb" && p_document.size() > 0 ) {
            accesstoken = p_document.at( 0 );
            //
            if ( accesstoken.empty() )
                return false;
            //
            p_document[0] = "***";  // for security reason it is hidden and won't be log
        } else {
            if ( ! get_accesstoken( accesstoken ) )  // retrieve AT from Authorization header
                return false;
        }
        //
        // Calls Urania, fills security context (m_security)
        if ( ! check_accesstoken( accesstoken ) )
            return false;
    }
    //
    // Convert the security data as JSON, used later by execute_script
    m_security_json =
      "{ \"user\"       : " + masnae::json_quote_string( m_security.user        ) +
      ", \"profile\"    : " + masnae::json_quote_string( m_security.profile     ) +
      ", \"role\"       : " + masnae::json_quote_string( m_security.role        ) +
      ", \"scopes\"     : " + masnae::json_quote_string( m_security.scopes      ) +
      ", \"permissions\": " + masnae::json_convert     ( m_security.permissions ) +
      "}";
    //
    return true;
}

//-------------------------------------------------------------------------
// Returns true if the module/document are found (p_result must be filled then)
bool JsRequestHandler::process_request ( const std::string &           p_module,
                                         masnae::Fastcpipp_Path &      p_document,
                                         masnae::ProcessingResponse &  p_result )
{
    if ( p_module == "ws" ) {
        // Check that the caller is allowed here
        if ( ! has_permission( "thoe.ws", p_result ) )   // set p_result if false
            return true;
        //
        process_script_request( p_document, p_result );  // always return a valid p_result
        return true;
    }
    //
    if ( p_module == "cb" ) {
        // The format for callback is: cb/<at>/<script>/abc/xyz
        // cb was remove above; skip <at> since process_script_request
        // expects <script> 1st
        auto document = masnae::Fastcpipp_Path( p_document.begin() + 1,
                                                p_document.end() );
        //
        process_script_request( document, p_result );  // always return a valid p_result
        return true;
    }
    //
    return false;
}

//-------------------------------------------------------------------------
// Returns information
// Must return a valid JSON string representing a non-empty object
std::string JsRequestHandler::specific_info ( void )
{
    return "{\"executor\":"     + g_ScriptExecutor.specific_info()                   +
           ",\"loader\":"       + g_ScriptLoader.specific_info()                     +
           ",\"libxml2\":"      + masnae::json_quote_string( LIBXML_DOTTED_VERSION ) +
           "}";
}

//-------------------------------------------------------------------------
// Executed the requested JavaScript code
void JsRequestHandler::process_script_request ( const masnae::Fastcpipp_Path &  p_document,
                                                masnae::ProcessingResponse &    p_result )
{
    // The script to execute is the first element of the document:
    //    bankTransfer/abc/xyz    gives    script:   bankTransfer
    //                                     document: /abc/xyz
    std::string  script, document;
    //
    for ( unsigned u = 0; u < p_document.size(); u++ )
        if ( u == 0 )
            script    =       p_document.at( u );
        else
            document += "/" + p_document.at( u );
    //
    // Check that the script name is valid
    auto nb_valid_char = std::count_if( script.begin(), script.end(), []( auto e ) {
        auto c = static_cast< unsigned char >( e );
        return std::isalnum( c ) || c == '_';
    });
    //
    if ( script.empty() || script.length() != static_cast< size_t >( nb_valid_char ) ) {
        p_result.set_jresp_error( "Invalid script name" );
        return;
    }
    //
    script += ".js";  // script files on disk are suffixed
    //
    // Check script existence (and eventually load it)
    if ( ! g_ScriptLoader.script_exists( script ) ) {
        p_result.set_jresp_error( "Script not found" );
        return;
    }
    //
    // Check that the environment data has only UTF-8 strings
    // (on failure, p_result is set)
    if ( ! is_utf8_safe( document, environment(), p_result ) )
        return;
    //
    // Execute the script (on success, p_result is set)
    if ( ! g_ScriptExecutor.execute_script( document,
                                            script,
                                            environment(),
                                            m_security_json,
                                            in_simulation(),
                                            p_result ) )
    {
        p_result.set_jresp_failed();
    }
}

//-------------------------------------------------------------------------

    // Add the given environment member to the list of string to check
    #define ADD_ENV_MEMBER(member)                      \
        strings_to_check.push_back(                     \
            std::pair( p_environment.member.c_str(),    \
            p_environment.member.length() ) );

// Check that the environment data has only UTF-8 strings
bool JsRequestHandler::is_utf8_safe ( const std::string &                    p_document,
                                      const masnae::Fastcpipp_Environment &  p_environment,
                                      masnae::ProcessingResponse &           p_result )
{
    std::vector< std::pair< const void *, size_t > >
        strings_to_check;
    //
    // Add all strings to check to the vector...
    //
    strings_to_check.push_back( std::pair( p_document.c_str(), p_document.length() ) );
    //
    // ... properties
    ADD_ENV_MEMBER( scriptName );
    //
    // ... parameters
    for ( const auto & x: p_environment.posts ) {
        strings_to_check.push_back( std::pair( x.first.c_str(),  x.first.length() ) );
        strings_to_check.push_back( std::pair( x.second.c_str(), x.second.length() ) );
    }
    //
    for ( const auto & x: p_environment.gets ) {
        strings_to_check.push_back( std::pair( x.first.c_str(),  x.first.length() ) );
        strings_to_check.push_back( std::pair( x.second.c_str(), x.second.length() ) );
    }
    //
    // ... headers
    ADD_ENV_MEMBER( authorization );
    ADD_ENV_MEMBER( acceptContentTypes );
    ADD_ENV_MEMBER( acceptCharsets );
    ADD_ENV_MEMBER( referer );
    ADD_ENV_MEMBER( contentType );
    //
    for ( const auto & x: p_environment.acceptLanguages )
        strings_to_check.push_back( std::pair( x.c_str(),  x.length() ) );
    //
    for ( const auto & x: p_environment.others ) {
        strings_to_check.push_back( std::pair( x.first.c_str(),  x.first.length()  ) );
        strings_to_check.push_back( std::pair( x.second.c_str(), x.second.length() ) );
    }
    //
    // ... attachments
    for ( const auto & x: p_environment.files ) {
        strings_to_check.push_back( std::pair( x.first.c_str(),              x.first.length() ) );
        strings_to_check.push_back( std::pair( x.second.filename.c_str(),    x.second.filename.length() ) );
        strings_to_check.push_back( std::pair( x.second.contentType.c_str(), x.second.contentType.length() ) );
    }
    //
    // ... and check the attachments
    for ( auto p: strings_to_check )
        if ( ! masnae::is_utf8( p.first, p.second ) ) {
            // Report bad item in JResp's message
            //
            size_t to_display = std::min( p.second, static_cast< size_t >( 16 ));  // first 16 chars only
            //
            std::string extract ( reinterpret_cast< const char* >( p.first ),
                                  to_display );
            //
            for ( auto & c: extract)   // remove dangerous chars
                c = c < 32 ? '.' : c;  // c is signed: ignore range 31 to -128
            //
            p_result.set_jresp_error( "Non UTF-8 charset detected: " + extract );
            //
            return false;
    }  
    //
    return true;
}

} // namespace thoe
