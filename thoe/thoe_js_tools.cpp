// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

// Copyright 2018 Pierre del Perugia
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <codecvt>
#include <locale>
#include <string>
#include <vector>

#include "masnae_common.h"
#include "thoe_mozjs.h"
#include "thoe_js_tools.h"

namespace thoe {

//-------------------------------------------------------------------------
// Validate received parameters, set an error and return false if parameters
// are missing or use wrong type.
// p_format uses letters BIDNSOo to specify types, / as separator between
// mandatory and optional arguments, and * to indicate variable arguments.
// e.g.:  "IS/S" defines a signature with mandatory Integer, String and optional String
//        "S*"   mandatory string eventually followed by other arguments
// Types are: Boolean, Integer (int32), Double, Number (double or int32),
//            String, Object, object_or_null
bool js_tools_check_argument ( JSContext *     p_context,
                               const char *    p_function,
                               JS::CallArgs &  p_args,
                               const char *    p_format )
{
    bool      optional = false, type_match;
    unsigned  arg      = 0;
    //
    while ( * p_format ) {  // check the format char by char
        // 
        if ( arg >= p_args.length() ) { // all arguments scanned
            if ( optional ||            // remaining format was optional or
                 *p_format == '/' )     // about to become optional
            {
                return true;
            } else {
                JS_ReportErrorUTF8( p_context, "function %s argument %u, type %c, missing",
                                               p_function,
                                               arg+1,
                                               * p_format );
                return false;
            }
        }
        //
        // Check argument type
        switch ( * p_format ) {
            case '*': return true;
            case 'B': type_match = p_args[ arg++ ].isBoolean(); break;  // boolean
            case 'I': type_match = p_args[ arg++ ].isInt32();   break;  // int32
            case 'D': type_match = p_args[ arg++ ].isDouble();  break;  // double
            case 'N': type_match = p_args[ arg++ ].isNumber();  break;  // double or int32
            case 'S': type_match = p_args[ arg++ ].isString();  break;  // string
            case 'O': type_match = p_args[ arg++ ].isObject();  break;  // object
            case 'o': type_match = p_args[ arg ].isObject() ||          // object or null
                                   p_args[ arg ].isNull();
                      arg++;
                      break;
            case '/': type_match = true; optional = true;       break; // switch to optional
            default:  type_match = true; arg++;                 break; // ignore unknown type
        }
        //
        if ( ! type_match ) {
            JS_ReportErrorUTF8( p_context, "function %s argument %u, type '%c' expected",
                                           p_function,
                                           arg,
                                           * p_format );
            return false;
        }
        //
        p_format++;
    }
    //
    // Too many argument passed to the function?
    if ( arg < p_args.length() ){
        JS_ReportErrorUTF8( p_context, "function %s: too many arguments",
                                       p_function );
        return false;
    }
    //
    return true;
}

//--------------------------------------------------------------------
// Convert a JSString to a std::string
std::string js_tools_convert ( JSContext *  p_context,
                               JSString *   p_string )
{
    /* old version, new one is twice faster
    JS::RootedString string ( p_context,
                              p_string );
    //
    char *       s = JS_EncodeStringToUTF8( p_context, string );
    std::string  t = s;
    //
    JS_free( p_context, s );
    //
    return t;
    */
    //
    std::string      utf8;
    JS::RootedString string( p_context, p_string );
    JSFlatString *   flatStr = JS_FlattenString( p_context, string );
    //
    if ( flatStr != nullptr )
    {
        size_t length = JS::GetDeflatedUTF8StringLength( flatStr );
        //
        utf8.resize( length );
        //
        JS::DeflateStringToUTF8Buffer( flatStr,
                                       mozilla::RangedPtr< char >( &utf8[0], length ) );  // ok with C++11
    }
    //
    return utf8;
}

//-------------------------------------------------------------------------
// Convert a Value (simple type only) to a std::string
bool js_tools_convert ( JSContext *      p_context,
                        JS::HandleValue  p_value,
                        std::string &    p_string )
{
         if ( p_value.isString() ) p_string = js_tools_convert( p_context, p_value.toString() );
    else if ( p_value.isTrue()   ) p_string = "true";
    else if ( p_value.isFalse()  ) p_string = "false";
    else if ( p_value.isInt32()  ) p_string = std::to_string( p_value.toInt32() );
    else if ( p_value.isDouble() ) p_string = std::to_string( p_value.toDouble() );
    else return false;  // others are ignored
    //
    return true;
}

//-------------------------------------------------------------------------
// Stringify an object, same as JSON.stringify

    // Function called when conversion is finished
    bool js_tools_stringify_cb ( const char16_t*  buf,
                                 uint32_t         len,
                                 void *           data )
    {
        std::u16string  json_wide( buf, len );
        //
        // Build a char16_t to utf8converter
        std::wstring_convert< std::codecvt_utf8_utf16< char16_t >, char16_t >
            converter;
        //
        // Our destination variable
        std::string * json = static_cast< std::string * >( data );
        //
        // Add in case the callback is called several times
        *json += converter.to_bytes( json_wide );
        //
        return true;
    }

bool js_tools_stringify ( JSContext *             p_context,
                          JS::MutableHandleValue  p_object,
                          std::string &           p_json )
{
    JS::RootedValue space( p_context,
                           JS::Int32Value( 2 ) );  // set padding to 2
    //
    p_json.erase();  // callback function will concat result to this variable
    //
    // The JS_Stringify function is not in the JSAPI reference but half
    // explained in jsapi.h and in the Mozilla MDN JSON.stringify() page        
    if ( ! JS_Stringify( p_context,
                         p_object,  // no idea why a Mutable is required by SpiderMonkey
                         nullptr,   // replacer function or filtering array of string
                         space,
                         & js_tools_stringify_cb,
                         & p_json ) )
	{
        JS_ReportErrorUTF8( p_context, "failed to stringify object" );
        //
		return false;
	}
    //
    return true;
}

//------------------------
// Add a string property to an existing object
bool js_tools_add_property ( JSContext *          p_context,
                             JS::RootedObject &   p_object,
                             const std::string &  p_name,
                             const std::string &  p_value )
{
    JS::ConstUTF8CharsZ  utf8_value ( p_value.c_str(), p_value.length() );  // no string copy here
    JS::RootedString     string( p_context,
                                 JS_NewStringCopyUTF8Z( p_context,
                                                        utf8_value ));
    //
    return JS_DefineProperty( p_context,
                              p_object,
                              p_name.c_str(),
                              string,
                              JSPROP_ENUMERATE|JSPROP_READONLY|JSPROP_PERMANENT );
}

//------------------------
// Add an array of string properties to an existing object
// Stops on the first add failing
bool js_tools_add_properties ( JSContext *                      p_context,
                               JS::RootedObject &               p_object,
                               const masnae::SingleKey_Value &  p_properties )
{
    for ( const auto & property : p_properties )
        if ( ! js_tools_add_property( p_context, p_object, property.first, property.second ) )
            return false;
    //
    return true;
}

//-------------------------------------------------------------------------
// Add an array-of-string property to an existing object
bool js_tools_add_property ( JSContext *                       p_context,
                             JS::RootedObject &                p_object,
                             const char *                      p_name,
                             const std::vector<std::string> &  p_values )
{
    JS::AutoValueVector elements( p_context );
    //
    for ( const auto & value: p_values ) {
        JS::ConstUTF8CharsZ  utf8_value( value.c_str(), value.length() );  // no string copy here
        JSString *           js_value = JS_NewStringCopyUTF8Z( p_context, utf8_value );
        //
        if ( js_value == nullptr )
            return false;
        if ( ! elements.append( JS::StringValue( js_value ) ) )
            return false;
    }
    //
    JS::RootedObject object( p_context,
                             JS_NewArrayObject( p_context, elements ) );
    //
    return JS_DefineProperty( p_context,
                              p_object,
                              p_name,
                              object,
                              JSPROP_ENUMERATE|JSPROP_READONLY|JSPROP_PERMANENT );
}

//-------------------------------------------------------------------------
// Generic function converting then inserting a value with a known type in a container
template< typename Container >
void insert_value ( JSContext *          p_context,
                    JS::HandleValue      p_value,
                    const std::string &  p_key,
                    Container &          p_container )
{
    std::string value;
    //
    if ( js_tools_convert( p_context, p_value, value ) )
        p_container.insert( std::pair( p_key, value ) );
}

//-------------------------------------------------------------------------
// Generic function converting then inserting values (with a known type) of
// an array in a container
template <typename cont>
void insert_array ( JSContext *          p_context,
                    JS::HandleObject     p_array,
                    const std::string &  p_key,
                    cont &               p_container )
{
    uint32_t length = 0;
    //
    // Get the number of elements in the array
    if ( JS_GetArrayLength( p_context, p_array, & length ) &&
         length > 0 )
    {
        JS::RootedValue element( p_context );
        //
        // Elements of the array are object properties named '0', '1', '2'...
        for ( uint32_t u = 0; u< length; u++ )
            if ( JS_GetProperty( p_context, p_array, std::to_string(u).c_str(), & element ) )
                insert_value( p_context, element, p_key, p_container );
    }
}

//-------------------------------------------------------------------------
// Copy all properties/values of the object in the KeyValue
// object, null and undefined values are not copied
bool js_tools_get_properties ( JSContext *               p_context,
                               JS::RootedObject &        p_object,
                               masnae::MultiKey_Value &  p_properties )
{
    // Retrieve the ids of all visible, owned, properties
    JS::AutoIdVector ids( p_context );
    //
    if ( ! js::GetPropertyKeys( p_context, p_object, JSITER_OWNONLY , & ids ) ) {
        JS_ReportErrorUTF8( p_context, "failed to get object properties" );
        return false;
    }
    //
    for ( size_t i = 0; i < ids.length(); i++ ) {
        JS::RootedValue k_value( p_context ), v_value( p_context );
        std::string     key;
        JS::RootedId    id( p_context, ids[i] );
        //
        // Get the key
        if ( ! JS_IdToValue( p_context, ids[i], & k_value) ) {
            JS_ReportErrorUTF8( p_context, "failed to get object property name" );
            return false;
        }
        if ( ! js_tools_convert( p_context, k_value, key ) ) // only simple type key are supported
            continue;
        //
        // Get the value
        if ( ! JS_GetPropertyById( p_context, p_object, id, & v_value ) ) {
            JS_ReportErrorUTF8( p_context, "failed to get object property value" );
            return false;
        }
        //
        // Arrays have a special handling (adding all values with simple type)
        if ( v_value.isObject() ) {
            bool              is_array = false;
            JS::RootedObject  array( p_context, & v_value.toObject() );
            //
            if ( JS_IsArrayObject ( p_context, array, & is_array ) &&
                 is_array )
            {
                insert_array( p_context, array, key, p_properties );
            }
        } else {
            insert_value( p_context, v_value, key, p_properties );
        }
    }
    //
    return true;
}

} // namespace thoe
