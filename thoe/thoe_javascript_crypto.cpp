// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

// Copyright 2018 Pierre del Perugia
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <openssl/rand.h>
#include <adawat/adawat_crypto.h>

#include "thoe_javascript.h"
#include "thoe_script_executor.h"
#include "thoe_js_tools.h"

namespace thoe {

//-------------------------------------------------------------------------
bool get_blob_parameter( JSContext *                     p_context,
                         const JS::MutableHandleValue &  p_argument,
                         masnae::Data &                  p_data,
                         const char *                    p_csLabel )
{
    JS::RootedValue  blob( p_context,
                           p_argument );
    //
    if ( ! js_is_blob( blob ) ) {
        JS_ReportErrorUTF8( p_context, "%s parameter is not a Blob object", p_csLabel );
        return false;
    }
    //
    js_get_blob_data( blob, p_data, false );
    //
    return true;
}

//-------------------------------------------------------------------------
// Returns a digest of the given data
//  Syntax:  digest( algorithm, data_in )
//  Example: let digest = crypto.digest( 'md5', data );
static
bool js_crypto_digest( JSContext * p_context, unsigned p_argc, JS::Value * p_argv )
{
    JS::CallArgs  args  = JS::CallArgsFromVp( p_argc, p_argv );
    //
    if ( ! js_tools_check_argument( p_context, "digest", args, "SO" ) )
        return false;
    //
    // Retrieve parameters...
    const EVP_MD *  algorithm;
    masnae::Data    data, digest;
    //
    // ... algorithm
    algorithm = adawat::crypto_md_from_name( js_tools_convert( p_context,
                                                               args[0].toString() ) );
    //
    if ( algorithm == nullptr ) {
        JS_ReportErrorUTF8( p_context, "crypto.digest: algorithm not found" );
        return false;
    }
    //
    // ... data
    if ( ! get_blob_parameter( p_context, args[1], data, "crypto.digest: data" ) )
        return false;
    //
    //------------------------
    // Executes the function
    if ( ! adawat::crypto_digest( data, digest, algorithm ) ) {
        JS_ReportErrorUTF8( p_context, "crypto.digest: function failed" );
        return false;
    }
    //
    //------------------------
    // Create an object of the result
    JS::RootedObject global( p_context,
                             JS_GetGlobalForObject( p_context,
                                                    & args.callee() ) );
    JS::RootedObject c_object( p_context,
                               js_new_blob( p_context,
                                            global,
                                            reinterpret_cast<uint8_t *>( digest.data() ),
                                            digest.size() ) );
    //
    if ( c_object == nullptr )
        return false;
    //
    //------------------------
    // Returns a new object to allow method chaining
    args.rval().setObject( * c_object );
    return true;
}

//-------------------------------------------------------------------------
// Returns the HMAC message authentication
//  Syntax:  hmac( algorithm, key, data_in )
//  Example: let digest = crypto.hmac( 'md5', key, data );
static
bool js_crypto_hmac( JSContext * p_context, unsigned p_argc, JS::Value * p_argv )
{
    JS::CallArgs  args  = JS::CallArgsFromVp( p_argc, p_argv );
    //
    if ( ! js_tools_check_argument( p_context, "hmac", args, "SOO" ) )
        return false;
    //
    // Retrieve parameters...
    const EVP_MD *  algorithm;
    masnae::Data    key, data, hmac;
    //
    // ... algorithm
    algorithm = adawat::crypto_md_from_name( js_tools_convert( p_context,
                                                               args[0].toString() ) );
    //
    if ( algorithm == nullptr ) {
        JS_ReportErrorUTF8( p_context, "crypto.hmac: algorithm not found" );
        return false;
    }
    //
    // ... key
    if ( ! get_blob_parameter( p_context, args[1], key, "crypto.hmac: key" ) )
        return false;
    //
    // ... data
    if ( ! get_blob_parameter( p_context, args[2], data, "crypto.hmac: data" ) )
        return false;
    //
    //------------------------
    // Executes the function
    if ( ! adawat::crypto_hmac( key, data, hmac, algorithm ) ) {
        JS_ReportErrorUTF8( p_context, "crypto.hmac: function failed" );
        return false;
    }
    //
    //------------------------
    // Create an object of the result
    JS::RootedObject global( p_context,
                             JS_GetGlobalForObject( p_context,
                                                    & args.callee() ) );
    JS::RootedObject c_object( p_context,
                               js_new_blob( p_context,
                                            global,
                                            reinterpret_cast<uint8_t *>( hmac.data() ),
                                            hmac.size() ) );
    //
    if ( c_object == nullptr )
        return false;
    //
    //------------------------
    // Returns a new object to allow method chaining
    args.rval().setObject( * c_object );
    return true;
}

//-------------------------------------------------------------------------
// Returns the derived password using pbkdf2
//  Syntax:  pbkdf2( algorithm, password, salt, iterations=100000, requested_size=16 )
//  Example: let derived_key = crypto.pbkdf2( 'md5', password, salt );
static
bool js_crypto_pbkdf2( JSContext * p_context, unsigned p_argc, JS::Value * p_argv )
{
    JS::CallArgs  args  = JS::CallArgsFromVp( p_argc, p_argv );
    //
    if ( ! js_tools_check_argument( p_context, "pbkdf2", args, "SOO/II" ) )
        return false;
    //
    // Retrieve parameters...
    const EVP_MD *  algorithm;
    masnae::Data    password, salt, pbkdf2;
    size_t          iterations     = 100000;
    size_t          requested_size = 16;
    //
    // ... algorithm
    algorithm = adawat::crypto_md_from_name( js_tools_convert( p_context,
                                                               args[0].toString() ) );
    //
    if ( algorithm == nullptr ) {
        JS_ReportErrorUTF8( p_context, "crypto.pbkdf2: algorithm not found" );
        return false;
    }
    //
    // ... password
    if ( ! get_blob_parameter( p_context, args[1], password, "crypto.pbkdf2: password" ) )
        return false;
    //
    // ... salt
    if ( ! get_blob_parameter( p_context, args[2], salt, "crypto.pbkdf2: salt" ) )
        return false;
    //
    // ... iterations
    if ( p_argc >= 4 ) {
        int32_t param = args[3].toInt32();
        //
        if ( param < 1 || param > 1000000000 ) {
            JS_ReportErrorUTF8( p_context, "crypto.pbkdf2: iterations out of range" );
            return false;
        }
        //
        iterations = static_cast< size_t >( param );  // param >=1
    }
    //
    // ... requested_size
    if ( p_argc >= 5 ) {
        int32_t param = args[4].toInt32();
        //
        if ( param < 1 || param > 1000 ) {
            JS_ReportErrorUTF8( p_context, "crypto.pbkdf2: size out of range" );
            return false;
        }
        //
        requested_size = static_cast< size_t >( param );  // param >=1
    }
    //
    //------------------------
    // Executes the function
    if ( ! adawat::crypto_pbkdf2( password, salt, pbkdf2, iterations, requested_size, algorithm ) ) {
        JS_ReportErrorUTF8( p_context, "crypto.pbkdf2: function failed" );
        return false;
    }
    //
    //------------------------
    // Create an object of the result
    JS::RootedObject global( p_context,
                             JS_GetGlobalForObject( p_context,
                                                    & args.callee() ) );
    JS::RootedObject c_object( p_context,
                               js_new_blob( p_context,
                                            global,
                                            reinterpret_cast<uint8_t *>( pbkdf2.data() ),
                                            pbkdf2.size() ) );
    //
    if ( c_object == nullptr )
        return false;
    //
    //------------------------
    // Returns a new object to allow method chaining
    args.rval().setObject( * c_object );
    return true;
}

//-------------------------------------------------------------------------
// Encrypts or decrypts the given data
//  Syntax:  cipher( algorithm, key, iv, data_in, encrypt, padding=true )
//  Example: let encrypted_data = crypto.cipher( 'aes-128-cbc', key, iv, data, true );
static
bool js_crypto_cipher( JSContext * p_context, unsigned p_argc, JS::Value * p_argv )
{
    JS::CallArgs  args  = JS::CallArgsFromVp( p_argc, p_argv );
    //
    if ( ! js_tools_check_argument( p_context, "cipher", args, "SOOOB/B" ) )
        return false;
    //
    // Retrieve parameters...
    const EVP_CIPHER *  algorithm;
    masnae::Data        key, iv, data, code;
    bool                encrypt, padding = true;
    //
    // ... algorithm
    algorithm = adawat::crypto_cipher_from_name( js_tools_convert( p_context,
                                                                   args[0].toString() ) );
    //
    if ( algorithm == nullptr ) {
        JS_ReportErrorUTF8( p_context, "crypto.cipher: algorithm not found" );
        return false;
    }
    //
    // ... key
    if ( ! get_blob_parameter( p_context, args[1], key,  "crypto.cipher: key" ) )
        return false;
    //
    // ... iv
    if ( ! get_blob_parameter( p_context, args[2], iv,   "crypto.cipher: iv" ) )
        return false;
    //
    // ... data
    if ( ! get_blob_parameter( p_context, args[3], data, "crypto.cipher: data" ) )
        return false;
    //
    // ... encrypt or decrypt
    encrypt =  args[4].toBoolean();
    //
    // ... padding
    if ( p_argc >= 6 )
        padding  = args[5].toBoolean();
    //
    //------------------------
    // Executes the function
    if ( ! adawat::crypto_cipher( key, iv, data, code, encrypt, algorithm, padding ) ) {
        JS_ReportErrorUTF8( p_context, "crypto.cipher: function failed" );
        return false;
    }
    //
    //------------------------
    // Create an object of the result
    JS::RootedObject global( p_context,
                             JS_GetGlobalForObject( p_context,
                                                    & args.callee() ) );
    JS::RootedObject c_object( p_context,
                               js_new_blob( p_context,
                                            global,
                                            reinterpret_cast< uint8_t* >( code.data() ),
                                            code.size() ) );
    //
    if ( c_object == nullptr )
        return false;
    //
    //------------------------
    // Returns a new object to allow method chaining
    args.rval().setObject( * c_object );
    return true;
}

//-------------------------------------------------------------------------
// Generates random data
//  Syntax:  random( size )
//  Example: let random = crypto.random( 16 );
static
bool js_crypto_random( JSContext * p_context, unsigned p_argc, JS::Value * p_argv )
{
    JS::CallArgs  args  = JS::CallArgsFromVp( p_argc, p_argv );
    //
    if ( ! js_tools_check_argument( p_context, "random", args, "I" ) )
        return false;
    //
    // Retrieve parameter...
    int32_t param;
    size_t  requested_size;
    //
    // ... requested_size
    param = args[0].toInt32();
    if ( param < 1 || param > 10000000 ) {
        JS_ReportErrorUTF8( p_context, "crypto.random: size out of range" );
        return false;
    }
    //
    requested_size = static_cast< size_t >( param );  // param >=1
    //
    //------------------------
    // Executes the function
    masnae::Data    data;
    //
    data.resize( requested_size );
    //
    if ( RAND_bytes( data.data(), static_cast< int >( data.size() ) ) != 1 ) {
        JS_ReportErrorUTF8( p_context, "crypto.random: function failed" );
        return false;
    }
    //
    //------------------------
    // Create an object of the result
    JS::RootedObject global( p_context,
                             JS_GetGlobalForObject( p_context,
                                                    & args.callee() ) );
    JS::RootedObject c_object( p_context,
                               js_new_blob( p_context,
                                            global,
                                            reinterpret_cast< uint8_t* >( data.data() ),
                                            data.size() ) );
    //
    if ( c_object == nullptr )
        return false;
    //
    //------------------------
    // Returns a new object to allow method chaining
    args.rval().setObject( * c_object );
    return true;
}

//-------------------------------------------------------------------------
// Compare two strings in a constant time maner
//  Syntax:  equal( string1, string2 )
//  Example: if ( crypto.equal( user_hash, calculated_hash ) ) ...
static
bool js_crypto_equal( JSContext * p_context, unsigned p_argc, JS::Value * p_argv )
{
    JS::CallArgs  args  = JS::CallArgsFromVp( p_argc, p_argv );
    //
    if ( ! js_tools_check_argument( p_context, "equal", args, "SS" ) )
        return false;
    //
    // Retrieve parameter...
    std::string s1 = js_tools_convert( p_context, args[0].toString() );
    std::string s2 = js_tools_convert( p_context, args[1].toString() );
    //
    // Return result
    args.rval().setBoolean( 
        adawat::crypto_equal( s1, s2 )
    );
    //
    return true;
}

//-------------------------------------------------------------------------
// Define the crypto object in the global object
// Called only once by ScriptExecutor::execute_script if global
// is reused (m_optz_clear_global = false).
bool js_add_crypto_object( JSContext *                   p_context,
                           JS::HandleObject              p_global )
{
    // Create an new object 'crypto', as a property of the Global object
    JS::RootedObject crypto_object( p_context,
                                    JS_DefineObject( p_context,
                                                     p_global,
                                                     "crypto",
                                                     nullptr,
                                                     JSPROP_ENUMERATE|JSPROP_READONLY|JSPROP_PERMANENT ) );
    //
    if ( crypto_object == nullptr ) {
        js_service_error_log( "JavaScript: Failed to allocated crypto object", nullptr );
        return false;
    }
    //
    static const JSFunctionSpec functions[] = {
       //     name               function                      nargs flags
       JS_FN("digest",           js_crypto_digest,             2,    0),
       JS_FN("hmac",             js_crypto_hmac,               3,    0),
       JS_FN("pbkdf2",           js_crypto_pbkdf2,             5,    0),
       JS_FN("cipher",           js_crypto_cipher,             6,    0),
       JS_FN("random",           js_crypto_random,             1,    0),
       JS_FN("equal",            js_crypto_equal,              1,    0),
       //
       JS_FS_END
    };
    //
    if ( ! JS_DefineFunctions( p_context,
                               crypto_object,
                               functions ) )
    {
        js_service_error_log( "JavaScript: Failed to define crypto's methods", nullptr );
        return false;
    }
    //
    return true;
}

} // namespace thoe
