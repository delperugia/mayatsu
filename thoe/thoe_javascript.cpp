// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

// Copyright 2018 Pierre del Perugia
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "masnae_misc.h"
#include "thoe_script_executor.h"

namespace thoe {

// Log an error at the service level, eventually adding last error
void js_service_error_log( const std::string & p_message, ThreadFrame * p_frame )
{
    if ( p_frame == nullptr || p_frame->m_last_error.empty() )
        masnae::service_error_log( p_message );
    else
        masnae::service_error_log( p_message + 
                                   std::string( " (" ) + p_frame->m_last_error + ")" );
}

} // namespace thoe
