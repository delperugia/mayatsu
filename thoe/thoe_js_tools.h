// Copyright 2018 Pierre del Perugia
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

//
// Convenient functions manipulating SpiderMonkey types
//

#ifndef thoe_js_tools_h
#define thoe_js_tools_h

#include <string>
#include <vector>

#include "thoe_mozjs.h"

namespace thoe {

//-------------------------------------------------------------------------
// Validate received parameters, set an error and return false if parameters
// are missing or use wrong type.
// p_format uses letters BIDNSO to specify types, / as separator between
// mandatory and optional arguments, and * to indicate variable arguments.
// e.g.:  "IS/S" defines a signature with mandatory Integer, String and optional String
//        "S*"   mandatory string eventually followed by other arguments (at least one)
// Types are: Boolean, Integer (int32), Double, Number (double or int32),
//            String, Object
bool js_tools_check_argument ( JSContext *     p_context,
                               const char *    p_function,
                               JS::CallArgs &  p_args,
                               const char *    p_format );

//-------------------------------------------------------------------------
// Convert a JSString to a std::string
std::string js_tools_convert ( JSContext *  p_context,
                               JSString *   p_string );

//-------------------------------------------------------------------------
// Convert a Value (simple type only) to a std::string
bool js_tools_convert ( JSContext *      p_context,
                        JS::HandleValue  p_value,
                        std::string &    p_string );

//-------------------------------------------------------------------------
// Converts an object as a JSON string
bool js_tools_stringify ( JSContext *             p_context,
                          JS::MutableHandleValue  p_object,
                          std::string &           p_json );


//-------------------------------------------------------------------------
// Add a string property to an existing object
bool js_tools_add_property ( JSContext *          p_context,
                             JS::RootedObject &   p_object,
                             const std::string &  p_name,
                             const std::string &  p_value );

// Add an array of string properties to an existing object
bool js_tools_add_properties ( JSContext *                      p_context,
                               JS::RootedObject &               p_object,
                               const masnae::SingleKey_Value &  p_properties );

//-------------------------------------------------------------------------
// Add an array-of-string property to an existing object
bool js_tools_add_property ( JSContext *                       p_context,
                             JS::RootedObject &                p_object,
                             const char *                      p_name,
                             const std::vector<std::string> &  p_values );

//-------------------------------------------------------------------------
// Copy all simple type and arrays properties (bool, string, number) of the
// object in the MultiKey_Value. Only simple types of the arrays are copied
// (nested arrays are not copied)
bool js_tools_get_properties ( JSContext *               p_context,
                               JS::RootedObject &        p_object,
                               masnae::MultiKey_Value &  p_properties );

} // namespace thoe

#endif
