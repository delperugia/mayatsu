// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

// Copyright 2018 Pierre del Perugia
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma GCC diagnostic ignored "-Wold-style-cast"

#include <string>
#include <map>
#include <libxml/parser.h>
#include <libxml/tree.h>

#include "masnae_misc.h"

namespace thoe {

namespace xml2json {

//-------------------------------------------------------------------------
// To be called once when program starts
void init ( void )
{
    LIBXML_TEST_VERSION
    //
    xmlInitParser();
}

//-------------------------------------------------------------------------
// Convert collected items into a JSON object. Items with the same
// name are grouped into an array.
//   e.g.:  a:1, a:2, b:3  ==>  a:[1,2], b:3
// Collected keys and values are already quoted, however there may be
// empty items (corresponding to text between nodes in XML
static
std::string build_node ( const masnae::MultiKey_Value &  p_map )
{
    std::string last_key, last_value, result;
    int         last_count = 0;
    //
    result.reserve( p_map.size() * 32 );  // arbitrary average key+value size
    //
    for ( const auto & x : p_map ) {
        if ( x.second.empty() )     // ignore TEXT node not in leaf
            continue;
        //
        if ( last_key != x.first )  // moving to a new key name
        {
            if ( last_count == 1 )
                result += ',' + last_key + ":"  + last_value;
            else if ( last_count > 1 )
                result += ',' + last_key + ":[" + last_value + "]";
            //
            // Restart with new key
            last_key   = x.first;
            last_value = x.second;
            last_count = 1;
        }
        else  // add to array
        {
            last_value += "," + x.second;
            last_count++;
        }
    }
    //
    // Add current key
    if ( last_count == 1 )
        result += ',' + last_key + ":"  + last_value;
    else if ( last_count > 1 )
        result += ',' + last_key + ":[" + last_value + "]";
    //
    // Encapsulated in an object (if there is something to encapsulate)
    if ( ! result.empty() ) {
        result[0]  = '{';  // replace ',' by '{'
        result    += '}';
    }
    //
    return result;
}

//-------------------------------------------------------------------------
// Convert, prefix and quote an libxml2 string into a JSON string
static
std::string __ ( std::string p_prefix, const xmlChar *  p_text )
{
    return masnae::json_quote_string( p_prefix +
                                      reinterpret_cast< const char* >( p_text ) );
}

//-------------------------------------------------------------------------
// Returns a node either as a JSON object or eventually (if possible)
// as a single string
static
std::string walk_node( xmlNode * p_node )
{
    masnae::MultiKey_Value items;
    //
    // Start by collecting attributes. They will be prefixed by '@'
    for ( xmlAttr * a = p_node->properties; a != nullptr; a = a->next )
        items.insert( std::pair( __( "@", a->name ),
                                 __( "",  a->children->content ) ) );
    //
    // If the node contains only a text, puts its content directly
    // (it must not have attributes neither)
    if ( p_node->children != nullptr             &&
         p_node->children->type == XML_TEXT_NODE &&
         p_node->children->next == nullptr )
    {
        if ( items.empty() )
            return __( "", p_node->children->content );  // returns only the node text content
    }
    //
    // Then browse and add all children nodes...
    for ( xmlNode * n = p_node->children; n != nullptr; n = n->next )
        // ... walk into nodes
        if ( n->type == XML_ELEMENT_NODE )
            items.insert( std::pair( __( "", n->name ),
                                     walk_node( n ) ) );
        // ... add texts
        else if ( n->type == XML_TEXT_NODE )
                if ( ! xmlIsBlankNode( n ) )
                    items.insert( std::pair(__( "#", n->name ),
                                            __( "",  n->content ) ) );
    //
    return build_node( items );  // finally build an object with collected items
}

//-------------------------------------------------------------------------
// Converts an XML document into a JSON object
// If a node has no attribute and contains only a single text,
// the corresponding JSON object property will be a string.
// If not, each node is converted to a JSON objects where:
//  - each attribute is added as a property (prefixed with '@')
//  - each sub-node is added as a property
//  - the child texts are name #text
//  - nodes with same name are grouped into array
// E.g.:
//  <alpha id="9">                  {
//      lorem                         "alpha": {
//      <beta/>                         "#text": [
//      <gamma></gamma>                   "\n    lorem\n    ",
//      <delta>d</delta>                  "\n    ipsum\n    ",
//      <epsilon a="2" b="3" />           "\n    dolor\n"
//      <zeta c="4">z</zeta>            ],
//      ipsum                           "@id": "9",
//      <eta>one</eta>                  "delta": "d",
//      <eta>two</eta>                  "epsilon": { "@a": "2", "@b": "3" },
//      <eta d="1">three</eta>          "eta":     [ "one",
//                                                   "two"
//  </alpha>                                         { "#text": "three", "@d": "1" } ],
//                                      "zeta":    { "#text": "z", "@c": "4" }
//                                    }
//                                  }
//
// This must be very close to the description found at:
//   https://www.xml.com/pub/a/2006/05/31/converting-between-xml-and-json.html
//
// Note that there is a limitation with proposal at:
//   https://github.com/henrikingo/xml2json
// even if more convenient, attributes association can easily be lost
//  <a>                             a:{
//      <b a="1">one</b>              b:  [one,two,three],
//      <b      >two</b>              b@a:[1,3]
//      <b a="3">three</b>          }
//  </a>
//
bool convert ( const std::string &  p_xml,
               std::string &        p_json )
{
    // An extra safety because this operation could be costly
    if ( p_xml.length() > 1000000 )
        return false;
    //
    // Parse XML document
    xmlDoc *  doc = xmlReadMemory( p_xml.c_str(),
                                   static_cast< int >( p_xml.length() ),  // cast protected by check above
                                   "inline",
                                   nullptr,  // the document encoding
                                   XML_PARSE_NOERROR | XML_PARSE_NOWARNING |
                                   XML_PARSE_NONET   | XML_PARSE_COMPACT );
    //
    if ( doc == nullptr )
        return false;
    //
    // Convert it to JSON
    xmlNode * root   = xmlDocGetRootElement( doc );
    bool      result = false;
    //
    if ( root != nullptr && root->parent != nullptr ) {
        p_json = walk_node( root->parent );
        result = true;
    }
    //
    // Cleanup
    xmlFreeDoc( doc );
    //
    return result;
}

} // namespace xml2json

} // namespace thoe
