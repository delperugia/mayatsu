// Copyright 2018 Pierre del Perugia
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

//
// Manage scripts: load and optimize them
// There is one global instance of this class
//

#ifndef thoe_script_loader_h
#define thoe_script_loader_h

#include <fastcgi++/manager.hpp>
#include <map>
#include <shared_mutex>
#include <string>
#include <unordered_set>

#include "masnae_bus.h"
#include "masnae_monitor_files.h"

namespace thoe {

//-------------------------------------------------------------------------
class ScriptLoader : public  masnae::bus::Subscriber,
                     private masnae::MonitorFiles
{
    public:
        ScriptLoader  ();
        //
        // To be called once before any execution
        bool        start         ( void );
        //
        // To be called once before stopping the program
        void        stop          ( void );
        //
        // Return module information as a JSON string
        std::string specific_info ( void );
        //
        // Returns true if the script exists (and load it in cache if needed)
        bool        script_exists ( const std::string &  p_name );
        //
        // Returns the source of a script from cache
        bool        script_get    ( const std::string &  p_name,
                                    std::string &        p_script );
        //
        // Remove a script from the cache
        void        script_remove ( const std::string &   p_name );
        //
        // Insert a script in the cache
        void        script_add    ( const std::string &   p_name,
                                    std::string &         p_script );
        //
        // Remove all scripts from cache
        void        clear         ( void );
        //
        // Called by the bus with a new message
        virtual
        void        bus_event     ( const masnae::bus::Message &  p_message );
        //
    private:
        //
        // Configuration
        std::shared_mutex                      m_mutex_config;
        std::string                            m_scripts_folder;  // folder where scripts are located
        //
        // Script source cache
        std::shared_mutex                      m_mutex_scripts;
        std::map< std::string, std::string >   m_scripts;         // scripts sources, indexed by name
        //
        // Read config from config object
        void    update_config    ( void );
        //
        // Function called when a script file changes
        virtual
        void    monitoring_event ( const std::string &   p_file );
};

} // namespace thoe

#endif
