// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

// Copyright 2018 Pierre del Perugia
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <adawat/adawat_string.h>
#include <sstream>
#include <thread>

#include "masnae_misc.h"
#include "masnae_global.h"
#include "thoe_mozjs.h"
#include "thoe_common.h"
#include "thoe_javascript.h"
#include "thoe_script_executor.h"
#include "thoe_js_tools.h"

/*-------------------------------------------------------------------------

Note on code compilation

SpiderMonkey standard invocation is similar to:

    JSContext * cx = JS_NewContext(...
    JS::InitSelfHostedCode( cx )
    {
        JSAutoRequest     ar    ( cx );
        JS::RootedObject  global( cx, JS_NewGlobalObject(...
        //
        {
            JSAutoCompartment ac( cx, global )
            JS_InitStandardClasses( cx...
            JS_DefineFunction     ( cx...
            //
            JS::CompileOptions  opts( cx );
            JS::RootedValue     rval( cx );
            //
            opts.setFile(...
            //
            JS::Evaluate ( cx, opts...
        }
    }

To speed up execution, we following steps are used:
    1. create the Context only once
    2. compiled code is kept between executions. Evaluate is replaced by:
        JS::Compile         in PersistentRootedScript of the Frame
        JS_ExecuteScript

However second optimization make the following code to crash SpiderMonkey:
    var a = [ 1, 2 ];
    a.forEach( function(i) { } );

This is apparently due to using a different Global than the one used
during compilation. SpiderMonkey also crashes if the array is in a Global
object:
    var o = {}; o.a = [ 1, 2 ];
    o.a.forEach( function(i) {} );

Note: this crash happens with version 52 of SpiderMonkey but not with version 60.

But accessing Thoe global objects works:
    request.attachments.forEach( function( a ) {...

So if your code does not allocate global arrays, you can consider using
this second optimization. If it does, there is another optimization:
    3. create the Global object only once

Then the faulty code works but since it is the same Global object
between execution, global variables remains. Thus the following code
is valid:
    var first_time;
    if ( first_time == undefined ) {
        first_time = false;
        ...

Timing:

                Code cached                                     Code not cached
                                                        
with clear frame & global                               
    ctx 0.076739s init 0.012643s exec 0.006510s     ctx 0.072508s init 0.009498s exec 0.005860s           
    ctx 0.069414s init 0.011450s exec 0.006140s     ctx 0.071330s init 0.012027s exec 0.006310s           
    ctx 0.067124s init 0.012403s exec 0.006104s     ctx 0.063975s init 0.009155s exec 0.005593s           
                                                                                        
with clear global                                                                       
    ctx 0.071419s init 0.009615s exec 0.006171s     ctx 0.070480s init 0.009291s exec 0.005885s           
    ctx 0.026946s init 0.008171s exec 0.015600s     ctx 0.017575s init 0.008981s exec 0.005758s           
    ctx 0.015901s init 0.006466s exec 0.006022s     ctx 0.017601s init 0.009032s exec 0.005779s           
                                                                                        
no clear                                                                                
    ctx 0.071758s init 0.009443s exec 0.005883s     ctx 0.070007s init 0.009276s exec 0.005771s           
    ctx 0.007049s init 0.000404s exec 0.004081s     ctx 0.011737s init 0.004473s exec 0.004947s           
    ctx 0.006273s init 0.000345s exec 0.003684s     ctx 0.012110s init 0.005278s exec 0.004645s           
 
-------------------------------------------------------------------------*/

namespace thoe {
    
//-------------------------------------------------------------------------
// ThreadFrame
//-------------------------------------------------------------------------

//-------------------------------------------------------------------------
void ThreadFrame::pre_execute ( const masnae::Fastcpipp_Environment &  p_environment,
                                bool                                   p_in_simulation,
                                masnae::ProcessingResponse &           p_result,
                                bool                                   p_optz_clear_frame,
                                bool                                   p_optz_clear_global,
                                bool                                   p_optz_cache_code )
{
    m_environment       = & p_environment;
    m_in_simulation     = p_in_simulation;
    m_response          = & p_result;
    m_optz_clear_frame  = p_optz_clear_frame;
    m_optz_clear_global = p_optz_clear_global;
    m_optz_cache_code   = p_optz_cache_code;
    m_system_exit       = false;
}

//-------------------------------------------------------------------------
void ThreadFrame::post_execute ( void )
{
    module_unload();
}

//-------------------------------------------------------------------------
// Checks if a module is already loaded (or loading)
// Returns the previously loaded object if found
ThreadFrame::module_result ThreadFrame::module_get ( const std::string &  p_name,
                                                     JSObject * &         p_module )
{
    auto module = m_loaded_modules.find( p_name );
    //
    if ( module == m_loaded_modules.end() ) {   // new, unknown, module
        m_loaded_modules[ p_name ] = nullptr;   // mark it as Loading
        return module_result::initialized;
    }
    //
    if ( module->second == nullptr )            // existing module, marked as Loading
        return module_result::loading;
    //
    p_module = module->second->get();           // existing module, fully loaded
    return module_result::ok;
}

//-------------------------------------------------------------------------
// Add a new loaded module (as a JavaScript object)
void ThreadFrame::module_loaded ( const std::string &  p_name,
                                  JSObject *           p_module )
{
    m_loaded_modules[ p_name ] = new (std::nothrow) JS::PersistentRootedObject( m_context, p_module );
}

//-------------------------------------------------------------------------
// Delete all loaded module objects
void ThreadFrame::module_unload ( void )
{
    // Remove reference to loaded libraries
    for ( auto module : m_loaded_modules ) {
        if ( module.second != nullptr ) {
            module.second->reset();
            delete module.second;
        }
    }
    //
    m_loaded_modules.clear();    
}

//-------------------------------------------------------------------------
// Compile a script, cache the produced binary
// Called by ScriptExecutor and js_require
bool ThreadFrame::compile_code ( const std::string &      p_name,
                                 const std::string &      p_script,
                                 bool                     p_returns_value,
                                 JS::MutableHandleScript  p_binary )
{
    // Check if this source is already compiled
    auto compiled = m_compiled_codes.find( p_script );
    if ( compiled != m_compiled_codes.end() ) {
        p_binary.set( * compiled->second );
        return true;
    }
    //
    // Allocated a new binary code object
    JS::PersistentRootedScript * binary = new (std::nothrow) JS::PersistentRootedScript( m_context );
    if ( binary == nullptr ) {
        m_last_error = "Out of memory";
        return false;
    }
    //
    // And compile the script
    JS::CompileOptions  opts( m_context );
    //
    opts.setFile         ( p_name.c_str() )
        .setUTF8         ( true )
        .setNoScriptRval ( ! p_returns_value );
    //
    if ( ! JS::Compile( m_context,
                        opts, p_script.c_str(), p_script.length(),
                        binary ) )
    {
        binary->reset();
        delete binary;
        return false;
    }
    //
    // Cache and return the binary code
    p_binary.set( * binary );
    //
    if ( m_optz_cache_code ) {
        m_compiled_codes[ p_script ] = binary;
    } else {
        binary->reset();
        delete binary;
    }
    //
    return true;
}

//-------------------------------------------------------------------------
// Delete all pre-compiled codes
void ThreadFrame::compiled_clear ( void )
{
    // Remove reference to loaded libraries
    for ( auto binary : m_compiled_codes ) {
        if ( binary.second != nullptr ) {
            binary.second->reset();
            delete binary.second;
        }
    }
    //
    m_compiled_codes.clear();
}

//-------------------------------------------------------------------------
// ScriptExecutor
//-------------------------------------------------------------------------

    static
    JSClassOps global_ops = {
        nullptr,                  // addProperty
        nullptr,                  // delProperty
    #if MOZJS_MAJOR_VERSION < 60
        nullptr,                  // getProperty
        nullptr,                  // setProperty
    #endif
        nullptr,                  // enumerate
    #if MOZJS_MAJOR_VERSION >= 60
        nullptr,                  // newEnumerate
    #endif
        nullptr,                  // resolve
        nullptr,                  // mayResolve
        nullptr,                  // finalize
        nullptr,                  // call
        nullptr,                  // hasInstance
        nullptr,                  // construct
        JS_GlobalObjectTraceHook  // trace
    };
    //
    static
    JSClass global_class = {
        "global",
        JSCLASS_GLOBAL_FLAGS,
        & global_ops,
        { nullptr, nullptr, nullptr }         // 'reserved' members (to keep compiler happy)
    };

//-------------------------------------------------------------------------
// Called by the main to initialize SpiderMonkey
// Receives the derived class's list of functions to add to the language
bool ScriptExecutor::start ( void )
{
    masnae::globals::publisher->subscribe( this );
    //
    // Simulate the event to load the config
    bus_event( { .type = masnae::bus::Message::config_updated } );
    //
    // Reset statistics
    m_nb_evaluates_done = 0;
    //
    // Fastcpipp_Header comment:
    // 'It is important that SpiderMonkey be initialized, and the first context
    //  be created, in a single-threaded fashion.  Otherwise the behavior of the
    //  library is undefined.'
    if ( ! JS_Init() ) {
        js_service_error_log( "ScriptExecutor: JS_Init failed", nullptr );
        return false;
    }
    //
    if ( ! init_frame() ) {
        js_service_error_log( "ScriptExecutor: init_frame failed", nullptr );
        return false;
    }
    //
    return true;
}

//-------------------------------------------------------------------------
// There is no way to know when a thread stops, so contexts are not released
void ScriptExecutor::stop ( void )
{
    JS_ShutDown();
    //
    if ( masnae::globals::publisher != nullptr )
        masnae::globals::publisher->unsubscribe( this );
}

//-------------------------------------------------------------------------
// Return statistics on the executor
std::string ScriptExecutor::specific_info ( void )
{
    uint64_t nb_evaluates_done;
    //
    {
        std::lock_guard<std::mutex> lock( m_mutex_stats );
        //
        nb_evaluates_done   = m_nb_evaluates_done;
    }
    //
    return "{\"spidermonkey_version\":" + masnae::json_quote_string(
                                            adawat::str_sprintf( "%u.%u",
                                                MOZJS_MAJOR_VERSION,
                                                MOZJS_MINOR_VERSION
                                          ) ) +
           ",\"nb_evaluates_done\":"    + std::to_string( nb_evaluates_done ) +
           "}";
}

//-------------------------------------------------------------------------
// Execute the given script name
// Returns false if execution fails
// Return true otherwise (the script can set p_result)
bool ScriptExecutor::execute_script ( const std::string &                    p_document,
                                      const std::string &                    p_script_name,
                                      const masnae::Fastcpipp_Environment &  p_environment,
                                      const std::string &                    p_security_json,
                                      bool                                   p_in_simulation,
                                      masnae::ProcessingResponse &           p_result )
{
    // Create ore re-user the thread frame (including JavaScript Context)
    if ( ! init_frame() ) { // out of ram?
        js_service_error_log( "ScriptExecutor: Frame allocation failed", nullptr );
        return false;
    }
    //
    // Create or re-use Global object
    bool global_is_new;
    if ( ! init_global( global_is_new ) ) {
        js_service_error_log( "ScriptExecutor: Failed to allocated global object",
                              m_thread_frame );
        return false;
    }
    //
    // Separated memory heap
    bool result;
    //
    {
        JSAutoCompartment ac( m_thread_frame->m_context, * m_thread_frame->m_global );
        //
        // Eventually, add all functions, classes and object to the Global object
        if ( global_is_new )
            if ( ! configure_global_consts( p_document,
                                            p_environment,
                                            * m_thread_frame->m_global ) )
                return false;
        //
        if ( ! configure_global_vars( p_document,
                                      p_environment,
                                      p_security_json,
                                      * m_thread_frame->m_global ) )
            return false;
        //
        // Compile and execute
        result = evaluate( p_document,
                           p_environment,
                           p_in_simulation,
                           * m_thread_frame->m_global,
                           p_script_name,
                           p_result );
    }
    //
    // Cleanup
    clear_global();  // eventually delete Global object
    //
    clear_frame();   // delete Context and frame, or do a GC
    //
    return result;
}

//-------------------------------------------------------------------------
void ScriptExecutor::bus_event ( const masnae::bus::Message &  p_message )
{
    switch ( p_message.type )
    {
        case masnae::bus::Message::config_updated:
            update_config();
            break;
        default:
            break;
    }
}

//-------------------------------------------------------------------------
// Returns the text of the last execution exception
std::string ScriptExecutor::get_last_error ( void )
{
    std::string     message;
    JS::RootedValue exception( m_thread_frame->m_context );
    //
    // If there is no exception, assume callback_error was called
    if ( ! JS_GetPendingException( m_thread_frame->m_context, & exception) )
        return m_thread_frame->m_last_error;;
    //
    if ( exception.isObject() )  // exception is a report
    {
        JS::RootedObject  object( m_thread_frame->m_context,
                                  exception.toObjectOrNull() );
        JSErrorReport *   report = JS_ErrorFromException( m_thread_frame->m_context,
                                                          object );
        //
        if ( report ) {
            message = report->message().c_str();
            if ( report->filename != nullptr )
                message += ", file " + std::string( report->filename );
            if ( report->lineno != 0 )
                message += ", line " + std::to_string( report->lineno );
            if ( report->column != 0 )
                message += ", column " + std::to_string( report->column );
        } else {
            message = "empty exception report";
        }
    }
    else  // a simple text
    {
        JSString * text = JS::ToString( m_thread_frame->m_context, exception );
        //
        if ( text == nullptr )
            message = "unknown exception";
        else
            message = js_tools_convert( m_thread_frame->m_context, text );  // a literal exception
    }
    //
    JS_ClearPendingException( m_thread_frame->m_context );
    //
    return message;
}

//-------------------------------------------------------------------------
// Add all functions and classes to the GLobal object
bool ScriptExecutor::configure_global_consts ( const std::string &                    p_document,
                                               const masnae::Fastcpipp_Environment &  p_environment,
                                               JS::HandleObject                       p_global )
{
    // Add global properties (Array, Boolean, Date, decodeURI, encodeURI...
    if ( ! JS_InitStandardClasses( m_thread_frame->m_context,
                                   p_global ) )
    {
        js_service_error_log( "ScriptExecutor: Failed to define standard classes",
                              m_thread_frame );
        return false;
    }
    //
    // Define Blob class (must come before Request since Request can create Blob)
    if ( ! js_add_blob_class( m_thread_frame->m_context,
                              p_global ) )
    {
        return false;
    }
    //
    // Define the MySQL class in the global object
    if ( ! js_add_mysql_class( m_thread_frame->m_context,
                               p_global ) )
    {
        return false;
    }
    //
    // Define the global money class
    if ( ! js_add_money_class( m_thread_frame->m_context,
                               p_global ) )
    {
        return false;
    }
    //
    // Add the require function
    if ( ! js_add_require_function( m_thread_frame->m_context,
                                    p_global ) )
    {
        return false;
    }
    //
    // Add global crypto object
    if ( ! js_add_crypto_object( m_thread_frame->m_context,
                                 p_global ) )
    {
        return false;
    }
    //
    // Define global http object with request, jresp and raw methods
    if ( ! js_add_http_object( m_thread_frame->m_context,
                               p_document,
                               p_environment,
                               p_global ) )
    {
        return false;
    }
    //
    // Add global system object (exit, log, usid)
    if ( ! js_add_system_object( m_thread_frame->m_context,
                                 p_global ) )
    {
        return false;
    }
    //
    return true;
}

//-------------------------------------------------------------------------
// Add all objects to the GLobal object
bool ScriptExecutor::configure_global_vars ( const std::string &                    p_document,
                                             const masnae::Fastcpipp_Environment &  p_environment,
                                             const std::string &                    p_security_json,
                                             JS::HandleObject                       p_global )
{
    // Add the global request object
    if ( ! js_add_request_object( m_thread_frame->m_context,
                                  p_document,
                                  p_environment,
                                  p_security_json,
                                  p_global ) )
    {
        return false;
    }
    //
    return true;
}

//-------------------------------------------------------------------------
// Execute the script. The script itself will set p_result using the
// global object Request
bool ScriptExecutor::evaluate ( const std::string &                    p_document,
                                const masnae::Fastcpipp_Environment &  p_environment,
                                bool                                   p_in_simulation,
                                JS::HandleObject                       p_global,
                                const std::string &                    p_script_name,
                                masnae::ProcessingResponse &           p_result )
{
    //------------------------
    // Copy (or reference) contextual variables into the Frame
    {
        std::shared_lock<std::shared_mutex>  lock( m_mutex_config );
        //
        m_thread_frame->pre_execute( p_environment,
                                     p_in_simulation,
                                     p_result,
                                     m_optz_clear_frame,
                                     m_optz_clear_global,
                                     m_optz_cache_code );
    }
    //
    //------------------------
    // Retrieve source of this script
    std::string  script;
    if ( ! g_ScriptLoader.script_get( p_script_name, script ) ) {
        js_service_error_log( "ScriptExecutor: Failed to get script '" + p_script_name + "'source",
                              m_thread_frame );
        return false;
    }
    //
    // Compile and eventually cache it
    JS::RootedScript  binary( m_thread_frame->m_context );
    if ( ! m_thread_frame->compile_code( p_script_name, script, false,
                                         & binary ) )
    {
        std::string last_error = get_last_error();
        //
        p_result.add_error( "ScriptExecutor: " + last_error );
        p_result.add_log  ( p_result.regular, "error: " + last_error );
        return false;
    }
    //
    //------------------------
    // The script is not suppose to return a result. Instead it sets its result
    // using Request.respond_json. The Request object is also used to retrieve
    // caller parameters. The Request object has access to p_environment and
    // p_result via m_thread_frame:
    JS::RootedValue  rval ( m_thread_frame->m_context );  // not used
    bool successful = JS_ExecuteScript( m_thread_frame->m_context,
                                        binary,
                                        & rval );
    //
    // In case or error, retrieve the reason
    if ( ! successful ) {
        if ( m_thread_frame->m_system_exit ) { // not a real error but a call to exit()
            successful = true;
        } else {
            std::string last_error = get_last_error();
            //
            p_result.add_error( "ScriptExecutor: " + last_error );
            p_result.add_log  ( p_result.regular, "error: " + last_error );
        }
    }
    //
    //------------------------
    // Reset the Frame
    m_thread_frame->post_execute();
    //
    //------------------------
    // Update stats
    {
        std::lock_guard<std::mutex> lock( m_mutex_stats );
        //
        m_nb_evaluates_done++;
    }
    //
    return successful;
}

//-------------------------------------------------------------------------
// Reads configuration from global config object.
bool ScriptExecutor::update_config ( void )
{
    bool    ok = true;
    //
    int64_t max_memory_mb     =   masnae::globals::config_handler->get_int64(
                                      "/spidermonkey/max_memory_mb",
                                      16
                                  );                                
    bool    optz_clear_frame  =   masnae::globals::config_handler->get_bool(
                                      "/spidermonkey/optz_clear_frame",
                                      false
                                   );
    bool    optz_clear_global =   masnae::globals::config_handler->get_bool(
                                      "/spidermonkey/optz_clear_global",
                                      false
                                  );
    bool    optz_cache_code   =   masnae::globals::config_handler->get_bool(
                                      "/spidermonkey/optz_cache_code",
                                      true
                                  );
    //
    ok &= max_memory_mb >= 1 && max_memory_mb <= 128;
    //
    if ( ok ) {
        std::lock_guard<std::shared_mutex>  lock( m_mutex_config );
        //
        m_max_memory_mb     = max_memory_mb;
        m_optz_clear_frame  = optz_clear_frame;
        m_optz_clear_global = optz_clear_global;
        m_optz_cache_code   = optz_cache_code;
    } else {
        masnae::service_error_log( "ScriptExecutor: invalid config" );
    }
    //
    return ok;
}

//-------------------------------------------------------------------------
// Each thread must have its own frame to hold its variables.
// Use a local class member to keep it
thread_local
ThreadFrame * ScriptExecutor::m_thread_frame = nullptr;

//-------------------------------------------------------------------------
// Check that this thread has created his frame, create it if needed
// Default options:
// JS::ContextOptionsRef(cx).setBaseline(true)
                         // .setIon(true)
                         // .setAsmJS(true)
                         // .setWasm(false)
                         // .setWasmAlwaysBaseline(false)
                         // .setThrowOnAsmJSValidationFailure(false)
                         // .setNativeRegExp(true)
                         // .setUnboxedArrays(false)
                         // .setAsyncStack(true)
                         // .setThrowOnDebuggeeWouldRun(true)
                         // .setDumpStackOnDebuggeeWouldRun(false)
                         // .setWerror(false)
                         // .setStrictMode(false)
                         // .setExtraWarnings(false)
bool ScriptExecutor::init_frame ( void )
{
    if ( m_thread_frame == nullptr ) {
        //
        int64_t max_memory_mb;
        {
            std::shared_lock<std::shared_mutex>  lock( m_mutex_config );
            //
            max_memory_mb = m_max_memory_mb;
        }
        //
        // Create frame
        m_thread_frame = new (std::nothrow) ThreadFrame;
        if ( m_thread_frame == nullptr )
            return false;
        //
        m_thread_frame->m_global            = nullptr;
        m_thread_frame->m_optz_clear_frame  = false;
        m_thread_frame->m_optz_clear_global = false;
        m_thread_frame->m_optz_cache_code   = false;
        //
        // Create JavaScript context
        m_thread_frame->m_context = JS_NewContext( max_memory_mb * 1024 * 1024 );
        if ( m_thread_frame->m_context == nullptr ) {
            delete m_thread_frame;
            return false;
        }
        //
        // Add all JavaScript global objects and functions
        // See: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects
        if ( ! JS::InitSelfHostedCode( m_thread_frame->m_context ) ) {
            JS_DestroyContext( m_thread_frame->m_context );
            delete m_thread_frame;
            return false;
        }
        //
        // Configure JavaScript context
        JS_SetContextPrivate      ( m_thread_frame->m_context, m_thread_frame );  // context data is the frame
        JS::SetWarningReporter    ( m_thread_frame->m_context, callback_error );
        JS::SetOutOfMemoryCallback( m_thread_frame->m_context, callback_out_of_memory, nullptr );
        JS_AddInterruptCallback   ( m_thread_frame->m_context, callback_interrupt );
    }
    //
    return true;
}

//-------------------------------------------------------------------------
// Delete frame
void ScriptExecutor::clear_frame ( void )
{
    // There is nothing to do if the frame does not exist
    if ( m_thread_frame == nullptr )
        return;
    //
    m_thread_frame->m_last_error.erase();
    //
    if ( m_thread_frame->m_optz_clear_frame ) {
        clear_global();                    // delete global object
        m_thread_frame->compiled_clear();  // delete pre-compiled code
        //
        JS_DestroyContext( m_thread_frame->m_context );
        //
        delete m_thread_frame;
        m_thread_frame = nullptr;
    } else {
        // JS_MaybeGC cannot be used because eventually no memory will be freed
        // and the next context usage may fail to initialize (usually in
        // JS_InitStandardClasses called from configure_global, after ~300 executions
        JS_GC( m_thread_frame->m_context );
    }
}

//-------------------------------------------------------------------------
// Check that this thread has created his global object, create it if needed
// Returns true if global object exists, set p_created to true if it was just created
bool ScriptExecutor::init_global ( bool &  p_created )
{
    p_created = false;
    //
    // There is nothing to do if the global object is already created
    if ( m_thread_frame->m_global != nullptr )
        return true;
    //
    // Create a new one
    JS_BeginRequest( m_thread_frame->m_context );
    //
    m_thread_frame->m_global =
        new (std::nothrow) JS::PersistentRootedObject( m_thread_frame->m_context, 
                                                       JS_NewGlobalObject( m_thread_frame->m_context,
                                                                           & global_class,
                                                                           nullptr,
                                                                           JS::FireOnNewGlobalHook,
                                                                           JS::CompartmentOptions() ) );
    //
    // If creation fails, stop the Request context
    if ( m_thread_frame->m_global == nullptr ) {
        JS_EndRequest( m_thread_frame->m_context );
        return false;
    }
    //
    p_created = true;
    return true;  //-V1020  PVS thinks a call to JS_EndRequest is missing
}

//-------------------------------------------------------------------------
// Clear or delete the global object
void ScriptExecutor::clear_global ( void )
{
    // There is nothing to do if the global object does not exist
    if ( m_thread_frame->m_global == nullptr )
        return;
    //
    if ( m_thread_frame->m_optz_clear_frame || m_thread_frame->m_optz_clear_global ) {
        delete m_thread_frame->m_global;
        m_thread_frame->m_global = nullptr;
        //
        JS_EndRequest( m_thread_frame->m_context );
    }
}

//-------------------------------------------------------------------------
// Function called by engine on error
// The error is kept and can be retrieved later by get_last_error
void ScriptExecutor::callback_error ( JSContext *      p_context,
                                      JSErrorReport *  p_report )
{
    ThreadFrame * frame = static_cast<ThreadFrame*>( JS_GetContextPrivate( p_context ) );
    //
    frame->m_last_error = p_report->message().c_str();
}

//-------------------------------------------------------------------------
// Function called by engine when there is no more memory
// The error is kept and can be retrieved later by get_last_error
void ScriptExecutor::callback_out_of_memory ( JSContext *      p_context,
                                              void *           p_data )
{
    ThreadFrame * frame = static_cast<ThreadFrame*>( JS_GetContextPrivate( p_context ) );
    //
    frame->m_last_error = "Out of memory";
}

//-------------------------------------------------------------------------
// Function called by engine on interruption. As stated in jsapi.h:
//  "To schedule the GC and for other activities the engine internally triggers
//   interrupt callbacks. The embedding should thus not rely on callbacks being
//   triggered through the external API only."
// We could add a timer calling JS_RequestInterruptCallback to handle timeout
bool ScriptExecutor::callback_interrupt ( JSContext *  p_context )
{
    //
    return true;
}

} // namespace thoe

//-------------------------------------------------------------------------
// This function is referenced by Vector (via maybeCheckSimulatedOOM, in jsalloc.h)
// which is used by js_tools_add_property when using JS::AutoValueVector append
// method. But is missing from the library (and is therefor declare here in js
// namespace).
// What should it do? Call JS_ReportOutOfMemory? But we are not in execute....
#if MOZJS_MAJOR_VERSION < 60
void js::ReportOutOfMemory ( js::ExclusiveContext * )
{
}
#endif
