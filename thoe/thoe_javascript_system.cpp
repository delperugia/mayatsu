// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

// Copyright 2018 Pierre del Perugia
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "masnae_misc.h"
#include "thoe_javascript.h"
#include "thoe_js_tools.h"
#include "thoe_misc.h"
#include "thoe_script_executor.h"

namespace thoe {

//-------------------------------------------------------------------------
// Returns true if the script is executed from the CLI in simulation mode
static
bool js_system_cli( JSContext * p_context, unsigned p_argc, JS::Value * p_argv )
{
    ThreadFrame * frame = static_cast< ThreadFrame* >( JS_GetContextPrivate( p_context ) );
    JS::CallArgs  args  = JS::CallArgsFromVp( p_argc, p_argv );
    //
    args.rval().setBoolean( frame->m_in_simulation );
    return true;
}

//-------------------------------------------------------------------------
// Disable file logging for the current request
static
bool js_system_disable_log( JSContext * p_context, unsigned p_argc, JS::Value * p_argv )
{
    ThreadFrame * frame = static_cast< ThreadFrame* >( JS_GetContextPrivate( p_context ) );
    //
    frame->m_response->disable_log();
    //
    return true;
}

//-------------------------------------------------------------------------
// Exit from the current JS::Evaluate
static
bool js_system_exit( JSContext * p_context, unsigned p_argc, JS::Value * p_argv )
{
    ThreadFrame * frame = static_cast< ThreadFrame* >( JS_GetContextPrivate( p_context ) );
    //
    frame->m_system_exit = true;
    //
    JS_ClearPendingException( p_context );
    return false;
}

//-------------------------------------------------------------------------
// Add a log line to the response.
// Syntax:  log( string, object + );
// Example:
//   system.log('R', 'Hello', obj);
// First parameter is the level and must be one of the string:
//  'R', 'V' or 'D' (Regular, Verbose or Debug)
static
bool js_system_log( JSContext * p_context, unsigned p_argc, JS::Value * p_argv )
{
    ThreadFrame * frame = static_cast< ThreadFrame* >( JS_GetContextPrivate( p_context ) );
    JS::CallArgs  args  = JS::CallArgsFromVp( p_argc, p_argv );
    //
    if ( ! js_tools_check_argument( p_context, "log", args, "S*" ) )
        return false;
    //
    std::string level  = js_tools_convert( p_context, args[0].toString() );
    std::string message;
    //
    for ( unsigned u = 1; u < args.length(); u++ ) {
        std::string text;
        //
        if ( ! js_tools_convert( p_context, args[ u ], text ) )
            if ( ! js_tools_stringify( p_context, args[ u ], text ) )
                text = "?";
        //
        if ( ! message.empty() )
            message += ",";
        //
        message += text;
    }
    //
    //-------
    masnae::ProcessingResponse::log_level e_level;
    //
         if ( level == "R" )  e_level = masnae::ProcessingResponse::regular;
    else if ( level == "V" )  e_level = masnae::ProcessingResponse::verbose;
    else                      e_level = masnae::ProcessingResponse::debug;  // by default, to restrict information leak
    //
    frame->m_response->add_log( e_level, message );
    //
    return true;
}

//-------------------------------------------------------------------------
// Returns current response id
static
bool js_system_usid( JSContext * p_context, unsigned p_argc, JS::Value * p_argv )
{
    ThreadFrame * frame = static_cast< ThreadFrame* >( JS_GetContextPrivate( p_context ) );
    JS::CallArgs  args  = JS::CallArgsFromVp( p_argc, p_argv );
    //
    //------------------------
    JS::RootedString string( p_context,
                             JS_NewStringCopyZ( p_context,
                                                frame->m_response->get_id().c_str() ) );
    //
    args.rval().setString( string );
    return true;
}

//-------------------------------------------------------------------------
// Convert an XML string into a JavaScript object or null
// Syntax:  xml2json( string );
static
bool js_system_xml2json( JSContext * p_context, unsigned p_argc, JS::Value * p_argv )
{
    JS::CallArgs  args  = JS::CallArgsFromVp( p_argc, p_argv );
    //
    if ( ! js_tools_check_argument( p_context, "xml2json", args, "S" ) )
        return false;
    //
    // Convert XML into JSON
    std::string json,
                xml  = js_tools_convert( p_context, args[0].toString() );
    //
    // On error (bad XML) return null
    if ( ! xml2json::convert( xml, json ) ||
         ! masnae::is_utf8( json.c_str(), json.length() ) )
    {
        args.rval().setNull();
        return true;
    }
    //
    // Build a JavaSCript object
    JS::ConstUTF8CharsZ  json_utf8  ( json.c_str(),
                                      json.length() );  // no string copy here
    JS::RootedString     json_string( p_context,
                                      JS_NewStringCopyUTF8Z( p_context,
                                                              json_utf8 ) );
    if ( ! json_string ) {
        JS_ReportErrorUTF8( p_context, "xml2json: failed to convert response to Unicode" );
        return false;
    }
    //
    JS::RootedValue rval( p_context );
    //
    if ( ! JS_ParseJSON( p_context, json_string, & rval) )
        return false;
    //
    // Return result
    args.rval().setObject( rval.toObject() );
    //
    return true;
}

//-------------------------------------------------------------------------
// Define the system object in the global object
// Called only once by ScriptExecutor::execute_script if global
// is reused (m_optz_clear_global = false).
bool js_add_system_object( JSContext *                   p_context,
                           JS::HandleObject              p_global )
{
    // Create an new object 'system', as a property of the Global object
    JS::RootedObject system_object( p_context,
                                    JS_DefineObject( p_context,
                                                     p_global,
                                                     "system",
                                                     nullptr,
                                                     JSPROP_ENUMERATE|JSPROP_READONLY|JSPROP_PERMANENT ) );
    //
    if ( system_object == nullptr ) {
        js_service_error_log( "JavaScript: Failed to allocated system object", nullptr );
        return false;
    }
    //
    static const JSFunctionSpec functions[] = {
       //     name               function                      nargs flags
       JS_FN("cli",              js_system_cli,                  0,    0),
       JS_FN("disable_log",      js_system_disable_log,          0,    0),
       JS_FN("exit",             js_system_exit,                 0,    0),
       JS_FN("log",              js_system_log,                  2,    0),
       JS_FN("usid",             js_system_usid,                 0,    0),
       JS_FN("xml2json",         js_system_xml2json,             1,    0),
       JS_FS_END
    };
    //
    if ( ! JS_DefineFunctions( p_context,
                               system_object,
                               functions ) )
    {
        js_service_error_log( "JavaScript: Failed to define system's methods", nullptr );
        return false;
    }
    //
    return true;
}

} // namespace thoe
