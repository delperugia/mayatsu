// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

// Copyright 2018 Pierre del Perugia
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <iostream>
#include <sstream>

#include "masnae_simulator.h"
#include "thoe_common.h"
#include "thoe_request.h"

namespace thoe {

//-------------------------------------------------------------------------
void console ( void )
{
    thoe::JsRequestHandler  handler;
    //
    handler.security_disable_uriana();
    //
    masnae::Fastcpipp_Path      document;
    masnae::ProcessingResponse  response;
    handler.check_security( "/ws", document, response );
    //
    masnae::console( handler );
}

//-------------------------------------------------------------------------
// The test program must return a JSend result with data:
//      { x: 42, y: "forty-two", z: [ 2, 3, 7 ] }
// To be launched with:
//      ./thoe.fcgi -t thoe_validate.js -f `pwd`/thoe_config_validate.json
void validate ( const std::string &  p_test_url, bool *  p_succeeded )
{
    thoe::JsRequestHandler      handler;
    masnae::ProcessingResponse  response;
    //   
    handler.security_disable_uriana();
    //
    masnae::Fastcpipp_Path  document;
    handler.check_security( p_test_url, document, response );
    //
    * p_succeeded = false;
    //
    std::cout << p_test_url << "\n";
    //
    if ( ! masnae::exec_url( p_test_url, handler, response ) )
        return;
    //
    if ( ! response.is_raw() ) {
        std::ostringstream  os;
        response.send_jresp( os );
        //
        std::cout << os.str() << "\n";
        //
        rapidjson::Document jresp;
        jresp.Parse< rapidjson::kParseFullPrecisionFlag |
                     rapidjson::kParseCommentsFlag >( os.str().c_str() );
        //
        * p_succeeded = 
            ! jresp.HasParseError() && jresp.IsObject() &&
            //
            jresp.HasMember("status") && jresp["status"].IsString() &&
            jresp["status"] == "success" && 
            //
            jresp.HasMember("data") && jresp["data"].IsObject() &&
            //
            jresp["data"].HasMember("x")  &&
            jresp["data"].HasMember("y")  &&
            jresp["data"].HasMember("z")  &&
            jresp["data"]["x"].IsUint()   && jresp["data"]["x"] == 42 &&
            jresp["data"]["y"].IsString() && jresp["data"]["y"] == "forty-two" &&
            jresp["data"]["z"].IsArray()  && jresp["data"]["z"].Size() == 3 &&
               jresp["data"]["z"][0] == 2 &&
               jresp["data"]["z"][1] == 3 &&
               jresp["data"]["z"][2] == 7
            ;
    }
    //
    if ( * p_succeeded )
        std::cout << "success\n";
    else
        std::cout << "failure\n";
    //
    if ( response.has_error() )
        std::cout << "error:   " << response.get_error() << "\n";
    //
    if ( response.has_log() )
        std::cout << "log:\n"    << response.get_log()   << "\n";
}

} // namespace thoe
