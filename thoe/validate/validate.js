//-------------------------------------------------------------------------
// Require
// data is [2,3], fct adds 7
system.log('R', 'Require');

var gamma = require( 'validate_lib.js' );

//-------------------------------------------------------------------------
// money
system.log('R', 'Money');

// In a block in case the re-use Global is activated (would cause a
// "redeclaration of let a" error
{
    let a = new Amount('2.5','EUR');                     // 2.5
    let b = new Amount().set('5','EUR');                 // 5
    let c = new Amount('1.000','EUR');                   // 1
    let d = a.mul('3').add(b).sub(c).div('2');           // 5.75
    let e = new Amount('10','EUR').div('3').round( Amount.rounding_up, 2 );  // 3.34
    let f = new Amount('200','EUR');
    let g = new Amount('9223372036854.775804','EUR').add( 
            new Amount('0.000001','EUR') );
    let h = new Amount('-9223372036854.775805','EUR').sub( 
            new Amount('0.000001','EUR') );
    let i = new Amount('0.000001','EUR').add( 
            new Amount('99.999999','EUR') );

    a.set('654321.1234','USD');

    if ( d.value() != '5.75' || d.currency() != 'EUR'   ||
         e.value() != '3.34' || e.currency() != 'EUR'   ||
         //
         a.format(3,',',a.grouping_myriad,'.',a.currency_code_before,'') != 'USD65.4321,123' ||
         f.percent('42').value()    != '84'             ||
         //
         g.value() !=  '9223372036854.775805' ||
         h.value() != '-9223372036854.775806' ||
         i.value() != '100' ||
         //
         f.percent('50').compareTo( i ) != 0 ||
         //
         false
        )
    {
        request.respond_error( 'money error') ;
        system.exit();
    }
}

//-------------------------------------------------------------------------
// Attachments
system.log('R', 'Attachments');

if ( request.attachments[0].blob.size != 15  || // size is 15, length is 8
     request.attachments[0].blob.toString() != '_€_中国_É_' )
{
    request.respond_error( 'bad attachment' );
    system.exit();
}

request.attachments.forEach( function( i ) {
    system.log( 'V', ' ' + i.name );
} );

//-------------------------------------------------------------------------
// Parameters
// b is "7", a$ is ["2","3"]
system.log('R', 'Parameters');

var alpha = request.params.b
            *
            request.params.a$.reduce( (a,x) => a * x , 1 );

//-------------------------------------------------------------------------
// Security. request.security is equal to:
//  {
//    "user"       : "111",
//    "profile"    : "profile_test",
//    "role"       : "role_test",
//    "scopes"     : "refresh profile",
//    "permissions": {
//      "perm_1":  "one",
//      "perm_2":  "two",
//      "thoe.ws": ""
//    }
//  }
system.log('R', 'Security');

if (  request.security.user     != "111"                       ||
      request.security.profile  != "profile_test"              ||
      request.security.role     != "role_test"                 ||
      request.security.scopes   != "refresh profile"           ||
      Object.keys( request.security.permissions ).length != 3  ||
      request.security.permissions.perm_1 != "one"             ||
      request.security.permissions.perm_2 != "two"
    )
{
    request.respond_error( 'Security error' ) ;
    system.exit();
}

//-------------------------------------------------------------------------
// Http
// keep latest object in 'beta' for debug: it will be reported in final response
system.log('R', 'Http');

var beta = http.request( 'adr_post', { d: 3 } );  // answer: { "form": { "d": "3" },...
if ( beta.code == 200 && beta.type == 'application/json' && beta.body ) {
    beta = JSON.parse( beta.body.toString() );
    //
    if ( beta && beta.form && beta.form.d == 3 )
        beta = 'forty-two';
}

//-------------------------------------------------------------------------
// Blob
system.log('R', 'Blob');
{
    let my_blob = new Blob;
    my_blob.set( system.usid() );
    my_blob.toFile( '_thoe_validate.tmp' );
}
{
    let my_blob = new Blob;
    my_blob.fromFile( '_thoe_validate.tmp' );
    if ( my_blob.toString() != system.usid() ) {
        request.respond_error( 'Blob file error' ) ;
        system.exit();
    }
}
{
    let my_blob = new Blob;
    my_blob.set( '01234567' );
    //
    if (    my_blob.toString(          )        != '01234567'
         || my_blob.toString( 'base16' )        != '3031323334353637'
         || my_blob.toString( 'base32' )        != 'GAYTEMZUGU3DO==='
         || my_blob.toString( 'base32hex' )     != '60OJ4CPK6KR3E==='
         || my_blob.toString( 'base64'  )       != 'MDEyMzQ1Njc='
         || my_blob.toString( 'base64url'  )    != 'MDEyMzQ1Njc='
         || my_blob.toString( 'base64_xx' )     != 'A12mAnEpBXQ='
         //
         || my_blob.toString( 'base64', false ) != 'MDEyMzQ1Njc'
       )
    {
        request.respond_error( 'Blob encoding error' ) ;
        system.exit();
    }
    //
    if (    my_blob.set( '3031323334353637', 'base16' ).toString() != '01234567'
         || my_blob.set( 'GAYTEMZUGU3DO===', 'base32' ).toString() != '01234567'
         || my_blob.set( 'MDEyMzQ1Njc====',  'base64' ).toString() != '01234567'
         //
         || my_blob.set( '0001020304', 'base16' ).toString( 'base16' ) != '0001020304'
       )
    {
        request.respond_error( 'Blob decoding error' ) ;
        system.exit();
    }
    //
    my_blob.set( '01234567' );
    //
    if ( my_blob.toArrayBuffer().join() != '48,49,50,51,52,53,54,55' ) {
        request.respond_error( 'Blob toArrayBuffer error' ) ;
        system.exit();
    }
    //
    let s = '';
    for ( let i = 0; i < my_blob.size; i++ )
        s += ',' + my_blob.at( i );
    //
    if ( s != ',48,49,50,51,52,53,54,55' ) {
        request.respond_error( 'Blob at error' ) ;
        system.exit();
    }
}

//-------------------------------------------------------------------------
// Blob
system.log('R', 'Crypto');
{
    let my_blob = new Blob;
    my_blob.set( 'The quick brown fox jumps over the lazy dog' );
    //
    if ( crypto.digest( 'md5', my_blob ).toString('base16') != '9E107D9D372BB6826BD81D3542A419D6' ) {
        request.respond_error( 'Cripto digest error' ) ;
        system.exit();
    }
    //
    let my_key = new Blob;
    my_key.set( 'key' );
    //
    if ( crypto.hmac( 'md5', my_key, my_blob ).toString('base16') != '80070713463E7749B90C2DC24911E275' ) {
        request.respond_error( 'Cripto hmac error' ) ;
        system.exit();
    }
    //
    if ( crypto.pbkdf2( 'sha1', my_blob, my_key, 100, 32 ).toString('base16') != 'BD9404D4D7CCB0A3FBCC429A8D148DBAEF788CE450E067C296D018B443C90E7D' ) {
        request.respond_error( 'Cripto pbkdf2 error' ) ;
        system.exit();
    }
    //
    my_key.set( '0123456789ABCDEF' );
    //
    let iv      = crypto.random( 16 );
    let coded   = crypto.cipher( 'aes-128-cbc', my_key, iv, my_blob, true );
    let decoded = crypto.cipher( 'aes-128-cbc', my_key, iv, coded,   false );
    //
    if ( my_blob.toString('base16') != decoded.toString('base16') ) {
        request.respond_error( 'Cripto cipher error' ) ;
        system.exit();
    }
    //
    if ( ! crypto.equal( '123', '123' ) ||
           crypto.equal( '123', '124' ) ||
           crypto.equal( '12',  '123' ) )
    {
        request.respond_error( 'Cripto equal error' ) ;
        system.exit();
    }
}

//-------------------------------------------------------------------------
// System
system.log('R', 'System');

var xo = system.xml2json( '<a><b>beta</b><c d="1"/></a>' );
if ( xo == null || xo.a == undefined ||
     xo.a.b != 'beta' || xo.a.c['@d'] != '1' ||
     system.xml2json( '<a>' ) != null    )
{
    request.respond_error( 'System xml2json error' ) ;
    system.exit();
}

if ( ! system.cli() )
{
    request.respond_error( 'Not executed from CLI' ) ;
    system.exit();
}

//-------------------------------------------------------------------------
// Send result
system.log('R', 'Send');
//
request.respond_success( {
    x:  alpha,
    y:  beta,
    z:  gamma.fct( gamma.data, 7 )
} );
