// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

// Copyright 2018 Pierre del Perugia
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <rapidjson/pointer.h>
#include <regex>
#include <unistd.h>
#include <adawat/adawat_file.h>

#include "masnae_misc.h"
#include "masnae_global.h"
#include "thoe_common.h"
#include "thoe_script_loader.h"

namespace thoe {

//-------------------------------------------------------------------------
ScriptLoader::ScriptLoader () :
    masnae::MonitorFiles( "ScriptLoader" )
{
}

//-------------------------------------------------------------------------
// Called by the main to initialize the class
// The monitoring will start when the configuration is read (via the bus)
bool ScriptLoader::start ( void )
{
    update_config();
    //
    masnae::globals::publisher->subscribe( this );
    //
    return true;
}

//-------------------------------------------------------------------------
// Call before program termination
void ScriptLoader::stop ( void )
{
    stop_monitoring();
    //
    if ( masnae::globals::publisher != nullptr )
        masnae::globals::publisher->unsubscribe( this );
}

//-------------------------------------------------------------------------
// Return statistics on the executor
std::string ScriptLoader::specific_info ( void )
{
    uint64_t nb_scripts_cached;
    //
    {
        std::shared_lock< std::shared_mutex > lock( m_mutex_scripts );
        //
        nb_scripts_cached = m_scripts.size();
    }
    //
    return "{\"nb_scripts_cached\":" + std::to_string( nb_scripts_cached ) +
           "}";
}

//-------------------------------------------------------------------------
// Returns true if the script exists (and load it in cache if needed)
// p_name is relative to m_scripts_folder. It must not be absolute and must
// not go up
bool ScriptLoader::script_exists ( const std::string & p_name )
{
    // Try first from cache
    {
        std::shared_lock< std::shared_mutex > lock( m_mutex_scripts );
        //
        if ( m_scripts.count( p_name ) == 1 )
            return true;
    }
    //
    // Check that there is no path going up nor that the path is absolute
    // Only allow alphanum and _ characters, with extension
    //    Allowed                       Rejected
    //      prefix.name.js                  /a.e
    //      name.js                         ./a.e
    //      name                            ../a.e
    //      folder/name.js                  f/../a.e
    //      folder.ext/name.js              ~/a.e
    //                                      f/.a
    //
    std::regex  R( "^(\\w+(\\.\\w+)*\\/)*(\\w+(\\.\\w+)*)$" );   // regexp: ^(\w+(\.\w+)*\/)*(\w+(\.\w+)*)$
    if ( ! std::regex_match( p_name, R ) )
        return false;
    //
    // Read script from disk
    std::string scripts_folder, script;
    //
    {
        std::shared_lock< std::shared_mutex >  lock( m_mutex_config );
        //
        scripts_folder = m_scripts_folder;
    }
    //
    if ( ! adawat::file_read( scripts_folder + p_name, script ) ) {
        if ( errno != ENOENT )  // do not log 'No such file or directory' errors
            masnae::service_error_log( "ScriptLoader failed to open file " + p_name +
                                       ", errno=" + std::to_string( errno ) );
        //
        return false;
    }
    //
    // And put it in cache
    script_add( p_name, script );
    //
    return true;
}

//-------------------------------------------------------------------------
// Returns the source of a script from cache
// script_exists must have been called before
bool ScriptLoader::script_get ( const std::string &  p_name,
                                std::string &        p_script )
                   
{
    std::shared_lock< std::shared_mutex > lock( m_mutex_scripts );
    //
    auto x = m_scripts.find( p_name );
    //
    if ( x == m_scripts.end() ) {
        return false;
    } else {
        p_script = x->second;
        return true;
    }
}

//-------------------------------------------------------------------------
// Remove a script from the cache
void ScriptLoader::script_remove ( const std::string &  p_name )
{
    masnae::service_info_log( "ScriptLoader removing " + p_name );
    //
    std::lock_guard< std::shared_mutex > lock( m_mutex_scripts );
    //
    m_scripts.erase( p_name );
}

//-------------------------------------------------------------------------
// Insert a script in the cache
void ScriptLoader::script_add( const std::string &  p_name, 
                               std::string &        p_script )
{
    masnae::service_info_log( "ScriptLoader adding " + p_name );
    //
    std::lock_guard< std::shared_mutex > lock( m_mutex_scripts );
    //
    m_scripts[ p_name ] = p_script;
}

//-------------------------------------------------------------------------
// Remove all scripts from cache
void ScriptLoader::clear ( void )
{
    masnae::service_info_log( "ScriptLoader clearing cache" );
    //
    std::lock_guard< std::shared_mutex > lock( m_mutex_scripts );
    //
    m_scripts.clear();
}

//-------------------------------------------------------------------------
void ScriptLoader::bus_event ( const masnae::bus::Message &  p_message )
{
    switch ( p_message.type )
    {
        case masnae::bus::Message::config_updated:
            update_config();
            break;
        default:
            break;
    }
}

//-------------------------------------------------------------------------
// Read config from config object
void ScriptLoader::update_config ( void )
{
    std::string new_folder =
        masnae::globals::config_handler->get_string(
            "/scripts/folder",
            thoe::constants::k_default_script_folder
        );
    //
    // If folder is not absolute, prepend with current directory
    if ( new_folder[0] != '/' ) {
        char path[ PATH_MAX ];
        //
        new_folder.insert( 0, "/" );    // getcwd returns a path without trailing /
        new_folder.insert( 0, getcwd( path, sizeof( path ) ) );
    }
    //
    // If folder has changed, need to restart monitoring
    if ( new_folder != m_scripts_folder ) {  // no lock:: cannot only be changed below
        stop_monitoring();
        //
        {   // but need lock here because script_exists continue to use it
            std::lock_guard< std::shared_mutex >  lock( m_mutex_config );
            //
            m_scripts_folder = new_folder;
        }
        //
        start_monitoring( m_scripts_folder, true );
        //
        // Clear the cache: script_exists may have load script while changing folder
        clear();
    }
}

//-------------------------------------------------------------------------
// Function called when a script file changes
void ScriptLoader::monitoring_event ( const std::string &   p_file )
{
    script_remove( p_file );    
}

} // namespace thoe
