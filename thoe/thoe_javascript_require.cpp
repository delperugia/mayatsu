// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

// Copyright 2018 Pierre del Perugia
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "masnae_misc.h"
#include "thoe_common.h"
#include "thoe_javascript.h"
#include "thoe_script_executor.h"
#include "thoe_js_tools.h"

namespace thoe {

//-------------------------------------------------------------------------
// Load an external library
// Syntax: var module = require( 'module_name.js' );
// Usage is close to the one in Node
static
bool js_require( JSContext * p_context, unsigned p_argc, JS::Value * p_argv )
{
    ThreadFrame * frame = static_cast< ThreadFrame* >( JS_GetContextPrivate( p_context ) );
    JS::CallArgs  args  = JS::CallArgsFromVp( p_argc, p_argv );
    //
    //------------------------
    // The parameter is the module name (not the file one: no extension)
    // This allows to use the regular script loader
    if ( ! js_tools_check_argument( p_context, "require", args, "S" ) )
        return false;
    //
    std::string module_name = js_tools_convert( p_context, args[0].toString() );
    //
    //------------------------
    // If the module is already loaded, returns the existing object
    JSObject * module;
    //
    switch ( frame->module_get( module_name, module ) ) {
        case ThreadFrame::module_result::ok:
            // Returns existing object
            args.rval().setObject( * module );
            return true;
        case ThreadFrame::module_result::loading:
            // There is a circular dependency problem ( A depends on B depends on A )
            JS_ReportErrorUTF8( p_context, "require circular loop detected, module '%s'", module_name.c_str() );
            return false;
        case ThreadFrame::module_result::initialized:
            // Library is now marked as Loading
            break;
        default: // not possible by construction, just to keep the compiler happy
            return false;
    }
    //
    //------------------------
    std::string module_source;
    if ( ! g_ScriptLoader.script_exists( module_name ) ||
         ! g_ScriptLoader.script_get( module_name, module_source ) )
    {
        JS_ReportErrorUTF8( p_context, "require module '%s' not found", module_name.c_str() );
        return false;
    }
    //
    //---------------------
    // The library script is enclosed in the anonymous function below. Only
    // functions and members added by the script to the 'exports' parameter
    // will be reachable. Note the the anonymous function is called with
    // parameter {} (a new empty object). It is this object (known as 'exports'
    // by the script) that will be returned (and use by the invoker of require).
    std::string require_source =
        "(function (exports) { 'use strict'; "                             \
        " exports.id = " + masnae::json_quote_string( module_name ) + ";"  +
          module_source                                                    +
        " return exports; "                                                \
        "}({}));";
    //
    // Node modules also have parent, filename, loaded, children, paths
    //
    //---------------------
    // Compile the wrapped script
    JS::RootedScript  binary( p_context );
    if ( ! frame->compile_code( module_name, require_source, true,
                                & binary ) )
        return false;
    //
    //---------------------
    // Execute the wrapped script
    JS::RootedValue  rval( p_context );
    bool successful = JS_ExecuteScript( p_context,
                                        binary,
                                        & rval );
    //
    if ( ! successful )
        return false;
    //
    //---------------------
    // The wrapped script returns an object that we return
    if ( ! rval.isObject() )
        return false;
    //
    // Update the loaded libraries' binary
    frame->module_loaded( module_name, & rval.toObject() );
    //
    args.rval().setObject( rval.toObject() );
    //
    return true;
}

//-------------------------------------------------------------------------
// Define the require function in the global object
// Called only once by ScriptExecutor::execute_script if global
// is reused (m_optz_clear_global = false).
bool js_add_require_function( JSContext *       p_context,
                              JS::HandleObject  p_global )
{
    if ( ! JS_DefineFunction( p_context,
                              p_global,
                              "require",
                              js_require,
                              1,
                              0 ) )
    {
        js_service_error_log( "ScriptExecutor: Failed to define require function", nullptr );
        return false;
    }
    //
    return true;
}

} // namespace thoe
