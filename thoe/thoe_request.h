// Copyright 2018 Pierre del Perugia
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

//
// Class called by Masnae to process a request
//

#ifndef thoe_request_h
#define thoe_request_h

#include "masnae_request_handler.h"

namespace thoe {

//-------------------------------------------------------------------------
class JsRequestHandler : public masnae::RequestHandler
{
    public:
        JsRequestHandler  ();
        //
        // Implementation of the base class pure functions
        //
        // Returns true if request is allowed
        virtual
        bool        check_security  ( const std::string &           p_module,
                                      masnae::Fastcpipp_Path &      p_document,
                                      masnae::ProcessingResponse &  p_result );
        //
        // Returns true if the module/document are found (p_result must be filled then)
        virtual                      
        bool        process_request ( const std::string &           p_module,
                                      masnae::Fastcpipp_Path &      p_document,
                                      masnae::ProcessingResponse &  p_result );
        //
        // Returns information
        // Must return a valid JSON string representing a non-empty object
        virtual
        std::string specific_info   ( void );
        //
        // If invoked, check_security will not call Uriana but use
        // a default built-in context
        void        security_disable_uriana ( void )  { m_security_simulate_uriana = true; }
        //
    private:
        //
        // Check that the environment data has only UTF-8 strings
        bool is_utf8_safe   ( const std::string &                    p_document,
                              const masnae::Fastcpipp_Environment &  p_environment,
                              masnae::ProcessingResponse &           p_result );
        //
        // Function called by process_request...                
        //                                                               
        // ... the Script service                                        
        void process_script_request ( const masnae::Fastcpipp_Path &  p_document,
                                      masnae::ProcessingResponse &    p_result );
        //
        // Security
        bool            m_security_simulate_uriana = false;  // simulate Uriana call when checking security
        std::string     m_security_json;                     // same as RequestHandler::m_security but in JSON format
};

} // namespace thoe

#endif
