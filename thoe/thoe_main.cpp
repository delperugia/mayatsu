// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

// Copyright 2018 Pierre del Perugia
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <curl/curl.h>
#include <fastcgi++/manager.hpp>
#include <iostream>
#include <libxml/xmlversion.h>
#include <mariadb/mariadb_version.h>
#include <rapidjson/rapidjson.h>
#include <unistd.h>
#include <adawat/adawat_common.h>
#include <adawat/adawat_crashhandler.h>

#include "../mayatsu_version.h"
#include "masnae_bus.h"
#include "masnae_config.h"
#include "masnae_manager.hpp"
#include "masnae_misc.h"
#include "masnae_global.h"
#include "masnae_simulator.h"
#include "masnae_ws_tables.h"
#include "thoe_common.h"
#include "thoe_misc.h"
#include "thoe_script_executor.h"
#include "thoe_script_loader.h"
#include "thoe_validate.h"

using namespace std::string_literals;

//-------------------------------------------------------------------------
// Global variables
bool                            s_RunConsole    = false;
bool                            s_RunSelfTests  = false;
bool                            s_Succeeded     = false;
std::string                     s_ConfigFile;
std::string                     s_SelfTestUrl;
//
masnae::bus::Publisher          g_Bus;
masnae::config::Handler         g_ConfigHandler;
masnae::logger::Handler         g_LoggerHandler;
masnae::mysql::Handler          g_MySQLHandler;
masnae::ws_tables::Handler      g_WsTablesHandler;
masnae::ws_caches::Handler      g_WsCachesHandler;
masnae::http::Handler           g_HttpHandler;
masnae::request::Manager<thoe::JsRequestHandler>
                                g_Manager( thoe::constants::k_threads_per_cpu );
thoe::ScriptLoader              g_ScriptLoader;
thoe::ScriptExecutor            g_ScriptExecutor;

//-------------------------------------------------------------------------
// Parse command line, returns false if program must exit
bool parse_command_line ( int argc, char * argv [] )
{
    int c;
    //
    while ( (c = getopt( argc, argv, "ct:vf:h")) != -1 ) {
        switch (c) {
            case 'c':   s_RunConsole = true;
                        break;
            case 't':   s_SelfTestUrl  = optarg;
                        s_RunSelfTests = true;
                        break;
            case 'v':   printf( "Adawat:       %s\n",    adawat::k_adawat_version );
                        printf( "FastCGI++:    %s\n",    Fastcgipp::version );
                        printf( "Mayatsu:      %s\n",    mayatsu::k_version );
                        printf( "cURL:         %s\n",    curl_version() );
                        printf( "MariaDb:      %s\n",    MARIADB_PACKAGE_VERSION );
                        printf( "RapidJSON :   %s\n",    RAPIDJSON_VERSION_STRING );
                        printf( "SpiderMonkey: %u.%u\n", MOZJS_MAJOR_VERSION, MOZJS_MINOR_VERSION );
                        printf( "libxml2:      %s\n",    LIBXML_DOTTED_VERSION );
                        return false;
            case 'f':   s_ConfigFile = optarg;
                        break;
            case 'h':
            default:    printf( "-c console\n" );
                        printf( "-v version\n" );
                        printf( "-f <config>\n" );
                        printf( "-t <test_url>\n" );
                        return false;
        }
    }
    //
    return true;
}

//-------------------------------------------------------------------------
int main ( int argc, char * argv [] )
{
    //------------------------
    if ( ! adawat::crashhandler_setup() )
        return EXIT_FAILURE;
    //
    thoe::xml2json::init();  // may also exit() if .so is not compatible
    //
    //------------------------
    masnae::globals::init( "Thoe",
                           & g_Bus,
                           & g_ConfigHandler,
                           & g_LoggerHandler,
                           & g_MySQLHandler,
                           & g_WsTablesHandler,
                           & g_WsCachesHandler,
                           & g_HttpHandler,
                           & g_Manager );
    //
    //------------------------
    // The web server should define something like:
    //    "THOE_CONFIG" => "/var/www/cgi-config/thoe.json"
    const char * config_file = getenv( thoe::constants::k_env_var );
    if ( config_file == NULL )
        s_ConfigFile = thoe::constants::k_default_config_file;
    else
        s_ConfigFile = config_file;
    //
    //------------------------
    if ( ! parse_command_line( argc, argv ) )  // returns false if program must exit
        return EXIT_SUCCESS;
    //
    //------------------------
    masnae::service_info_log( "Thoe starting, "s + mayatsu::k_version );
    //
    //------------------------
    // Start all modules
    if ( g_ConfigHandler.start( s_ConfigFile )  &&
         g_LoggerHandler.start( thoe::constants::k_default_log_folder ) &&
         g_MySQLHandler.start()                 &&
         g_WsTablesHandler.start()              &&
         g_WsCachesHandler.start()              &&
         g_HttpHandler.start()                  &&
         g_ScriptLoader.start()                 &&
         g_ScriptExecutor.start()               &&
         g_Manager.init() )
    {
        // Execute as a console or as FastCGI
        if ( s_RunConsole ) {
            s_Succeeded = true;
            //
            std::thread t1( thoe::console );
            t1.join();
        } else if ( s_RunSelfTests ) {
            std::thread t1( thoe::validate, s_SelfTestUrl, & s_Succeeded );
            t1.join();
        } else {
            if ( g_Manager.run() )  // wait for script_loader stop() or http server stop
                s_Succeeded = true;
        }
    }
    //
    //------------------------
    // Stop modules
    g_ScriptExecutor.stop();
    g_ScriptLoader.stop();
    g_HttpHandler.stop();
    g_WsCachesHandler.stop();
    g_WsTablesHandler.stop();
    g_MySQLHandler.stop();
    g_LoggerHandler.stop();
    g_ConfigHandler.stop();
    //
    //------------------------
    return s_Succeeded ? EXIT_SUCCESS : EXIT_FAILURE;
}
