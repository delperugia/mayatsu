// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

// Copyright 2018 Pierre del Perugia
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "masnae_mysql.h"
#include "masnae_global.h"
#include "masnae_misc.h"
#include "thoe_common.h"
#include "thoe_javascript.h"
#include "thoe_script_executor.h"
#include "thoe_js_tools.h"

namespace thoe {

#pragma GCC diagnostic ignored "-Wmissing-field-initializers"

//-------------------------------------------------------------------------
// Amount class declaration
static
bool js_mysql_constructor( JSContext * p_context, unsigned p_argc, JS::Value * p_argv );

static
void js_mysql_finalize( JSFreeOp * p_free_op, JSObject * p_object );

static
JSClassOps js_mysql_class_ops = {                      
    nullptr,                // addProperty
    nullptr,                // delProperty
#if MOZJS_MAJOR_VERSION < 60
    nullptr,                // getProperty
    nullptr,                // setProperty
#endif
    nullptr,                // enumerate
#if MOZJS_MAJOR_VERSION >= 60
    nullptr,                // newEnumerate
#endif
    nullptr,                // resolve
    nullptr,                // mayResolve
    js_mysql_finalize,      // finalize
    nullptr,                // call
    nullptr,                // hasInstance
    js_mysql_constructor,   // construct
    nullptr                 // trace
};

static
JSClass js_mysql_class = {
    "MySQL",
    JSCLASS_HAS_PRIVATE | JSCLASS_FOREGROUND_FINALIZE,
    & js_mysql_class_ops
};

//-------------------------------------------------------------------------
// Constructor:
//  let db = new MySQL();
//  let db = new MySQL( true );  // writer requested
static
bool js_mysql_constructor( JSContext * p_context, unsigned p_argc, JS::Value * p_argv )
{
    JS::CallArgs args = JS::CallArgsFromVp( p_argc, p_argv );
    //
    // The constructor can eventually be called with a writer request
    if ( args.length() != 0 )
        if ( ! js_tools_check_argument( p_context, "MySQL.constructor", args, "B" ) )
            return false;
    //
    // Build the object
    JSObject * object = JS_NewObjectForConstructor( p_context, & js_mysql_class, args );
    if ( object == nullptr ) {
        JS_ReportOutOfMemory( p_context );
        return false;
    }
    //
    // By default request a reader
    masnae::mysql::access_type access = masnae::mysql::reader;
    //
    if ( args.length() > 0 )
        if ( args[0].toBoolean() )
            access = masnae::mysql::writer;
    //
    // Allocate a connection (might be null)
    auto connection = masnae::globals::mysql_handler->get_connection_ptr( access );
    //
    JS_SetPrivate( object, connection.release() );
    //
    // Return the new object
    args.rval().setObject( * object );
    return true;
}

//-------------------------------------------------------------------------
// Note that finalize is called twice, 2nd time private is nullptr
static
void js_mysql_finalize( JSFreeOp * p_free_op, JSObject * p_object )
{
    auto connection = static_cast< masnae::mysql::Connection* >( JS_GetPrivate( p_object ) );
    //
    if ( connection != nullptr )
        delete connection;
}

//-------------------------------------------------------------------------
// Execute an insert query
// Returns an object with the result and the new id
static
bool js_mysql_do_insert( JSContext * p_context, unsigned p_argc, JS::Value * p_argv )
{
    JS::CallArgs args  = JS::CallArgsFromVp( p_argc, p_argv );
    //
    if ( ! args.thisv().isObject() || 
         JS_GetClass( & args.thisv().toObject() ) != & js_mysql_class )
    {
        JS_ReportErrorUTF8( p_context, "MySQL.do_insert: called on non-object" );
        return false;
    }
    //
    //-------
    if ( ! js_tools_check_argument( p_context, "MySQL.do_insert", args, "S" ) )
        return false;
    //
    std::string query = js_tools_convert( p_context, args[0].toString() );
    //
    // Retrieve the connection
    auto * connection = static_cast< masnae::mysql::Connection* >( JS_GetPrivate( & args.thisv().toObject() ) );
    //
    unsigned     result;
    std::string  id;
    //
    // Execute the query
    if ( connection == nullptr )
        result = MSN_MYSQL_ERR_NO_HANDLE;
    else
        result = connection->do_insert( query, id );
    //
    // Build the result object
    std::string json = "{ \"result\": " + std::to_string( result )        +
                       " ,\"id\": "     + masnae::json_quote_string( id ) +
                       "}";
    //
    // Build a JavaSCript object
    JS::ConstUTF8CharsZ  json_utf8  ( json.c_str(),
                                      json.length() );  // no string copy here
    JS::RootedString     json_string( p_context,
                                      JS_NewStringCopyUTF8Z( p_context,
                                                             json_utf8 ) );
    if ( ! json_string ) {
        JS_ReportErrorUTF8( p_context, "MySQL.do_insert: failed to convert response to Unicode" );
        return false;
    }
    //
    JS::RootedValue rval( p_context );
    //
    if ( ! JS_ParseJSON( p_context, json_string, & rval) )
        return false;
    //
    // Return result
    args.rval().setObject( rval.toObject() );
    return true;
}

//-------------------------------------------------------------------------
// Execute a update query
// Returns an object with the result and the number of affected rows
static
bool js_mysql_do_update( JSContext * p_context, unsigned p_argc, JS::Value * p_argv )
{
    JS::CallArgs args  = JS::CallArgsFromVp( p_argc, p_argv );
    //
    if ( ! args.thisv().isObject() || 
         JS_GetClass( & args.thisv().toObject() ) != & js_mysql_class )
    {
        JS_ReportErrorUTF8( p_context, "MySQL.do_update: called on non-object" );
        return false;
    }
    //
    //-------
    if ( ! js_tools_check_argument( p_context, "MySQL.do_update", args, "S" ) )
        return false;
    //
    std::string query = js_tools_convert( p_context, args[0].toString() );
    //
    // Retrieve the connection
    auto * connection = static_cast< masnae::mysql::Connection* >( JS_GetPrivate( & args.thisv().toObject() ) );
    //
    unsigned  result;
    size_t    affected = 0;
    //
    // Execute the query
    if ( connection == nullptr )
        result = MSN_MYSQL_ERR_NO_HANDLE;
    else
        result = connection->do_update( query, affected );
    //
    // Build the result object
    std::string json = "{ \"result\": "    + std::to_string( result   ) +
                       " ,\"affected\": "  + std::to_string( affected ) +
                       "}";
    //
    // Build a JavaSCript object
    JS::ConstUTF8CharsZ  json_utf8  ( json.c_str(),
                                      json.length() );  // no string copy here
    JS::RootedString     json_string( p_context,
                                      JS_NewStringCopyUTF8Z( p_context,
                                                             json_utf8 ) );
    if ( ! json_string ) {
        JS_ReportErrorUTF8( p_context, "MySQL.do_update: failed to convert response to Unicode" );
        return false;
    }
    //
    JS::RootedValue rval( p_context );
    //
    if ( ! JS_ParseJSON( p_context, json_string, & rval) )
        return false;
    //
    // Return result
    args.rval().setObject( rval.toObject() );
    return true;
}

//-------------------------------------------------------------------------
// Execute a select query
// Returns an object with the result and the retrieved rows
static
bool js_mysql_do_select( JSContext * p_context, unsigned p_argc, JS::Value * p_argv )
{
    JS::CallArgs args  = JS::CallArgsFromVp( p_argc, p_argv );
    //
    if ( ! args.thisv().isObject() || 
         JS_GetClass( & args.thisv().toObject() ) != & js_mysql_class )
    {
        JS_ReportErrorUTF8( p_context, "MySQL.do_select: called on non-object" );
        return false;
    }
    //
    //-------
    if ( ! js_tools_check_argument( p_context, "MySQL.do_select", args, "S" ) )
        return false;
    //
    std::string query = js_tools_convert( p_context, args[0].toString() );
    //
    // Retrieve the connection
    auto * connection = static_cast< masnae::mysql::Connection* >( JS_GetPrivate( & args.thisv().toObject() ) );
    //
    unsigned                     result;
    masnae::SingleKeyValue_List  rows;
    //
    // Execute the query
    if ( connection == nullptr )
        result = MSN_MYSQL_ERR_NO_HANDLE;
    else
        result = connection->do_select( query, rows );
    //
    // Build the result object
    std::string json = "{ \"result\": " + std::to_string( result )     +
                       " ,\"data\": "   + masnae::json_convert( rows ) +
                       "}";
    //
    // Build a JavaSCript object
    JS::ConstUTF8CharsZ  json_utf8  ( json.c_str(),
                                      json.length() );  // no string copy here
    JS::RootedString     json_string( p_context,
                                      JS_NewStringCopyUTF8Z( p_context,
                                                             json_utf8 ) );
    if ( ! json_string ) {
        JS_ReportErrorUTF8( p_context, "MySQL.do_select: failed to convert response to Unicode" );
        return false;
    }
    //
    JS::RootedValue rval( p_context );
    //
    if ( ! JS_ParseJSON( p_context, json_string, & rval) )
        return false;
    //
    // Return result
    args.rval().setObject( rval.toObject() );
    return true;
}

//-------------------------------------------------------------------------
// Execute a delete query
// Returns an object with the result and the number of affected rows
static
bool js_mysql_do_delete( JSContext * p_context, unsigned p_argc, JS::Value * p_argv )
{
    JS::CallArgs args  = JS::CallArgsFromVp( p_argc, p_argv );
    //
    if ( ! args.thisv().isObject() || 
         JS_GetClass( & args.thisv().toObject() ) != & js_mysql_class )
    {
        JS_ReportErrorUTF8( p_context, "MySQL.do_delete: called on non-object" );
        return false;
    }
    //
    //-------
    if ( ! js_tools_check_argument( p_context, "MySQL.do_delete", args, "S" ) )
        return false;
    //
    std::string query = js_tools_convert( p_context, args[0].toString() );
    //
    // Retrieve the connection
    auto * connection = static_cast< masnae::mysql::Connection* >( JS_GetPrivate( & args.thisv().toObject() ) );
    //
    unsigned  result;
    size_t    affected = 0;
    //
    // Execute the query
    if ( connection == nullptr )
        result = MSN_MYSQL_ERR_NO_HANDLE;
    else
        result = connection->do_delete( query, affected );
    //
    // Build the result object
    std::string json = "{ \"result\": "    + std::to_string( result   ) +
                       " ,\"affected\": "  + std::to_string( affected ) +
                       "}";
    //
    // Build a JavaSCript object
    JS::ConstUTF8CharsZ  json_utf8  ( json.c_str(),
                                      json.length() );  // no string copy here
    JS::RootedString     json_string( p_context,
                                      JS_NewStringCopyUTF8Z( p_context,
                                                             json_utf8 ) );
    if ( ! json_string ) {
        JS_ReportErrorUTF8( p_context, "MySQL.do_delete: failed to convert response to Unicode" );
        return false;
    }
    //
    JS::RootedValue rval( p_context );
    //
    if ( ! JS_ParseJSON( p_context, json_string, & rval) )
        return false;
    //
    // Return result
    args.rval().setObject( rval.toObject() );
    return true;
}

//-------------------------------------------------------------------------
// Release the database connection
static
bool js_mysql_release( JSContext * p_context, unsigned p_argc, JS::Value * p_argv )
{
    JS::CallArgs args  = JS::CallArgsFromVp( p_argc, p_argv );
    //
    if ( ! args.thisv().isObject() || 
         JS_GetClass( & args.thisv().toObject() ) != & js_mysql_class )
    {
        JS_ReportErrorUTF8( p_context, "MySQL.release: called on non-object" );
        return false;
    }
    //
    // Retrieve the connection
    auto * connection = static_cast< masnae::mysql::Connection* >( JS_GetPrivate( & args.thisv().toObject() ) );
    //
    if ( connection != nullptr ) {
        delete connection;
        JS_SetPrivate( & args.thisv().toObject(), nullptr );
    }
    //
    return true;
}

//-------------------------------------------------------------------------
// Escape special characters of a string to be inserted in an SQL statement
static
bool js_mysql_sql_escape( JSContext * p_context, unsigned p_argc, JS::Value * p_argv )
{
    JS::CallArgs args  = JS::CallArgsFromVp( p_argc, p_argv );
    //
    //-------
    if ( ! js_tools_check_argument( p_context, "MySQL.sql_escape", args, "S" ) )
        return false;
    //
    std::string escaped =
        masnae::mysql::sql_escape(
            js_tools_convert( p_context, args[0].toString() )
	);
    //
    //------------------------
    JS::RootedString string( p_context,
                             JS_NewStringCopyZ( p_context,
                                                escaped.c_str() ) );
    //
    args.rval().setString( string );
    return true;
}

//-------------------------------------------------------------------------
static
const JSPropertySpec js_mysql_properties[] = {
    //
    JS_PS_END
};

static
const JSFunctionSpec js_mysql_methods[] = 
{
    //     name              function                   nargs flags
    JS_FN("do_insert",       js_mysql_do_insert,        1,    0),
    JS_FN("do_update",       js_mysql_do_update,        1,    0),
    JS_FN("do_select",       js_mysql_do_select,        1,    0),
    JS_FN("do_delete",       js_mysql_do_delete,        1,    0),
    JS_FN("release",         js_mysql_release,          0,    0),
    JS_FN("sql_escape",      js_mysql_sql_escape,       1,    0),
    //
    JS_FS_END,
};

//-------------------------------------------------------------------------
// Define the MySQL class in the global object
// Called only once by ScriptExecutor::execute_script if global
// is reused (m_optz_clear_global = false).
bool js_add_mysql_class( JSContext *       p_context,
                         JS::HandleObject  p_global )
{
    if ( ! JS_InitClass( p_context,
                         p_global,
                         nullptr,               // parent_proto
                         & js_mysql_class,
                         js_mysql_constructor,
                         0,                     // nb constructor args
                         js_mysql_properties,
                         js_mysql_methods,
                         js_mysql_properties,   // static_ps
                         nullptr ) )            // static_fs
    {
        js_service_error_log( "JavaScript: Failed to define MySQL class", nullptr );
        return false;
    }
    //
    return true;
}

} // namespace thoe
