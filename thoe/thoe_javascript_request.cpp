// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

// Copyright 2018 Pierre del Perugia
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "masnae_common.h"
#include "masnae_misc.h"
#include "thoe_mozjs.h"
#include "thoe_common.h"
#include "thoe_javascript.h"
#include "thoe_script_executor.h"
#include "thoe_js_tools.h"

namespace thoe {

//-------------------------------------------------------------------------
// Add a header to the response
// Syntax: respond_header( header:string, value:string )
static
bool js_request_respond_header( JSContext * p_context, unsigned p_argc, JS::Value * p_argv )
{
    ThreadFrame * frame = static_cast< ThreadFrame* >( JS_GetContextPrivate( p_context ) );
    JS::CallArgs  args  = JS::CallArgsFromVp( p_argc, p_argv );
    //
    if ( ! js_tools_check_argument( p_context, "respond_header", args, "SS" ) )
        return false;
    //
    //------------------------
    frame->m_response->add_http_header( js_tools_convert( p_context, args[0].toString() ),
                                        js_tools_convert( p_context, args[1].toString() ) );
    //
    return true;
}

//-------------------------------------------------------------------------
// Set the HTTP response status
// Syntax: respond_http_status( status:string )
// e.g.: respond_http_status( '204 No Content' );
static
bool js_request_respond_http_status( JSContext * p_context, unsigned p_argc, JS::Value * p_argv )
{
    ThreadFrame * frame = static_cast< ThreadFrame* >( JS_GetContextPrivate( p_context ) );
    JS::CallArgs  args  = JS::CallArgsFromVp( p_argc, p_argv );
    //
    if ( ! js_tools_check_argument( p_context, "respond_http_status", args, "S" ) )
        return false;
    //
    //------------------------
    frame->m_response->set_http_status( js_tools_convert( p_context, args[0].toString() ) );
    //
    return true;
}

//-------------------------------------------------------------------------
// Send a successful result
// Syntax: respond_success( data:object  )
static
bool js_request_respond_success( JSContext * p_context, unsigned p_argc, JS::Value * p_argv )
{
    ThreadFrame * frame = static_cast< ThreadFrame* >( JS_GetContextPrivate( p_context ) );
    JS::CallArgs  args  = JS::CallArgsFromVp( p_argc, p_argv );
    //
    if ( ! js_tools_check_argument( p_context, "respond_success", args, "o" ) )
        return false;
    //
    //------------------------
    std::string json;
    if ( ! js_tools_stringify( p_context, args[0], json ) )
        return false;
    //
    frame->m_response->set_jresp_success( std::move( json ) );
    //
    return true;
}

//-------------------------------------------------------------------------
// Send an error result
// Syntax: respond_error( message:string )
static
bool js_request_respond_error( JSContext * p_context, unsigned p_argc, JS::Value * p_argv )
{
    ThreadFrame * frame = static_cast< ThreadFrame* >( JS_GetContextPrivate( p_context ) );
    JS::CallArgs  args  = JS::CallArgsFromVp( p_argc, p_argv );
    //
    if ( ! js_tools_check_argument( p_context, "respond_error", args, "S" ) )
        return false;
    //
    //------------------------
    frame->m_response->set_jresp_error ( js_tools_convert( p_context,
                                                           args[0].toString() ) );
    //
    return true;
}

//-------------------------------------------------------------------------
// Send a failure result
// Syntax: respond_failed()
static
bool js_request_respond_failed( JSContext * p_context, unsigned p_argc, JS::Value * p_argv )
{
    ThreadFrame * frame = static_cast< ThreadFrame* >( JS_GetContextPrivate( p_context ) );
    JS::CallArgs  args  = JS::CallArgsFromVp( p_argc, p_argv );
    //
    if ( ! js_tools_check_argument( p_context, "respond_failed", args, "" ) )
        return false;
    //
    //------------------------
    frame->m_response->set_jresp_failed();
    //
    return true;
}

//-------------------------------------------------------------------------
// Send a successful result
// Syntax: respond_rejected( reason:string, data:object  )
static
bool js_request_respond_rejected( JSContext * p_context, unsigned p_argc, JS::Value * p_argv )
{
    ThreadFrame * frame = static_cast< ThreadFrame* >( JS_GetContextPrivate( p_context ) );
    JS::CallArgs  args  = JS::CallArgsFromVp( p_argc, p_argv );
    //
    if ( ! js_tools_check_argument( p_context, "respond_rejected", args, "So" ) )
        return false;
    //
    //------------------------
    std::string json;
    if ( ! js_tools_stringify( p_context, args[1], json ) )
        return false;
    //
    frame->m_response->set_jresp_rejected ( js_tools_convert( p_context,
                                                              args[0].toString() ),
                                            json );
    //
    return true;
}

//-------------------------------------------------------------------------
// Send a successful result
// Syntax: respond_continue( data:object  )
static
bool js_request_respond_continue( JSContext * p_context, unsigned p_argc, JS::Value * p_argv )
{
    ThreadFrame * frame = static_cast< ThreadFrame* >( JS_GetContextPrivate( p_context ) );
    JS::CallArgs  args  = JS::CallArgsFromVp( p_argc, p_argv );
    //
    if ( ! js_tools_check_argument( p_context, "respond_continue", args, "O" ) )
        return false;
    //
    //------------------------
    std::string json;
    if ( ! js_tools_stringify( p_context, args[0], json ) )
        return false;
    //
    frame->m_response->set_jresp_continue( json );
    //
    return true;
}

//-------------------------------------------------------------------------
// Set the request result as a raw answer
// Syntax: respond_raw( content_type:string, data:Blob )
// e.g.: respond_raw( 'text/plain', b );
static
bool js_request_respond_raw( JSContext * p_context, unsigned p_argc, JS::Value * p_argv )
{
    ThreadFrame * frame = static_cast< ThreadFrame* >( JS_GetContextPrivate( p_context ) );
    JS::CallArgs  args  = JS::CallArgsFromVp( p_argc, p_argv );
    //
    if ( ! js_tools_check_argument( p_context, "respond_raw", args, "SO" ) )
        return false;
    //
    if ( ! js_is_blob( args[1] ) ) {
        JS_ReportErrorUTF8( p_context, "respond_raw: object is not a Blob" );
        return false;
    }
    //
    // Remove the data from the Blob
    masnae::Data data;
    //
    js_get_blob_data( args[1], data, true );  // move it since it is the last instruction
    //
    // Fill the response
    frame->m_response->set_raw   ( std::move( data ) );
    frame->m_response->add_http_header( "Content-Type",
                                        js_tools_convert( p_context, args[0].toString() ) );
    //
    return true;
}

//-------------------------------------------------------------------------
// Returns all received headers
static
bool js_get_all_headers( JSContext * p_context, unsigned p_argc, JS::Value * p_argv )
{
    ThreadFrame * frame = static_cast< ThreadFrame* >( JS_GetContextPrivate( p_context ) );
    JS::CallArgs  args  = JS::CallArgsFromVp( p_argc, p_argv );
    //
    std::string headers_json = masnae::json_convert( frame->m_environment->others );
    //
    // Build a JavaSCript object
    JS::ConstUTF8CharsZ  headers_utf8  ( headers_json.c_str(),
                                         headers_json.length() );  // no string copy here
    JS::RootedString     headers_string( p_context,
                                         JS_NewStringCopyUTF8Z( p_context,
                                                                headers_utf8 ) );
    if ( ! headers_string ) {
        JS_ReportErrorUTF8( p_context, "get_all_headers: failed to convert response to Unicode" );
        return false;
    }
    //
    JS::RootedValue rval( p_context );
    //
    if ( ! JS_ParseJSON( p_context, headers_string, & rval) )
        return false;
    //
    // Return result
    args.rval().setObject( rval.toObject() );
    //
    return true;
}

//-------------------------------------------------------------------------
// The following namespace contains convenient functions called by
// js_add_request_object. They throw a string on error.
namespace build {

    //-------------------------------------------------------------------------
    // Add properties to Request
    // Note that p_document (remaining of the received RequestURI) is passed
    // to the script as the RequestUri:
    //
    //   "remote_address": "::ffff:51.15.89.151",
    //   "remote_port": "57558",
    //   "script_name": "/v1/thoe",
    //   "request_method": "GET",
    //   "request_uri": "/1",
    //
    void add_properties ( JSContext *                            p_context,
                          const std::string &                    p_document,
                          const masnae::Fastcpipp_Environment &  p_environment,
                          JS::RootedObject &                     p_request_object )
    {
        bool ok = js_tools_add_properties(
            p_context,
            p_request_object,
            { { "remote_address", masnae::convert_address( p_environment.remoteAddress ) },
    //        { "remote_port",    std::to_string( p_environment.remotePort )             },  futur: if ever needed
    //        { "script_name",    p_environment.scriptName                               },  futur: if ever needed
              { "request_method", masnae::convert_method( p_environment.requestMethod )  },
              { "request_uri",    p_document                                             } }
        );
        //
        if ( ! ok )
           throw std::runtime_error( "failed to add Request's properties" );
    }
    //
    //-------------------------------------------------------------------------
    // Add security property to Request
    void add_security ( JSContext *                            p_context,
                        const std::string &                    p_security_json,
                        JS::RootedObject &                     p_request_object )
    {
        //-----
        JS::ConstUTF8CharsZ  jresp_utf8  ( p_security_json.c_str(),
                                           p_security_json.length() );  // no string copy here
        JS::RootedString     jresp_string( p_context,
                                           JS_NewStringCopyUTF8Z( p_context,
                                                                  jresp_utf8 ) );
        if ( ! jresp_string )
            throw std::runtime_error( "failed to convert Request's security property" );
        //
        //-----
        JS::RootedValue rval( p_context );
        //
        if ( ! JS_ParseJSON( p_context, jresp_string, & rval) )
            throw std::runtime_error( "failed to parse Request's security property" );
        //
        if ( ! JS_DefineProperty( p_context,
                                  p_request_object,
                                  "security",
                                  rval, // HandleValue , HandleObject 
                                  JSPROP_ENUMERATE|JSPROP_READONLY|JSPROP_PERMANENT ) )
           throw std::runtime_error( "failed to add Request's security property" );
    }

    //-------------------------------------------------------------------------
    // Add methods to Request
    void add_methods ( JSContext *         p_context,
                       JS::RootedObject &  p_request_object )
    {
        static const JSFunctionSpec request_methods[] = {
           //     name                  function                         nargs flags
           JS_FN("respond_header",      js_request_respond_header,       2,    0),
           JS_FN("respond_http_status", js_request_respond_http_status,  1,    0),
           JS_FN("respond_success",     js_request_respond_success,      1,    0),
           JS_FN("respond_error",       js_request_respond_error,        1,    0),
           JS_FN("respond_failed",      js_request_respond_failed,       0,    0),
           JS_FN("respond_rejected",    js_request_respond_rejected,     2,    0),
           JS_FN("respond_continue",    js_request_respond_continue,     1,    0),
           JS_FN("respond_raw",         js_request_respond_raw,          2,    0),
           //
           JS_FN("get_all_headers",     js_get_all_headers,              0,    0),
           //
           JS_FS_END
        };
        //
        if ( ! JS_DefineFunctions( p_context,
                                   p_request_object,
                                   request_methods ) )
            throw std::runtime_error( "Failed to define Request's methods" );
    }

    //-------------------------------------------------------------------------
    // Create an object in Request to hold received parameters
    void add_parameters ( JSContext *                            p_context,
                          const masnae::Fastcpipp_Environment &  p_environment,
                          JS::RootedObject &                     p_request_object )
    {
        // Create the property holding the parameters
        JS::RootedObject property( p_context,
                                   JS_DefineObject( p_context,
                                                    p_request_object,
                                                    "params",
                                                    nullptr,
                                                    JSPROP_ENUMERATE|JSPROP_READONLY|JSPROP_PERMANENT ));
        //
        if ( property == nullptr )
            throw std::runtime_error( "Failed to allocated Request's params" );
        //
        // Merge post and get parameters
        auto params = masnae::merge_parameters( p_environment.posts, p_environment.gets );
        //
        // Add parameters to the property
        for ( const auto & param: params ) {
            bool added;
            //
            // Because we use $ suffix to indicate an array, $ are prohibited
            if ( param.first.empty() || param.first.back() == '$' )
                throw std::runtime_error( "Invalid parameter name" );
            //
            if ( param.second.size() > 1 )  // param set several times: create an JavaScript array
                added = js_tools_add_property( p_context,
                                               property,
                                               (param.first + '$').c_str(), // suffixed by $
                                               param.second );              // as an array of strings
            else
                added = js_tools_add_property( p_context,
                                               property,
                                               param.first.c_str(),
                                               param.second.front() );      // as a string
            //
            if ( ! added )
                throw std::runtime_error( "failed to add Request's params properties" );
        }
    }

    //-------------------------------------------------------------------------
    // Create an object in Request to hold received headers
    // (FastCGI only have some of them available)
    //
    //      "authorization": "",
    //      "accept": "*/*",
    //      "accept_charset": "",
    //      "accept_languages": ["en-GB"]
    //      "referer": ""
    //      "content_type": "",
    //      "etag": "0",
    //      "if_modified_since": "1994-10-29T19:43:31.000Z"     created as a JavaScript Date object
    //
    void add_headers ( JSContext *                            p_context,
                       const masnae::Fastcpipp_Environment &  p_environment,
                       JS::RootedObject &                     p_request_object )
    {
        // Create the property holding the headers
        JS::RootedObject property( p_context,
                                   JS_DefineObject( p_context,
                                                    p_request_object,
                                                    "headers",
                                                    nullptr,
                                                    JSPROP_ENUMERATE|JSPROP_READONLY|JSPROP_PERMANENT ));
        //
        if ( property == nullptr )
            throw std::runtime_error( "Failed to allocated Request's headers" );
        //
        // Add regular headers to the property. Since most are empty most of
        // the time, only add those not empty
        masnae::SingleKey_Value  headers;
        //
        if ( ! p_environment.authorization.empty() )      headers.insert( std::pair( "authorization",  p_environment.authorization       ) );
        if ( ! p_environment.acceptContentTypes.empty() ) headers.insert( std::pair( "accept",         p_environment.acceptContentTypes  ) );
        if ( ! p_environment.acceptCharsets.empty() )     headers.insert( std::pair( "accept_charset", p_environment.acceptCharsets       ) );
        if ( ! p_environment.referer.empty() )            headers.insert( std::pair( "referer",        p_environment.referer              ) );
        if ( ! p_environment.contentType.empty() )        headers.insert( std::pair( "content_type",   p_environment.contentType          ) );
        if (   p_environment.etag != 0 )                  headers.insert( std::pair( "etag",           std::to_string( p_environment.etag ) ) );
        //
        bool ok = js_tools_add_properties( p_context, property, headers );
        //
        // Accept-Languages
        if ( ! p_environment.acceptLanguages.empty() ) {
           ok &= js_tools_add_property( p_context,
                                        property,
                                        "accept_languages",
                                        p_environment.acceptLanguages );
        }
        //
        // If-Modified-Since
        if ( p_environment.ifModifiedSince != 0 ) {
           JS::RootedObject date( p_context,
                                  JS::NewDateObject( p_context,
                                                     JS::TimeClip( p_environment.ifModifiedSince * 1000.0 ) ) );
           //
           ok &= JS_DefineProperty( p_context,
                                    property,
                                    "if_modified_since",
                                    date,
                                    JSPROP_ENUMERATE|JSPROP_READONLY|JSPROP_PERMANENT );
        }
        //
        if ( ! ok)
            throw std::runtime_error( "failed to add Request's headers properties" );
    }

    //-------------------------------------------------------------------------
    // Create an array of objects in Request to hold received attachments
    void add_attachments ( JSContext *                            p_context,
                           JS::HandleObject                       p_global,
                           const masnae::Fastcpipp_Environment &  p_environment,
                           JS::RootedObject &                     p_request_object )
    {
        // Create an array...
        JS::AutoValueVector attachments( p_context );
        //
        for ( const auto & file: p_environment.files )
        {
            // ... with objects containing attachment details inside
            JS::RootedObject attachment( p_context,
                                         JS_NewObject( p_context, nullptr ));
            //
            // First: the basic string properties
            bool ok = js_tools_add_properties(
                p_context,
                attachment,
                { { "name",     file.first                          },
                  { "filename", file.second.filename                },
                  { "type",     file.second.contentType             },
                  { "size",     std::to_string( file.second.size )  } }
            );
            //
            // Then: the blob property
            JS::RootedObject blob_object( p_context,
                                          js_new_blob( p_context,
                                                       p_global,
                                                       reinterpret_cast< uint8_t * >( file.second.data.get() ),
                                                       file.second.size ) );
            //
            ok &= blob_object != nullptr &&  // object can be null if for some reason data copy failed
                  JS_DefineProperty( p_context,
                                     attachment,
                                     "blob",
                                     blob_object,
                                     JSPROP_ENUMERATE|JSPROP_READONLY|JSPROP_PERMANENT );
            //
            // Append to the array
            if ( ! ok || ! attachments.append( JS::ObjectValue( *attachment ) ) )
                throw std::runtime_error( "Failed to allocated Request's attachments item" );
        }
        //
        // Put the array in a new property of the global Request object
        JS::RootedObject object( p_context,
                                 JS_NewArrayObject( p_context, attachments ) );
        //
        if ( ! JS_DefineProperty( p_context,
                                  p_request_object,
                                  "attachments",
                                  object,
                                  JSPROP_ENUMERATE|JSPROP_READONLY|JSPROP_PERMANENT ) )
            throw std::runtime_error( "Failed to allocated Request's attachments" );
    }

} // namespace build

//-------------------------------------------------------------------------
// Define the request global object
//
// Note: params and header are not accessed through functions but are created
// as properties of the request object because it is faster:
//
//    @10M:  request.params.uid = 88ms,  request.param('uid') = 9521ms
//
// Note: request property is not permanent (and delete as first step) because
// in case global is re-used (m_optz_clear_global = false) we don't want to
// keep previous values
//
bool js_add_request_object( JSContext *                            p_context,
                            const std::string &                    p_document,
                            const masnae::Fastcpipp_Environment &  p_environment,
                            const std::string &                    p_security_json,
                            JS::HandleObject                       p_global )
{
    JS_DeleteProperty( p_context, p_global, "request" );
    //
    // Create an new object 'request', as a property of the Global object
    JS::RootedObject request_object( p_context,
                                     JS_DefineObject( p_context,
                                                      p_global,
                                                      "request",
                                                      nullptr,
                                                      JSPROP_ENUMERATE|JSPROP_READONLY/*|JSPROP_PERMANENT*/ ) );
    //
    if ( request_object == nullptr ) {
        js_service_error_log( "JavaScript: Failed to allocated request object", nullptr );
        return false;
    }
    //
    // Add the various parts in the request object
    // Note: cookies are not used
    try
    {
        build::add_properties ( p_context, p_document, p_environment, request_object );
        build::add_security   ( p_context, p_security_json, request_object );
        build::add_methods    ( p_context, request_object );
        build::add_parameters ( p_context, p_environment, request_object );
        build::add_headers    ( p_context, p_environment, request_object );
        build::add_attachments( p_context, p_global, p_environment, request_object );
    }
    catch ( const std::exception & e )
    {
        js_service_error_log( "JavaScript: Failed to configure request object: " + std::string( e.what() ),
                              nullptr );
        return false;
    }
    //
    return true;
}

} // namespace thoe
