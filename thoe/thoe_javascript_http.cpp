// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

// Copyright 2018 Pierre del Perugia
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <codecvt>
#include <locale>
#include <rapidjson/document.h>

#include "masnae_misc.h"
#include "masnae_global.h"
#include "thoe_mozjs.h"
#include "thoe_common.h"
#include "thoe_javascript.h"
#include "thoe_script_executor.h"
#include "thoe_js_tools.h"

namespace thoe {

//-------------------------------------------------------------------------
bool copy_attachments( JSContext *         p_context,
                       JS::HandleObject    p_object,
                       masnae::KeyData &   p_properties )
{
    // Retrieve the ids of all visible, owned, properties
    JS::AutoIdVector ids( p_context );
    //
    if ( ! js::GetPropertyKeys( p_context, p_object, JSITER_OWNONLY , & ids ) ) {
        JS_ReportErrorUTF8( p_context, "http: failed to get attachments properties" );
        return false;
    }
    //
    for ( size_t i = 0; i < ids.length(); i++ ) {
        JS::RootedValue k_value( p_context ), v_value( p_context );
        std::string     key;
        JS::RootedId    id( p_context, ids[i] );
        //
        // Get the key
        if ( ! JS_IdToValue( p_context, ids[i], & k_value) ) {
            JS_ReportErrorUTF8( p_context, "http: failed to get attachments property name" );
            return false;
        }
        if ( ! js_tools_convert( p_context, k_value, key ) ) // only simple type key are supported
            continue;
        //
        // Get the value
        if ( ! JS_GetPropertyById( p_context, p_object, id, & v_value ) ) {
            JS_ReportErrorUTF8( p_context, "http: failed to get attachments property value" );
            return false;
        }
        //
        if ( ! js_is_blob( v_value ) ) {
            JS_ReportErrorUTF8( p_context, "http: attachment is not a Blob object" );
            return false;
        }
        //
        // Move or copy the data from the Blob instance to the given Data
        masnae::Data data;
        js_get_blob_data( v_value, data, false );
        //
        p_properties.insert( std::pair( std::move( key ),
                                   std::move( data ) ) );
    }
    //
    return true;
}

//-------------------------------------------------------------------------
// Retrieve parameters (module, parameters [,headers, attachments])
// from received args
static
bool prepare_call_url( JSContext *               p_context,
                       JS::CallArgs &            p_args,
                       std::string &             p_module_name,
                       masnae::MultiKey_Value &  p_headers,
                       masnae::MultiKey_Value &  p_parameters,
                       masnae::KeyData &         p_attachments )
{   
    // Module
    p_module_name = js_tools_convert( p_context, p_args[0].toString() );
    //
    // Parameters
    JS::RootedObject  parameters_obj( p_context,
                                      & p_args[1].toObject() );
    //
    if ( ! js_tools_get_properties ( p_context, parameters_obj, p_parameters ) )
        return false;
    //
    // Headers
    if ( p_args.length() > 2 ) {  // headers are optional
        JS::RootedObject  headers_obj( p_context,
                                       & p_args[2].toObject() );
        //
        if ( ! js_tools_get_properties ( p_context, headers_obj, p_headers ) )
            return false;
    }
    //
    // Attachments
    if ( p_args.length() > 3 ) {  // attachments are optional
        JS::RootedObject  attachments_obj( p_context,
                                           & p_args[3].toObject() );
        //
        if ( ! copy_attachments( p_context, attachments_obj, p_attachments) )
            return false;
    }
    //
    return true;
}

//-------------------------------------------------------------------------
static
bool build_http_response( JSContext *          p_context,
                          JS::CallArgs &       p_args,
                          int                  p_result,
                          const std::string &  p_content_type,
                          masnae::Data &       p_body,
                          JS::RootedObject &   p_response )
{
    // First: add the string properties
    bool ok = js_tools_add_properties(
        p_context,
        p_response,
        { { "code",     std::to_string( p_result ) },
          { "type",     p_content_type             } }
    );
    //
    // Then: the blob property
    JS::RootedObject global( p_context,
                             JS_GetGlobalForObject( p_context,
                                                    & p_args.callee() ) );
    //
    JS::RootedObject blob_object( p_context,
                                  js_new_blob( p_context,
                                               global,
                                               reinterpret_cast< uint8_t* >( p_body.data() ),
                                               p_body.size() ) );
    //
    ok &= blob_object != nullptr &&  // object can be null if for some reason data copy failed
          JS_DefineProperty( p_context,
                             p_response,
                             "body",
                             blob_object,
                             JSPROP_ENUMERATE|JSPROP_READONLY|JSPROP_PERMANENT );
    //
    if ( ! ok ) {
        JS_ReportErrorUTF8( p_context, "http: failed to set response properties" );
        return false;
    }
    //
    return true;
}

//-------------------------------------------------------------------------
// Syntax:  http.raw( module_name, headers, blob );
// Returns: { code, type, blob }
// Example:
//  var result = http.raw( 'module', {'Content-Type':'text/plain'}, blob );
static
bool js_http_raw( JSContext *   p_context,
                  unsigned      p_argc,
                  JS::Value *   p_argv )
{
    JS::CallArgs  args  = JS::CallArgsFromVp( p_argc, p_argv );
    //
    if ( ! js_tools_check_argument( p_context, "http.raw", args, "SOO" ) )
        return false;
    //
    // Retrieve parameters...
    int                     result;
    std::string             module_name, received_content_type;
    masnae::MultiKey_Value  headers;
    masnae::Data            sent_body, received_body;
    //
    // ... module
    module_name = js_tools_convert( p_context, args[0].toString() );
    //
    // ... headers
    JS::RootedObject  headers_obj( p_context,
                                   & args[1].toObject() );
    if ( ! js_tools_get_properties ( p_context, headers_obj, headers ) )
        return false;
    //
    // ... parameters
    JS::RootedValue  blob_value( p_context,
                                 args[2] );
    if ( ! js_is_blob( blob_value ) ) {
        JS_ReportErrorUTF8( p_context, "http: blob parameter is not a Blob object" );
        return false;
    }
    //
    // Copy the data from the Blob instance to the given Data
    js_get_blob_data( blob_value, sent_body, false );
    //
    // Call the URL
    result = masnae::globals::http_handler->request2(
        module_name,
        headers, sent_body,
        received_content_type, received_body
    );
    //
    // Build response (an object)
    JS::RootedObject response( p_context,
                               JS_NewObject( p_context, nullptr ));
    //
    if ( ! build_http_response( p_context, args,
                                result, received_content_type, received_body,
                                response ) )
        return false;
    //
    // Return result
    args.rval().setObject( * response );
    //
    return true;
}

//-------------------------------------------------------------------------
// Syntax:  http.request( module_name, parameters[, headers[, attachments]] );
// Returns: { code, type, blob }
// Example:
//  var result = http.request( 'module', {a:'x',b:2}, {Range:'bytes=1-10'} );
static
bool js_http_request( JSContext * p_context, unsigned p_argc, JS::Value * p_argv )
{
    JS::CallArgs  args  = JS::CallArgsFromVp( p_argc, p_argv );
    //
    if ( ! js_tools_check_argument( p_context, "http.request", args, "SO/OO" ) )
        return false;
    //
    // Retrieve parameters
    std::string             module_name;
    masnae::MultiKey_Value  headers, parameters;
    masnae::KeyData         attachments;
    //
    if ( ! prepare_call_url( p_context, args,
                             module_name, headers, parameters, attachments ) )
        return false;
    //
    // Call the URL
    int           result;
    std::string   content_type;
    masnae::Data  body;
    //
    result = masnae::globals::http_handler->request1(
        module_name,
        headers, parameters, attachments,
        content_type, body
    );
    //
    // Build response (an object)
    JS::RootedObject response( p_context,
                               JS_NewObject( p_context, nullptr ));
    //
    if ( ! build_http_response( p_context, args,
                                result, content_type, body,
                                response ) )
        return false;
    //
    // Return result
    args.rval().setObject( * response );
    //
    return true;
}

//-------------------------------------------------------------------------
// Syntax:  http.jresp( module_name, parameters[, headers[, attachments]] );
// Returns: a JResp object
// Example:
//  var result = http.jresp( 'module', {a:'x',b:2} );
static
bool js_jresp_request( JSContext * p_context, unsigned p_argc, JS::Value * p_argv )
{
    JS::CallArgs  args  = JS::CallArgsFromVp( p_argc, p_argv );
    //
    if ( ! js_tools_check_argument( p_context, "http.jresp", args, "SO/OO" ) )
        return false;
    //
    // Retrieve parameters
    std::string             module_name;
    masnae::MultiKey_Value  headers, parameters;
    masnae::KeyData         attachments;
    //
    if ( ! prepare_call_url( p_context, args,
                             module_name, headers, parameters, attachments ) )
        return false;
    //
    // Call the URL
    rapidjson::Document response;       // not used
    std::string         response_json;  // guaranteed to be JResp in UTF-8 by request_jresp
    //
    masnae::globals::http_handler->request_jresp(
        module_name,
        headers, parameters, attachments,
        response, response_json
    );
    //
    // Build a JavaSCript object
    JS::ConstUTF8CharsZ  jresp_utf8  ( response_json.c_str(),
                                       response_json.length() );  // no string copy here
    JS::RootedString     jresp_string( p_context,
                                       JS_NewStringCopyUTF8Z( p_context,
                                                              jresp_utf8 ) );
    if ( ! jresp_string ) {
        JS_ReportErrorUTF8( p_context, "http.jresp: failed to convert response to Unicode" );
        return false;
    }
    //
    JS::RootedValue rval( p_context );
    //
    if ( ! JS_ParseJSON( p_context, jresp_string, & rval) )
        return false;
    //
    // Return result
    args.rval().setObject( rval.toObject() );
    //
    return true;
}

//-------------------------------------------------------------------------
// Syntax:  http.json( module_name, parameters[, headers[, attachments]] );
// Returns: a JResp object
// Example:
//  var result = http.json( 'module', {a:'x',b:2} );
static
bool js_json_request( JSContext * p_context, unsigned p_argc, JS::Value * p_argv )
{
    JS::CallArgs  args  = JS::CallArgsFromVp( p_argc, p_argv );
    //
    if ( ! js_tools_check_argument( p_context, "http.json", args, "SO/OO" ) )
        return false;
    //
    // Retrieve parameters
    std::string             module_name;
    masnae::MultiKey_Value  headers, parameters;
    masnae::KeyData         attachments;
    //
    if ( ! prepare_call_url( p_context, args,
                             module_name, headers, parameters, attachments ) )
        return false;
    //
    // Call the URL
    rapidjson::Document response;       // not used
    std::string         response_json;  // guaranteed to be JResp in UTF-8 by request_json
    //
    masnae::globals::http_handler->request_json(
        module_name,
        headers, parameters, attachments,
        response, response_json
    );
    //
    // Build a JavaSCript object
    JS::ConstUTF8CharsZ  jresp_utf8  ( response_json.c_str(),
                                       response_json.length() );  // no string copy here
    JS::RootedString     jresp_string( p_context,
                                       JS_NewStringCopyUTF8Z( p_context,
                                                              jresp_utf8 ) );
    if ( ! jresp_string ) {
        JS_ReportErrorUTF8( p_context, "http.json: failed to convert response to Unicode" );
        return false;
    }
    //
    JS::RootedValue rval( p_context );
    //
    if ( ! JS_ParseJSON( p_context, jresp_string, & rval) )
        return false;
    //
    // Return result
    args.rval().setObject( rval.toObject() );
    //
    return true;
}

//-------------------------------------------------------------------------
// Define the http object in the global object
// Called only once by ScriptExecutor::execute_script if global
// is reused (m_optz_clear_global = false).
bool js_add_http_object( JSContext *                            p_context,
                         const std::string &                    p_document,
                         const masnae::Fastcpipp_Environment &  p_environment,
                         JS::HandleObject                       p_global )
{
    // Create an new object 'http', as a property of the Global object
    JS::RootedObject http_object( p_context,
                                  JS_DefineObject( p_context,
                                                   p_global,
                                                   "http",
                                                   nullptr,
                                                   JSPROP_ENUMERATE|JSPROP_READONLY|JSPROP_PERMANENT ) );
    //
    if ( http_object == nullptr ) {
        js_service_error_log( "JavaScript: Failed to allocated http object", nullptr );
        return false;
    }
    //
    static const JSFunctionSpec functions[] = {
       //     name      function                      nargs flags
       JS_FN("raw",     js_http_raw,                    3,    0),
       JS_FN("request", js_http_request,                4,    0),
       JS_FN("jresp",   js_jresp_request,               4,    0),
       JS_FN("json",    js_json_request,                4,    0),
       JS_FS_END
    };
    //
    if ( ! JS_DefineFunctions( p_context,
                               http_object,
                               functions ) )
    {
        js_service_error_log( "JavaScript: Failed to define http's methods", nullptr );
        return false;
    }
    //
    return true;
}

} // namespace thoe
