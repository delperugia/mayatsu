// Copyright 2018 Pierre del Perugia
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

//
// Wrapper around SpiderMonkey: load and execute a script
// There is one global instance of this class
//

#ifndef thoe_script_executor_h
#define thoe_script_executor_h

#include <string>

#include "masnae_bus.h"
#include "masnae_request_handler.h"
#include "thoe_mozjs.h"
#include "thoe_script_loader.h"

namespace thoe {

//-------------------------------------------------------------------------
// Data used during the execution of a script (during JS::Evaluate).
// Allocated by thread, m_environment and m_response are set before
// calling Evaluate and are only guarantee during the script execution
// Note: PersistentRootedObject copy constructor is buggy: if used
// directly in map, and even if properly reset, Object is not release
// and next GC hangs
struct ThreadFrame
{
    // SpiderMonkey objects
    JSContext *                             m_context;           // JavaScript context, persistent
    JS::PersistentRootedObject *            m_global;            // JavaScript global object, persistent
    std::map< std::string , JS::PersistentRootedObject * >       
                                            m_loaded_modules;    // used by require to handle multiple dependencies
    std::map< std::string , JS::PersistentRootedScript * >       
                                            m_compiled_codes;    // JavaScript binaries of main and libraries scripts
    //                                                           
    // Data used by the script                                   
    const masnae::Fastcpipp_Environment *   m_environment;       // FastCgi++ environment
    bool                                    m_in_simulation;     // RequestHandler is in simulation mode
    masnae::ProcessingResponse *            m_response;          // response to FastCgi++
    bool                                    m_system_exit;       // execution stopped by calling exit()
    std::string                             m_last_error;        
    //                                                           
    // Copy of the ScriptExecutor configuration                  
    bool                                    m_optz_clear_frame;  // if config changes
    bool                                    m_optz_clear_global; // if config changes
    bool                                    m_optz_cache_code;   // because js_require calls compile_code but doesn't have access to ScriptExecutor
    //
    //------
    void            pre_execute     ( const masnae::Fastcpipp_Environment &  p_environment,
                                      bool                                   p_in_simulation,
                                      masnae::ProcessingResponse &           p_result,
                                      bool                                   p_optz_clear_frame,
                                      bool                                   p_optz_clear_global,
                                      bool                                   p_optz_cache_code );
    void            post_execute    ( void );
    //
    //------
    enum class module_result { ok, loading, initialized };
    //
    module_result   module_get      ( const std::string &      p_name,
                                    JSObject * &             p_module );
    void            module_loaded   ( const std::string &      p_name,
                                    JSObject *               p_module );
    void            module_unload   ( void );
    //
    //------   
    bool            compile_code    ( const std::string &      p_name,
                                      const std::string &      p_script,
                                      bool                     p_returns_value,
                                      JS::MutableHandleScript  p_binary );
    //
    void            compiled_clear  ( void );
};

//-------------------------------------------------------------------------
class ScriptExecutor : public masnae::bus::Subscriber
{
    public:
        ScriptExecutor() = default;
        //
        // To be called once before any execution
        bool        start          ( void );
        // 
        // To be called once before stopping the program
        void        stop           ( void );
        // 
        // Return module information as a JSON string
        std::string specific_info  ( void );
        //
        // Execute the given script name
        // Returns false if execution fails (set p_result.m_log_message)
        // Return true otherwise (the script can set p_result)
        bool        execute_script ( const std::string &                    p_document,
                                     const std::string &                    p_script_name,
                                     const masnae::Fastcpipp_Environment &  p_environment,
                                     const std::string &                    p_security_json,
                                     bool                                   p_in_simulation,
                                     masnae::ProcessingResponse &           p_result );
        //
        // Called by the bus with a new message
        virtual
        void        bus_event      ( const masnae::bus::Message &  p_message );
        //
        // Disable object copy
        ScriptExecutor          ( const ScriptExecutor & ) = delete;
        void operator=          ( const ScriptExecutor & ) = delete;
        //
    private:
        //
        // Configuration
        std::shared_mutex             m_mutex_config;
        int64_t                       m_max_memory_mb       = 0;
        bool                          m_optz_clear_frame    = false;
        bool                          m_optz_clear_global   = false;
        bool                          m_optz_cache_code     = false;
        //
        // Parse the configuration
        bool         update_config    ( void );
        //
        // Statistics counters  
        std::mutex                    m_mutex_stats;
        uint64_t                      m_nb_evaluates_done   = 0;    // nb successful evaluates done
        // 
        // Each thread must have its own context. Use the local class member to keep it.
        // Note: may be never deleted because FastCGI++ does not notify when the thread is stopped.
        static thread_local        
        ThreadFrame *                 m_thread_frame;
        //
        // Returns the text of the last execution exception
        std::string  get_last_error   ( void );
        //
        // Add all functions and classes to the Global object
        bool  configure_global_consts ( const std::string &                    p_document,
                                        const masnae::Fastcpipp_Environment &  p_environment,
                                        JS::HandleObject                       p_global );
        //
        // Add all objects to the Global object
        bool  configure_global_vars   ( const std::string &                    p_document,
                                        const masnae::Fastcpipp_Environment &  p_environment,
                                        const std::string &                    p_security,
                                        JS::HandleObject                       p_global );
        //
        // Execute the script, set result
        bool  evaluate                ( const std::string &                    p_document,
                                        const masnae::Fastcpipp_Environment &  p_environment,
                                        bool                                   p_in_simulation,
                                        JS::HandleObject                       p_global,
                                        const std::string &                    p_script_name,                                
                                        masnae::ProcessingResponse &           p_result );
        //
        // Check that this thread has created his context, create it if needed
        bool  init_frame              ( void );
        //
        // Delete frame
        void  clear_frame             ( void );
        //
        // Check that this thread has created his global object, create it if needed
        bool  init_global             ( bool &           p_created );
        //
        // Clear or delete the global object
        void  clear_global            ( void );
        //
        // Function called by engine on error
        static
        void  callback_error          ( JSContext *      p_context,
                                        JSErrorReport *  p_report );
        //
        // Function called by engine when there is no more memory
        static
        void  callback_out_of_memory  ( JSContext *      p_context,
                                        void *           p_data );
        //
        // Function called by engine on interruption
        static
        bool  callback_interrupt      ( JSContext *      p_context );
    };

} // namespace thoe

#endif
