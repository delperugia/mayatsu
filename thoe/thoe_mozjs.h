// Copyright 2018 Pierre del Perugia
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

//
// Including SpiderMonkey header files
//

#ifndef thoe_mozjs_h
#define thoe_mozjs_h

#if THOE_MOZJS_VERSION == 52
    #include <mozjs-52/jsfriendapi.h>
    #include <mozjs-52/jsapi.h>
    #include <mozjs-52/js/Conversions.h>
    #include <mozjs-52/js/Date.h>
    #include <mozjs-52/js/Initialization.h>
#elif THOE_MOZJS_VERSION == 60
    #include <mozjs-60/jsfriendapi.h>
    #include <mozjs-60/jsapi.h>
    #include <mozjs-60/js/Conversions.h>
    #include <mozjs-60/js/Date.h>
    #include <mozjs-60/js/Initialization.h>
#else
    #error Unhandled SpiderMonkey version
#endif

#endif
