// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

// Copyright 2018 Pierre del Perugia
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <adawat/adawat_money.h>

#include "thoe_common.h"
#include "thoe_javascript.h"
#include "thoe_script_executor.h"
#include "thoe_js_tools.h"

namespace thoe {

#pragma GCC diagnostic ignored "-Wmissing-field-initializers"

//-------------------------------------------------------------------------
// Amount class declaration
static
bool js_amount_constructor( JSContext * p_context, unsigned p_argc, JS::Value * p_argv );

static
void js_amount_finalize( JSFreeOp * p_free_op, JSObject * p_object );

static
JSClassOps js_amount_class_ops = {                      
    nullptr,                // addProperty
    nullptr,                // delProperty
#if MOZJS_MAJOR_VERSION < 60
    nullptr,                // getProperty
    nullptr,                // setProperty
#endif
    nullptr,                // enumerate
#if MOZJS_MAJOR_VERSION >= 60
    nullptr,                // newEnumerate
#endif
    nullptr,                // resolve
    nullptr,                // mayResolve
    js_amount_finalize,     // finalize
    nullptr,                // call
    nullptr,                // hasInstance
    js_amount_constructor,  // construct
    nullptr                 // trace
};

static
JSClass js_amount_class = {
    "Amount",
    JSCLASS_HAS_PRIVATE | JSCLASS_FOREGROUND_FINALIZE,
    & js_amount_class_ops
};

//-------------------------------------------------------------------------
// If a result is an error, set an exception
bool throwOnError ( JSContext *             p_context,
                    const char *            p_operation,
                    adawat::Amount::result  p_result )
{
    switch ( p_result )
    {
        case adawat::Amount::result::ok:
            return false;
        case adawat::Amount::result::overflow:
            JS_ReportErrorUTF8( p_context, "Amount: overflow in %s", p_operation );
            break;
        case adawat::Amount::result::division_by_zero:
            JS_ReportErrorUTF8( p_context, "Amount: division by zero in %s", p_operation );
            break;
        case adawat::Amount::result::incompatible_currency:
            JS_ReportErrorUTF8( p_context, "Amount: incompatible currency in %s", p_operation );
            break;
        case adawat::Amount::result::invalid_value:
            JS_ReportErrorUTF8( p_context, "Amount: invalid value in %s", p_operation );
            break;
        case adawat::Amount::result::invalid_currency:
            JS_ReportErrorUTF8( p_context, "Amount: invalid currency in %s", p_operation );
            break;
        case adawat::Amount::result::invalid_mode:
            JS_ReportErrorUTF8( p_context, "Amount: invalid mode in %s", p_operation );
            break;
        case adawat::Amount::result::invalid_precision:
            JS_ReportErrorUTF8( p_context, "Amount: invalid precision in %s", p_operation );
            break;
        default:
            JS_ReportErrorUTF8( p_context, "Amount: unknown error in %s", p_operation );
            break;
    }
    //
    return true;
}

//-------------------------------------------------------------------------
// Instantiate a new Amount object, initialized with data
JSObject * js_new_amount( JSContext *         p_context,
                          JS::HandleObject    p_global,
                          adawat::Amount &    p_amount )
{
    // Retrieve the Amount class constructor
    JS::RootedValue value( p_context );
    //
    if ( ! JS_GetProperty( p_context, p_global, "Amount", & value) ) {
        JS_ReportErrorUTF8(p_context, "Amount: class not found");
        return nullptr;
    }
    //
    if ( ! value.isObject() ) {
        JS_ReportErrorUTF8(p_context, "Amount: class is not an object");
        return nullptr;
    }
    //
    JS::RootedObject constructor( p_context,
                                  & value.toObject() );
    //
    // Create a new object with this constructor
    JS::RootedObject object( p_context,
                             JS_New( p_context,
                                     constructor,
                                     JS::HandleValueArray::empty() ) );
    //
    if ( object == nullptr ) {
        JS_ReportErrorUTF8(p_context, "Amount: new amount is not an object");
        return nullptr;
    }
    //
    // Copy the data in the amount
    auto amount = static_cast< adawat::Amount* >( JS_GetPrivate( object ) );
    //
    * amount = p_amount;
    //
    return object;
}

//-------------------------------------------------------------------------
// Check if an object is an instance of Amount
bool js_is_amount( JS::HandleValue p_value )
{
    return p_value.isObject() &&
           JS_GetClass( & p_value.toObject() ) == & js_amount_class;
}

//-------------------------------------------------------------------------
// Constructor:
//  let a = new Amount();
//  let a = new Amount( '123.1', 'EUR' );
static
bool js_amount_constructor( JSContext * p_context, unsigned p_argc, JS::Value * p_argv )
{
    JS::CallArgs args = JS::CallArgsFromVp( p_argc, p_argv );
    //
    // The constructor can eventually be called with value/currency
    if ( args.length() != 0 )
        if ( ! js_tools_check_argument( p_context, "Amount.constructor", args, "SS" ) )
            return false;
    //
    // Build the object
    JSObject * object = JS_NewObjectForConstructor( p_context, & js_amount_class, args );
    if ( object == nullptr ) {
        JS_ReportOutOfMemory( p_context );
        return false;
    }
    //
    // Allocate private data (an adawat Amount object)
    auto amount = std::unique_ptr< adawat::Amount >( new (std::nothrow) adawat::Amount() );
    if ( amount == nullptr ) {
        JS_ReportOutOfMemory( p_context );
        return false;
    }
    //
    // Eventually set the value
    if ( args.length() != 0 ) {
        std::string value    = js_tools_convert( p_context, args[0].toString() );
        std::string currency = js_tools_convert( p_context, args[1].toString() );
        //
        if ( throwOnError ( p_context,
                            "constructor",
                            amount->set( value, currency ) ) )  // set exception on error
            return false;
    }
    //
    JS_SetPrivate( object, amount.release() );
    //
    // Return the new object
    args.rval().setObject( * object );
    return true;
}

//-------------------------------------------------------------------------
// Note that finalize is called twice, 2nd time private is nullptr
static
void js_amount_finalize( JSFreeOp * p_free_op, JSObject * p_object )
{
    auto amount = static_cast< adawat::Amount* >( JS_GetPrivate( p_object ) );
    //
    if ( amount != nullptr )
        delete amount;
}

//-------------------------------------------------------------------------
// Add two amounts and return new object
//  let a = new Amount( '0.3','EUR' );
//  let b = new Amount( '0.7','EUR' )
//  let c = a.add( b );
static
bool js_amount_add( JSContext * p_context, unsigned p_argc, JS::Value * p_argv )
{
    JS::CallArgs args = JS::CallArgsFromVp( p_argc, p_argv );
    //
    if ( ! js_tools_check_argument( p_context, "Amount.add", args, "O" ) )
        return false;
    //
    //------------------------
    // Retrieve parameters
    JS::RootedValue b_value( p_context,
                             args[0] );
    if ( ! js_is_amount( b_value ) ) {
        JS_ReportErrorUTF8( p_context, "Amount: parameter is not an Amount object" );
        return false;
    }
    //
    // Retrieve the amounts
    auto a = static_cast< adawat::Amount* >( JS_GetPrivate( & args.thisv().toObject() ) );
    auto b = static_cast< adawat::Amount* >( JS_GetPrivate( & b_value.toObject() ) );
    //
    //------------------------
    // Create the result
    adawat::Amount c ( * a );
    //
    if ( throwOnError ( p_context,
                        "add",
                        c.add( * b ) ) )  // set exception on error
        return false;
    //
    //------------------------
    // Create an object of the result
    JS::RootedObject global( p_context,
                             JS_GetGlobalForObject( p_context,
                                                    & args.callee() ) );
    JS::RootedObject c_object( p_context,
                               js_new_amount( p_context, global, c ) );
    //
    if ( c_object == nullptr )
        return false;
    //
    //------------------------
    // Returns a new object to allow method chaining
    args.rval().setObject( * c_object );
    return true;
}

//-------------------------------------------------------------------------
// Compare two amounts, returns <b: -1, =b: 1, >b: 1
//  if ( new Amount( '1','EUR' ).compareTo( new Amount( '0.7','EUR' ) ) == 1 )
static
bool js_amount_compareto( JSContext * p_context, unsigned p_argc, JS::Value * p_argv )
{
    JS::CallArgs args = JS::CallArgsFromVp( p_argc, p_argv );
    //
    if ( ! js_tools_check_argument( p_context, "Amount.compareTo", args, "O" ) )
        return false;
    //
    //------------------------
    // Retrieve parameters
    JS::RootedValue b_value( p_context,
                             args[0] );
    if ( ! js_is_amount( b_value ) ) {
        JS_ReportErrorUTF8( p_context, "Amount: parameter is not an Amount object" );
        return false;
    }
    //
    // Retrieve the amounts
    auto a = static_cast< adawat::Amount* >( JS_GetPrivate( & args.thisv().toObject() ) );
    auto b = static_cast< adawat::Amount* >( JS_GetPrivate( & b_value.toObject() ) );
    //
    //------------------------
    int result;
    //
    if ( throwOnError ( p_context,
                        "compareTo",
                        a->comp( * b, result ) ) )  // set exception on error
        return false;
    //
    //------------------------
    args.rval().setInt32( result );
    return true;
}

//-------------------------------------------------------------------------
// Returns the currency of the amount as a string
static
bool js_amount_currency( JSContext * p_context, unsigned p_argc, JS::Value * p_argv )
{
    JS::CallArgs args = JS::CallArgsFromVp( p_argc, p_argv );
    //
    //------------------------
    // Retrieve the amount
    auto amount = static_cast< adawat::Amount* >( JS_GetPrivate( & args.thisv().toObject() ) );
    //
    //------------------------
    JS::RootedString string( p_context,
                             JS_NewStringCopyZ( p_context,
                                                amount->currency().c_str() ) );
    //
    args.rval().setString( string );
    return true;
}

//-------------------------------------------------------------------------
// Multiply the amount by a value and return new object
//  let b = new Amount( '1','EUR' ).div( '2' );
static
bool js_amount_div( JSContext * p_context, unsigned p_argc, JS::Value * p_argv )
{
    JS::CallArgs args = JS::CallArgsFromVp( p_argc, p_argv );
    //
    if ( ! js_tools_check_argument( p_context, "Amount.div", args, "S" ) )
        return false;
    //
    //------------------------
    std::string value = js_tools_convert( p_context, args[0].toString() );
    //
    // Retrieve the amount
    auto a = static_cast< adawat::Amount* >( JS_GetPrivate( & args.thisv().toObject() ) );
    //
    //------------------------
    // Create the result
    adawat::Amount b ( * a );
    //
    if ( throwOnError ( p_context,
                        "div",
                        b.div( value ) ) )  // set exception on error
        return false;
    //
    //------------------------
    // Create an object of the result
    JS::RootedObject global( p_context,
                             JS_GetGlobalForObject( p_context,
                                                    & args.callee() ) );
    JS::RootedObject b_object( p_context,
                               js_new_amount( p_context, global, b ) );
    //
    if ( b_object == nullptr )
        return false;
    //
    //------------------------
    // Returns a new object to allow method chaining
    args.rval().setObject( * b_object );
    return true;
}

//-------------------------------------------------------------------------
// Retrieves the object amount as a formatted string
//  let s = new Amount( '12345.6789','EUR' ).round( 3, 2 ).format( 2 );
static
bool js_amount_format( JSContext * p_context, unsigned p_argc, JS::Value * p_argv )
{
    JS::CallArgs args = JS::CallArgsFromVp( p_argc, p_argv );
    //
    if ( ! js_tools_check_argument( p_context, "Amount.format", args, "/ISISIS" ) )
        return false;
    //
    //------------------------
    unsigned    precision          = 32 ;   // digits to keep (truncated)
    std::string decimal_separator  = ".";
    unsigned    group_mode         = 3  ;   // 0 none, 3 thousands, 4 myriad or 7 Indian 2,2,3
    std::string group_separator    = " ";
    unsigned    currency_mode      = 2  ;   // 0 none, 1 code before, 2 code after, 3 symbol before, 4 symbol after
    std::string currency_separator = " ";
    //
    if ( p_argc >= 1 )
        precision          = static_cast< unsigned >( args[0].toInt32() );
    if ( p_argc >= 2 )
        decimal_separator  = js_tools_convert( p_context, args[1].toString() );
    if ( p_argc >= 3 )
        group_mode         = static_cast< unsigned >( args[2].toInt32() );
    if ( p_argc >= 4 )
        group_separator    = js_tools_convert( p_context, args[3].toString() );
    if ( p_argc >= 5 )
        currency_mode      = static_cast< unsigned >( args[4].toInt32() );
    if ( p_argc >= 6 )
        currency_separator = js_tools_convert( p_context, args[5].toString() );
    //
    // Retrieve the amount
    auto amount = static_cast< adawat::Amount* >( JS_GetPrivate( & args.thisv().toObject() ) );
    //
    //------------------------
    JS::RootedString string( p_context,
                             JS_NewStringCopyZ( p_context,
                                                amount->format( precision,
                                                                decimal_separator,
                                                                group_mode,
                                                                group_separator,
                                                                currency_mode,
                                                                currency_separator ).c_str() ) );
    //
    args.rval().setString( string );
    return true;
}

//-------------------------------------------------------------------------
// Multiply the amount by a value and return new object
//  let a = new Amount( '1','EUR' ).mul( '2' );
static
bool js_amount_mul( JSContext * p_context, unsigned p_argc, JS::Value * p_argv )
{
    JS::CallArgs args = JS::CallArgsFromVp( p_argc, p_argv );
    //
    if ( ! js_tools_check_argument( p_context, "Amount.mul", args, "S" ) )
        return false;
    //
    //------------------------
    std::string value = js_tools_convert( p_context, args[0].toString() );
    //
    // Retrieve the amount
    auto a = static_cast< adawat::Amount* >( JS_GetPrivate( & args.thisv().toObject() ) );
    //
    //------------------------
    // Create the result
    adawat::Amount b ( * a );
    //
    if ( throwOnError ( p_context,
                        "mul",
                        b.mul( value ) ) )  // set exception on error
        return false;
    //
    //------------------------
    // Create an object of the result
    JS::RootedObject global( p_context,
                             JS_GetGlobalForObject( p_context,
                                                    & args.callee() ) );
    JS::RootedObject b_object( p_context,
                               js_new_amount( p_context, global, b ) );
    //
    if ( b_object == nullptr )
        return false;
    //
    //------------------------
    // Returns a new object to allow method chaining
    args.rval().setObject( * b_object );
    return true;
}

//-------------------------------------------------------------------------
// Multiply the amount by a value and return new object
//  let a = new Amount( '10','EUR' ).percent( '40' );
static
bool js_amount_percent( JSContext * p_context, unsigned p_argc, JS::Value * p_argv )
{
    JS::CallArgs args = JS::CallArgsFromVp( p_argc, p_argv );
    //
    if ( ! js_tools_check_argument( p_context, "Amount.percent", args, "S" ) )
        return false;
    //
    //------------------------
    std::string value = js_tools_convert( p_context, args[0].toString() );
    //
    // Retrieve the amount
    auto a = static_cast< adawat::Amount* >( JS_GetPrivate( & args.thisv().toObject() ) );
    //
    //------------------------
    // Create the result
    adawat::Amount b ( * a );
    //
    if ( throwOnError ( p_context,
                        "percent",
                        b.percent( value ) ) )  // set exception on error
        return false;
    //
    //------------------------
    // Create an object of the result
    JS::RootedObject global( p_context,
                             JS_GetGlobalForObject( p_context,
                                                    & args.callee() ) );
    JS::RootedObject b_object( p_context,
                               js_new_amount( p_context, global, b ) );
    //
    if ( b_object == nullptr )
        return false;
    //
    //------------------------
    // Returns a new object to allow method chaining
    args.rval().setObject( * b_object );
    return true;
}

//-------------------------------------------------------------------------
// Rounds a number according to the given mode
//  let a = new Amount( '123.456','EUR' ).round( 3, 2 );
static
bool js_amount_round( JSContext * p_context, unsigned p_argc, JS::Value * p_argv )
{
    JS::CallArgs args = JS::CallArgsFromVp( p_argc, p_argv );
    //
    if ( ! js_tools_check_argument( p_context, "Amount.round", args, "II" ) )
        return false;
    //
    //------------------------
    unsigned     mode      = static_cast< unsigned >( args[0].toInt32() );   // 1 up, 2 down, 3 commercial
    uint32_t     precision = static_cast< uint32_t >( args[1].toInt32() );
    //
    // Retrieve the amount
    auto a = static_cast< adawat::Amount* >( JS_GetPrivate( & args.thisv().toObject() ) );
    //
    //------------------------
    // Create the result
    adawat::Amount b ( * a );
    //
    if ( throwOnError ( p_context,
                        "round",
                        b.round( mode, precision ) ) )  // set exception on error (invalid mode/precision)
        return false;
    //
    //------------------------
    // Create an object of the result
    JS::RootedObject global( p_context,
                             JS_GetGlobalForObject( p_context,
                                                    & args.callee() ) );
    JS::RootedObject b_object( p_context,
                               js_new_amount( p_context, global, b ) );
    //
    if ( b_object == nullptr )
        return false;
    //
    //------------------------
    // Returns a new object to allow method chaining
    args.rval().setObject( * b_object );
    return true;
}

//-------------------------------------------------------------------------
// Set the amount with the given value and currency
//  let a = (new Amount).set( '12.5','EUR' );
static
bool js_amount_set( JSContext * p_context, unsigned p_argc, JS::Value * p_argv )
{
    JS::CallArgs args = JS::CallArgsFromVp( p_argc, p_argv );
    //
    if ( ! js_tools_check_argument( p_context, "Amount.set", args, "SS" ) )
        return false;
    //
    std::string value    = js_tools_convert( p_context, args[0].toString() );
    std::string currency = js_tools_convert( p_context, args[1].toString() );
    //
    // Retrieve the amount
    auto amount = static_cast< adawat::Amount* >( JS_GetPrivate( & args.thisv().toObject() ) );
    //
    //------------------------
    // Set the new amount
    if ( throwOnError ( p_context,
                        "round",
                        amount->set( value, currency ) ) )  // set exception on error
        return false;
    //
    //------------------------
    // Returns the object to allow method chaining
    args.rval().setObject( args.thisv().toObject() );
    return true;
}

//-------------------------------------------------------------------------
// Sub two amounts and return new object
//  let a = new Amount( '1','EUR' );
//  let b = new Amount( '0.7','EUR' )
//  let c = a.add( b );
static
bool js_amount_sub( JSContext * p_context, unsigned p_argc, JS::Value * p_argv )
{
    JS::CallArgs args = JS::CallArgsFromVp( p_argc, p_argv );
    //
    if ( ! js_tools_check_argument( p_context, "Amount.sub", args, "O" ) )
        return false;
    //
    //------------------------
    // Retrieve parameters
    JS::RootedValue b_value( p_context,
                             args[0] );
    if ( ! js_is_amount( b_value ) ) {
        JS_ReportErrorUTF8( p_context, "Amount: parameter is not an Amount object" );
        return false;
    }
    //
    // Retrieve the amounts
    auto a = static_cast< adawat::Amount* >( JS_GetPrivate( & args.thisv().toObject() ) );
    auto b = static_cast< adawat::Amount* >( JS_GetPrivate( & b_value.toObject() ) );
    //
    //------------------------
    // Create the result
    adawat::Amount c ( * a );
    //
    if ( throwOnError ( p_context,
                        "sub",
                        c.sub( * b ) ) )  // set exception on error
        return false;
    //
    //------------------------
    // Create an object of the result
    JS::RootedObject global( p_context,
                             JS_GetGlobalForObject( p_context,
                                                    & args.callee() ) );
    JS::RootedObject c_object( p_context,
                               js_new_amount( p_context, global, c ) );
    //
    if ( c_object == nullptr )
        return false;
    //
    //------------------------
    // Returns a new object to allow method chaining
    args.rval().setObject( * c_object );
    return true;
}

//-------------------------------------------------------------------------
// Returns the value of the amount as a string
static
bool js_amount_value( JSContext * p_context, unsigned p_argc, JS::Value * p_argv )
{
    JS::CallArgs args = JS::CallArgsFromVp( p_argc, p_argv );
    //
    //------------------------
    // Retrieve the amount
    auto amount = static_cast< adawat::Amount* >( JS_GetPrivate( & args.thisv().toObject() ) );
    //
    //------------------------
    JS::RootedString string( p_context,
                             JS_NewStringCopyZ( p_context,
                                                amount->value().c_str() ) );
    //
    args.rval().setString( string );
    return true;
}

//-------------------------------------------------------------------------
static
const JSPropertySpec js_amount_properties[] = {
    JS_INT32_PS( "grouping_none",           0, JSPROP_READONLY | JSPROP_ENUMERATE | JSPROP_PERMANENT ),
    JS_INT32_PS( "grouping_thousand",       3, JSPROP_READONLY | JSPROP_ENUMERATE | JSPROP_PERMANENT ),
    JS_INT32_PS( "grouping_myriad",         4, JSPROP_READONLY | JSPROP_ENUMERATE | JSPROP_PERMANENT ),
    JS_INT32_PS( "grouping_Indian",         7, JSPROP_READONLY | JSPROP_ENUMERATE | JSPROP_PERMANENT ),
    //
    JS_INT32_PS( "currency_none",           0, JSPROP_READONLY | JSPROP_ENUMERATE | JSPROP_PERMANENT ),
    JS_INT32_PS( "currency_code_before",    1, JSPROP_READONLY | JSPROP_ENUMERATE | JSPROP_PERMANENT ),
    JS_INT32_PS( "currency_code_after",     2, JSPROP_READONLY | JSPROP_ENUMERATE | JSPROP_PERMANENT ),
    JS_INT32_PS( "currency_symbol_before",  3, JSPROP_READONLY | JSPROP_ENUMERATE | JSPROP_PERMANENT ),
    JS_INT32_PS( "currency_symbol_after",   4, JSPROP_READONLY | JSPROP_ENUMERATE | JSPROP_PERMANENT ),
    //
    JS_INT32_PS( "rounding_up",             1, JSPROP_READONLY | JSPROP_ENUMERATE | JSPROP_PERMANENT ),
    JS_INT32_PS( "rounding_down",           2, JSPROP_READONLY | JSPROP_ENUMERATE | JSPROP_PERMANENT ),
    JS_INT32_PS( "rounding_commercial",     3, JSPROP_READONLY | JSPROP_ENUMERATE | JSPROP_PERMANENT ),
    //
    JS_PS_END
};

static
const JSFunctionSpec js_amount_methods[] = 
{
    //     name          function                nargs flags
    JS_FN("add",         js_amount_add,          1,    0),
    JS_FN("compareTo",   js_amount_compareto,    1,    0),
    JS_FN("currency",    js_amount_currency,     0,    0),
    JS_FN("div",         js_amount_div,          1,    0),
    JS_FN("format",      js_amount_format,       6,    0),
    JS_FN("mul",         js_amount_mul,          1,    0),
    JS_FN("percent",     js_amount_percent,      1,    0),
    JS_FN("round",       js_amount_round,        2,    0),
    JS_FN("set",         js_amount_set,          2,    0),
    JS_FN("sub",         js_amount_sub,          1,    0),
    JS_FN("value",       js_amount_value,        0,    0),
    //
    JS_FS_END,
};

//-------------------------------------------------------------------------
// Define the Money class in the global object
// Called only once by ScriptExecutor::execute_script if global
// is reused (m_optz_clear_global = false).
bool js_add_money_class( JSContext *       p_context,
                         JS::HandleObject  p_global )
{
    if ( ! JS_InitClass( p_context,
                         p_global,
                         nullptr,               // parent_proto
                         & js_amount_class,
                         js_amount_constructor,
                         0,                     // nb constructor args
                         js_amount_properties,
                         js_amount_methods,
                         js_amount_properties,  // static_ps
                         nullptr ) )            // static_fs
    {
        js_service_error_log( "JavaScript: Failed to define amount class", nullptr );
        return false;
    }
    //
    return true;
}

} // namespace thoe
