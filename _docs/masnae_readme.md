# Masnae

# Table of Contents

  - [About](#about)
  - [Available components](#available-components)
    - [Config management](#config-management)
    - [MySQL manager](#mysql-manager)
    - [HTTP client](#http-client)
    - [Internal bus](#internal-bus)
    - [Request handler](#request-handler)
      - [Handling different Content-Types](#handling-different-content-types)
      - [Security](#security)
      - [Log](#log)
    - [Table manager](#table-manager)
    - [Cache manager](#cache-manager)
    - [Logger](#logger)
  - [Initialization](#initialization)
  - [Validation and Console](#validation-and-console)
  - [License](#license)

# About

Masnae is a framework library used to develop WebService using C++.

It is based on FastCGI++ and the resulting binary is a CGI module that
is loaded by a web server such as Apache or lighttpd.

By properly configuring the web server, it is possible to have a
persistent processes running the module and therefor to have persistent
processing (such as handling SMPP connections or SIP calls).

# Available components

Masnae integrates the following components:

  | Name              | Description
  |-------------------|------------------------------------
  | Config management | monitor and read configuration file
  | MySQL manager     | pool of MySQL database connections
  | HTTP client       | pool of cURL connections
  | Internal bus      | used to send notifications between modules
  | Request handler   | handle HTTP request
  | Table manager     | web service access to internal tables
  | Cache manager     | SQL queries cache

Plus the utility files for:

  - Common constants and types
  - Common functions
  - Globals variables

## Config management

The configuration of all components is stored in a JSON file. The file
name is specified by the program. Masnae monitors this configuration
file. Upon modification, the configuration is read and modules notified.

The following methods are available in the `config::Handler` object to
retrieve elements of the configuration:

  - key_exist, get_members, get_size
  - get_bool, get_int64, get_string

## MySQL manager

The MariaDB client is integrated in Masnae. Two sets of connections are
established: one for reading, one for reading and writing.

Connection details are declared in the configuration, in key `mysql`:

```json
  "mysql" : {
    "user" :    "masnae",
    "password": "123456",
    "schema":   "test",
    "max_size": 2,
    "reader": {
      "host":        "localhost",
      "unix_socket": "/var/run/mysqld/mysqld.sock"
    },
    "writer": {
      "host":        "localhost",
      "unix_socket": "/var/run/mysqld/mysqld.sock"
    },
    "compress":   false,
    "timeout_ms": 1000
  }
```

| Key         | Default | Description
|-------------|---------|--------------
| user        |         | login
| password    |         | credential
| schema      |         | database to use
| host        |         | server address or name
| port        |         | port number
| max_size    | 2       | maximum of connections (per reader/writer) to open
| unix_socket |         | the socket file name used for local connections (`socket` parameter  of `server.cnf`)
| compress    | false   | compress data between Masnae and the database server
| timeout_ms  | 1000    | timeout to get a handle from the pool or create a new one

One of `port` or `unix_socket` must be set. `compress` and `timeout_ms` should
usually not be set.

To make requests, start by retrieving a new connection. Either a writer
(for INSERT, UPDATE, DELETE or to make transaction) or a reader one.

```cpp
  {
    SingleKeyValue_List  rows;
    Connection           cnx = g_MySQLHandler.get_connection ( mysql::reader );  // reader connection
    //
    if ( cnx.is_valid() ) {
      unsigned result = Cnx.do_select( "SELECT * FROM employees", rows );
      //
      if ( result == 0 )
        for ( auto & row: rows )
            std::cout << row["first_name"] << " " << row["last_name"] << "\n";
    }
  }
```

Before using a connection returned by `get_connection`, check by calling
`is_valid` that the object is valid (connected to the database). The
`Connection` class offers then the following methods:

  - do_insert, do_update, do_select, do_delete
  - start_transaction, commit_transaction, rollback_transaction

These methods return native MariaDB connector results (see
[MariaDB](https://mariadb.com/kb/en/library/mariadb-error-codes/)),
or one of:

 | Name                         | Value | Description
 |------------------------------|-------|-------------
 | MSN_MYSQL_ERR_NO_HANDLE      |     1 | no available connection to database
 | MSN_MYSQL_ERR_NOT_WRITER     |     2 | writer operation on reader connection
 | MSN_MYSQL_ERR_IN_TRANSACTION |     3 | already in a transaction
 | MSN_MYSQL_ERR_NO_TRANSACTION |     4 | no active transaction

The number of rows returned is limited by default to 1000. This can be
changed by calling the `set_max_results` method.

Convenient functions are available when building SQL queries and
handling results:
  - build_resp_error
  - datetime_local
  - datetime_utc
  - sql_escape

## HTTP client

The cURL library is integrated in Masnae. Available URLs to call are
declared in the configuration, in key `modules` of `http`:

```json
  "http" : {
      "modules" : {
          "mod_ex":  { "url": "http://example.com/" }
      }
  }
```

Each key of modules (here `mod_ex`) is an alias to an URL. This alias is
the parameter `p_module` in one of these HTTP request methods:

  - request1: to make a standard call with parameters, x-www-form-urlencoded or Mime
  - request2: to pass your own body
  - request_jresp: a standard call, expecting and returning a JResp object
  - request_json: a standard call, expecting a JSON response and returning a JResp object

These functions use the module name, eventually followed by a document:

```cpp
    request1( "mod_ex/path/document"...
```

will call here:

    `http://example.com/path/document`

These functions return the cURL result code (one of CURLcode described in
[cURL](https://curl.haxx.se/libcurl/c/libcurl-errors.html) or the HTTP code or)
or one of:

| Name                        | Value | Description
|-----------------------------|-------|-------------
| MSN_HTTP_ERR_NO_HANDLE      |   -1  | failed to allocated a cURL handle
| MSN_HTTP_ERR_NOT_CONFIGURE  |   -2  | failed to configured cURL handle
| MSN_HTTP_ERR_NAME_NOT_FOUND |   -3  | module name not found
| MSN_HTTP_ERR_BAD_RESPONSE   |   -4  | call was successfully done, but failed to retrieve response

Beside the `url` key, the following keys are available:

| Name            | Default | Description
|-----------------|---------|-------------
| timeout         |     30  | request maximum time, including DNS lookup, in secondes
| connect_timeout |     10  | timeout for the connect phase, in secondes

## Internal bus

To connect one of your module to the bus, derive it from the Subscriber
class and implement bus_event. You can also add your own message type by
extending Message.

## Request handler

Web service development is done by deriving from `RequestHandler` whose
base class is `Fastcgipp::Request`. FastCGI++ manual describes
this class usage.

Masnae handles itself the following documents:

  - /mgt/tables     queries on tables
  - /info           module information
  - /echo           received request

All others documents are passed to the derived class, via the virtual
function `process_request`.

Web service populates the `ProcessingResponse` class in their
`process_request` function using:

  - set_jresp_success, set_jresp_failed, set_jresp_error, set_jresp_rejected, set_jresp_continue
  - set_raw

Masnae returns by default JSON responses using the
[JResp](https://gitlab.com/delperugia/papers/blob/master/jresp.md) format,
but raw format (including binary) can be configured (using method `set_raw`).
It is also possible to add HTTP headers using `add_http_header`.

The `ProcessingResponse` class also contains the response id. This is a
unique id generated by Masnae when the request is received. It is sent
back to the caller in the `X-Response-ID` header. It is an Adawat uid,
whose type is configured using the configuration key `uid_type`:

```json
  "request" : { "uid_type" : "uid_short_ts" }
```

Default is 'uid_short_ts', possible values are:

  - uid_short_ts
  - uid_medium_ts
  - uid_long_ts

The maximum POST body size and number of threads started by FastCGI++
are set by default by the derived class. However these values can be
changed using the keys `nb_threads` and `max_post_size_b` (-1 meaning
use default):

```json
  "request" : {
    "nb_threads":      16,
    "max_post_size_b": 1000000
  }
```

| Name                  | Default | Description
|-----------------------|---------|------------------------------------
| nb_threads            |     -1  | number of worker threads (range -1,1-128)
| max_post_size_b       |     -1  | maximal post size in bytes (range -1,0-1000000000)
| urania_cache_lifetime |     30  | Urania's /ws/permissions cache, in seconds (range 0-3600), 0 meaning disable.

Convenient functions are available in __masnae_misc.h__ to convert common
types to the JSON answer to put in the data response.

### Handling different Content-Types

FastCGI++ natively only support `multipart/form-data` and
`application/x-www-form-urlencoded`. To received something else, override
inProcessor to handle this type.

### Caching

By default the following HTTP header is added to GET and HEAD
requests to disable caching:

    Cache-Control: no-store, must-revalidate

### Security

Before processing a request, the virtual function `check_security` is
called. If it returns false, the request is rejected with reason 401.

This function must identify the caller and fill the `m_security` member
(class SecurityContext) of RequestHandler. This class contains the
following strings, not used by Masnae itself but corresponding to the
security context defined by Urania:

    user, profile, role, scopes

and a list of permissions (with parameters). Masnae defines (and control)
the following permissions when one of its internal web service is called:

  | Document          | Quieted | Permissions
  |-------------------|---------|------------------
  | /info             | y       | -
  | /echo             | y       | -
  | /mgt/tables       | n       | -
  | /mgt/tables/name  | n       | masnae.tables.write, masnae.tables.read
  | /mgt/caches       | n       | -
  | /mgt/caches/name  | n       | masnae.caches.write

The functions `has_permission` to check permissions available in
`m_security`.

If your modules relies on Urania to fill the security context
(calling '/ws/permissions'), you simply have to call the RequestHandler
method `check_accesstoken` (HTTP module `urania` must be defined then).

HTTP calls to Urania are cached for 30s by default.

### Log

To log message at the service level, use the two functions
`service_info_log` and `service_error_log`. To see them (when using
Lighttp server here), use:

  - service lighttpd status
  - journalctl -u lighttpd

To log error and debug trace per request, use the two methods
`add_error` and `add_log` of the ProcessingResponse object:

  - add_error: stores message into http server log (/var/log/*/error.log)
  - add_log:   stores message in request's log (see below)

The virtual function `store_log` is called at the end of the call. The
received ProcessingResponse object contains the log to store (accessible
using method `get_log`) and response id (using method `get_id`). The
logs are stored in single files, see [Logger](##Logger).

## Table manager

To have Masnae to handle a database table, derive from the Definition
object, setting in the constructor:

  - m_table_name,    the table name
  - m_fields,        description of the fields

These derived classes must be instantiated after calling:

    masnae :: globals :: init

and before:

    masnae :: config :: Handler :: start

Only datetime, char, varchar, unsigned, signed and number are supported.
Table are accessed using the URL /mgt/tables/..., using the syntax:

    /mgt/tables/<table_name>

Parameters are:

  | Name    | Description                                                     | Example
  |---------|-----------------------------------------------------------------|----------
  | command | one of INSERT, UPDATE, DELETE, SELECT; upper or lower case
  | fields  | an object with fields/values for INSERT and UPDATE              | { "name": "D","age": 42 }
  | fields  | an array with a list of fields for SELECT                       | [ "name","age" ]
  | filter  | it is a MongoDB like JSON queries, but regexp are not supported | { "name": "D" }
  | order   | a positive or negative number is set to each field to sort on, positive means Ascending, negative means Descending | { "name": 1 }
  | range   | lower and upper (excluded) bound of a range to retrieve         | [ 5, 10 ]

A maximum of 1000 rows are returned/affected (default parameter range
value is 1000). If a range is specified and is too large, the request is
rejected.

UPDATE and DELETE return the number of affected rows in data:

```json
    { "status": "success", "data": 2 }
````

INSERT returns the new generated id as a string in data:

```json
    { "status": "success", "data": "486452" }
```

SELECT returns an array of objects (with string properties):

```json
    { "status": "success", "data": [
        { "name":"alpha", "age":"1" },
        { "name":"beta", "age":"2" }
      ]
    }
```

Possible rejected reasons in the returned JResp are:

  - forbidden
  - db_exists
  - db_not_null
  - db_locked
  - db_too_long
  - db_foreign_key
  - db_other (`data` contains the native database error as a string)

Calling the URL /mgt/tables (without table name) returns the
list of existing tables.

## Cache manager

Database data can be cached in memory using the 'ws_caches::Handler'.

Define queries to cache, with the name of the column to use to retrieved
cached data (the key). The queries can return several rows with the same
key.

Queries must be added using 'add_cache' after calling:

    masnae :: globals :: init

and before:

    masnae :: config :: Handler :: start

The URL /mgt/caches/<cache_name> can be used to control each cache.
Supported parameters are:

  | Name    | Description                                                     | Example
  |---------|-----------------------------------------------------------------|----------
  | command | reload; upper or lower case

reload will reload the data from the database.  

Possible rejected reasons in the returned JResp are:

  - forbidden
  - exists
  - not_null
  - locked
  - foreign_key
  - other (`data` contains the native database error as a string)

Calling the URL /mgt/caches (without cache name) returns the
list of existing caches.

## Logger

Log stored in the `ProcessingResponse` class during request processing
can be stored in text files. One file is created by request, using the
request id as file name. Files are stored using the following directory
structure:

    <year>/<month>-<day>/<hour>-<minute>/<file>

starting from the configured log folder. Sub-folders are created
dynamicaly. Configuration is located under the `log` key:

```json
    "logger" : {
        "folder" : "/tmp/"
    }
```

| Key            | Default           | Description
|----------------|-------------------|--------------
| enabled        | false             | disable logging
| file_mode      | 0640              | in octal
| directory_mode | 0750              | in octal
| folder         | <module dependent | ending with /
| quiet          | false             | do not log all requests to file (module dependent)

To clean log files, the following crontab lines are suggested:

```text
00 3 * * * find /var/log/<i><b>module</b></i>/ -mindepth 2 -maxdepth 2 -type f -ctime +120 -name '*.zip' -delete
01 3 * * * find /var/log/<i><b>module</b></i>/ -mindepth 2 -maxdepth 2 -type d -ctime +30  -exec zip -rm {}.zip {} \;
```

Log files older than 120 days are zipped (per day), and zip files older than 120 days are deleted.

# Initialization

The main program must declare an instance of each like this:

```cpp
  masnae::bus::Publisher                g_Bus;
  masnae::config::Handler               g_ConfigHandler;
  masnae::logger::Handler               g_LoggerHandler;
  masnae::mysql::Handler                g_MySQLHandler;
  masnae::ws_caches::Handler            g_WsCachesHandler;
  masnae::ws_tables::Handler            g_WsTablesHandler;
  masnae::http::Handler                 g_HttpHandler;
  masnae::request::Manager<my_request>  g_Manager;
```

where `my_request` is your specialization of masnae::RequestHandler, and
must call init before any other call to the library:

```cpp
  masnae::globals::init( "Program name",
                         & g_Bus,
                         & g_ConfigHandler,
                         & g_LoggerHandler,
                         & g_MySQLHandler,
                         & g_WsTablesHandler,
                         & g_WsCachesHandler,
                         & g_HttpHandler,
                         & g_Manager );
```

They must be started and stopped as follow:

```cpp
  g_ConfigHandler.start( "..." )
  g_LoggerHandler.start( "..." )
  g_MySQLHandler.start()
  g_WsTablesHandler.start()
  g_WsCachesHandler.start()
  g_HttpHandler.start()
  g_Manager.init()
  ...
  g_Manager.run()
  ...
  g_HttpHandler.stop();
  g_WsCachesHandler.stop();
  g_WsTablesHandler.stop();
  g_MySQLHandler.stop();
  g_LoggerHandler.stop();
  g_ConfigHandler.stop();
```

# Validation and Console

To add automatic self-tests to your program, use the `exec_url`
function. This simulate the execution of the given url (eventually
containing parameters) as if your module was invoked by the web
server. This allows you to write embedded self-test scenarios.

And to add an interactive console to your program, use the
`console` function. Users can enter URL (eventually containing
parameters) and the resulting responses are displayed.

Both functions are located in __masnae_simulator.h__ file.

Ideally, the console function should be invoked by the `-c` command
line option, and self-test by `-t`.

# License

Copyright 2018 Pierre del Perugia

[Apache license v2](https://www.apache.org/licenses/LICENSE-2.0.txt)
