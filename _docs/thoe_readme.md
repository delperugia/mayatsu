# Thoe

# Table of Contents

  - [About](#about)
  - [Configuration](#configuration)
    - [Security](#security)
  - [JavaScript](#javascript)
    - [require function](#require-function)
    - [request object](#request-object)
      - [Received request](#received-request)
      - [Response sent back](#response-sent-back)
    - [http object](#http-object)
    - [Blob class](#blob-class)
    - [system object](#system-object)
    - [Amount class](#amount-class)
    - [crypto object](#crypto-object)
    - [MySQL class](#mysql-class)
  - [Deployment](#deployment)
    - [lighttpd](#lighttpd)
  - [Extending JavaScript](#extending-javascript)
  - [License](#license)

# About

Thoe is a server side JavaScript engine running on top of Masnae.

It is based on SpiderMonkey.

# Configuration

Default configuration file is `/var/www/cgi-config/thoe.json`. This can
be overloaded either using the environment variable `THOE_CONFIG`
or the command line option -f.

Beside Masnae configuration items, Thoe uses:

```json
  "spidermonkey" : {
    "max_memory_mb" :    16,
    "optz_clear_frame":  false,
    "optz_clear_global": false,
    "optz_cache_code":   true
  },
  "scripts" : {
    "folder" :   "/var/www/cgi-scripts/",
    "cb_key_scripts": [ "1uh6lKM5o8/spdo_notify" ]
  }
```

The available keys in the `spidermonkey` node are:

  | Name              | Description                                       | Default
  |-------------------|---------------------------------------------------|--------
  | max_memory_mb     | memory limit in MB for SpiderMonkey (range 1-128) | 16
  | optz_clear_frame  | delete everything between executions              | false
  | optz_clear_global | delete only JavaScript Global object              | false
  | optz_cache_code   | cache compile code                                | true

Code caching makes the following script to crash SpiderMonkey 52
(when var a is global), but not with version 60:

```javascript
  var a = [ 1, 2 ];
  a.forEach( function(i) { } );
```

Keeping the same Global object between execution prevents this crash
but all global variables remains. Thus the following code is valid:

```javascript
  var first_time;
  if ( first_time == undefined ) {
    first_time = false;
```

`scripts` keys are:

  - `folder`: where scripts are located (default `/var/www/cgi-scripts/`).
    If the path is not absolute (no leading /), it is then relative to
    the current working directory;

  - `cb_key_scripts`: array of secret/script paths that bypass Uriana
    validation (no AccessToken required).

## Security

Security is enforced in Thoe by calling Urania when an API call is
received. Internal management calls to '/mgt/' use Masnae
permissions. In order to call your scripts, caller must have the
permission `thoe.ws`.

It is sometime necessary to received callback notifications from
external parties that cannot add an `Authorization` header to their
HTTP call. In that case it is possible to pass the AccessToken
in the URI using the document prefix `cb` instead of `ws`:

  `https://api.example.com/thoe/v1/cb/1uh6lKM5o8/spdo_notify`

will behave like:

  `https://api.example.com/thoe/v1/ws/spdo_notify`

Your scripts can check the caller permissions using the `request`
system object.

The HTTP module `urania` must be configured, for example:

```json
  "http" : {
    "modules" : {
      "urania":  { "url": "http://localhost/urania/v1/" }
    }
  }
```

# JavaScript

The JavaScript engine is triggered by the document prefix `/ws/`
on the URL. The trailing path is the name of the script to execute,
for example:

    `https://api.example.com/thoe/v1/ws/my_script`

will try to execute my_script.js, from the configured scripts folder.

They must have the .js extension (e.g.: my_script.js). Thoe uses
a cache mechanism on this folder. Script can be located in sub-folders,
but file and folder names must only use alphanumerical and underscore
characters.

Thoe adds the following global functions and objects to JavaScript:

  - require function / exports object
  - request object
  - http object
  - Blob class
  - system object
  - Amount class
  - crypto object

## require function

This function is used to load external JavaScript library. Library files are
regular script files exporting variables and function. For example a
file lib.js containing:

```javascript
  exports.x = 2;
  exports.f = v => v * 2;
```

and use like this:

```javascript
  var my_lib = require( 'lib.js' );
  var r     = my_lib.f( 20 ) + lib.x;
```

Module also exports its name in property id:

```javascript
  my_lib.id = 'lib.js'
```

Error reported:

  - Failed to execute: missing ; before statement, file lib.js, line 14, column 24
  - Failed to execute: require circular loop detected, module 'lib.js', line 2, column 9

## request object

The request object has two roles:

  - expose the receive request
  - handle the response to send back

### Received request

The request object contains the following members:

  - remote_address, request_method, request_uri
  - params, headers, attachments
  - security context

Possible headers (they are not always present) are:

  - accept
  - accept_charset
  - accept_languages, an array of strings
  - authorization
  - content_type
  - etag
  - if_modified_since, a JavaScript Date object
  - referer

Example:

```javascript
  if ( request.request_method == 'POST' && request.params.a == 42 )
  ...
```

Other headers can be retrieved using the `get_all_headers` method:

```javascript
request.get_all_headers()
→ { "HTTP_ACCEPT_ENCODING": "gzip, deflate",
    "HTTP_CONNECTION": "keep-alive",
    "HTTP_CONTENT_LENGTH": "426",
    "HTTP_ORIGIN": "http://192.168.0.2",
    "PATH_TRANSLATED": "/var/www/html/cb/at_user/hello",
    "REDIRECT_STATUS": "200",
    "SCRIPT_FILENAME": "/var/www/html/thoe/v1",
    "SERVER_NAME": "192.168.0.2",
    "SERVER_PROTOCOL": "HTTP/1.1",
    "SERVER_SOFTWARE": "lighttpd/1.4.45"
}
```

Cookies are not used (state-less server)

POST maximal default size is hard-coded to 1 MB. See `C_THOE_POST_MAX_SIZE`
in __thoe_common.h__, and Masnae configuration item `max_post_size_b`
in `request`.

2 worker threads are created by detected CPU by default.
See `C_THOE_THREADS_PER_CPU` in __thoe_common.h__,
and Masnae configuration item `nb_threads` in `request`.

If a parameter appears several time, an array with all values is created,
and the parameter name is suffixed by $. To guarantee that $ suffix is only
used for that case, parameters ending with $ are forbidden (a 500
"Internal Server Error" is returned).

Example of an object received:

```shell
curl 'http://api.example.com/thoe/v1/ws/js_r/1' \
    -F "a=1" -F "b=2" -F "a=3" \
    -F "alpha=@x.txt;type=text/plain" -F "beta=@x.bin" \
    -H "Expect:" -H 'Authorization: Bearer eVi8ePD'
→ { "status": success, "data": {
    "remote_address": "::ffff:127.0.0.1",
    "request_method": "POST",
    "request_uri":    "/1",
    "params":  { "a$": [ "1", "3" ], "b": "2" },
    "headers": { "authorization": "Bearer eVi8ePD" },
    "attachments": [
      { "name": "alpha", "filename": "x.txt",
        "type": "text/plain", "size": "118" },
      { "name": "beta",  "filename": "x.bin",
        "type": "application/octet-stream", "size": "1252"} ]
  }}
```

### Response sent back

Thoe can send either JSend or raw responses.

request object has the following methods available:

  | Method                                        | Description
  |-----------------------------------------------|---------------
  | respond_header( header:string, value:string ) | adds a header to the HTTP response
  | respond_http_status( status:string )          | set the HTTP response status
  | respond_success( result:object )              | returns { status:"success", data:`result`}
  | respond_error( what:string )                  | returns { status:"error", message:`what`}
  | respond_failed()                              | returns { status:"failed"}
  | respond_rejected( why:string, result:data )   | returns { status:"rejected", reason:`why`, data:`result`}
  | respond_continue( result:data )               | returns { status:"continue", data:`result`}
  | respond_raw( type:string, body:Blob )         | adds HTTP header `Content-Type` with value `type`, and set `body` content as HTTP response

Example:

```javascript
  request.respond_success( {
    name: 'adams',
    age:  42
  } );
  
  request.respond_http_status( '204 No Content' );

  request.respond_header( 'X-User', '123-456' );
```

## http object

http object's methods are used to place HTTP query. HTTP queries are
using cURL. Positive returned codes are cURL's one.

Negative codes are:

  | Code | Description
  |------|-------------
  |  -1  | failed to allocated a cURL handle
  |  -2  | failed to configured cURL handle
  |  -3  | module name not found
  |  -4  | call was successfully done, but failed to retrieve response

HTTP calls come in several flavors, each available through a method:

-----------------------------------------
`raw( module:string, headers:object, body:Blob )` \
is used to POST a custom body: calls `module`, using properties
of `headers` as HTTP headers and `body`'s content as HTTP payload.
`module` can be followed by a document.

```javascript
  var o = http.raw( 'adr_post/pwth/doc', {'Content-Type':'text/plain'}, my_blob );
→ {
    "code": "200",
    "type": "text/plain",
    "body": [Object Blob]
  }
```

-----------------------------------------
`request( module:string, parameters:object, headers:?object, attachments:?Blob[] )` \
is a generic HTTP call with form data: calls `module`, passing
properties of `parameters`.
`module` can be followed by a document.

If a property of `parameters` is an array, the parameter is passed
several time, one per value of the array. E.g.: `{ a:[1,2], b:2 }`
produces `x-www-form-urlencoded` parameters:

```php
  a=1&a=2&b=2
```

`headers` is an object whose properties are headers to add to the request.

`attachments` is an object whose properties are Blobs. The property
names must exist in parameters, and the value in parameters must be
the attachment content types.

```javascript
  var o = http.request( 'module',
                        { x:     1,
                          a:     [ 2, 3 ],
                          f:     'image/png' },
                        { Date:  'Tue, 15 Nov 1994 08:12:31 GMT' },
                        { f:     my_blob } );
→ {
    "code": "200",
    "type": "text/plain",
    "body": [Object Blob]
  }
```

-----------------------------------------
`jresp( module:string, parameters:object, headers:?object, attachments:?Blob[] )` \
same as `request`, but the received response must use JResp format. The function
always returns a JResp object.

```javascript
  var o = http.jresp( "module", { a: 42 } );
→ {
    "status": "success",
    "data":   42
  }
```

```javascript
  var o = http.jresp( "module", { a: 42 } );
→ {
    "status":  "rejected",
    "reason":  "http",
    "data":    404
  }
```

Possible reasons for the rejected status are:

  | Reason        | Description
  |---------------|------------------
  | invalid_utf8  | response is not a valid UTF-8 text
  | invalid_jresp | response is not a valid JResp
  | not_json      | response type is not application/json
  | http          | HTTP error received, HTTP code is set in data

-----------------------------------------
`json( module:string, parameters:object, headers:?object, attachments:?Blob[] )` \
same as `request`, but the received response must use JSON format. Result
is a JResp object with the called module response stored in data.

```javascript
  var o = http.json( "module", { some_id: 42 } );
→ {
    "status":  "success",
    "data": {
        "company": "Google",
        "website": "https://www.google.com/",
        "addressLine1": "111 8th Ave",
        "addressLine2": "4th Floor",
        "state": "NY",
        "city": "New York",
        "zip": "10011"
    }
  }
```

```javascript
  var o = http.jresp( "module", { a: 42 } );
→ {
    "status":  "rejected",
    "reason":  "http",
    "data":    404
  }
```

Possible reasons for the rejected status are:

  | Reason        | Description
  |---------------|------------------
  | invalid_utf8  | response is not a valid UTF-8 text
  | invalid_json  | response is not a valid JSon
  | not_json      | response type is not application/json
  | http          | HTTP error received, HTTP code is set in data

## Blob class

The Blob class is used by request attachments and
HTTP functions to manipulate files.

The object has the following properties:

  | Name      | Description
  |-----------|------------------
  | size      | size in bytes of the Blob

and functions:

  | Name                                          | Description
  |-----------------------------------------------|------------------
  | at( index:integer )                           | returns the element at position `index` (first has index 0)
  | fromFile( name:string )                       | initialize the Blob with the content of the given file name, returns the object
  | set( text:string, encoding:?string )          | initialize the Blob with the given string, eventually encoded using `encoding`, returns the object
  | toArrayBuffer()                               | returns the Blob data as an Uint8Array
  | toFile( name:string )                         | save the Blob to the given file name
  | toString( encoding:?string, padded:?boolean ) | returns the Blob as a string encoded using `encoding`, eventually `padded` (default false). If not encoded, data must be n UTF-8 string or an error will be generated

Where `encoding` is one of the following encoding (default 'none'):

    none, base16, base32, base32hex, base64, base64url, base64_xx

Examples:

```javascript
  var b = new Blob;
  b.set( 'Hello, world!' );
  http.raw( 'adr_post', {'Content-Type':'text/plain'}, b );

  var text_received = request.attachments[0].blob.toString();
```

## system object

The system object offers the following methods:

  - cli
  - disable_log
  - exit
  - log
  - usid
  - xml2json

-----------------------------------------
`cli()` \
returns true if the script is executed from the CLI in simulation mode.

-----------------------------------------
`disable_log()` \
disable file logging for the current request

-----------------------------------------
`exit()` \
used to terminate a script execution.

-----------------------------------------
`log( level:string, ... )` \
adds a log line to the current transaction processing.
Syntax is  where `level` is the log level and must be one of:

  - **R** regular
  - **V** verbose
  - **D** debug

All objects passed after `level` will be JSONified.

```javascript
  var obj = { a: 1, b: 't' };
  system.log('R', 'entering, obj is: ', obj);
```

```text
→ 15:45:12.161658 R entering, obj is: ,{
    "a": 1,
    "b": "t"
  }
```

-----------------------------------------
`usid()` \
returns the current response id. See Masnae `ProcessingResponse`
response id, in `Request` handler.

-----------------------------------------
`xml2json( xml:string )` \
converts an XML document into JSON and returns it as a JavaScript
object. Returns `null` if the XML is malformed or not encoded in UTF-8.

The conversion conventions used are described here:

    <https://www.xml.com/pub/a/2006/05/31/converting-between-xml-and-json.html>

-----------------------------------------

## Amount class

The Amount class is used to represent an amount (a value with a currency).

The object has the following properties (both static and dynamic) used
in format and round methods:

  | Name                   | Value
  |------------------------|-------
  | grouping_none          | 0
  | grouping_thousand      | 3
  | grouping_myriad        | 4
  | grouping_Indian        | 7
  | currency_none          | 0
  | currency_code_before   | 1
  | currency_code_after    | 2
  | currency_symbol_before | 3
  | currency_symbol_after  | 4
  | rounding_up            | 1
  | rounding_down          | 2
  | rounding_commercial    | 3

and methods :
  
  - compareTo
  - currency, value, format
  - set
  - add, div, mul, percent, round, sub

-----------------------------------------
`value()` \
returns the value as a string.

-----------------------------------------
`set( value:string, currency:string )` \
set the `value` and `currency` in the object, returns the object.

-----------------------------------------
`mul( value:string )` \
returns a new object, amount multiplied by `value`.

-----------------------------------------
`div( value:string )` \
returns a new object, amount divided by `value`.

-----------------------------------------
`percent( percent:string )` \
returns a new object, percent of the amount.

-----------------------------------------
`round( mode:in32, decimals:int32 )` \
returns a new object, amount with decimals rounded to:

  - `mode`: one of the rounding mode, see above
  - number of `decimals`: 0 to 5

-----------------------------------------
`currency()` \
returns the currency as a string.

-----------------------------------------
`add( b:Amount )` \
returns a new object, `b` added to amount.

-----------------------------------------
`sub( b:Amount )` \
returns a new object, `b` subtracted from amount.

-----------------------------------------
`format( precision:?int32, dec_sep:?string, grp_mode:/int32, grp_sep:?string, cur_mode:?int32, cur_sep:?string )` \
formats the amount as follow:

  | Name       | Default                 | Description
  |------------|-------------------------|----------------------
  | precision  | 32                      | precision: decimal to keep (truncated)
  | dec_sep    | '.'                     | decimal separator
  | grp_mode   | grouping_thousand (3)   | one of the grouping mode, see above
  | grp_sep    | ' '                     | group separator
  | cur_mode   | currency_code_after (2) | one of the currency mode, see above
  | cur_sep    | ' '                     | currency separator

-----------------------------------------
`compareTo( b:Amount )` \
returns:

  - -1 if `b` < amount
  - 0 if `b` = amount
  - +1 if `b` > amount

-----------------------------------------

Examples:

```javascript
  let a = new Amount( '15','EUR' );
  let b = new Amount( '5','EUR' ).add( a ).mul( '2' );  // 40 EUR

  let a = new Amount( '654321.1234', 'USD' );
  system.log( 'R', a.format( 3,                        '.',
                             a.grouping_myriad,        ',',
                             a.currency_symbol_before, ''   ) ); // '$65,4321.123'

  let a = new Amount( '10', 'EUR' )
          .div( '3' )
          .round( Amount.rounding_up, 2 );  // 3.34 EUR
```

On error (overflow, incompatible currencies...) an exception is raised:

```javascript
  try {
      let a = new Amount( '15','EUR' );
      let b = new Amount( '5','USD' ).add( a );

      system.log('R','b='+b.value()+' '+b.currency());
  } catch ( e ) {
      system.log('R', e + ' ' + e.fileName + ':' + e.lineNumber);
  }
```

## crypto object

The crypto object offers the following methods:

  - digest
  - hmac
  - pbkdf2
  - cipher
  - random
  - equal

Available digest algorithms are:

```text
  blake2b512        blake2s256        gost              md4
  md5               rmd160            sha1              sha224
  sha256            sha384            sha512
```

Available cipher algorithms are:

```text
  aes-128-cbc       aes-128-ecb       aes-192-cbc       aes-192-ecb
  aes-256-cbc       aes-256-ecb       base64            bf
  bf-cbc            bf-cfb            bf-ecb            bf-ofb
  camellia-128-cbc  camellia-128-ecb  camellia-192-cbc  camellia-192-ecb
  camellia-256-cbc  camellia-256-ecb  cast              cast-cbc
  cast5-cbc         cast5-cfb         cast5-ecb         cast5-ofb
  des               des-cbc           des-cfb           des-ecb
  des-ede           des-ede-cbc       des-ede-cfb       des-ede-ofb
  des-ede3          des-ede3-cbc      des-ede3-cfb      des-ede3-ofb
  des-ofb           des3              desx              rc2
  rc2-40-cbc        rc2-64-cbc        rc2-cbc           rc2-cfb
  rc2-ecb           rc2-ofb           rc4               rc4-40
  seed              seed-cbc          seed-cfb          seed-ecb
  seed-ofb
```

Both lists can be obtained by executing `openssl help` command.

-----------------------------------------
`digest( algorithm:string, data:Blob )` \
returns the digest of `data` using the given `algorithm` as a new Blob.

```javascript
  let digest = crypto.digest( 'md5', data );
```

-----------------------------------------
`hmac( algorithm:string, key:Blob, data:Blob )` \
returns the HMAC message authentication of `data` using `key`,
with given `algorithm`.

```javascript
  let digest = crypto.hmac( 'md5', key, data );
```

-----------------------------------------
`pbkdf2( algorithm:string, password:Blob, salt:Blob, iterations:?int32, size:?int32 )` \
calculates the derived password using pbkdf2. Returns a new Blob
using given `algorithm`, of `password` with `salt`, `iterations`
and a requested `size` in bytes.

`iterations` must be between 1 and 1000000000 (default: 100000), `size` between 1 and 1000 (default: 32). Output size requested should not be larger than the algorithm used:

  - SHA-1 20 bytes
  - SHA-224 28 bytes
  - SHA-256 32 bytes
  - SHA-384 48 bytes
  - SHA-512 64 bytes

```javascript
  let digest = crypto.pbkdf2( 'md5', password, salt );
```

-----------------------------------------
`cipher( algorithm:string, key:Blob, iv:Blob, data:Blob, encrypt:boolean, padded:?boolean )` \
encrypts or decrypts `data` using `key` and initialization vector `iv` with given
`algorithm`. Set `encrypt` to true to encrypt, to false to decrypt.
`padded` default is true.

```javascript
  let encrypted_data = crypto.cipher( 'aes-128-cbc', key, iv, data, true );
```

-----------------------------------------
`random( size:int32 )` \
returns a new Blob filled with cryptographically strong pseudo-random data.
Fills the Blob with `size` bytes (between 1 and 10000000).

```javascript
  let iv = crypto.random( 16 );
```

-----------------------------------------
`equal( string1:string, string2:string )` \
compares two strings in a constant time maner. Returns
true if they are equal.

```javascript
  if ( crypto.equal( user_hash, calculated_hash ) ) ...
```

-----------------------------------------

## MySQL class

The MySQL class class gives access to configured database.

By default a reader connection is allocated. To have a writer connection,
pass `true` to the constructore:

```javascript
  var db = new MySQL( true );
```

The object has the following functions:

  | Name                      | Returns
  |---------------------------|------------------
  | do_insert( query:string ) | { result: , id:       }
  | do_update( query:string ) | { result: , affected: }
  | do_select( query:string ) | { result: , data:     }
  | do_delete( query:string ) | { result: , affected: }
  | release                   | -
  | sql_escape( text:string ) | escaped string

The result codes are those of the database interface of Masnae.

`release` is not strictly necessary but since JavaScript garbage
collection can be triggered well after the object becomes out
of scope, and since database connection are scare resources,
it is a goog idea to release them as soon as possible.

```javascript
  var db = new MySQL;
  //
  let { result, data } = db.do_select( 'select name,age from tbl where age >= 18' );
  if ( result == 0 )
    data.forEach(...
```

# Deployment

## lighttpd

Start by installing lighttpd and creating the folders that will contains the
Thoe binary, config and scripts.

```shell
  sudo apt-get install lighttpd
  sudo lighttpd-enable-mod fastcgi
  sudo mkdir /var/www/cgi-bin /var/www/cgi-config /var/www/cgi-scripts
  sudo chown root.www-data /var/www/cgi-*
  sudo chmod 750           /var/www/cgi-*
```

Then edit the fastcgi config file `/etc/lighttpd/conf-enabled/10-fastcgi.conf`.
Add the following section, that configure Thoe module:

```perl
  fastcgi.server += ( "/thoe/v1" =>
    ((
        "bin-path"    => "/var/www/cgi-bin/thoe.fcgi",
        "socket"      => "/var/run/lighttpd/thoe.socket",
        "check-local" => "disable",
        "bin-environment" => (
              "THOE_CONFIG" => "/var/www/cgi-config/thoe.json"
        )
    ))
  )
```

Create the module config file `/var/www/cgi-config/thoe.json`, for example:

```json
  {
    "spidermonkey": {
      "max_memory_mb":  16
    },
    "scripts": {
      "folder": "/var/www/cgi-scripts/"
    },
    "request" : {
      "uid_type": "uid_short_ts"
    },
    "http" : {
      "verbose": false,
      "modules": {
        "ws1":  { "url": "http://api.example.com/module/v1/" }
      }
    }
  }
```

Copy your module binary in `/var/www/cgi-bin/` and scripts in `/var/www/cgi-scripts/`.

# Extending JavaScript

To find information on SpiderMonkey engine, refer to:
- <http://jsclass.jcoglan.com/classes.html>
- <http://www.programering.com/a/MzM5UzNwATk.html>
- <https://aoeex.com/phile/coding-with-spidermonkey/part4>
- <https://developer.mozilla.org/en-US/docs/Mozilla/Projects/SpiderMonkey/JSAPI_Cookbook>
- <https://developer.mozilla.org/en-US/docs/Mozilla/Projects/SpiderMonkey/JSAPI_User_Guide>
- <https://github.com/mongodb/eng-blog-examples/blob/master/spider-monkey-integration/example_type_embedding.cpp>
- <https://github.com/mozilla/positron/blob/master/js/xpconnect/loader/mozJSSubScriptLoader.cpp>
- <https://github.com/RipcordSoftware/libjsapi/blob/master/src/libjsapi/dynamic_object.cpp>

# License

Copyright 2018 Pierre del Perugia

[Apache license v2](https://www.apache.org/licenses/LICENSE-2.0.txt)
