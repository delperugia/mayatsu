
    Selecting a JavaScript library

The two main existing JavaScript libraries available for C++ are 
Google V8 and Mozilla Spidermonkey, used respectively in Chrome
and Firefox.

V8 seems to be ~50 times faster but the version shipped with Ubuntu 18.04
is a very old one (3.14) coming with Gnome or similar. Compiling a new one
is non trivial.

V8 compiles by default with clang, and when compiling with gcc, they
only validate with gcc 4.8. So their code produce warning with gcc 7,
and they treat warning as error.

Since the overall performances are still largely acceptable
(3000 vs 4726), the effort does not seems justified.
