# Admete

# Table of Contents

  - [About](#about)
  - [Configuration](#configuration)
    - [Timezone](#timezone)
  - [Web services](#web-services)
    - [Types and categories](#types-and-categories)
  - [Deployment](#deployment)
    - [Initial AccessToken](#initial-accesstoken)
    - [lighttpd](#lighttpd)
  - [Trial balance](#trial-balance)
  - [Accounting closing](#accounting-closing)
  - [License](#license)

# About

Admete is an accounting module, using double entry accounting.
Accounts can be created in multiple currencies. It supports
reservations and reversal.

# Configuration

Default configuration file is `/var/www/cgi-config/admete.json`.
This can be overloaded either using the environment variable
`ADMETE_CONFIG` or the command line option -f.

Beside Masnae configuration items, Admete uses:

```json
  "account" : {
    "key"    : "luhn",
    "length" : 10,
    "prefix" : "1"
  },
```

The available keys are:

  | Name            | Description                                                                   | Default
  |-----------------|-------------------------------------------------------------------------------|--------
  | /account/key    | check key at the end of the account number, one of: none, luhn, mod97,damm    | none
  | /account/length | account number length (without key), prefix + length must not exceed 19 digits| 9
  | /account/prefix | default prefix digits, must not start with 0, maximum 17 digits               | 1
  | /reservation/cleaning_probability | probability that old reservations are deleted when creating new ones. A maximum of 2*cleaning_probability are deleted (range: 1-10000). | 100

## Timezone

Timezone must be configured in MySQL and kept up-to-date.
This can be done using a command line like:

```shell
mysql_tzinfo_to_sql /usr/share/zoneinfo | mysql -u root mysql
```

# Web services

The API's functions are protected by the following permissions.
Caller identification is done using the AccessToken received in
the Authorization header (Bearer type).

  | Document               | Permission                | Permission's parameter
  |------------------------|---------------------------|-----------------------
  | /ws/account/create     | admete.account_create     |
  | /ws/account/delete     | admete.account_delete     |
  | /ws/account/ledger     | admete.account_ledger     |
  | /ws/account/select     | admete.account_select     |
  | /ws/account/usage      | admete.account_usage      |
  | /ws/exchange           | admete.exchange           |
  | /ws/reservation/create | admete.reservation_create |
  | /ws/reservation/delete | admete.reservation_delete |
  | /ws/reservation/select | admete.reservation_select |
  | /ws/reverse            | admete.reverse            |
  | /ws/transfer           | admete.transfer           |

## /ws/account/create
Create a new account.

Parameters:
  - `currency`  
    3 letters currency code: EUR, USD, JPY, GBP,...
  - `overdraft`  
    Minimum balance allowed. Usually a negative value. Default 0.
  - `ceiling`  
    Maximum balance allowed. Usually a positive value. Default 10000000000.

Returns JResp results:
  - success,    data: {"account":"11","ceiling":"10000000000","overdraft":"0"}
  - error
  - failed
  - rejected:
    - prefix_length

## /ws/account/delete
Delete accounts. Their balance must be null and they must
not be present in t_posting.

Parameters:
  - `accounts`  
    csv of accounts to delete

Returns JResp results:
  - success
  - error
  - failed
  - rejected:
    - not_zero,   data: account
    - used

## /ws/account/ledger
Search and return accounts. Use the same syntax as the /mgt/table/
syntax, with parameters:
  - `fields`
  - `filter`
  - `order`
  - `range`

Available fields in `fields` and `filter` expressions are:

    k_account, k_journal, k_post, d_date, f_amount, s_currency, k_journal_reversal , s_type, s_category, s_narration, s_reference

Returns JResp results:
  - see /mgt/table/

## /ws/account/select
Search and return accounts. Use the same syntax as the /mgt/table/
syntax, with parameters:
  - `fields`
  - `filter`
  - `order`
  - `range`

Available fields in `fields` and `filter` expressions are:

    k_account, f_balance, s_currency, f_overdraft , f_ceiling

Returns JResp results:
  - see /mgt/table/

## /ws/account/usage
Returns the number and sum of transaction made by an account
the requested periods of time. Results for a period include
results of the nested periods
(e.g.: using `1,7,30`, 7 includes 1, 30 includes 7 and 1).

Parameters:
  - `mode`  
    one of: negative, positive, sum, sum_absolute
  - `account`  
    account number
  - `periods`  
    CSV of periods, in days (e.g.: 1,7,30)
  - `timezone`  
    user's timezone to define 'day' (e.g.: Europe/Paris)
  - `type`  
    optional, the journal type to count
  - `category`  
    optional, the journal category to count

Returns JResp results:
  - success,    data: { "1":{"amount":"3","count":"1","currency":"EUR"},"7":{"amount":"3","count":"1","currency":"EUR"}}
  - error
  - failed
  - rejected:
    - invalid_mode

## /ws/exchange

Records a double list of operations on accounts, one per currency.
The sum of all amounts in each list must be null, all accounts must use
the same currency as the transaction.
The final balance of each account must be in the overdraft / ceiling range.

Parameters:
  - `records_1` and `records_2`  
    JSON arrays of objects, one per currency. Each object describe
    an operation (a credit or debit on an account) and has the following
    members:
    - a:   the account number
    - m:   the amount (positive or negative)
    - r:   optional, the reservation id

    E.g.:

    ```json
        [ {"a":"10000001","m":"-2"},
          {"a":"10000002","m":"1"},
          {"a":"10000003","m":"1"} ]
    ```

    ```json
        [ {"a":"10000001","m":"-2","r":"849318"},
          {"a":"10000002","m":"1"},
          {"a":"10000003","m":"1"} ]
    ```

  - `currency_1` and `currency_2`  
    Three characters codes of each currency
  - `type`  
    optional, the transaction type
  - `categoty`  
    optional, the transaction categoty
  - `narration`  
    optional, a description of the transaction
  - `reference`  
    optional, a unique reference, typically a UUID

Returns JResp results:
  - success,    data: {"date":"2019-02-07T13:54:36","sequence":"60-3","transaction":"439"}
  - failed
  - error
  - rejected:
    - overflow,               data: account
    - invalid_currency
    - not_zero
    - reservation_credited,   data: reservation
    - overdraft,              data: account
    - ceiling,                data: account
    - incompatible_currency,  data: account
    - not_found,              data: account
    - reservation_not_found,  data: reservation
    - reservation_too_low,    data: reservation
    - invalid_value,          data: amount
    - duplicate_reference

## /ws/reservation/create
Create a new reservation on an account.

Parameters:
  - `account`  
    the account id
  - `limit`  
    amount to reserve
  - `lifetime`  
    reservation duration in seconds
  - `narration`  
    description of the reservation

Returns JResp results:
  - success,    data: {"expiration":"2019-03-01T07:07:56","reservation":"kihL0o"}
  - error
  - failed
  - rejected:
    - incompatible_currency   account
    - overdraft               account
    - ceiling                 account
    - not_found
    - not_positive
    - invalid_precision       limit
    - invalid_value           limit
    - overflow                limit / account
    - invalid_lifetime

## /ws/reservation/delete
Delete reservations. Use the same syntax as the /mgt/table/
syntax, with parameters:
  - `filter`
  - `order`
  - `range`

Available fields in `filter` expressions are:

    k_account, k_reservation, f_amount, d_expiration, s_narration

Returns JResp results:
  - see /mgt/table/
  
## /ws/reservation/select
Search and return reservations. Use the same syntax as the /mgt/table/
syntax, with parameters:
  - `fields`
  - `filter`
  - `order`
  - `range`

Available fields in `fields` and `filter` expressions are:

    k_account, k_reservation, f_amount, d_expiration, s_narration

Returns JResp results:
  - see /mgt/table/

## /ws/reverse
Cancel a transaction by creation a new transaction
with the same records but negated amount.
A transaction can only be reversed once.

Parameters:
  - `journal`  
    journal id to reverse
  - `reference`  
    optional, a unique reference, typically a UUID

Returns JResp results:
  - success,    data: {"date":"2019-02-07T13:54:36","sequence":"60-3","transaction":"439"}
  - failed
  - error
  - rejected:
    - not_found
    - already_reversed,   data: journal
    - overflow,           data: account
    - overdraft,          data: account
    - ceiling,            data: account
    - duplicate_reference

Note: reversal transaction have the same type, category and narration as
the reversed operation.

## /ws/transfer
Records a list of operations on accounts. The sum of all amounts must
be null, all accounts must use the same currency as the transaction.
The final balance of each account must be in the overdraft / ceiling range.

Parameters:
  - `records`  
    A JSON array of objects. Each object describe an operation
    (a credit or debit on an account) and has the following
    members:
    - a:   the account number
    - m:   the amount (positive or negative)
    - r:   optional, the reservation id

    E.g.:

    ```json
        [ {"a":"10000001","m":"-2"},
          {"a":"10000002","m":"1"},
          {"a":"10000003","m":"1"} ]
    ```

    ```json
        [ {"a":"10000001","m":"-2","r":"849318"},
          {"a":"10000002","m":"1"},
          {"a":"10000003","m":"1"} ]
    ```

  - `currency`  
    A three characters code
  - `type`  
    optional, the transaction type
  - `categoty`  
    optional, the transaction categoty
  - `narration`  
    optional, a description of the transaction
  - `reference`  
    optional, a unique reference, typically a UUID

Returns JResp results:
  - success,    data: {"date":"2019-02-07T13:54:36","sequence":"60-3","transaction":"439"}
  - failed
  - error
  - rejected:
    - overflow,               data: account
    - invalid_currency
    - not_zero
    - reservation_credited,   data: reservation
    - overdraft,              data: account
    - ceiling,                data: account
    - incompatible_currency,  data: account
    - not_found,              data: account
    - reservation_not_found,  data: reservation
    - reservation_too_low,    data: reservation
    - invalid_value,          data: amount
    - duplicate_reference

## Types and categories

Types and categories are not predefined. Admete uses the following:

  - type:     `closing`
  - category: `system`

Suggested types:
  - deposit, withdrawal, transfer, external_transfer, repayment, interest, payment...

For payment categories you can use plain labels like `transport`,
`food`, `sports`... or for example the Merchant Category Code
(MCC, ISO 18245).

# Deployment

## Sequence

An initial rows must be inserted into the t_sequencer table:

```sql
INSERT INTO t_sequencer () VALUES ();
```

## lighttpd

Start by installing lighttpd and creating the folders that will contains the
Admete binary and config.

```shell
  sudo apt-get install lighttpd
  sudo lighttpd-enable-mod fastcgi
  sudo mkdir /var/www/cgi-bin /var/www/cgi-config
  sudo chown root.www-data /var/www/cgi-*
  sudo chmod 750           /var/www/cgi-*
```

Then edit the fastcgi config file `/etc/lighttpd/conf-enabled/10-fastcgi.conf`.
Add the following section, that configure Admete module:

```perl
  fastcgi.server += ( "/admete/v1" =>
    ((
        "bin-path"    => "/var/www/cgi-bin/admete.fcgi",
        "socket"      => "/var/run/lighttpd/admete.socket",
        "check-local" => "disable",
        "bin-environment" => (
              "ADMETE_CONFIG" => "/var/www/cgi-config/admete.json"
        )
    ))
  )
```

Create the module config file `/var/www/cgi-config/admete.json`, for example:

```json
  {
    "mysql" : {
      "user":   "admete", "password": "123456",
      "schema": "admete",
      "reader": { "host": "localhost",
                  "unix_socket": "/var/run/mysqld/mysqld.sock" },
      "writer": { "host": "localhost",
                  "unix_socket": "/var/run/mysqld/mysqld.sock" }
    }
  }
```

Copy your module binary in `/var/www/cgi-bin/`.

# Trial balance

The database integrity can be checked by running the following
SQL queries:

Sum of accounts, per currency, must be zero:

```sql
    SELECT s_currency, SUM(f_balance) as balance
    FROM t_accounts
    GROUP BY s_currency
    HAVING balance != 0
```

Sum of each transaction must be zero:

```sql
    SELECT k_journal, s_currency, SUM(f_amount) AS balance
    FROM t_posting
    GROUP BY k_journal, s_currency
    HAVING balance != 0;
```

Each account must be within its limits:

```sql
    SELECT k_account, f_balance, f_overdraft, f_ceiling
    FROM t_accounts
    WHERE f_balance < f_overdraft OR f_balance > f_ceiling;
```

Each accounts' balance must match the sum of its posting:

```sql
    SELECT p.k_account, a.f_balance, SUM(f_amount) AS balance
    FROM t_posting p
    JOIN t_accounts a ON a.k_account=p.k_account
    GROUP BY k_account
    HAVING f_balance != balance;
```

It is possible to execute these commands by running `admete.fcgi`
with the `-i` option:

```shell
    ./admete.fcgi -f ./admete_validate_config.json -i
```

This produces a report like this one:

```text
    Admete[31639] [inf] Admete starting, 1.5-0
    Admete[31639] [inf] Config monitoring file: ./admete_validate_config.json
    Admete[31639] [inf] Config: configuration updated
    Admete[31639] [inf] MySQL new connection 94461195202752
    Starting trial balance integrity check
        2019-02-12T14:22:00+02:00 starting
        2019-02-12T14:22:00+01:00 sum_of_accounts:     ok
        2019-02-12T14:22:00+01:00 sum_of_transactions: ok
        2019-02-12T14:22:00+01:00 account_limits:      ok
        2019-02-14T15:13:40+01:00 account_match_posting: 2 error(s)
          account 10000301, -6.10000000 <> 16.00000000
          account 10000302, 4.05000000 <> -16.00000000
    Trial balance finished
    Admete[31639] [inf] Config thread_monitoring stopped
```

If a problem is detected the exit code is `EXIT_FAILURE` (1).
If not, `EXIT_SUCCESS` (0).

# Accounting closing

Old accounting periods can be removed from the database
using the command line option `-p`:

```shell
    ./admete.fcgi -f ./admete_validate_config.json -p 201902
```

This will remove all journals and posting equal or prior to
the given period. A new journal entry is added per currency,
with posting for accounts with non-null balances.

Closing transaction have the type 'closing' and category 'system'.
Narration contains the closed period.

This produces a report like this one:

```text
    Admete[27302] [inf] Admete starting, 1.5-0
    Admete[27302] [inf] Config monitoring file: ./admete_validate_config.json
    Admete[27302] [inf] Config: configuration updated
    Admete[27302] [inf] MySQL new connection 93934102073536
    Starting period closing 201902
        2019-02-16T17:37:56+01:00 starting transaction
        2019-02-16T17:37:56+01:00 5 accounts to forward
        2019-02-16T17:37:56+01:00 currencies: EUR,USD
        2019-02-16T17:37:56+01:00 5 postings deleted
        2019-02-16T17:37:56+01:00 2 journals deleted
        2019-02-16T17:37:56+01:00 EUR: journal 102799, posting 307558
        2019-02-16T17:37:56+01:00 3 new posting inserted for EUR
        2019-02-16T17:37:56+01:00 USD: journal 102800, posting 307561
        2019-02-16T17:37:56+01:00 2 new posting inserted for USD
        2019-02-16T17:37:56+01:00 temporary table deleted
        2019-02-16T17:37:56+01:00 transaction commited
    Period closing finished
    Admete[27302] [inf] Config thread_monitoring stopped
```

If a problem is detected the exit code is `EXIT_FAILURE` (1).
If not, `EXIT_SUCCESS` (0).

During the execution of this command, transactions are not possible
(journal and posting tables are locked). If these tables are huge,
this may take several minutes.

# License

Copyright 2019 Pierre del Perugia

[Apache license v2](https://www.apache.org/licenses/LICENSE-2.0.txt)
