# Urania 

# Table of Contents

  - [About](#about)
  - [Configuration](#configuration)
  - [Tables](#tables)
  - [Web services](#web-services)
  - [OAuth](#oauth)
    - [Authorization Code Grant](#authorization-code-grant)
    - [Resource Owner Password Credentials Grant](#resource-owner-password-credentials-grant)
    - [Implicit Grant](#implicit-grant)
    - [Refreshing](#refreshing)
    - [Remember Me](#remember-me)
  - [Deployment](#deployment)
    - [Initial AccessToken](#initial-accesstoken)
    - [lighttpd](#lighttpd)
  - [Futur](#futur)
  - [License](#license)

# About

Urania is in charge of authenticating users, managing roles
and permissions, and delivering and controlling AccessTokens.

# Configuration

Default configuration file is `/var/www/cgi-config/urania.json`.
This can be overloaded either using the environment variable
`URANIA_CONFIG` or the command line option -f.

Beside Masnae configuration items, Urania uses:

```json
  "accesstoken" : {
    "cleaning_probability" : 100
  },
  "http" : {
    "modules" : {
      "google": { "url": "https://www.google.com/" }
    }
  },
  "oauth" : {
      "login_page" : "/var/www/cgi-config/urania_login_en.html",
	  "captcha" : {
        "mode" :          "google",
        "google_secret" : "***"
	}
  },
  "password" : {
    "score_minimal" : 3
  }
```

The available keys are:

  | Name                              | Description                                                      | Default
  |-----------------------------------|------------------------------------------------------------------|--------
  | /accesstoken/cleaning_probability | probability that old AccesToken are deleted when calling signin. A maximum of 2*cleaning_probability are deleted (range: 1-10000). | 100
  | /password/otp_length              | lost password OTP length, range 4 to 9                           | 6
  | /password/otp_validity            | lost password OTP validity in seconds                            | 120
  | /password/score_minimal           | minimum password complexity, range 0 (very bad) to 5 (very good) | 3
  | /oauth/accesstoken_lifetime       | generated access token life time in seconds, minimum 1           | 3600
  | /oauth/refreshtoken_lifetime      | generated refresh token life time in seconds, minimum 1          | 86400
  | /oauth/cleaning_probability       | probability that authorize request delete old sessions/codes, in seconds, range 1 to 10000 | 10
  | /oauth/code_lifetime              | oauth code life time in seconds, range 1 to 3600                 | 10
  | /oauth/login_page                 | login page template                                              | /var/www/cgi-config/urania_login_en.html
  | /oauth/session_lifetime           | login session lifetime in seconds, range 1 to 3600               | 120
  | /oauth/usage_maximum              | maximum login sessions per client, range 1 to 10000              | 100
  | /oauth/captcha/mode               | CAPTCHA used in login page (none, google)                        | none
  | /oauth/captcha/google_secret      | if using Google reCAPTCHA, secret key                            |
  | /oauth/captcha/google_hostname    | if using Google reCAPTCHA, hostname to verify; optional          |
  | /oauth/captcha/google_action      | if using Google reCAPTCHA v3, action to verify; optional         |
  | /oauth/captcha/google_score       | if using Google reCAPTCHA v3, minimal required score             | 0.5

Note: if using captcha mode `google` the HTTP module `google` must be set.
reCAPTCHA v2 and v3 are supported. If using v3, the reCAPTCHA response
must be stored in a field named `g-recaptcha-response`, e.g.:

```html
   <input type="hidden" name="g-recaptcha-response" id="g-recaptcha-response" />
```

# Tables

Urania uses a role based access control model. Any number
of permissions can be associated to a role, each permission
has a parameter. Permissions are not defined in Urania itself,
but by the applications using Urania.

Users have a profile. A profile defines several roles. Users
have one of his profile's roles active at an instant. The
role active is selected by conditions defined in the profile.

User can have a password and several access tokens.

The following tables are manageable using the '/mgt/tables' API:

## t_roles
 List roles. Roles can inherit permissions from another role.

## t_roles_permissions
 List permissions associated to a role, each with given parameters.
 Permissions are defined by the module itself, not in the database.

## t_profiles
 List available profiles. A profile is a set of roles, only one
 of them active at a time depending on external conditions.
 Each profile defines the maximum password age.

## t_profiles_roles
 List possible roles in a given profile. The active role within
 a profile is selected using the condition.

## t_users
 List users. Each user has a profile and an active role within
 this profile.

## t_groups
 Groups of users. Each user can be in several groups. Group
 names are defined by the module itself, not in the database.

## t_oauth
 Users acting like OAuth client and their configuration

The following tables are only accessible using the `/oauth/`
and `/ws/` API:

 - t_accesstokens, t_passwords

# Web services

The API's functions are protected by the following permissions.
Caller identification is done using the AccessToken received in
the Authorization header (Bearer type).

  | Document                 | Quieted | Permission              | Permission's parameter
  |--------------------------|---------|-------------------------|-----------------------
  | /ws/signout              | n       | urania.signout          |
  | /ws/permissions          | y       | -                       |
  | /ws/password_change      | n       | urania.password_change  |
  | /ws/password_lost_1      | n       | urania.password_lost_1  | allowed profiles
  | /ws/password_lost_2      | n       | urania.password_lost_2  | allowed profiles
  | /ws/password_score       | y       | -                       |
  | /ws/password_set         | n       | urania.password_set     | allowed profiles
  | /ws/password_need_change | n       | -                       |
  | /ws/children             | n       | -                       |
  | /ws/lineage              | n       | -                       |
  | /ws/siblings             | n       | -                       |

## /ws/signout
Delete current AccesToken and the corresponding RefreshToken.

Parameters: -

Returns JResp results:
  - success
  - error
  - failed
  - rejected:
    - forbidden

## /ws/permissions
Returns the permissions of the current AccessToken.

Parameters: -

Returns JResp results:
  - success,    data:  { "user": "1",  "profile": "web",  "role": "web",  
                         "permissions": { "dummy":"un",  
                                          "urania.password_set":"" } }
  - error
  - failed
  - rejected:
    - forbidden

## /ws/password_change
Change the current user's password.
All existing user's AccessTokens except the one used to call this function are disabled.

Parameters:
  - `password`  
    user's current password
  - `password_new`  
    user's new password

Returns JResp results:
  - success
  - error
  - failed
  - rejected:
    - no_password
    - forbidden
    - weak_password
    - banned,           data: { "expiration": 3 }

## /ws/password_lost_1
Start the password recovery process: generate an OTP

The user's profile must be allowed in the permission's parameter.
Field `s_parameters` is a csv of profiles that are allowed (e.g.: 
"web,user").

Parameters:
  - `user`  
    user's identifier

Returns JResp results:
  - success,            data: { "otp": "123456" }
  - error
  - failed
  - rejected:
    - no_password
    - forbidden
    - banned,           data: { "expiration": 3 }

## /ws/password_lost_2
Complete the password recovery process: set password

The user's profile must be allowed in the permission's parameter.
Field `s_parameters` is a csv of profiles that are allowed (e.g.: 
"web,user").

All existing user's AccessTokens are disabled.

Parameters:
  - `user`  
    user's identifier
  - `otp`  
    OTP generated during first step
  - `password`  
    new user's password

Returns JResp results:
  - success
  - error
  - failed
  - rejected:
    - no_password
    - forbidden
    - weak_password     (nb: OTP remains valid)
    - banned,           data: { "expiration": 3 }

## /ws/password_score
Return a password score (and minimal required value). Score
ranges from 0 (worst) to 5 (best). `minimal` is the value
of `score_minimal` in the configuration file.

Score is based on password entropy estimation (in bits):

  | Bits | Score
  |------|:------:
  | < 16 |   0
  | < 28 |   1
  | < 40 |   2
  | < 52 |   3
  | < 64 |   4
  | ≥ 64 |   5

Parameters:
  - `password`  
    password to evaluate

Returns JResp results:
  - success,        data: { "score": 2, "minimal": 4 }
  - error

## /ws/password_set
Forcefully set a user's password.

The user's profile must be allowed in the permission's parameter.
Field `s_parameters` is a csv of profiles that are allowed (e.g.: 
"web,user").

All existing user's AccessTokens are disabled.

Parameters:
  - `user`  
    user's identifier
  - `password`  
    user's password

Returns JResp results:
  - success
  - error
  - failed
  - rejected:
    - no_password
    - forbidden
    - weak_password

## /ws/password_need_change
Check if a user's password is expired or uses an old hash

Parameters:
  - `user`  
    user's identifier

Returns JResp results:
  - success,        data: { "need_change": true  }
  - error
  - failed
  - rejected:
    - no_password

## /ws/family_children
Returns all children of a user as an array of user id:

Parameters: -

Returns JResp results:
  - success,        data: ["4","5","6"]
  - error
  - failed

## /ws/family_lineage
Returns all ancestors of a user (including himself)

Parameters: -

Returns JResp results:
  - success,        data: ["7","4","3"]
  - error
  - failed

## /ws/family_siblings
Returns all siblings of a user (including himself)

Parameters: -

Returns JResp results:
  - success,        data: ["7","8"]
  - error
  - failed

# OAuth

Urania offers an OAuth 2.0 interface to sign in. OAuth clients are regular
users. No permission is required (and none should normally be set) but the user
must be present in the `oauth` table. The fields to set in this table are:

  - `s_name`  
     the client's service name (eventually with HTML entities)
  - `s_logo`  
     the client's logo as an URI
  - `s_allowed_modes`  
     CSV of allowed authentication methods: code, token and password
  - `s_redirect_uri`  
     CSV of allowed callback URI, first one is the default if
     not specified. Simple string comparison is used to match URI.    
  - `b_pkce`  
     boolean forcing clients to use Proof Key for Code Exchange.

`name` and `logo` are displayed on the login page, `name` can contain HTML
entities, but beware of injection. File __urania_login_en.html__ contains
a sample login template. The following tags are replaced:
  - `{{attempt}}`    the login attempt number (0 the first time)
  - `{{client_id}}`  the OAuth client id
  - `{{id}}`         the masnae response id
  - `{{logo}}`       the s_logo field
  - `{{name}}`       the s_name field
  - `{{session_id}}` a hidden system data that must be returned by the form
  - `{{user_id}}`    when re-prompting the password, the previoulsy entered user_id; empty the 1st time
  - `{{feed_back}}`  in case of error, a string giving the reason of the problem, one of:              
    - `banned:X`    user account is locked for another _X_ seconds
    - `captcha`     CAPTCHA validation failed
    - `error`       an error 'server side) occured
    - `timeout`     user too long to answer, session is gone

Also the comment strings "CAPTCHA-->" and "<!--CAPTCHA" are removed
(code is activated) on noninitial login attempts.

Note that for `error` and `timeout`, `name` and `logo` may be empty. 

## Authorization Code Grant

  Key in `s_allowed_modes`: `code`
  
  The authorization endpoint is `/oauth/authorize` and the token
  endpoint is `/oauth/token`. The `access_token` returned is a regular
  AccessToken that can be use with all Web Services.
  
  The token endpoint accepts client identification either using HTTP Basic
  authentication or in `client_id` / `client_secret` parameters.
  
  If the scope `refresh` is specified, a refresh token is returned.
  
  The `d_last_login` filed is updated.

## Resource Owner Password Credentials Grant

  Key in `s_allowed_modes`: `password`
  
  The refresh access token endpoint is `/oauth/token`.
  
  If the scope `refresh` is specified, a refresh token is returned.

  The `d_last_login` filed is updated.

## Implicit Grant

  Key in `s_allowed_modes`: `token`
  
  The authorization endpoint is `/oauth/authorize`. The `access_token` returned
  is a regular AccessToken that can be use with all Web Services.

  The `d_last_login` filed is updated.

  The `d_last_login` filed is updated.

## Refreshing 

  The refresh access token endpoint is `/oauth/token`. The `scope`
  parameter is not used today and new AccessTokens have the same
  scope as the original one.

  When refreshing an access token, the previous one is invalidated.

  The `d_last_login` filed is not updated.

## Remember Me

The *Remember Me* feature is implemented using refresh tokens, the clients
must thus use the scope `refresh`. Moreover it is only available for
security reason in Authorization Code Grant mode, if using HTTPS.

A call to `/oauth/authorize` will immediately redirect to the client
redirect uri without displaying the login page. A cookie is used in
the user's browser to store the refresh token.

To activate the *Remember Me* feature, add "remember_me=true" to the
call to `/oauth/authorize`:

```
   .../oauth/authorize?response_type=code&remember_me=true&client_id=...
```

# Deployment

## Initial AccessToken

Since an AccessToken is required for all API calls, some system users
must be created manually, along with AccesTokens. It is not necessary
to create passwords. These users usually have a single profile/role.

For example:
  - web and/or smartapp user (OAuth client usually)
  - back-office with urania.password_set, masnae.tables and masnae.caches
  - standard user roles with urania.signout and urania.password_change

## lighttpd

Start by installing lighttpd and creating the folders that will contains the
Urania binary and config.

```
  sudo apt-get install lighttpd
  sudo lighttpd-enable-mod fastcgi
  sudo mkdir /var/www/cgi-bin /var/www/cgi-config
  sudo chown root.www-data /var/www/cgi-*
  sudo chmod 750           /var/www/cgi-*
```

Then edit the fastcgi config file `/etc/lighttpd/conf-enabled/10-fastcgi.conf`.
Add the following section, that configure Urania module:

```
  fastcgi.server += ( "/urania/v1" =>
    ((
        "bin-path"    => "/var/www/cgi-bin/urania.fcgi",
        "socket"      => "/var/run/lighttpd/urania.socket",
        "check-local" => "disable",
        "bin-environment" => (
              "URANIA_CONFIG" => "/var/www/cgi-config/urania.json"
        )
    ))
  )
```

Because Urania uses in-memory structure for OAuth sessions, a single
instance must be created ("max-procs" => 1). However several servers
can be used (with round robin DNS for example).

Create the module config file `/var/www/cgi-config/urania.json`, for example:

```json
  {
    "mysql" : {
      "user":   "urania", "password": "123456",
      "schema": "urania",
      "reader": { "host": "localhost",
                  "unix_socket": "/var/run/mysqld/mysqld.sock" },
      "writer": { "host": "localhost",
                  "unix_socket": "/var/run/mysqld/mysqld.sock" }
    }
  }
```

Copy your module binary in `/var/www/cgi-bin/`.

# Futur

  - Adding OpenID Connect support
  - Two-factor authentication using:
    - TOTP
    - OTP
    - FIDO
  - Protect Remember-Me cookies by adding:
    - Accept-Language, User-Agent, Referer headers
    - IP location (country at least)

# License

Copyright 2018 Pierre del Perugia

[Apache license v2](https://www.apache.org/licenses/LICENSE-2.0.txt)
