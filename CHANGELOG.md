# Release history

## [1.7-0] 2019-06-24
  - Adding OAuth Proof Key for Code Exchange (PKCE)

## [1.6-1] 2019-05-28
  - Using Adawat 1.6-0
  - Code review

## [1.6-0] 2019-05-11
  - SQL optimizations in Admete
  - New statistics

## [1.5-1] 2019-04-29
  - Quiet mode logging
  - Security code fixes in Urania
  - Urania password expiration and hash reconfiguration
  - Remember Me in Urania login
  - Admete user, reference, type and category in transactions
  - Admete usage and transfer web services
  - New functions in Thoe: system.cli, system.disable_log, Blob.at, Blob.toArrayBuffer

## [1.5-0] 2019-03-01
  - Admete module
  - Database auto-reconnect
  - Multi scopes not recognized in OAuth

## [1.4-0] 2018-12-22
  - Thoe: DB class

## [1.3-0] 2018-12-20
  - Masnae & Thoe: request_json & http.json new functions

## [1.2-0] 2018-12-11
  - Uriana: adding OAuth
  - FastCGI++ 3.1alpha

## [1.1-0] 2018-11-13
  - Adding Uriana and implementing security

## [1.0-0] 2018-10-25
  - Initial release
