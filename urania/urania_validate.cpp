// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

// Copyright 2018 Pierre del Perugia
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <fastcgi++/http.hpp>
#include <iostream>
#include <rapidjson/document.h>
#include <sstream>

#include <adawat/adawat_common.h>
#include <adawat/adawat_crypto.h>
#include <adawat/adawat_encoding.h>
#include <adawat/adawat_string.h>
#include <adawat/adawat_url.h>

#include "masnae_common.h"
#include "masnae_mysql.h"
#include "masnae_global.h"
#include "masnae_misc.h"
#include "masnae_simulator.h"

#include "urania_common.h"
#include "urania_misc.h"
#include "urania_request.h"

namespace urania {

bool        g_Verbose = false;
std::string g_AtWeb   = "at_web";

//-------------------------------------------------------------------------
void console ( void )
{
    urania::UraniaRequestHandler  handler;
    //
    masnae::console( handler );
}

//-------------------------------------------------------------------------
// Delete a user password directly in the database
void password_delete ( const std::string & p_user_id )
{
    size_t                     affected;
    masnae::mysql::Connection  db = masnae::globals::mysql_handler->get_connection( masnae::mysql::writer );
    //
    db.do_delete( "delete from t_passwords "
                  "where k_user= '" + masnae::mysql::sql_escape( p_user_id ) + "'",
                  affected );
}

//-------------------------------------------------------------------------
bool password_set ( const std::string &  p_actor_at,
                    const std::string &  p_user_id,
                    const std::string &  p_password )
{
    urania::UraniaRequestHandler  handler;
    masnae::ProcessingResponse    response;
    std::string                   nu;
    //
    handler.simulation_start();  // switch to our own environment variable
    //
    handler.simulation_environment().authorization = "Bearer " + p_actor_at;
    //
    masnae::Fastcpipp_Path  document;
    handler.check_security( "/ws", document, response );
    //
    exec_url( "/ws/password_set?user="     + p_user_id +
                              "&password=" + p_password,
                              handler, response, g_Verbose );
    //
    return is_jresp_success( response, "", nu );
}

//-------------------------------------------------------------------------
bool signout ( const std::string &  p_at )
{
    urania::UraniaRequestHandler  handler;
    masnae::ProcessingResponse    response;
    std::string                   nu;
    //
    handler.simulation_start();  // switch to our own environment variable
    //
    handler.simulation_environment().authorization = "Bearer " + p_at;
    //
    masnae::Fastcpipp_Path  document;
    handler.check_security( "/ws", document, response );
    //
    exec_url( "/ws/signout", handler, response, g_Verbose );
    //
    return is_jresp_success( response, "", nu );
}

//-------------------------------------------------------------------------
bool permissions ( const std::string &  p_at,
                   const std::string &  p_dummy )
{
    urania::UraniaRequestHandler  handler;
    masnae::ProcessingResponse    response;
    //
    handler.simulation_start();  // switch to our own environment variable
    //
    handler.simulation_environment().authorization = "Bearer " + p_at;
    //
    masnae::Fastcpipp_Path  document;
    handler.check_security( "/ws", document, response );
    //
    exec_url( "/ws/permissions", handler, response, g_Verbose );
    //
    if ( ! response.is_raw() ) {
        std::ostringstream  os;
        response.send_jresp( os );
        //
        rapidjson::Document jresp;
        jresp.Parse< rapidjson::kParseFullPrecisionFlag |
                     rapidjson::kParseCommentsFlag >( os.str().c_str() );
        //
        if ( ! jresp.HasParseError() && jresp.IsObject() &&
             //
             jresp.HasMember("status") && jresp["status"].IsString() &&
             jresp["status"] == "success"  &&
             //
             jresp.HasMember("data") && jresp["data"].IsObject() &&
             //
             jresp["data"].HasMember("permissions")             &&
             jresp["data"]["permissions"].IsObject()            &&
             jresp["data"]["permissions"].HasMember("dummy")    &&
             jresp["data"]["permissions"]["dummy"].IsString()   &&
             jresp["data"]["permissions"]["dummy"].GetString() == p_dummy
           )
        {
             return true;
        }
    }
    //
    return false;
}

//-------------------------------------------------------------------------
bool password_change ( const std::string &  p_at,
                       const std::string &  p_password,
                       const std::string &  p_password_new )
{
    urania::UraniaRequestHandler  handler;
    masnae::ProcessingResponse    response;
    std::string                   nu;
    //
    handler.simulation_start();  // switch to our own environment variable
    //
    handler.simulation_environment().authorization = "Bearer " + p_at;
    //
    masnae::Fastcpipp_Path  document;
    handler.check_security( "/ws", document, response );
    //
    exec_url( "/ws/password_change?password="     + p_password +
                                 "&password_new=" + p_password_new,
                                 handler, response, g_Verbose );
    //
    return is_jresp_success( response, "", nu );
}

//-------------------------------------------------------------------------
bool compare ( const rapidjson::GenericValue<rapidjson::UTF8<> >::Array & p_a,
               masnae::KeyList p_b )
{
    if ( p_a.Size() != p_b.size() )
        return false;
    //
    size_t i = 0;
    //
    for ( const auto & e: p_a) {
        if ( ! e.IsString() )
            return false;
        //
        if ( e.GetString() != p_b[ i++ ] )
            return false;
    }
    //
    return true;
}

//-------------------------------------------------------------------------
bool children ( const std::string &  p_at, masnae::KeyList p_expected )
{
    urania::UraniaRequestHandler  handler;
    masnae::ProcessingResponse    response;
    //
    handler.simulation_start();  // switch to our own environment variable
    //
    handler.simulation_environment().authorization = "Bearer " + p_at;
    //
    masnae::Fastcpipp_Path  document;
    handler.check_security( "/ws", document, response );
    //
    exec_url( "/ws/children", handler, response, g_Verbose );
    //
    rapidjson::GenericValue< rapidjson::UTF8<> > data;
    //
    return is_jresp_success( response, data ) &&
           data.IsArray() &&
           compare( data.GetArray(), p_expected );
}

//-------------------------------------------------------------------------
bool lineage ( const std::string &  p_at, masnae::KeyList p_expected )
{
    urania::UraniaRequestHandler  handler;
    masnae::ProcessingResponse    response;
    //
    handler.simulation_start();  // switch to our own environment variable
    //
    handler.simulation_environment().authorization = "Bearer " + p_at;
    //
    masnae::Fastcpipp_Path  document;
    handler.check_security( "/ws", document, response );
    //
    exec_url( "/ws/lineage", handler, response, g_Verbose );
    //
    rapidjson::GenericValue< rapidjson::UTF8<> > data;
    //
    return is_jresp_success( response, data ) &&
           data.IsArray() &&
           compare( data.GetArray(), p_expected );
}

//-------------------------------------------------------------------------
bool siblings ( const std::string &  p_at, masnae::KeyList p_expected )
{
    urania::UraniaRequestHandler  handler;
    masnae::ProcessingResponse    response;
    //
    handler.simulation_start();  // switch to our own environment variable
    //
    handler.simulation_environment().authorization = "Bearer " + p_at;
    //
    masnae::Fastcpipp_Path  document;
    handler.check_security( "/ws", document, response );
    //
    exec_url( "/ws/siblings", handler, response, g_Verbose );
    //
    rapidjson::GenericValue< rapidjson::UTF8<> > data;
    //
    return is_jresp_success( response, data ) &&
           data.IsArray() &&
           compare( data.GetArray(), p_expected );
}

//-------------------------------------------------------------------------
bool oauth_auth ( const std::string &  p_type,
                  const std::string &  p_client_id,
                  std::string &        p_session_id )
{
    urania::UraniaRequestHandler  handler;
    masnae::ProcessingResponse    response;
    //
    handler.simulation_start();  // switch to our own environment variable
    //
    masnae::Fastcpipp_Path  document;
    handler.check_security( "/oauth", document, response );
    //
    exec_url( "/oauth/authorize?response_type=" + p_type      +
                              "&code_challenge=E9Melhoa2OwvFrEMTJguCHaoeK1t8URWbuGJSstw-cM"
                              "&client_id="     + p_client_id +
                              "&state=xyz&scope=refresh",
              handler, response, g_Verbose );
    //
    if ( ! adawat::str_starts_with( response.get_http_status(), "200" ) )
        return false;
    //
    std::string body( reinterpret_cast<const char*>( response.get_raw_data() ),
                      response.get_raw_size() );
    //
    // Extract hidden input session_id
    //   <input type="hidden"   name="session_id" value="xrOCd0AuiS92Du-tyVQfCtZ2" />
    size_t s = body.find( "session_id"     );
    size_t v = body.find( "value=", s      );
    size_t i = body.find( "\"",     v      );
    size_t j = body.find( "\"",     i + 1  );
    //
    if ( s != std::string::npos && v != std::string::npos &&
         i != std::string::npos && j != std::string::npos)
    {
        p_session_id = body.substr( i + 1, j - i - 1 );
        return true;
    }
    //
    return false;
}

//-------------------------------------------------------------------------
bool oauth_auth_error ( const std::string &  p_type,
                        const std::string &  p_client_id,
                        const std::string &  p_uri,
                        const std::string &  p_expected_code,
                        const std::string &  p_expected_error )
{
    urania::UraniaRequestHandler  handler;
    masnae::ProcessingResponse    response;
    //
    handler.simulation_start();  // switch to our own environment variable
    //
    masnae::Fastcpipp_Path  document;
    handler.check_security( "/oauth", document, response );
    //
    exec_url( "/oauth/authorize?response_type=" + p_type      +
                              "&client_id="     + p_client_id +
                              ( p_uri.empty() ? "" : "&redirect_uri="  + adawat::url_encode( p_uri ) ) +
                              "&state=xyz",
              handler, response, g_Verbose );
    //
    if ( ! adawat::str_starts_with( response.get_http_status(), p_expected_code ) )
        return false;
    //
    if ( p_expected_error.empty() )
        return true;
    //
    // Search error in Location
    //   Location: https://client.example.com/cb?error=access_denied&state=xyz
    std::string         location;
    std::ostringstream  os;
    response.send_headers( os );
    adawat::url_decode( os.str(), location );
    //
    size_t c = location.find( "error=" );
    size_t s = location.find( "&", c );
    //
    if ( c > 0 && s > 0 )
        if ( p_expected_error == location.substr( c + 6, s - c - 6 ) )
            return true;
    //
    return false;
}

//-------------------------------------------------------------------------
bool oauth_login ( const std::string &  p_type,
                   const std::string &  p_user_id,
                   const std::string &  p_user_password,
                   const std::string &  p_session,
                   std::string &        p_secret )
{
    urania::UraniaRequestHandler  handler;
    masnae::ProcessingResponse    response;
    //
    handler.simulation_start();  // switch to our own environment variable
    //
    masnae::Fastcpipp_Path  document;
    handler.check_security( "/oauth", document, response );
    //
    exec_url( "/oauth/login?user_id="    + p_user_id       +
                          "&user_pwd="   + p_user_password +
                          "&session_id=" + p_session,
              handler, response, g_Verbose );
    //
    if ( ! adawat::str_starts_with( response.get_http_status(), "302" ) )
        return false;
    //
    // Search code in Location
    //   Location: http://example.com/cbp?code=AsGZCY582pbhUSfWRpmKwl77&state=xyz
    //   Location: http://example.com/cb#access_token=2YotnFZFEjr1zCsicMWpAA&state=xyz...
    std::string         location;
    std::ostringstream  os;
    response.send_headers( os );
    adawat::url_decode( os.str(), location );
    //
    if ( location.find( "refresh" ) != std::string::npos )
        return false;
    //
    std::string field = p_type == "code" ? "code=" : "access_token=";
    //
    size_t c = location.find( field );
    size_t s = location.find( "&", c );
    //
    if ( c > 0 && s > 0 ) {
        p_secret = location.substr( c + field.length(), s - c - field.length() );
        return true;
    }
    //
    return false;
}

//-------------------------------------------------------------------------
bool oauth_login_error ( const std::string &  p_user_id,
                         const std::string &  p_user_password,
                         const std::string &  p_session,
                         const std::string &  p_expected_code,
                         const std::string &  p_expected_error )
{
    urania::UraniaRequestHandler  handler;
    masnae::ProcessingResponse    response;
    //
    handler.simulation_start();  // switch to our own environment variable
    //
    masnae::Fastcpipp_Path  document;
    handler.check_security( "/oauth", document, response );
    //
    exec_url( "/oauth/login?user_id="    + p_user_id       +
                          "&user_pwd="   + p_user_password +
                          "&session_id=" + p_session,
              handler, response, g_Verbose );
    //
    if ( ! adawat::str_starts_with( response.get_http_status(), p_expected_code ) )
        return false;
    //
    if ( p_expected_error.empty() )
        return true;
    //
    // Search error in Location
    //   Location: https://client.example.com/cb?error=access_denied&state=xyz
    std::string         location;
    std::ostringstream  os;
    response.send_headers( os );
    adawat::url_decode( os.str(), location );
    //
    size_t c = location.find( "error=" );
    size_t s = location.find( "&", c );
    //
    if ( c > 0 && s > 0 )
        if ( p_expected_error == location.substr( c + 6, s - c - 6 ) )
            return true;
    //
    return false;
}

//-------------------------------------------------------------------------
bool oauth_password ( const std::string &  p_user_id,
                      const std::string &  p_user_password,
                      std::string &        p_at,
                      std::string &        p_rt )
{
    urania::UraniaRequestHandler  handler;
    masnae::ProcessingResponse    response;
    //
    handler.simulation_start();  // switch to our own environment variable
    //
    masnae::Fastcpipp_Path  document;
    handler.check_security( "/oauth", document, response );
    //
    exec_url( "/oauth/token?grant_type=password"
                  "&username=" + p_user_id       +
                  "&password=" + p_user_password +
                  "&scope=refresh",
              handler, response, g_Verbose );
    //
    if ( ! adawat::str_starts_with( response.get_http_status(), "200" ) )
        return false;
    //
    std::string body( reinterpret_cast<const char*>( response.get_raw_data() ),
                      response.get_raw_size() );
    //
    // {"access_token":"YBIV4rkQ0yojtKqPIzUzo-bK","expires_in":"3600","token_type":"bearer"}
    rapidjson::Document jresp;
    jresp.Parse< rapidjson::kParseFullPrecisionFlag |
                 rapidjson::kParseCommentsFlag >( body.c_str() );
    //
    if ( ! jresp.HasParseError() && jresp.IsObject() &&
         //
         jresp.HasMember("access_token")  && jresp["access_token"].IsString() &&
         jresp.HasMember("refresh_token") && jresp["refresh_token"].IsString()
         )
    {
        p_at = jresp["access_token"].GetString();
        p_rt = jresp["refresh_token"].GetString();
        return true;
    }
    //
    return false;
}

//-------------------------------------------------------------------------
bool oauth_token ( const std::string &  p_client_id,
                   const std::string &  p_client_password,
                   const std::string &  p_code,
                   bool                 p_id_in_header,
                   std::string &        p_at,
                   std::string &        p_rt )
{
    urania::UraniaRequestHandler  handler;
    masnae::ProcessingResponse    response;
    //
    handler.simulation_start();  // switch to our own environment variable
    //
    if ( p_id_in_header ) {
        adawat::Data data;
        adawat::set_data( data, p_client_id + ":" + p_client_password );
        handler.simulation_environment().authorization =
            "Basic " + adawat::encoding_base64_encode( data );
    }
    //
    masnae::Fastcpipp_Path  document;
    handler.check_security( "/oauth", document, response );
    //
    exec_url( "/oauth/token?grant_type=authorization_code"
                  "&code_verifier=dBjftJeZ4CVP-mB92K27uhbUJU1p1r_wW1gFWFOEjXk"     +
                  ( p_id_in_header ? "" : "&client_id="     + p_client_id        ) +
                  ( p_id_in_header ? "" : "&client_secret=" + p_client_password  ) +
                  "&code="          + p_code,
              handler, response, g_Verbose );
    //
    if ( ! adawat::str_starts_with( response.get_http_status(), "200" ) )
        return false;
    //
    std::string body( reinterpret_cast<const char*>( response.get_raw_data() ),
                      response.get_raw_size() );
    //
    // {"access_token":"YBIV4rkQ0yojtKqPIzUzo-bK","expires_in":"3600","token_type":"bearer"}
    rapidjson::Document jresp;
    jresp.Parse< rapidjson::kParseFullPrecisionFlag |
                 rapidjson::kParseCommentsFlag >( body.c_str() );
    //
    if ( ! jresp.HasParseError() && jresp.IsObject() &&
         //
         jresp.HasMember("access_token")  && jresp["access_token"].IsString() &&
         jresp.HasMember("refresh_token") && jresp["refresh_token"].IsString()
         )
    {
        p_at = jresp["access_token"].GetString();
        p_rt = jresp["refresh_token"].GetString();
        return true;
    }
    //
    return false;
}

//-------------------------------------------------------------------------
bool oauth_refresh ( const std::string &  p_client_id,
                     const std::string &  p_client_password,
                     const std::string &  p_rt,
                     std::string &        p_at )
{
    urania::UraniaRequestHandler  handler;
    masnae::ProcessingResponse    response;
    //
    handler.simulation_start();  // switch to our own environment variable
    //
    masnae::Fastcpipp_Path  document;
    handler.check_security( "/oauth", document, response );
    //
    exec_url( "/oauth/token?grant_type=refresh_token"
                  "&client_id="     + p_client_id       +
                  "&client_secret=" + p_client_password +
                  "&refresh_token=" + p_rt,
              handler, response, g_Verbose );
    //
    if ( ! adawat::str_starts_with( response.get_http_status(), "200" ) )
        return false;
    //
    std::string body( reinterpret_cast<const char*>( response.get_raw_data() ),
                      response.get_raw_size() );
    //
    // {"access_token":"YBIV4rkQ0yojtKqPIzUzo-bK","expires_in":"3600","token_type":"bearer"}
    rapidjson::Document jresp;
    jresp.Parse< rapidjson::kParseFullPrecisionFlag |
                 rapidjson::kParseCommentsFlag >( body.c_str() );
    //
    if ( ! jresp.HasParseError() && jresp.IsObject() &&
         //
         jresp.HasMember("access_token")  && jresp["access_token"].IsString()
         )
    {
        p_at = jresp["access_token"].GetString();
        return true;
    }
    //
    return false;
}

//-------------------------------------------------------------------------
bool oauth_password_error ( const std::string &  p_user_id,
                            const std::string &  p_user_password,
                            const std::string &  p_expected_code )
{
    urania::UraniaRequestHandler  handler;
    masnae::ProcessingResponse    response;
    //
    handler.simulation_start();  // switch to our own environment variable
    //
    masnae::Fastcpipp_Path  document;
    handler.check_security( "/oauth", document, response );
    //
    exec_url( "/oauth/token?grant_type=password"
                  "&username=" + p_user_id       +
                  "&password=" + p_user_password,
              handler, response, g_Verbose );
    //
    if ( ! adawat::str_starts_with( response.get_http_status(), "400" ) )
        return false;
    //
    std::string body( reinterpret_cast<const char*>( response.get_raw_data() ),
                      response.get_raw_size() );
    //
    // {"error":"invalid_request"}
    rapidjson::Document jresp;
    jresp.Parse< rapidjson::kParseFullPrecisionFlag |
                 rapidjson::kParseCommentsFlag >( body.c_str() );
    //
    if ( ! jresp.HasParseError() && jresp.IsObject() &&
         //
         jresp.HasMember("error") && jresp["error"].IsString() )
    {
        if ( p_expected_code == jresp["error"].GetString() )
            return true;
    }
    //
    return false;
}

//-------------------------------------------------------------------------
bool oauth_refresh_error ( const std::string &  p_client_id,
                           const std::string &  p_client_password,
                           const std::string &  p_rt,
                           const std::string &  p_expected_code )
{
    urania::UraniaRequestHandler  handler;
    masnae::ProcessingResponse    response;
    //
    handler.simulation_start();  // switch to our own environment variable
    //
    masnae::Fastcpipp_Path  document;
    handler.check_security( "/oauth", document, response );
    //
    exec_url( "/oauth/token?grant_type=refresh_token"
                  "&client_id="     + p_client_id       +
                  "&client_secret=" + p_client_password +
                  "&refresh_token=" + p_rt,
              handler, response, g_Verbose );
    //
    if ( ! adawat::str_starts_with( response.get_http_status(), "400" ) )
        return false;
    //
    std::string body( reinterpret_cast<const char*>( response.get_raw_data() ),
                      response.get_raw_size() );
    //
    // {"error":"invalid_request"}
    rapidjson::Document jresp;
    jresp.Parse< rapidjson::kParseFullPrecisionFlag |
                 rapidjson::kParseCommentsFlag >( body.c_str() );
    //
    if ( ! jresp.HasParseError() && jresp.IsObject() &&
         //
         jresp.HasMember("error") && jresp["error"].IsString() )
    {
        if ( p_expected_code == jresp["error"].GetString() )
            return true;
    }
    //
    return false;
}

//-------------------------------------------------------------------------
bool oauth_token_error ( const std::string &  p_client_id,
                         const std::string &  p_client_password,
                         const std::string &  p_uri,
                         const std::string &  p_code,
                         const std::string &  p_expected_code )
{
    urania::UraniaRequestHandler  handler;
    masnae::ProcessingResponse    response;
    //
    handler.simulation_start();  // switch to our own environment variable
    //
    masnae::Fastcpipp_Path  document;
    handler.check_security( "/oauth", document, response );
    //
    exec_url( "/oauth/token?grant_type=authorization_code"
                  "&client_id="     + p_client_id       +
                  "&client_secret=" + p_client_password +
                  ( p_uri.empty() ? "" : "&redirect_uri="  + adawat::url_encode( p_uri ) ) +
                  "&code="          + p_code,
              handler, response, g_Verbose );
    //
    if ( ! adawat::str_starts_with( response.get_http_status(), "400" ) )
        return false;
    //
    std::string body( reinterpret_cast<const char*>( response.get_raw_data() ),
                      response.get_raw_size() );
    //
    // {"error":"invalid_request"}
    rapidjson::Document jresp;
    jresp.Parse< rapidjson::kParseFullPrecisionFlag |
                 rapidjson::kParseCommentsFlag >( body.c_str() );
    //
    if ( ! jresp.HasParseError() && jresp.IsObject() &&
         //
         jresp.HasMember("error") && jresp["error"].IsString() )
    {
        if ( p_expected_code == jresp["error"].GetString() )
            return true;
    }
    //
    return false;
}

//-------------------------------------------------------------------------
bool signin ( const std::string &  p_client_id,
              const std::string &  p_client_secret,
              const std::string &  p_user_id,
              const std::string &  p_password,
              std::string &        p_at )
{
    std::string session;
    //
    return oauth_auth ( "token", p_client_id, session ) &&
           oauth_login( "token", p_user_id,   p_password,      session, p_at );
}

//-------------------------------------------------------------------------
bool password_lost_1 ( const std::string &  p_at,
                       const std::string &  p_user_id,
                       std::string &        p_otp,
                       const std::string &  p_expected_reason = "" )
{
    urania::UraniaRequestHandler  handler;
    masnae::ProcessingResponse    response;
    //
    handler.simulation_start();  // switch to our own environment variable
    //
    handler.simulation_environment().authorization = "Bearer " + p_at;
    //
    masnae::Fastcpipp_Path  document;
    handler.check_security( "/ws", document, response );
    //
    exec_url( "/ws/password_lost_1?user=" + p_user_id,
                                  handler, response, g_Verbose );
    //
    if ( p_expected_reason.empty() ) // expect a success
        return is_jresp_success( response, "/data/otp", p_otp );
    else
        return is_jresp_rejected( response, p_expected_reason, "" );
}

//-------------------------------------------------------------------------
bool password_lost_2 ( const std::string &  p_at,
                       const std::string &  p_user_id,
                       const std::string &  p_otp,
                       const std::string &  p_password_new,
                       const std::string &  p_expected_reason = "" )
{
    urania::UraniaRequestHandler  handler;
    masnae::ProcessingResponse    response;
    std::string                   nu;
    //
    handler.simulation_start();  // switch to our own environment variable
    //
    handler.simulation_environment().authorization = "Bearer " + p_at;
    //
    masnae::Fastcpipp_Path  document;
    handler.check_security( "/ws", document, response );
    //
    exec_url( "/ws/password_lost_2?user="     + p_user_id +
                                 "&otp="      + p_otp     +
                                 "&password=" + p_password_new,
                                 handler, response, g_Verbose );
    //
    if ( p_expected_reason.empty() ) // expect a success
        return is_jresp_success( response, "", nu );
    else
        return is_jresp_rejected( response, p_expected_reason, "" );
}

//-------------------------------------------------------------------------
bool password_need_change ( const std::string &  p_at,
                            const std::string &  p_user_id,
                            bool                 p_expected_state )
{
    urania::UraniaRequestHandler  handler;
    masnae::ProcessingResponse    response;
    bool                          need_change;
    //
    handler.simulation_start();  // switch to our own environment variable
    //
    handler.simulation_environment().authorization = "Bearer " + p_at;
    //
    masnae::Fastcpipp_Path  document;
    handler.check_security( "/ws", document, response );
    //
    exec_url( "/ws/password_need_change?user=" + p_user_id,
                                       handler, response, g_Verbose );
    //
    return is_jresp_success( response, "/data/need_change", need_change ) &&
           p_expected_state == need_change;
}

//-------------------------------------------------------------------------
void start (const char * p_cszModule)
{
    printf("%s:", p_cszModule);
}

//-------------------------------------------------------------------------
bool check (const char * p_cszModule, unsigned p_uSection, unsigned p_uTest, bool p_bCondition)
{
    if ( g_Verbose || ! p_bCondition )
        printf("\n   %s.%02u.%02u: %s", p_cszModule, p_uSection, p_uTest, p_bCondition ? "ok" : "error");
    //
    return p_bCondition;
}

//-------------------------------------------------------------------------
bool stop (const char * p_cszModule, bool p_bCondition)
{
    if ( g_Verbose || ! p_bCondition )
        printf("\n%s: %s\n", p_cszModule, p_bCondition ? "ok" : "error");
    else
        printf(" ok\n");
    //
    return p_bCondition;
}

//-------------------------------------------------------------------------
bool validate_misc ( void )
{
    std::string  a, b;
    bool         ok = true;
    //
    ok = ok && check( "misc", 1, 1, parse_basic_authentification( "Basic czZCaGRSa3F0MzpnWDFmQmF0M2JW", a, b) &&
                               a == "s6BhdRkqt3" && b == "gX1fBat3bV" );
    //
    ok = ok && check( "misc", 2, 1, add_parameters_to_url( "http://server/doc",     false, { { "a", "1"} } ) ==
                                "http://server/doc?a=1" );
    ok = ok && check( "misc", 2, 2, add_parameters_to_url( "http://server/doc",     false, { { "a", "1"}, { "b", "2"} } ) ==
                                "http://server/doc?a=1&b=2" );
    ok = ok && check( "misc", 2, 3, add_parameters_to_url( "http://server/doc?z=1", false, { { "a", "1"} } ) ==
                                "http://server/doc?z=1&a=1" );
    ok = ok && check( "misc", 2, 4, add_parameters_to_url( "http://server/doc?z=1", false, { { "a", "1"}, { "b", "2"} } ) ==
                                "http://server/doc?z=1&a=1&b=2" );
    ok = ok && check( "misc", 2, 5, add_parameters_to_url( "http://server/doc",     false, { { "q", "a=1" } } ) ==
                                "http://server/doc?q=a%3D1" );
    ok = ok && check( "misc", 2, 6, add_parameters_to_url( "http://server/doc",     true,  { { "a", "1" } } ) ==
                                "http://server/doc#a=1" );
    ok = ok && check( "misc", 2, 7, add_parameters_to_url( "http://server/doc?z=1", true,  { { "a", "1" } } ) ==
                                "http://server/doc?z=1#a=1" );
    //
    return ok;
}

//-------------------------------------------------------------------------
bool validate_oauth ( void )
{
    std::string session, code, at, rt, at2;
    bool        ok = true;
    //
    // Prepare test accounts
    ok = ok && check( "oauth", 0, 1, password_set( g_AtWeb, "2", "SeKre_789" ) );
    ok = ok && check( "oauth", 0, 2, password_set( g_AtWeb, "3", "SeKre_789" ) );
    ok = ok && check( "oauth", 0, 3, password_set( g_AtWeb, "7", "SeKre_789" ) );
    password_delete( "9" );
    //
    // Authorization Code - password in parameters
    ok = ok && check( "oauth", 1, 1, oauth_auth   ( "code", "1", session ) );
    ok = ok && check( "oauth", 1, 2, oauth_login  ( "code", "2", "SeKre_789", session, code ) );
    ok = ok && check( "oauth", 1, 3, oauth_token  (         "1", "SeKre_789", code, false, at, rt ) );
    ok = ok && check( "oauth", 1, 5, oauth_refresh( "1", "SeKre_789", rt, at ) );
    ok = ok && check( "oauth", 1, 6, signout      ( at ) );
    ok = ok && check( "oauth", 1, 7, oauth_refresh_error (  "1", "SeKre_789", rt, "invalid_grant" ) );  // signout delete rt
    //
    // Authorization Code - password in header
    ok = ok && check( "oauth", 2, 1, oauth_auth ( "code", "1", session ) );
    ok = ok && check( "oauth", 2, 2, oauth_login( "code", "2", "SeKre_789", session, code ) );
    ok = ok && check( "oauth", 2, 3, oauth_token(         "1", "SeKre_789", code, true, at, rt ) );
    ok = ok && check( "oauth", 2, 4, signout    ( at ) );
    //
    // Password
    ok = ok && check( "oauth", 3, 1, oauth_password ( "2", "SeKre_789", at, rt ) );
    ok = ok && check( "oauth", 3, 2, oauth_refresh  ( "2", "SeKre_789", rt, at2 ) );
    ok = ok && check( "oauth", 3, 3, ! permissions  ( at, "" ) );  // at deleted by refresh
    ok = ok && check( "oauth", 3, 4, signout        ( at2 ) );
    //
    // Implicit Grant
    ok = ok && check( "oauth", 4, 1, oauth_auth ( "token", "1", session ) );
    ok = ok && check( "oauth", 4, 2, oauth_login( "token", "2", "SeKre_789", session, at ) );
    ok = ok && check( "oauth", 4, 3, signout    ( at ) );
    //
    // Errors
    ok = ok && check( "oauth", 5,  1, oauth_auth_error( "code", "1", "http://bad.example.com", "400", "" ) );
    ok = ok && check( "oauth", 5,  2, oauth_auth_error( "code", "9", "",                       "400", "" ) );
    ok = ok && check( "oauth", 5,  3, oauth_password_error(     "1", "SeKre_789", "unauthorized_client" ) );
    //                        
    ok = ok && check( "oauth", 5,  4, oauth_login_error(         "2", "SeKre_789", "bad_session", "200", "" ) );
    //                        
    ok = ok && check( "oauth", 5,  5, oauth_auth       ( "code", "1", session ) &&
                                      oauth_login_error(         "9", "SeKre_789", session, "200", "" ) );
    //                     
    ok = ok && check( "oauth", 5,  6, oauth_auth       ( "code", "1", session ) &&
                                      oauth_login_error(         "7", "bad_password", session, "200", "" ) &&
                                      oauth_login      ( "code", "2", "SeKre_789", session, at ) );
    //                        
    ok = ok && check( "oauth", 5,  7, oauth_auth ( "code", "1", session )        &&
                                      oauth_login( "code", "2", "SeKre_789", session, code )  &&
                                      oauth_token_error(   "2", "SeKre_789", "", code, "invalid_grant" ) );
    //                        
    ok = ok && check( "oauth", 5,  8, oauth_auth ( "code", "1", session )        &&
                                      oauth_login( "code", "2", "SeKre_789", session, code )  &&
                                      oauth_token_error(   "1", "SeKre_789", "http://bad.example.com", code, "invalid_grant" ) );
    //                        
    ok = ok && check( "oauth", 5,  9, oauth_token_error(   "1", "SeKre_789", "", "bad_code", "invalid_grant" ) );
    //                        
    ok = ok && check( "oauth", 5, 10, oauth_password (       "2", "SeKre_789", at, rt ) &&
                                      oauth_refresh_error (  "1", "SeKre_789", rt, "invalid_grant" ) );
    //
    ok = ok && check( "oauth", 5, 11, oauth_auth ( "code", "1", session )        &&
                                      oauth_login( "code", "2", "SeKre_789", session, code )  &&
                                      oauth_token_error(   "1", "bad_pwd", "", code, "invalid_grant" ) );  // "1" is now banned
    //
    ok = ok && check( "oauth", 5, 12, oauth_password_error ( "9", "bad_pwd", "unauthorized_client" ) );
    ok = ok && check( "oauth", 5, 13, oauth_password (       "2", "SeKre_789", at, rt ) &&
                                      oauth_refresh_error (  "2", "bad_pwd", rt, "invalid_grant" ) );     // "2" is now banned
    ok = ok && check( "oauth", 5, 14, oauth_password_error ( "3", "bad_pwd", "unauthorized_client" ) );
    //
    password_delete( "3" );
    password_delete( "7" );
    password_delete( "9" );
    // 1 & 2 remain
    //
    return ok;
}

//-------------------------------------------------------------------------
// Users 1 & 2 have password 'SeKre_789'. Others don't have password
bool validate_password ( void )
{
    std::string at, at2, otp;
    bool        ok = true;
    //
    // Prepare test accounts
    password_delete( "3" );
    //
    // at_web (user 1) doesn't have permission to signout
    ok = ok && check( "password", 1, 1, ! signout     ( g_AtWeb ) );
    ok = ok && check( "password", 1, 2,   permissions ( g_AtWeb, "un" ) );
    //
    // User 2 already have a password
    ok = ok && check( "password", 2, 1, ! password_set( g_AtWeb, "2", "simple" ) );
    ok = ok && check( "password", 2, 2,   password_set( g_AtWeb, "2", "SeKre_789" ) );
    //
    // User 2's signs in, check his rights and signout
    ok = ok && check( "password", 3, 1, signin     ( "1", "SeKre_789", "2", "SeKre_789", at ) &&
                                        permissions( at, "deux" ) &&
                                        signout    ( at ) );
    //
    // Users 3 doesn't have a password
    ok = ok && check( "password", 4, 1, password_set( g_AtWeb, "3", "SeKre_789" ) );
    //
    // Changing password, inheritance
    ok = ok && check( "password", 5, 1, signin         ( "1", "SeKre_789", "3", "SeKre_789", at ) &&
                                        password_change( at, "SeKre_789", "Secret_2A6B" ) &&
                                        permissions    ( at, "trois" ) &&
                                        signout        ( at ) &&
                                        signin         ( "1", "SeKre_789", "3", "Secret_2A6B", at ) );
    //
    // Lost password
    ok = ok && check( "password", 6, 1, password_lost_1( g_AtWeb, "3", otp ) &&
                                        password_lost_2( g_AtWeb, "3", otp, "SeKre_456" ) &&
                                        signin         ( "1", "SeKre_789", "3", "SeKre_456", at ));
    //
    ok = ok && check( "password", 6, 2, password_lost_1( g_AtWeb, "3", otp ) &&
                                        password_lost_2( g_AtWeb, "3", "bad", "SeKre_456", "forbidden" ));
    //
    ok = ok && check( "password", 7, 1, password_need_change( g_AtWeb, "3", false ) );
    //
    // Need change
    //
    password_delete( "3" );
    //
    return ok;
}

//-------------------------------------------------------------------------
bool validate_family ( void )
{
    std::string at;
    bool        ok = true;
    //
    // Prepare test accounts
    ok = ok && check( "family", 1, 1, password_set( g_AtWeb, "3", "SeKre_789" ) );
    ok = ok && check( "family", 1, 2, password_set( g_AtWeb, "7", "SeKre_789" ) );
    ok = ok && check( "family", 1, 2, password_set( g_AtWeb, "9", "SeKre_789" ) );
    //
    // Children, siblings and lineage
    ok = ok && check( "family", 2, 1, signin         ( "1", "SeKre_789", "7", "SeKre_789", at ) &&
                                 children       ( at, { } ) &&
                                 siblings       ( at, { "7", "8" } ) &&
                                 lineage        ( at, { "7","4","3" } ) &&
                                 signout        ( at ) );
    ok = ok && check( "family", 2, 2, signin         ( "1", "SeKre_789", "9", "SeKre_789", at ) &&
                                 siblings       ( at, { "9" } ) &&
                                 lineage        ( at, { "9","8","4","3" } ) &&
                                 signout        ( at ) );
    ok = ok && check( "family", 2, 3, signin         ( "1", "SeKre_789", "3", "SeKre_789", at ) &&
                                 lineage        ( at, { "3" } ) &&
                                 children       ( at, { "4","5","6" } ) &&
                                 signout        ( at ) );
    //
    password_delete( "3" );
    password_delete( "7" );
    password_delete( "9" );
    //
    return ok;
}

//-------------------------------------------------------------------------
bool validate_info (void)
{
    bool                    ok = true;
    rapidjson::Document     doc;
    //
    doc.Parse< rapidjson::kParseFullPrecisionFlag | rapidjson::kParseCommentsFlag >(
        g_AccessTokenManager.specific_info().c_str() );
    ok &= check( "info", 1, 1, !doc.HasParseError() && doc.IsObject() );
    //
    doc.Parse< rapidjson::kParseFullPrecisionFlag | rapidjson::kParseCommentsFlag >(
        g_OAuthManager.specific_info().c_str() );
    ok &= check( "info", 1, 2, !doc.HasParseError() && doc.IsObject() );
    //
    doc.Parse< rapidjson::kParseFullPrecisionFlag | rapidjson::kParseCommentsFlag >(
        g_PasswordManager.specific_info().c_str() );
    ok &= check( "info", 1, 3, !doc.HasParseError() && doc.IsObject() );
    //
    return ok;
}

//-------------------------------------------------------------------------
// Self tests function
void validate ( bool *  p_succeeded )
{
    // Pre-allocate a DB handle
    {
        masnae::globals::mysql_handler->get_connection( masnae::mysql::writer );
    }
    //
    *p_succeeded = true;
    //
    start ("misc       ");
    *p_succeeded = *p_succeeded && stop ("misc",       validate_misc() );
    //
    start ("oauth      ");
    *p_succeeded = *p_succeeded && stop ("oauth",      validate_oauth() );
    //
    start ("password   ");
    *p_succeeded = *p_succeeded && stop ("password",   validate_password() );
    //
    start ("family     ");
    *p_succeeded = *p_succeeded && stop ("family",     validate_family() );
    //
    start ("info       ");
    *p_succeeded = *p_succeeded && stop ("info",       validate_info() );
    //
    printf ("-------\n");
    printf ("validation : %s\n", *p_succeeded ? "ok" : "error");
}

} // namespace urania
