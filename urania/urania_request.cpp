// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

// Copyright 2018 Pierre del Perugia
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <adawat/adawat_string.h>

#include "masnae_mysql.h"
#include "masnae_global.h"
#include "masnae_misc.h"
#include "urania_common.h"
#include "urania_misc.h"
#include "urania_request.h"

namespace urania {

//-------------------------------------------------------------------------
UraniaRequestHandler::UraniaRequestHandler() :
    masnae::RequestHandler( urania::constants::k_post_max_size )
{
}

//-------------------------------------------------------------------------
// Returns true if the peer is allowed to call this web service
// Retrieve the user and his permissions associated to the AccessToken
// received (in the header Authorization) and store them in the base
// class RequestHandler, in a SecurityContext object
bool UraniaRequestHandler::check_security ( const std::string &           p_module,
                                            masnae::Fastcpipp_Path &      p_document,
                                            masnae::ProcessingResponse &  p_result )
{
    if ( p_module == "oauth" )  // no AccessToken expected
        return true;
    //
    // Else, module: /info, /echo, /mgt, /ws
    //
    std::string  at;
    //
    if ( ! get_accesstoken( at ) )  // retrieve AT from Authorization header
        return false;
    //
    // Retrieve user's details in the security context in RequestHandler
    if ( ! g_AccessTokenManager.get_security( at, m_security, p_result ) )
        return false;
    //
    return true;
}

//-------------------------------------------------------------------------
// Returns true if the module/document are found (p_result must be filled then)
bool UraniaRequestHandler::process_request ( const std::string &           p_module,
                                             masnae::Fastcpipp_Path &      p_document,
                                             masnae::ProcessingResponse &  p_result )
{
    bool handled = false;  // if not handled here, will call the derived class (if any)
    //
         if ( p_module == "oauth" )  handled = process_oauth_request( p_document, p_result );
    else if ( p_module == "ws" )     handled = process_ws_request   ( p_document, p_result );
    //
    return handled;
}

//-------------------------------------------------------------------------
// Returns information
// Must return a valid JSON string representing a non-empty object
std::string UraniaRequestHandler::specific_info ( void )
{
    return "{\"tokens\":"    + g_AccessTokenManager.specific_info() +
           ",\"oauth\":"     + g_OAuthManager.specific_info()       +
           ",\"password\":"  + g_PasswordManager.specific_info()    +
           "}";
}

//-------------------------------------------------------------------------
// Find and return a cookie from the request
bool UraniaRequestHandler::get_cookie( const std::string &  p_key,
                                       std::string &        p_value )
{
    const auto cookie = environment().cookies.find( p_key );
    //
    if ( cookie == environment().cookies.cend() )
        return false;
    //
    p_value = cookie->second;
    return true;
}

//-------------------------------------------------------------------------
// Returns true if the module/document are found (p_result is already be filled)
bool UraniaRequestHandler::process_oauth_request ( const masnae::Fastcpipp_Path &  p_document,
                                                   masnae::ProcessingResponse &    p_result )
{
    if ( p_document.size() == 0 )
        return false;
    //
    // Merge post and get parameters
    auto params = masnae::merge_parameters( environment().posts,
                                            environment().gets );
    //
    // Function bodies located in urania_request_request_oauth.cpp
         if ( p_document[ 0 ] == "authorize" )  process_oauth_authorize( params, p_result );
    else if ( p_document[ 0 ] == "login" )      process_oauth_login    ( params, p_result );
    else if ( p_document[ 0 ] == "token" )      process_oauth_token    ( params, p_result );
    //
    else return false;
    //
    return true;
}

//-------------------------------------------------------------------------
// Returns true if the module/document are found (p_result is already be filled)
bool UraniaRequestHandler::process_ws_request ( const masnae::Fastcpipp_Path &  p_document,
                                                masnae::ProcessingResponse &    p_result )
{
    if ( p_document.size() == 0 )
        return false;
    //
    // Merge post and get parameters
    auto params = masnae::merge_parameters( environment().posts,
                                            environment().gets );
    //
         if ( p_document[ 0 ] == "signout" )          process_ws_signout        ( params, p_result );
    else if ( p_document[ 0 ] == "permissions" )      process_ws_permissions    ( params, p_result );
    //                                                                          
    // Function bodies located in urania_request_password.cpp                   
    else if ( p_document[ 0 ] == "password_change" )  process_ws_password_change( params, p_result );
    else if ( p_document[ 0 ] == "password_lost_1" )  process_ws_password_lost_1( params, p_result );
    else if ( p_document[ 0 ] == "password_lost_2" )  process_ws_password_lost_2( params, p_result );
    else if ( p_document[ 0 ] == "password_score" )   process_ws_password_score ( params, p_result );
    else if ( p_document[ 0 ] == "password_set" )     process_ws_password_set   ( params, p_result );
    else if ( p_document[ 0 ] == "password_need_change" )
                                                      process_ws_password_need_change
                                                                                ( params, p_result );
    //
    // Function bodies located in urania_request_family.cpp
    else if ( p_document[ 0 ] == "children" )         process_ws_children       ( params, p_result );
    else if ( p_document[ 0 ] == "lineage" )          process_ws_lineage        ( params, p_result );
    else if ( p_document[ 0 ] == "siblings" )         process_ws_siblings       ( params, p_result );
    //
    else return false;
    //
    return true;
}

//-------------------------------------------------------------------------
// Delete the current AccessToken
void UraniaRequestHandler::process_ws_signout ( const masnae::SingleKey_ValueList &  p_parameters,
                                                masnae::ProcessingResponse &         p_result )
{
    // Check that the caller is allowed here
    if ( ! has_permission( "urania.signout", p_result ) )
        return;
    //
    std::string  at;
    //
    if ( ! get_accesstoken( at ) ) {  // retrieve AT from Authorization header
        p_result.set_jresp_error( "Bad Authorization header" );
    }
    else {
        AccessTokenDetails  details;
        //
        // Retrieve token details, delete it
        if ( ! g_AccessTokenManager.get_token   ( at, details, p_result ) ||
             ! g_AccessTokenManager.delete_token( at, p_result )          )
            p_result.set_jresp_failed();
        //
        // Eventually delete the peer token (AT for RT, RT for AT)
        if ( ! details.peer_token_sha.empty() )
            g_AccessTokenManager.delete_token_sha( details.peer_token_sha, p_result );
    }
}

//-------------------------------------------------------------------------
// Returns the permissions of the current AccessToken
void UraniaRequestHandler::process_ws_permissions ( const masnae::SingleKey_ValueList &  p_parameters,
                                                    masnae::ProcessingResponse &         p_result )
{
    p_result.set_jresp_success( std::move(
        "{ \"user\": "        + masnae::json_quote_string( m_security.user        ) + "," +
        "  \"profile\": "     + masnae::json_quote_string( m_security.profile     ) + "," +
        "  \"role\": "        + masnae::json_quote_string( m_security.role        ) + "," +
        "  \"scopes\": "      + masnae::json_quote_string( m_security.scopes      ) + "," +
        "  \"permissions\": " + masnae::json_convert     ( m_security.permissions ) + "}"
    ) );
    //
    if ( masnae::globals::logger_handler->quiet() )  // do not generate log for this request
        p_result.disable_log();
}

} // namespace urania
