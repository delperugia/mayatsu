// Copyright 2018 Pierre del Perugia
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

//
// Definitions of global constants, types and functions
//

#ifndef urania_common_h
#define urania_common_h

#include "masnae_common.h"
#include "urania_accesstoken.h"
#include "urania_oauth.h"
#include "urania_password.h"

//-------------------------------------------------------------------------
namespace urania {
namespace constants {

constexpr size_t    k_post_max_size         = 1024;
constexpr unsigned  k_threads_per_cpu       = 2;
constexpr char      k_env_var[]             = "URANIA_CONFIG";
constexpr char      k_default_config_file[] = "/var/www/cgi-config/urania.json";
constexpr char      k_default_login_file[]  = "/var/www/cgi-config/urania_login_en.html";
constexpr char      k_default_log_folder[]  = "/var/log/urania/";

// urania_password.cpp
constexpr char      k_hash_param[]          = "m=8192,t=4,p=2";  // Argon2 hashing parameters
                                                                 //   "m=8192,t=4,p=2":    56ms
                                                                 //   "m=16384,t=5,p=2":  131ms
                                                                 //   "m=16384,t=7,p=2":  164ms
                                                                 //   "m=16384,t=10,p=2": 231ms
                                                                 //   "m=32768,t=7,p=2":  324ms
                                                                 //   "m=32768,t=10,p=2": 450ms
constexpr unsigned  k_locking_period_min    = 3;                 // password locking minimal time in seconds
constexpr unsigned  k_locking_period_max    = 8*3600;            // password locking maximal time in seconds
constexpr unsigned  k_reset_period_min      = 3;                 // reseting password minimal period time in seconds
constexpr unsigned  k_reset_period_max      = 8*3600;            // reseting password maximal period time in seconds
constexpr unsigned  k_digest_size           = 32;                // password digest size in bytes

// urania_accesstoken.cpp
constexpr unsigned  k_token_size            = 22;                // access and refresh tokens size in chars (22 = ~131 bits)

// urania_oauth.cpp
constexpr unsigned  k_oauth_code_size       = 22;                // code and session size in chars (22 = ~131 bits)

} // namespace constants
} // namespace urania

//-------------------------------------------------------------------------
// Global variables
extern urania::AccessTokenManager   g_AccessTokenManager;
extern urania::OAuthManager         g_OAuthManager;
extern urania::PasswordManager      g_PasswordManager;

#endif
