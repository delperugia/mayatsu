// Copyright 2018 Pierre del Perugia
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

//
// Definitions of database tables handled by WSTable manager
//

#ifndef urania_tables_h
#define urania_tables_h

#include "masnae_mysql.h"
#include "urania_tables.h"

namespace urania {

// Tables t_accesstokens and t_passwords are not accessed directly
// but only via /ws/ calls

//-------------------------------------------------------------------------
class TableGroups : public masnae::ws_tables::Definition
{
    public:
        TableGroups() :
            masnae::ws_tables::Definition( "t_groups" )
        {
            m_fields = {
                // name           type ins   upt   req   def.ins  def.upt
                { "k_user",     { 'S', true, true, true, nullptr, nullptr } },
                { "k_group",    { 'S', true, true, true, nullptr, nullptr } }
            };
        }
};

//-------------------------------------------------------------------------
class TableOAuth : public masnae::ws_tables::Definition
{
    public:
        TableOAuth() :
            masnae::ws_tables::Definition( "t_oauth" )
        {
            m_fields = {
                // name                type ins   upt   req   def.ins  def.upt
                { "k_user",          { 'S', true, true, true, nullptr, nullptr } },
                { "s_name",          { 'S', true, true, true, nullptr, nullptr } },
                { "s_logo",          { 'S', true, true, true, nullptr, nullptr } },
                { "s_allowed_modes", { 'S', true, true, true, nullptr, nullptr } },
                { "s_redirect_uri",  { 'S', true, true, true, nullptr, nullptr } },
                { "b_pkce",          { 'I', true, true, true, nullptr, nullptr } }
            };
        }
};

//-------------------------------------------------------------------------
class TableProfiles : public masnae::ws_tables::Definition
{
    public:
        TableProfiles() :
            masnae::ws_tables::Definition( "t_profiles" )
        {
            m_fields = {
                // name                    type ins   upt   req   def.ins  def.upt
                { "k_profile",           { 'S', true, true, true, nullptr, nullptr } },
                { "i_password_lifetime", { 'I', true, true, true, nullptr, nullptr } }
            };
        }
};

//-------------------------------------------------------------------------
class TableProfilesRoles : public masnae::ws_tables::Definition
{
    public:
        TableProfilesRoles() :
            masnae::ws_tables::Definition( "t_profiles_roles" )
        {
            m_fields = {
                // name            type ins   upt   req    def.ins  def.upt
                { "k_profile",   { 'S', true, true, true,  nullptr, nullptr } },
                { "k_role",      { 'S', true, true, true,  nullptr, nullptr } },
                { "s_condition", { 'S', true, true, false, "''",    nullptr } }
            };
        }
};

//-------------------------------------------------------------------------
class TableRoles : public masnae::ws_tables::Definition
{
    public:
        TableRoles() :
            masnae::ws_tables::Definition( "t_roles" )
        {
            m_fields = {
                // name              type ins   upt   req   def.ins  def.upt
                { "k_role",        { 'S', true, true, true, nullptr, nullptr } },
                { "k_role_parent", { 'S', true, true, true, nullptr, nullptr } }
            };
        }
};

//-------------------------------------------------------------------------
class TableRolesPermissions : public masnae::ws_tables::Definition
{
    public:
        TableRolesPermissions() :
            masnae::ws_tables::Definition( "t_roles_permissions" )
        {
            m_fields = {
                // name              type ins   upt   req    def.ins  def.upt
                { "k_role",        { 'S', true, true, true,  nullptr, nullptr } },
                { "k_permission",  { 'S', true, true, true,  nullptr, nullptr } },
                { "s_parameters",  { 'S', true, true, false, "''",    nullptr } }
            };
        }
};

//-------------------------------------------------------------------------
class TableUsers : public masnae::ws_tables::Definition
{
    public:
        TableUsers() :
            masnae::ws_tables::Definition( "t_users" )
        {
            m_fields = {
                // name              type ins   upt   req    def.ins            def.upt
                { "k_user",        { 'S', true, true, true,  nullptr,           nullptr } },
                { "k_profile",     { 'S', true, true, true,  nullptr,           nullptr } },
                { "k_role",        { 'S', true, true, true,  nullptr,           nullptr } },
                { "d_creation",    { 'S', true, true, false, "utc_timestamp()", nullptr } },
                { "k_user_parent", { 'S', true, true, false, "NULL",            nullptr } }
            };
        }
};

} // namespace urania

#endif
