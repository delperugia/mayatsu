// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

// Copyright 2018 Pierre del Perugia
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <linux/limits.h>
#include <rapidjson/pointer.h>
#include <regex>
#include <unistd.h>
#include <adawat/adawat_crypto.h>
#include <adawat/adawat_encoding.h>
#include <adawat/adawat_file.h>
#include <adawat/adawat_string.h>

#include "masnae_global.h"
#include "masnae_misc.h"
#include "urania_common.h"
#include "urania_oauth.h"

namespace urania {

//-------------------------------------------------------------------------
OAuthSession::OAuthSession ()
{
    creation     = time( nullptr );
    expire       = creation + 1;
    at_expire_in = creation + 1;
    //
    with_remember_me = false;
    attempt          = 0;       
}

//-------------------------------------------------------------------------
// [PKCE] check that verifier received match the challenge set
bool OAuthSession::check_challenge ( const std::string &  p_verifier ) const
{
    if ( challenge.empty() )  // no challenge was set by client
        return true;
    //
    if ( challenge_method == "plain" )
        return adawat::crypto_equal( challenge, p_verifier );
    //
    // Using SHA 256 method (S256)
    adawat::Data  verifier, hash;
    //
    adawat::set_data( verifier, p_verifier );
    //
    return
        adawat::crypto_digest( verifier, hash, EVP_sha256() )
        &&
        adawat::encoding_base64_encode( hash, adawat::base64_type::url, false ) == challenge;
}

//-------------------------------------------------------------------------
OAuthManager::OAuthManager () :
    masnae::MonitorFiles( "OAuthManager" )
{
    m_captcha.mode = captcha_mode::e_none;
}

//-------------------------------------------------------------------------
// Called by the main to initialize the class
bool OAuthManager::start ( void )
{
    masnae::globals::publisher->subscribe( this );
    //
    return update_config();
}

//-------------------------------------------------------------------------
// Call before program termination
void OAuthManager::stop ( void )
{
    stop_monitoring();
    //
    if ( masnae::globals::publisher != nullptr )
        masnae::globals::publisher->unsubscribe( this );
}

//-------------------------------------------------------------------------
// Return statistics on the module
std::string OAuthManager::specific_info ( void )
{
    // Retrieve data
    size_t                       nb_sessions;
    decltype( m_session_usage )  usage;
    masnae::SingleKey_Value      data;
    //
    std::string  session_usage, session_lifetime;
    //
    {
        std::shared_lock< std::shared_mutex > lock( m_mutex_config );
        //
        nb_sessions = m_sessions.size();
        usage       = m_session_usage;
        //
        masnae::add_stats( "usage",    m_stats_session_usage,    data );
        masnae::add_stats( "lifetime", m_stats_session_lifetime, data );
    }
    //
    // Build JSON response (number of sessions per client)
    std::string result;
    //
    if ( usage.empty() ) {
        result = "{}";
    } else {
        result.reserve( usage.size() * 16 );  // arbitrary size
        //
        for ( const auto & x: usage )
            result += ',' + masnae::json_quote_string( x.first ) +
                            ':' +
                            std::to_string( x.second );
        //
        result[0]  = '{';  // replace ',' by '{'
        result    += '}';
    }
    //
    return "{\"sessions\": " + std::to_string( nb_sessions ) +
           ",\"stats\": "    + masnae::json_convert( data )  +
           ",\"usage\" : "   + result +
           "}";
}

//-------------------------------------------------------------------------
// Accessors

int64_t OAuthManager::get_accesstoken_lifetime (void)
{
    std::shared_lock< std::shared_mutex > lock( m_mutex_config );
    //
    return m_accesstoken_lifetime_s;
}

int64_t OAuthManager::get_refreshtoken_lifetime (void)
{
    std::shared_lock< std::shared_mutex > lock( m_mutex_config );
    //
    return m_refreshtoken_lifetime_s;
}

//-------------------------------------------------------------------------
void OAuthManager::bus_event ( const masnae::bus::Message &  p_message )
{
    switch ( p_message.type )
    {
        case masnae::bus::Message::config_updated:
            update_config();
            break;
        default:
            break;
    }
}

//-------------------------------------------------------------------------
// Find a client, create a session for m_session_lifetime_s
OAuthManager::create_result
    OAuthManager::create_session ( OAuthSession &  p_session,
                                   std::string &   p_session_id,
                                   std::string &   p_details )
{
    masnae::SingleKey_Value  client;
    //
    // No permission check: if a user is present in table oauth, he is allowed
    if ( ! masnae::globals::ws_caches_handler->find( "t_oauth", p_session.client_id, client) ) {
        p_details = "not listed in oauth table";
        return create_result::forbidden;
    }
    //
    // Eventually remove old sessions / codes
    maybe_clean();
    //
    // Get config
    int64_t session_lifetime_s, usage_maximum;
    //
    {
        std::shared_lock< std::shared_mutex > lock( m_mutex_config );
        //
        session_lifetime_s = m_session_lifetime_s;
        usage_maximum      = m_usage_maximum;
    }
    //
    // Start by fixing redirect_uri_to_use to be able to return further error
    // If received reponse_uri is empty, use default one (1st one).
    // If not, it must be one of the configured URI
    // Following OAuth 2.0 Security Best Current Practice, 3.1.3
    // only exact redirect URI matching is used (with possible
    // with native applications using a local web server)
    if ( p_session.redirect_uri_received.empty() ) {
        p_session.redirect_uri_to_use = adawat::str_token( client[ "s_redirect_uri" ], "," );
    } else {
        if ( ! adawat::str_csv_has_value( p_session.redirect_uri_received,
                                          client[ "s_redirect_uri" ] ) )
        {
            p_details = "undeclared URI";
            return create_result::forbidden;
        }
        //
       p_session.redirect_uri_to_use = p_session.redirect_uri_received;
    }
    //
    // Requested response type must be allowed (present in database field)
    if ( ! adawat::str_csv_has_value( p_session.mode,
                                      client[ "s_allowed_modes" ] ) ) {
        p_details = "mode not allowed";
        return create_result::forbidden;
    }
    //
    // Scopes are keywords, space separated
    if ( ! p_session.scopes.empty() ) {
        std::regex  R( "^\\w+( \\w+)*$" );  // regexp: ^\w+( \w+)*$
        //
        if ( ! std::regex_match( p_session.scopes, R ) ) {
            p_details = "malformed scope";
            return create_result::invalid_scope;
        }
    }
    //
    // [PKCE] verify that: - challenge is there if required;
    //                     - the challenge method is known
    if ( p_session.challenge.empty() ) {
        if ( client[ "b_pkce" ] == "1" ) {  // PKCE is mandatory for this client
            p_details = "challenge missing";
            return create_result::forbidden;
        }
    }
    //
    if ( ! p_session.challenge_method.empty() ) {
        if ( p_session.challenge_method != "S256" && 
             p_session.challenge_method != "plain" ) {
            p_details = "unsuported challenge method";
            return create_result::forbidden;
        }
    }
    //
    // Create session
    p_session_id        = adawat::uid_no_ts( constants::k_oauth_code_size );
    //               
    p_session.name      = client[ "s_name" ];
    p_session.logo      = client[ "s_logo" ];
    p_session.expire    = time( nullptr ) + session_lifetime_s + 5;  // +5: see find_session
    //
    // Store new session. Password session are not stored: they just last
    // for the duration of the web service call
    if ( p_session.mode != "password" )
    {
        std::lock_guard<std::shared_mutex>  lock( m_mutex_sessions );
        //
        if ( m_session_usage[ p_session.client_id ] >= usage_maximum ) {
            p_details = "too many open sessions";
            return create_result::full;
        }
        //
        m_sessions     [ p_session_id        ] = p_session;
        m_session_usage[ p_session.client_id ] ++;
        //
        m_stats_session_usage.add_value( m_session_usage[ p_session.client_id ] );
    }
    //
    return create_result::success;
}

//-------------------------------------------------------------------------
// Create a code to store a newly created Access Token
// We just rename the session and shorten its lifetime to m_code_lifetime_s
// Session must be checked just before by calling find_session that guarantee
// that the session will remains for 3s. If session is gone, an empty string 
// is returned.
std::string OAuthManager::create_code ( const std::string &  p_session_id,
                                        const std::string &  p_accesstoken,
                                        int64_t              p_at_expire_in,
                                        const std::string &  p_refreshtoken )
{
    std::string code = adawat::uid_no_ts( constants::k_oauth_code_size );
    //
    // Get config
    int64_t code_lifetime_s;
    //
    {
        std::shared_lock< std::shared_mutex > lock( m_mutex_config );
        //
        code_lifetime_s = m_code_lifetime_s;
    }
    //
    // Complete session, convert it to code
    {
        std::lock_guard<std::shared_mutex>  lock( m_mutex_sessions );
        //
        auto session = m_sessions.find( p_session_id );
        if ( session == m_sessions.end() )
            return "";
        //
        m_stats_session_lifetime.add_value( time( nullptr ) - session->second.creation );
        //
        session->second.accesstoken  = p_accesstoken;
        session->second.at_expire_in = p_at_expire_in;
        session->second.refreshtoken = p_refreshtoken;
        session->second.expire       = time( nullptr ) + code_lifetime_s + 5;  // +5: see find_session
        //
        // Convert this session in a code
        m_sessions[ code ] = session->second;  // create a new slot for the code
        m_sessions.erase( p_session_id );      // remove the old session
    }
    //
    return code;
}

//-------------------------------------------------------------------------
// Find a session that still have at least a few seconds to live
bool OAuthManager::find_session ( const std::string &  p_session_id,
                                  OAuthSession &       p_session,
                                  bool                 p_increment_attempt /* = true */ )
{
    std::shared_lock< std::shared_mutex > lock( m_mutex_sessions );
    //
    auto session = m_sessions.find( p_session_id );
    //
    if ( session == m_sessions.end() )
        return false;
    if ( ( session->second.expire - time( nullptr ) ) < 5 )  // to guarantee to caller that the session
        return false;                                        // will be there after find_session
    //
    if ( p_increment_attempt )
        session->second.attempt ++;
    //
    p_session = session->second;
    return true;
}

//-------------------------------------------------------------------------
// Find an authorization code
// Codes are just sessions
bool OAuthManager::find_code ( const std::string &  p_code,
                               OAuthSession &       p_session )
{
    return find_session( p_code, p_session, false );
}

//-------------------------------------------------------------------------
// Delete a session
void OAuthManager::delete_session ( const std::string &  p_session_id )
{
    std::lock_guard<std::shared_mutex>  lock( m_mutex_sessions );
    //
    auto session = m_sessions.find( p_session_id );
    //
    if ( session != m_sessions.end() ) {
        //
        m_stats_session_lifetime.add_value( time( nullptr ) - session->second.creation );
        //
        m_session_usage[ session->second.client_id ] --;
        m_sessions.erase( session );
    }
}

//-------------------------------------------------------------------------
// Delete an authorization code
// Codes are just sessions
void OAuthManager::delete_code ( const std::string &  p_code )
{
    delete_session( p_code );
}

//-------------------------------------------------------------------------
// Get the HTML code for the Login page
std::string OAuthManager::get_login_html ( void )
{
    std::shared_lock< std::shared_mutex >  lock( m_mutex_config );
    //
    return m_html_login_code;
}

//-------------------------------------------------------------------------
// Validate CAPTCH

    bool captcha_google ( const std::string &           p_secret,
                          const std::string &           p_hostname,
                          const std::string &           p_action,
                          double                        p_score,
                          const std::string &           p_response,
                          masnae::ProcessingResponse &  p_result )
    {
        // Call Google
        rapidjson::Document response;
        std::string         response_json, status, reason = "unknown", text;
        double              score;
        bool                verified;
        //
        masnae::globals::http_handler->request_json(
            "google/recaptcha/api/siteverify",
            {}, { { "secret",   p_secret   },
                  { "response", p_response }
            }, {},
            response, response_json
        );
        //
        // Received response is like:
        //    { "status": "success", "data":    // JResp encapsulation
        //        { "success": true|false,
        //          "challenge_ts": timestamp,  // timestamp of the challenge load (ISO format yyyy-MM-dd'T'HH:mm:ssZZ)
        //          "hostname": string,         // the hostname of the site where the reCAPTCHA was solved
        //          "error-codes": [...]        // optional } }
        //
        // Check that the call was successful
        if ( ! masnae::rapidjson_get_string( response, "/status", status ) ||
             status != "success" )
        {
            masnae::rapidjson_get_string( response, "/reason", reason );
            //
            p_result.add_log( masnae::ProcessingResponse::verbose,
                              "Call to Google failed: " + reason );
            //
            return false;
        }
        //
        // Get Google answer
        if ( ! masnae::rapidjson_get_bool( response, "/data/success", verified ) ) {
            p_result.add_log( masnae::ProcessingResponse::verbose,
                              "Invalid Google answer" );
            //
            return false;
        }
        //
        // If rejected, log why
        if ( ! verified ) {
            p_result.add_log( masnae::ProcessingResponse::verbose,
                              "CAPTCHA rejected" );
            //
            rapidjson::Value * ec = rapidjson::Pointer( "/data/error-codes" ).Get( response );
            //
            if ( ec != nullptr && ! ec->IsArray() )
                for ( auto & v : ec->GetArray() )
                    if ( v.IsString() )
                        p_result.add_log( masnae::ProcessingResponse::verbose,
                                          v.GetString() );
            //
            return false;
        }
        //
        // If requested, check hostname
        if ( ! p_hostname.empty() )
            if ( ! masnae::rapidjson_get_string( response, "/data/hostname", text ) ||
                 text != p_hostname )
            {
                p_result.add_log( masnae::ProcessingResponse::verbose,
                                  "Cannot match hostname: " + text );
                return false;
            }
        //
        // If requested, check action
        if ( ! p_action.empty() )
            if ( ! masnae::rapidjson_get_string( response, "/data/action", text ) ||
                 text != p_action )
            {
                p_result.add_log( masnae::ProcessingResponse::verbose,
                                  "Cannot match action: " + text );
                return false;
            }
        //
        // If a score is present, it must match configured minimum
        if ( masnae::rapidjson_get_double( response, "/data/score", score ) )
            if ( score < p_score )
            {
                p_result.add_log( masnae::ProcessingResponse::verbose,
                                  "Score is to low: " + std::to_string( score ) );
                return false;
            }
        //
        return true;
    }

bool OAuthManager::check_CAPTCHA ( const masnae::SingleKey_ValueList &  p_parameters,
                                   masnae::ProcessingResponse &         p_result )
{
    // Retrieve configuration
    decltype( m_captcha ) captcha;
    //
    {
        std::shared_lock< std::shared_mutex >  lock( m_mutex_config );
        //
        captcha = m_captcha;
    }
    //
    // Validata CAPTCHA
    switch ( captcha.mode )
    {
        default:
            return false;
        //
        case captcha_mode::e_none:
            return true;
        //
        case captcha_mode::e_google:
            {
                std::string  response;
                //
                p_result.add_log( masnae::ProcessingResponse::debug,
                                  "Calling Google reCAPTCHA validation" );
                //
                return
                    // Google reCAPTCHA adds a hidden parameter to the submitted form
                    masnae::get_parameter( p_parameters,
                                           "g-recaptcha-response",
                                           response,
                                           false,   // not empty
                                           false,   // mandatory
                                           p_result )
                    &&
                    captcha_google( captcha.google_secret,
                                    captcha.google_hostname,
                                    captcha.google_action,
                                    captcha.google_score,
                                    response,
                                    p_result );
            }
    }
}

//-------------------------------------------------------------------------
// Get the corresponding OAuth error tag (empty if no error)
std::string OAuthManager::get_error_tag ( create_result p_error )
{
    switch ( p_error )
    {
        case create_result::success:       return "";
        case create_result::forbidden:     return "unauthorized_client";
        case create_result::full:          return "temporarily_unavailable";
        case create_result::invalid_scope: return "invalid_scope";
        default:                           return "server_error";
    }
}

//-------------------------------------------------------------------------
// Reads configuration from global config object.
bool OAuthManager::update_config ( void )
{
    bool         ok = true;
    int64_t      new_accesstoken_lifetime_s;
    int64_t      new_refreshtoken_lifetime_s;
    int64_t      new_code_lifetime_s;
    int64_t      new_session_lifetime_s;
    int64_t      new_usage_maximum;
    int64_t      new_cleaning_probability;
    std::string  new_html_login_file;
    //
    // CAPTCHA
    std::string           new_captcha_mode_string;
    decltype( m_captcha ) new_captcha;
    //
    // Read new parameters
    new_accesstoken_lifetime_s =
        masnae::globals::config_handler->get_int64( "/oauth/accesstoken_lifetime",   3600 );
    //
    new_refreshtoken_lifetime_s =
        masnae::globals::config_handler->get_int64( "/oauth/refreshtoken_lifetime", 86400 );
    //
    new_code_lifetime_s =
        masnae::globals::config_handler->get_int64( "/oauth/code_lifetime",            10 );
    //
    new_session_lifetime_s =
        masnae::globals::config_handler->get_int64( "/oauth/session_lifetime",        120 );
    //
    new_usage_maximum =
        masnae::globals::config_handler->get_int64( "/oauth/usage_maximum",           100 );
    //
    new_cleaning_probability = 
        masnae::globals::config_handler->get_int64( "/oauth/cleaning_probability",     10 );
    //
    new_html_login_file =
        masnae::globals::config_handler->get_string( "/oauth/login_page",
                                                     urania::constants::k_default_login_file );
    //
    // CAPTCHA
    new_captcha_mode_string =
        masnae::globals::config_handler->get_string( "/oauth/captcha/mode",
                                                     "none" );
    //
    new_captcha.google_secret =
        masnae::globals::config_handler->get_string( "/oauth/captcha/google_secret" );
    //
    new_captcha.google_hostname =
        masnae::globals::config_handler->get_string( "/oauth/captcha/google_hostname" );
    //
    new_captcha.google_action =
        masnae::globals::config_handler->get_string( "/oauth/captcha/google_action" );
    //
    new_captcha.google_score =
        masnae::globals::config_handler->get_double( "/oauth/captcha/google_score", 0.5 );
    //
    // Check their value
    ok &= new_accesstoken_lifetime_s  >= 1;
    ok &= new_refreshtoken_lifetime_s >= 1;
    ok &= new_code_lifetime_s         >= 1 && new_code_lifetime_s      <=  3600;
    ok &= new_session_lifetime_s      >= 1 && new_session_lifetime_s   <=  3600;
    ok &= new_usage_maximum           >= 1 && new_usage_maximum        <= 10000;
    ok &= new_cleaning_probability    >= 1 && new_cleaning_probability <= 10000;
    //
    // CAPTCHA
    new_captcha.mode = captcha_mode::e_none;
    //
    if ( new_captcha_mode_string == "none" )
        new_captcha.mode = captcha_mode::e_none;
    else if ( new_captcha_mode_string == "google" )
        new_captcha.mode = captcha_mode::e_google;
    else
        ok &= false;
    //
    if ( ! ok ) {
        masnae::service_error_log( "OAuthManager: invalid config" );
        return false;
    }
    //
    // If file is not absolute, prepend with current directory
    if ( new_html_login_file[0] != '/' ) {
        char path[ PATH_MAX ];
        //
        new_html_login_file.insert( 0, "/" );    // getcwd returns a path without trailing /
        new_html_login_file.insert( 0, getcwd( path, sizeof( path ) ) );
    }
    //
    // Start by reconfiguring fie monitoring
    if ( new_html_login_file != m_html_login_file ) { // no lock:: cannot only be changed below
        stop_monitoring();
        //
        m_html_login_file = new_html_login_file;  // modified when monitoring is not running
        //
        if ( ! start_monitoring( m_html_login_file, false ) )
            return false;
        //
        monitoring_event( m_html_login_file );  // in case file was modified while not monitored
    }
    //
    // Then update standard parameters
    std::lock_guard<std::shared_mutex>  lock( m_mutex_config );
    //
    m_accesstoken_lifetime_s  = new_accesstoken_lifetime_s;
    m_refreshtoken_lifetime_s = new_refreshtoken_lifetime_s;
    m_code_lifetime_s         = new_code_lifetime_s;
    m_session_lifetime_s      = new_session_lifetime_s;
    m_usage_maximum           = new_usage_maximum;
    m_cleaning_probability    = new_cleaning_probability;                    
    //
    // CAPTCHA
    m_captcha                 = new_captcha;
    //
    return true;
}

//-------------------------------------------------------------------------
// When create_session is called, check if cleaning must be done and do it
void OAuthManager::maybe_clean ( void )
{
    // One chance out of m_cleaning_probability
    {
        std::shared_lock< std::shared_mutex >  lock( m_mutex_config );
        //
        std::uniform_int_distribution< int64_t > distribution( 0, m_cleaning_probability );
        //
        if ( distribution( m_generator ) != 0 )
            return;
    }
    //
    // Delete all old sessions and codes
    time_t now = time( nullptr );
    //
    std::lock_guard< std::shared_mutex >  lock( m_mutex_sessions );
    //
    for ( auto s = m_sessions.begin(); s != m_sessions.end(); )
        if ( s->second.expire < now ) {
            m_session_usage[ s->second.client_id ] --;
            m_sessions.erase( s++ );
        } else {
            s++;
        }
}

//-------------------------------------------------------------------------
// Called when HTML file changes
void OAuthManager::monitoring_event ( const std::string & p_file )
{
    std::string html;
    //
    if ( adawat::file_read( m_html_login_file, html ) ) {
        masnae::service_info_log( "OAuthManager loading HTML " + p_file );
        //
        std::lock_guard<std::shared_mutex>  lock( m_mutex_config );
        //
        m_html_login_code = html;
    } else {
        masnae::service_error_log( "OAuthManager failed to open file " + p_file +
                                   ", errno=" + std::to_string( errno ) );
    }
}

} // namespace urania
