// Copyright 2018 Pierre del Perugia
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

//
// Manage existing AccessTokens
//

#ifndef urania_accesstoken_h
#define urania_accesstoken_h

#include <random>
#include <shared_mutex>
#include <adawat/adawat_stats.h>
#include <adawat/adawat_uid.h>

#include "masnae_bus.h"
#include "urania_password.h"

namespace urania {

//-------------------------------------------------------------------------
// Informations present in an access or refresh token
struct AccessTokenDetails
{
    std::string  user;
    std::string  scopes;
    bool         refresh;
    std::string  client;
    std::string  peer_token_sha;  // contains the SHA2 of the token
};

//-------------------------------------------------------------------------
class AccessTokenManager : public masnae::bus::Subscriber
{
    public:
        // To be called once before any execution
        bool              start             ( void );
        //
        // To be called once before stopping the program
        void              stop              ( void );
        //
        // Return module information as a JSON string
        std::string       specific_info  ( void );
        //
        // Create an AccessToken on a user
        void    create_accesstoken      ( const std::string &           p_user,
                                          const std::string &           p_scopes,
                                          int64_t                       p_lifetime,
                                          std::string &                 p_accesstoken,
                                          masnae::ProcessingResponse &  p_result );
        //
        // Create a RefreshToken for a client on a user
        void    create_refreshtoken     ( const std::string &           p_user,
                                          const std::string &           p_client,
                                          const std::string &           p_scopes,
                                          int64_t                       p_lifetime,
                                          std::string &                 p_refreshtoken,
                                          masnae::ProcessingResponse &  p_result );
        //
        // Get details on an existing access or refresh token
        bool    get_token               ( const std::string &           p_accesstoken,
                                          AccessTokenDetails &          p_details,
                                          masnae::ProcessingResponse &  p_result );
        //
        // Using given AccessToken, sets the p_security object with
        // details/permissions associated
        bool    get_security            ( const std::string &           p_accesstoken,
                                          masnae::SecurityContext &     p_security,
                                          masnae::ProcessingResponse &  p_result );
        //
        // Delete an access or refresh token, using the raw or sha2 token id
        bool    delete_token            ( const std::string &           p_accesstoken,
                                          masnae::ProcessingResponse &  p_result );
        //
        bool    delete_token_sha        ( const std::string &           p_accesstoken_sha,
                                          masnae::ProcessingResponse &  p_result );
        //
        // Delete a user's access tokens, eventually keeping one of them
        bool    delete_user_tokens      ( const std::string &           p_user,
                                          masnae::ProcessingResponse &  p_result,
                                          const std::string &           p_except_accesstoken = "" );
        //
        // Make an Access and Refresh tokens point to each other
        void    update_peer             ( const std::string &           p_accesstoken,
                                          const std::string &           p_refreshtoken );
        //
        // Called by the bus with a new message
        virtual
        void    bus_event               ( const masnae::bus::Message &  p_message );
        //
    private:
        std::default_random_engine  m_generator;
        //
        // Configuration
        std::shared_mutex           m_mutex_config;
        int64_t                     m_cleaning_probability = 100;  // range 1-100-10000
        //
        // Statistics
        std::mutex                  m_stats_mutex;
        adawat::StatsValue          m_stats_cleaning_count;
        adawat::StatsValue          m_stats_cleaning_duration;
        //
        // Parse the configuration
        bool              update_config  ( void );
        //
        // Check if cleaning must be done and do it
        void              maybe_clean    ( void );
        //
        // Hash a token and encode the result using base 64
        std::string       sha2           ( const std::string &  p_token );
};

} // namespace urania

#endif
