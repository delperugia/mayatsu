// Copyright 2018 Pierre del Perugia
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

//
// Miscellaneous functions
//

#ifndef urania_misc_h
#define urania_misc_h

#include "masnae_common.h"

namespace urania {

//-------------------------------------------------------------------------
// Add parameters (and URL encode them) to an existing URL
// with or without existing parameters
std::string add_parameters_to_url ( std::string                      p_url,
                                    bool                             p_as_fragment,
                                    const masnae::SingleKey_Value &  p_parameters );
                                    
//-------------------------------------------------------------------------
// Extract login and password from a basic HTTP authentication
//   Authorization: Basic czZCaGRSa3F0MzpnWDFmQmF0M2JW
bool parse_basic_authentification( const std::string &  p_authorization,
                                   std::string &        p_client_id,
                                   std::string &        p_client_secret );

} // namespace urania

#endif
