// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

// Copyright 2018 Pierre del Perugia
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <algorithm>

#include <adawat/adawat_common.h>
#include <adawat/adawat_encoding.h>
#include <adawat/adawat_string.h>

#include "masnae_mysql.h"
#include "masnae_global.h"
#include "masnae_misc.h"
#include "urania_common.h"
#include "urania_misc.h"
#include "urania_oauth.h"
#include "urania_request.h"

namespace urania {

//-------------------------------------------------------------------------
// Build the cookie name used by a client. Each client used its own
// cookie (with its own RefreshToken) since each token can have
// different scopes. Encode special characters using _ prefix.
std::string cookie_name ( const OAuthSession &  p_session )
{
    std::string  encoded;
    char         s[4];    // placeholder for _ and 2 hexadecimal digits in a c-string
    //
    encoded.reserve( p_session.client_id.length() + 8 );  // arbitrary extra space
    //
    for ( const auto & c: p_session.client_id )
        if ( isalnum( c ) ) {
            encoded += c;
        } else {
            sprintf( s, "_%02X", c & 0xff );
            encoded += s;
        }
    //
    return "RememberMe_" + encoded;
}

//-------------------------------------------------------------------------
// Build a 302 URL redirection response
// Parameters are added (URL encoded) as query parameter (after the ?) or
// as fragment (after the #)
void redirect_response ( const std::string &              p_url,
                         bool                             p_as_fragment,
                         const masnae::SingleKey_Value &  p_parameters,
                         masnae::ProcessingResponse &     p_result )
{
    p_result.set_http_status( "302 Found" );
    //
    p_result.add_http_header( "Location",
                              add_parameters_to_url( p_url,
                                                     p_as_fragment,
                                                     p_parameters ) );
    //
    p_result.set_raw( "" );  // no body
}

//-------------------------------------------------------------------------
// Build a 302 URL redirection error response
//  response_type code:  4.1.2.1.  Error Response
//  response type token: 4.2.2.1.  Error Response
void redirect_response_error ( const OAuthSession &          p_session,
                               const std::string &           p_error,
                               const std::string &           p_description,
                               masnae::ProcessingResponse &  p_result )
{
    p_result.add_log( masnae::ProcessingResponse::regular,
                      "Redirect error: " + p_error + " " + p_description );
    //
    bool reject_request = false;
    bool as_fragment    = false;
    //
    if ( p_session.mode == "code" )
        as_fragment = false;
    else
    if ( p_session.mode == "token" )
        as_fragment = true;
    else
        reject_request = true;
    //
    if ( p_session.redirect_uri_to_use.empty() ) // no default or client set a forbidden URI
        reject_request = true;
    //
    if ( reject_request )
    {
        // Special case: mode is unknown, we we don't know
        // if fragment must be used or not. Instead, do a HTTP 400 error.
        // Same if there is no URI (no default or unauthorized one received):
        // (see 4.1.2.1, 4.2.2.1)
        p_result.set_http_status( "400 Bad Request" );
        p_result.set_raw        ( p_error + ": " + p_description );
        return;
    }
    //
    redirect_response( p_session.redirect_uri_to_use,
                       as_fragment,
                       { { "error",             p_error         },
                         { "error_description", p_description   },
                         { "state",             p_session.state } },
                       p_result );
}

//-------------------------------------------------------------------------
// Build a 302 URL redirection successful response
// response type code:  (4.1.2.  Authorization Response)
// response type code:  (4.2.2.  Access Token Response)
void redirect_response_ok ( const std::string &           p_session_id,
                            const OAuthSession &          p_session,
                            masnae::ProcessingResponse &  p_result )
{
    p_result.add_log( masnae::ProcessingResponse::regular,
                      "Redirect ok for " + p_session.mode      +
                      " client_id="      + p_session.client_id +
                      " session_id="     + p_session_id );
    //
    if ( p_session.mode == "token" ) {
        redirect_response( p_session.redirect_uri_to_use,
                           true,
                           { { "access_token", p_session.accesstoken,                   },
                             { "expires_in",   std::to_string( p_session.at_expire_in ) },
                             { "token_type",   "bearer"                                 },
                             { "state",        p_session.state                          } },
                           p_result );
        //
        // Session doesn't need to exist anymore
        g_OAuthManager.delete_session( p_session_id );
    }
    else if ( p_session.mode == "code" ) {
        // Convert session into code
        std::string code = g_OAuthManager.create_code( p_session_id,
                                                       p_session.accesstoken,
                                                       p_session.at_expire_in,
                                                       p_session.refreshtoken );
        //
        redirect_response( p_session.redirect_uri_to_use,
                           false,
                           { { "code",  code            },
                             { "state", p_session.state } },
                           p_result );
    }
}

//-------------------------------------------------------------------------
// Build an access token request response (5.  Issuing an Access Token)
// Is is just a regular JSon response
void token_response ( const masnae::SingleKey_Value &  p_parameters,
                      masnae::ProcessingResponse &     p_result )
{
    adawat::Data  body;
    //
    adawat::set_data( body, masnae::json_convert( p_parameters ) );
    //
    p_result.add_http_header( "Content-Type", "application/json");
    p_result.set_raw        ( std::move( body ) );
}

//-------------------------------------------------------------------------
// Build a successful token request response (5.2.  Error Response)
void token_response_error( const std::string &           p_error,
                           const std::string &           p_description,
                           masnae::ProcessingResponse &  p_result )
{
    p_result.add_log( masnae::ProcessingResponse::regular,
                      "Token error " + p_error + " " + p_description );
    //
    p_result.set_http_status( "400 Bad Request" );
    //
    token_response( { { "error",             p_error       },
                      { "error_description", p_description } },
                    p_result );
}

//-------------------------------------------------------------------------
// Build a successful token request response (5.1.  Successful Response)
void token_response_ok( const std::string &           p_session_id,
                        const OAuthSession &          p_session,
                        masnae::ProcessingResponse &  p_result )
{
    p_result.add_log( masnae::ProcessingResponse::regular,
                      "Token ok:"
                      " client_id="      + p_session.client_id +
                      " session_id="     + p_session_id );
    //
    masnae::SingleKey_Value parameters =
        { { "access_token",  p_session.accesstoken                    },
          { "expires_in",    std::to_string( p_session.at_expire_in ) },
          { "token_type",    "bearer"                                 } };
    //
    // Refresh is generated/sent if requested
    if ( ! p_session.refreshtoken.empty() )
        parameters[ "refresh_token" ] = p_session.refreshtoken;
    //
    token_response( parameters,
                    p_result );
}

//-------------------------------------------------------------------------
// Build the login page to display to the end user for login
void build_login_page ( const std::string &             p_session_id,
                        const OAuthSession &            p_session,
                        const std::string &             p_feed_back,
                        masnae::ProcessingResponse &    p_result )
{
    p_result.add_log( masnae::ProcessingResponse::regular,
                      "Building login page for " + p_session.name  ); 
    p_result.add_log( masnae::ProcessingResponse::verbose,
                      " fb=" + p_feed_back ); 
    //
    std::string html = g_OAuthManager.get_login_html();
    //
    adawat::str_replace( html, "{{attempt}}",   std::to_string( p_session.attempt ) );
    adawat::str_replace( html, "{{client_id}}", p_session.client_id );
    adawat::str_replace( html, "{{feed_back}}", p_feed_back         );
    adawat::str_replace( html, "{{id}}",        p_result.get_id()   );
    adawat::str_replace( html, "{{logo}}",      p_session.logo      );
    adawat::str_replace( html, "{{name}}",      p_session.name      );
    adawat::str_replace( html, "{{session_id}}",p_session_id        );
    adawat::str_replace( html, "{{user_id}}",   p_session.user_id   );
    adawat::str_replace( html, "{{scopes}}",    p_session.scopes    );
    //
    // On first login attempt, do no activate CAPTCHA
    if ( p_session.attempt > 0 ) {
        adawat::str_replace( html, "CAPTCHA-->",    "" );
        adawat::str_replace( html, "<!--CAPTCHA",   "" );
    }
    //
    // [RM]
    adawat::str_replace( html, "{{remember_me_display_none}}",
                               p_session.with_remember_me ? "" : "display: none;" );
    //
    // Return HTML page
    p_result.add_http_header( "Content-Type", "text/html" );
    p_result.set_raw        ( html );
}

//-------------------------------------------------------------------------
// Set the error and description text corresponding to result
// if result is an error.
// Logs error and returns false if result is an error.
bool failed_password_check ( PasswordManager::result_type   p_code,
                             const std::string &            p_details,
                             std::string &                  p_error,
                             std::string &                  p_description,
                             masnae::ProcessingResponse &   p_result )
{
    switch ( p_code )
    {
        case PasswordManager::result_type::success:
            return false;
        case PasswordManager::result_type::forbidden:
            p_error       = "invalid_grant";
            p_description = "forbidden";
            break;
        case PasswordManager::result_type::no_password:
            p_error       = "unauthorized_client";
            p_description = "no_password";
            break;
        case PasswordManager::result_type::banned:
            p_error       = "invalid_grant";
            p_description = "banned for " + p_details + "s";
            break;
        case PasswordManager::result_type::failure:
        default:
            p_error       = "invalid_grant";
            p_description = "failure";
            break;
    }
    //
    p_result.add_log( masnae::ProcessingResponse::verbose,
                      "password_check: " + 
                      std::to_string( static_cast< int >( p_code ) ) + " " +
                      p_error + " " + p_description );
    //
    return true;
}

//-------------------------------------------------------------------------
// Check user password and fill p_result on error
// Generates JSon with token context (implicit, refresh & password)
PasswordManager::result_type
    check_user_password ( const std::string &           p_user_id,
                          const std::string &           p_user_secret,
                          bool                          p_update_lastlogin,
                          std::string &                 p_details,
                          masnae::ProcessingResponse &  p_result )
{
    std::string error, description;
    //
    auto        result = g_PasswordManager.password_check( p_user_id,
                                                           p_user_secret,
                                                           p_update_lastlogin,
                                                           p_details,
                                                           p_result );
    //
    if ( failed_password_check( result, p_details,
                                error, description, p_result ) )
    {
        // Fill with the default error response. Caller can override this
        // if he does special treatment depending on result (he must call reset
        // on p_result and put what he wants)
        token_response_error( error, description, p_result );
    }
    //
    return result;
}

//-------------------------------------------------------------------------
// Check user password and fill p_result on error
// Generates a redirect response on error
PasswordManager::result_type
    check_user_password ( const OAuthSession &          p_session,
                          const std::string &           p_user_id,
                          const std::string &           p_user_secret,
                          bool                          p_update_lastlogin,
                          std::string &                 p_details,
                          masnae::ProcessingResponse &  p_result )
{
    std::string error, description;
    //
    auto        result = g_PasswordManager.password_check( p_user_id,
                                                           p_user_secret,
                                                           p_update_lastlogin,
                                                           p_details,
                                                           p_result );
    //
    if ( failed_password_check( result, p_details,
                                error, description, p_result ) )
    {
        // Fill with the default error response. Caller can override this
        // if he does special treatment depending on result (he must call reset
        // on p_result and put what he wants)
        redirect_response_error( p_session, error, description, p_result );
    }
    //
    return result;
}

//-------------------------------------------------------------------------
// 4.1.3.  Access Token Request
// Following process_oauth_authorize and process_oauth_login, use
// the code to retrieve the AccessToken
void process_token_authorization_code ( const masnae::SingleKey_ValueList &  p_parameters,
                                        const std::string &                  p_client_id,
                                        const std::string &                  p_client_secret,
                                        masnae::ProcessingResponse &         p_result )
{
    // Check remaining parameters
    std::string  code, redirect_uri, verifier;
    //
    if ( ! masnae::get_parameter( p_parameters, "code",         code,         false, false, p_result ) ||
         ! masnae::get_parameter( p_parameters, "redirect_uri", redirect_uri, false, true,  p_result ) ||
           // [PKCE]
         ! masnae::get_parameter( p_parameters, "code_verifier", verifier,    false, true, p_result ) )
    {
        token_response_error( "invalid_request", p_result.get_error(), p_result );
        return;
    }
    //
    p_result.add_log( masnae::ProcessingResponse::regular,
                      "Token request by code from client " + p_client_id );
    //
    // Find session
    OAuthSession  session;
    //
    if ( ! g_OAuthManager.find_code( code, session ) ) {
        token_response_error( "invalid_grant", "code not found", p_result );
        return;
    }
    //
    // In all other cases delete the code at the end
    std::string error_details;
    //
    // The code must belong to this client
    if ( p_client_id != session.client_id )
        token_response_error( "invalid_grant", "different client", p_result );
    //
    // If URI was set in /authorize, it must be the same in /token
    else if ( redirect_uri != session.redirect_uri_received )
        token_response_error( "invalid_grant", "different redirect_uri", p_result );
    //
    else if ( ! session.check_challenge( verifier ) )
        token_response_error( "invalid_grant", "invalid challenge", p_result );
    //
    // Check client password (fill p_result on error)
    // Called at the end to protect against client banning
    // (DoS by people only knowing client_id)
    else if ( check_user_password( p_client_id,
                                   p_client_secret,
                                   false,            // don't update LastLogin
                                   error_details,
                                   p_result ) == PasswordManager::result_type::success )
    {
        token_response_ok( code, session, p_result );
    }
    //
    // Code doesn't need to exist anymore
    g_OAuthManager.delete_code( code );
}

//-------------------------------------------------------------------------
// 4.3.  Resource Owner Password Credentials Grant
// A plain login.
void process_token_password ( const masnae::SingleKey_ValueList &  p_parameters,
                              const std::string &                  p_client_id,
                              const std::string &                  p_client_secret,
                              masnae::ProcessingResponse &         p_result )
{
    // Build a temporary session with minimal information
    OAuthSession  session;
    //
    session.mode      = "password";
    session.client_id = p_client_id;
    //
    // Check remaining parameters
    if ( ! masnae::get_parameter( p_parameters, "scope", session.scopes, false, true, p_result ) )
    {
        token_response_error( "invalid_request", p_result.get_error(), p_result );
        return;
    }
    //
    p_result.add_log( masnae::ProcessingResponse::regular,
                      "Token request by password from client " + p_client_id );
    //
    // Find client and check session. It will not be stored, but
    // will be verified. It is not needed to delete it.
    std::string session_id, error_description, error_details;
    //
    auto result = g_OAuthManager.create_session( session, session_id, error_description );
    //
    if ( result != OAuthManager::create_result::success ) {
        p_result.add_log( masnae::ProcessingResponse::verbose,
                          "create_session: " +
                          std::to_string( static_cast< int >( result ) ) + " " +
                          error_description );
        //
        token_response_error( g_OAuthManager.get_error_tag( result ),
                              error_description,
                              p_result );
        return;
    }
    //
    // Check user's credentials (p_result filled on error)
    if ( check_user_password( p_client_id,
                              p_client_secret,
                              true,             // update LastLogin
                              error_details,
                              p_result ) != PasswordManager::result_type::success )
        return;
    //
    // Create an AccessToken
    session.at_expire_in = g_OAuthManager.get_accesstoken_lifetime();
    //
    g_AccessTokenManager.create_accesstoken( session.client_id,
                                             session.scopes,
                                             session.at_expire_in,
                                             session.accesstoken,
                                             p_result );
    //
    // If requested, generate a RefreshToken
    if ( adawat::str_csv_has_value( "refresh", session.scopes, ' ' ) )  // space separated
    {
        g_AccessTokenManager.create_refreshtoken( session.client_id,
                                                  session.client_id,
                                                  session.scopes,
                                                  g_OAuthManager.get_refreshtoken_lifetime(),
                                                  session.refreshtoken,
                                                  p_result );
        //
        // Make an Access and Refresh tokens point to each other
        g_AccessTokenManager.update_peer( session.accesstoken, session.refreshtoken );
    }
    //
    token_response_ok( session_id, session, p_result );
}

//-------------------------------------------------------------------------
// 6.  Refreshing an Access Token
// Use a RefreshToken to create a new AccessToken

    // Build the intersection of two scopes (space-delimited list)
    std::string intersection ( const std::string  p_a,
                               const std::string  p_b )
    {
        auto a = adawat::str_split( p_a, " " );
        auto b = adawat::str_split( p_b, " " );
        //
        decltype( a ) c;
        std::set_intersection( a.begin(), a.end(),
                               b.begin(), b.end(),
                               std::back_inserter( c ) );
        //
        return adawat::str_join( c, " " );
    }

void process_token_refresh ( const masnae::SingleKey_ValueList &  p_parameters,
                             const std::string &                  p_client_id,
                             const std::string &                  p_client_secret,
                             masnae::ProcessingResponse &         p_result )
{
    // Check remaining parameters
    std::string  refresh_token, scopes;
    //
    if ( ! masnae::get_parameter( p_parameters, "refresh_token", refresh_token, false, false, p_result ) ||
         ! masnae::get_parameter( p_parameters, "scope",         scopes,        false, true,  p_result ) )
    {
        token_response_error( "invalid_request", p_result.get_error(), p_result );
        return;
    }
    //
    p_result.add_log( masnae::ProcessingResponse::regular,
                      "Token refresh request from client " + p_client_id );
    //
    // Start by retrieving RefreshToken details
    AccessTokenDetails  at_details;
    //
    if ( ! g_AccessTokenManager.get_token( refresh_token, at_details, p_result ) )
        token_response_error( "invalid_grant", "invalid token", p_result );
    else if ( ! at_details.refresh )
        token_response_error( "invalid_grant", "not a refresh token", p_result );
    else if ( at_details.client != p_client_id )
        token_response_error( "invalid_grant", "token for different client " +
                              at_details.client, p_result );
    else
    {
        std::string   error_details;
        //
        // Then, to avoid DoS by client banning with bad RefreshToken,
        // check client credentials. Fill p_result on error.
        if ( check_user_password( p_client_id,
                                  p_client_secret,
                                  false,            // don't update LastLogin
                                  error_details,
                                  p_result ) == PasswordManager::result_type::success )
        {
            OAuthSession  session;
            //
            // The new scopes is the intersection of the RefreshToken ones
            // and of the one receives in parameters
            session.scopes       = intersection( scopes, at_details.scopes );
            //
            session.client_id    = p_client_id;
            session.at_expire_in = g_OAuthManager.get_accesstoken_lifetime();
            //
            p_result.add_log( masnae::ProcessingResponse::regular,
                              "Token refreshes for user " + at_details.user +
                              ", scopes " + scopes );
            //
            // Delete previous AccessToken
            g_AccessTokenManager.delete_token_sha( at_details.peer_token_sha, p_result );
            //
            // Create new one
            g_AccessTokenManager.create_accesstoken( at_details.user,
                                                     session.scopes,
                                                     session.at_expire_in,
                                                     session.accesstoken,
                                                     p_result );
            //
            // Make an Access and Refresh tokens point to each other
            g_AccessTokenManager.update_peer( session.accesstoken, refresh_token );
            //
            token_response_ok( "---", session, p_result );
        }
    }
}

//-------------------------------------------------------------------------
// If a cookie with RefreshToken is received in "code" mode, try to use it
bool process_oauth_authorize_with_remember_me ( const std::string &           p_session_id,
                                                OAuthSession &                p_session,
                                                const std::string &           p_rememberme_token,
                                                masnae::ProcessingResponse &  p_result )
{
    // Start by retrieving the RefreshToken details
    AccessTokenDetails  at_details;
    //
    if ( ! g_AccessTokenManager.get_token( p_rememberme_token, at_details, p_result ) ) {
        p_result.add_log( masnae::ProcessingResponse::regular, "RememberMe: invalid token" );
        //
        // Clear cookie in user browser
        p_result.add_http_header( "Set-Cookie",
                                  cookie_name( p_session ) + "=; Expires=Thu, 01 Jan 1970 00:00:01 GMT;" );
        return false;
    }
    else if ( ! at_details.refresh ) {
        p_result.add_log( masnae::ProcessingResponse::regular, "RememberMe: not a refresh token" );
        return false;
    }
    else if ( at_details.client != p_session.client_id ) {
        p_result.add_log( masnae::ProcessingResponse::regular, "RememberMe: token for different client " +
                                                               at_details.client );
        return false;
    }
    else if ( at_details.scopes !=  p_session.scopes ) {
        p_result.add_log( masnae::ProcessingResponse::regular, "RememberMe: scopes have changed" );
        return false;
    }
    //
    // Delete previous AccessToken
    g_AccessTokenManager.delete_token_sha( at_details.peer_token_sha, p_result );
    //
    p_result.add_log( masnae::ProcessingResponse::regular,
                      "RememberMe:"
                      " for user "   + at_details.user +
                      " session_id=" + p_session_id );
    //
    p_session.user_id = at_details.user;
    //
    // Create an AccessToken
    p_session.at_expire_in = g_OAuthManager.get_accesstoken_lifetime();
    //
    g_AccessTokenManager.create_accesstoken( p_session.user_id,
                                             p_session.scopes,
                                             p_session.at_expire_in,
                                             p_session.accesstoken,
                                             p_result );
    //
    // Make an Access and Refresh tokens point to each other
    g_AccessTokenManager.update_peer( p_session.accesstoken, p_rememberme_token );
    //    
    // Continue the user session by using the existing RefreshToken 
    p_session.refreshtoken = p_rememberme_token;
    //
    // Convert session into code
    std::string code = g_OAuthManager.create_code( p_session_id,
                                                   p_session.accesstoken,
                                                   p_session.at_expire_in,
                                                   p_session.refreshtoken );
    //
    redirect_response( p_session.redirect_uri_to_use,
                       false,
                       { { "code",  code            },
                         { "state", p_session.state } },
                       p_result );
    //
    return true;
}

//-------------------------------------------------------------------------
// 4.1.  Authorization Code Grant
// 4.2.  Implicit Grant
//   First step: will display the customized login page to the user
void UraniaRequestHandler::process_oauth_authorize ( const masnae::SingleKey_ValueList &  p_parameters,
                                                     masnae::ProcessingResponse &         p_result )
{
    std::string   error, error_description;  // filled with error code only if an error occurred
    OAuthSession  session;
    std::string   session_id;
    std::string   remember_me;
    //
    // Check parameters
    // To be sure that all parameters are read, get_parameter is called in different
    // expressions (if not state would be empty if redirect_uri is empty)
    bool ok = true;
    //
    ok &= masnae::get_parameter( p_parameters, "response_type", session.mode,                  false, false, p_result );
    ok &= masnae::get_parameter( p_parameters, "client_id",     session.client_id,             false, false, p_result );
    ok &= masnae::get_parameter( p_parameters, "redirect_uri",  session.redirect_uri_received, false, true,  p_result );
    ok &= masnae::get_parameter( p_parameters, "scope",         session.scopes,                false, true,  p_result );
    ok &= masnae::get_parameter( p_parameters, "state",         session.state,                 false, true,  p_result );
    ok &= masnae::get_parameter( p_parameters, "remember_me",   remember_me,                   false, true,  p_result );
    // [PKCE]
    ok &= masnae::get_parameter( p_parameters, "code_challenge",        session.challenge,        false, true, p_result );
    ok &= masnae::get_parameter( p_parameters, "code_challenge_method", session.challenge_method, false, true, p_result );
    //
    p_result.add_log( masnae::ProcessingResponse::regular,
                      "Authorize from client " + session.client_id +
                      ", mode " + session.mode );
    //
    if ( ! ok  )
    {
        error             = "invalid_request";
        error_description = p_result.get_error();
    }
    else
    {
        // [RM] When processing an Authorization Code Grant, check if the client
        // wants to offer the RememberMe option (on the Login page).
        session.with_remember_me = session.mode == "code" &&
                                   remember_me  == "true";
        //
        if ( session.mode == "code" ||  // only accepted types here
             session.mode == "token" )
        {
            // Find client and create session
            auto result = g_OAuthManager.create_session( session, session_id, error_description );
            //
            if ( result != OAuthManager::create_result::success ) {
                error = g_OAuthManager.get_error_tag( result );
            }
        } else {
            error             = "unsupported_response_type";
            error_description = "unknown response_type";
        }
    }
    //
    // [RM] If there is a RememberMe cookie, try to use it to log in automatically.
    if ( error.empty() && session.mode == "code" && session.with_remember_me )
    {
        std::string remember_me_token;
        //
        if ( get_cookie( cookie_name( session ), remember_me_token ) &&
             process_oauth_authorize_with_remember_me( session_id,
                                                       session,
                                                       remember_me_token,
                                                       p_result ) )
        {
            return;  // returns a 302 URL redirection response
        }
    }
    //
    // Generate HTML login page or error response
    if ( error.empty() ) {
        p_result.add_log( masnae::ProcessingResponse::regular,
                          "Authorize: mode=" + session.mode      +
                          " client_id="      + session.client_id +
                          " session_id="     + session_id );
        //
        build_login_page( session_id,
                          session,
                          "",         // first time: no feedback message to display...
                          p_result );
    } else {
        p_result.add_log( masnae::ProcessingResponse::regular,
                          "Authorize error: mode=" + session.mode      +
                          " client_id="            + session.client_id +
                          ": " + error + ", " + error_description );
        //
        redirect_response_error( session,
                                 error,
                                 error_description,
                                 p_result );  // 302 redirect
    }
}

//-------------------------------------------------------------------------
// Retry login: rebuild the login page with some feed back to the user
void process_oauth_login_error ( const std::string &           p_session_id,
                                 const OAuthSession &          p_session,
                                 const std::string &           p_feed_back,
                                 const std::string &           p_log,
                                 masnae::ProcessingResponse &  p_result )
{
    // Log the failure cause
    p_result.add_log( masnae::ProcessingResponse::regular, p_log );
    //
    // We will put a different result
    p_result.reset_response();
    //
    // Display an error page (note that logo and name may not even be set then)
    build_login_page( p_session_id,
                      p_session,
                      p_feed_back,
                      p_result );  // retry login
}

//-------------------------------------------------------------------------
// In the login page, if the user press Cancel
void process_oauth_login_cancel ( const std::string &           p_session_id,
                                  const OAuthSession &          p_session,
                                  masnae::ProcessingResponse &  p_result )
{
    p_result.add_log( masnae::ProcessingResponse::regular, "Cancelled by user" );
    //
    redirect_response_error( p_session,
                             "access_denied",
                             "customer cancel request",
                             p_result );  // 302 redirect
    //
    g_OAuthManager.delete_session( p_session_id );  // session doesn't need to exist anymore
}

//-------------------------------------------------------------------------
// In the login page, when loging in with refresh token option
void process_oauth_login_with_refresh ( const std::string &                    p_user_id,
                                        OAuthSession &                         p_session,
                                        bool                                   p_token_in_cookie,
                                        const masnae::Fastcpipp_Environment &  p_environment,
                                        masnae::ProcessingResponse &           p_result )
{
    g_AccessTokenManager.create_refreshtoken( p_user_id,
                                              p_session.client_id,
                                              p_session.scopes,
                                              g_OAuthManager.get_refreshtoken_lifetime(),
                                              p_session.refreshtoken,
                                              p_result );
    //
    // [RM] If the user request to remain connected, send
    // the refresh token to his browser
    if ( p_token_in_cookie )
    {
        // Cookie's Path must point to the authorize endpoint
        // (e.g.: /urania/v1/oauth/authorize). Current endpoint is login
        // (e.g.: /urania/v1/oauth/login)
        std::string  authorize_path =
            p_environment.scriptName + "/oauth/authorize";
        //
        std::string  cookie = cookie_name( p_session ) + "=" + p_session.refreshtoken +
                              "; Secure; Path=" + authorize_path +
                              "; HttpOnly; SameSite=Strict;";
        std::time_t  now    = time( nullptr ) + g_OAuthManager.get_refreshtoken_lifetime() - 5;
        std::tm      utc;
        char         expire[ 64 ];
        //
        if ( gmtime_r( & now, & utc ) == nullptr ) {
            p_result.add_log( masnae::ProcessingResponse::regular,
                              "error creating cookie for RememberMe" );
        } else {
            strftime( expire, sizeof( expire ),
                      " Expires=%a, %d %b %Y %H:%M:%S GMT;", & utc );
            //
            p_result.add_http_header( "Set-Cookie", cookie + expire );
            //
            p_result.add_log( masnae::ProcessingResponse::regular,
                              "Cookie for RememberMe added" );
        }
    }
}

//-------------------------------------------------------------------------
// Called by our login page. Will redirect to client:
//   - directly with an AccessToken in implicit mode
//   - with a code in Authorization code mode
void UraniaRequestHandler::process_oauth_login ( const masnae::SingleKey_ValueList &  p_parameters,
                                                 masnae::ProcessingResponse &         p_result )
{
    OAuthSession  session;
    std::string   session_id, user_id, user_pwd;
    //
    // Check parameters
    // To be sure that all parameters are read, get_parameter is called in different
    // expressions (if not user_id would not be read if session_id is empty)
    bool ok = true;
    //
    ok &= masnae::get_parameter( p_parameters, "session_id", session_id, false, false, p_result );
    ok &= masnae::get_parameter( p_parameters, "user_id",    user_id,    true,  false, p_result );  // empty if cancel is set
    ok &= masnae::get_parameter( p_parameters, "user_pwd",   user_pwd,   true,  false, p_result );  // empty if cancel is set
    //
    if ( ! ok )  // retry login
        return process_oauth_login_error(
                  session_id,
                  session,
                  "error",                                            // feedback
                  "bad parameters: " + p_result.get_jresp_message(),  // log the error set by get_parameter
                  p_result
               );
    //
    // Find session (increment attempt)
    if ( ! g_OAuthManager.find_session( session_id, session ) )  // retry login
        return process_oauth_login_error(
                  session_id,
                  session,
                  "timeout",              // feedback
                  "session not found",    // log
                  p_result
               );
    //
    // Add extra contextual information in the session
    session.user_id = user_id;  // note that this is local, not stored in g_OAuthManager
    //
    // If the user pressed Cancel, go back to client now
    if ( p_parameters.count( "cancel" ) > 0 )
        return process_oauth_login_cancel(
                  session_id, 
                  session,
                  p_result
               );
    //
    // CAPTCHA verification
    // On first login attempt, there is no CAPTCHA
    if ( session.attempt > 1 )
        if ( ! g_OAuthManager.check_CAPTCHA( p_parameters, p_result ) ) // retry login
            return process_oauth_login_error(
                      session_id,
                      session,
                      "captcha",                    // feedback
                      "CAPTCHA validation failed",  // log
                      p_result
                   );
    //
    // Check user's credentials
    std::string error_details;
    auto        result = check_user_password( session,
                                              user_id,
                                              user_pwd,
                                              true,      // update LastLogin
                                              error_details,
                                              p_result );
    //
    switch ( result )
    {
        case PasswordManager::result_type::success:
            break;
        case PasswordManager::result_type::no_password:  // for security reason do not make a difference
        case PasswordManager::result_type::forbidden:    // if the account or password is wrong
            // We will put a different result
            p_result.reset_response();
            //
            // Prompt again the user for his password
            build_login_page( session_id,
                              session,
                              "forbidden",
                              p_result );  // retry
            return;
        case PasswordManager::result_type::banned:
            // We will put a different result
            p_result.reset_response();
            //
            // Prompt again the user for his password
            build_login_page( session_id,
                              session,
                              "banned:" + error_details,  // error_details contains the number of seconds
                              p_result );  // retry
            return;
        default:
            // p_result is already filled
            g_OAuthManager.delete_session( session_id );  // session doesn't need to exist anymore
            return;
    }
    //
    // Create an AccessToken
    session.at_expire_in = g_OAuthManager.get_accesstoken_lifetime();
    //
    g_AccessTokenManager.create_accesstoken( user_id,
                                             session.scopes,
                                             session.at_expire_in,
                                             session.accesstoken,
                                             p_result );
    //
    // If requested, generate a RefreshToken. This is only done when using
    // authorization grant (code) mode. Also, if requested by user, returns
    // the RefreshToken in a cookie to implement the Remember Me feature.
    if ( session.mode == "code" &&                                      // not for implicit 4.2.2.
         adawat::str_csv_has_value( "refresh", session.scopes, ' ' ) )  // space separated
    {
        // If requested (and allowed) to store Refresh token in cookie
        bool remember_me = p_parameters.count( "remember_me" ) > 0 &&
                           session.with_remember_me;
        //
        process_oauth_login_with_refresh( user_id, 
                                          session,
                                          remember_me,
                                          environment(),
                                          p_result );
        //
        // Make an Access and Refresh tokens point to each other
        g_AccessTokenManager.update_peer( session.accesstoken, session.refreshtoken );
    }
    //
    // For implicit grant, delete session (no code)
    // For authorization grant, session is converted into a code
    redirect_response_ok( session_id,
                          session,
                          p_result );
}

//-------------------------------------------------------------------------
// Access token requests:
// 4.1.3. Access Token Request
// 4.3.  Resource Owner Password Credentials Grant
// 6. Refreshing an Access Token
void UraniaRequestHandler::process_oauth_token ( const masnae::SingleKey_ValueList &  p_parameters,
                                                 masnae::ProcessingResponse &         p_result )
{
    // Common part between 'refresh_token', 'password' and 'authorization_code'
    std::string  grant_type, parameter_id, parameter_secret;
    //
    // Check parameters
    bool ok = true;
    //
    // Start by retrieving the requested type
    ok &= masnae::get_parameter( p_parameters, "grant_type", grant_type, false, false, p_result );
    //
    // Depending on grant_type, the caller credentials are in different parameters
    if ( grant_type == "authorization_code" || grant_type == "refresh_token" ) {
        parameter_id     = "client_id";
        parameter_secret = "client_secret";
    } else
    if ( grant_type == "password" ) {
        parameter_id     = "username";
        parameter_secret = "password";
    }
    else
    {
        token_response_error( "unsupported_grant_type", "only authorization_code, refresh_token and password are supported", p_result );
        //
        p_result.add_log( masnae::ProcessingResponse::verbose,
                          "grant_type requested: " + grant_type );
    }
    //
    // Read then caller credentials
    std::string id, secret;
    //
    ok &= masnae::get_parameter( p_parameters, parameter_id,     id,     false, true, p_result );
    ok &= masnae::get_parameter( p_parameters, parameter_secret, secret, false, true, p_result );
    //
    if ( ! ok ) {
        token_response_error( "invalid_request", p_result.get_error(), p_result );
        return;
    }
    //
    // If credentials are not passed as parameters, they must be
    // in HTTP Basic authentication scheme
    if ( id.empty() || secret.empty() )
        if ( ! parse_basic_authentification( environment().authorization,
                                             id, secret ) )
        {
            token_response_error( "invalid_request", "no identification found", p_result );
            return;
        }
    //
    // Process request
    if ( grant_type == "authorization_code" )
        process_token_authorization_code( p_parameters, id, secret, p_result );
    else
    if ( grant_type == "refresh_token" )
        process_token_refresh( p_parameters, id, secret, p_result );
    else
    if ( grant_type == "password" )
        process_token_password( p_parameters, id, secret, p_result );
    else
        token_response_error( "unsupported_grant_type", "server error", p_result );
}

} // namespace urania
