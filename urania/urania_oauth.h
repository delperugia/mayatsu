// Copyright 2018 Pierre del Perugia
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

//
// Manage OAuth signin
//

#ifndef urania_oauth_h
#define urania_oauth_h

#include <random>
#include <shared_mutex>
#include <adawat/adawat_stats.h>

#include "masnae_bus.h"
#include "masnae_monitor_files.h"

namespace urania {

//-------------------------------------------------------------------------
// A session (then a code) context
struct OAuthSession {
    //
    // Filled by UraniaRequestHandler: received parameters
    // Mode is the response_type parameter for the authorize endpoint
    // and grant_type for the token endpoint, password request
    std::string  mode;                   // code, token or password
    std::string  client_id;
    std::string  redirect_uri_received;  // if empty, use default
    std::string  scopes;                 // space separated
    std::string  state;
    bool         with_remember_me;       // Authorization Code Grant: RememberMe feature activated
    std::string  challenge;              // PKCE: client's challenge
    std::string  challenge_method;       // PKCE: challenge transformation (default 'S256')
    //
    // Filled by OAuthManager on creation
    std::string  name;                   // eventually containing HTML
    std::string  logo;                   // an image URI
    std::string  redirect_uri_to_use;    // either received one or configured default uri
    //
    // Incremented at each password attempt
    unsigned     attempt;                // incremented by find
    //
    // Filled by OAuthManager when signed-in
    std::string  user_id;                // user login, only local not in g_OAuthManager
    std::string  accesstoken;            // generated access token
    int64_t      at_expire_in;           // access token lifetime in second
    std::string  refreshtoken;           // generated refresh token
    //
    time_t       creation;               // session creation date
    time_t       expire;                 // session / code expiration date
    //
         OAuthSession    ();
    //   
    // [PKCE] check that verifier received match the challenge set   
    bool check_challenge ( const std::string &  p_verifier ) const;
};

//-------------------------------------------------------------------------
class OAuthManager : public  masnae::bus::Subscriber,
                     private masnae::MonitorFiles
{
    public:
        enum class create_result {
            success, forbidden, full, invalid_scope
        };
        //
        OAuthManager  ();
        //
        // To be called once before any execution
        bool              start          ( void );
        //
        // To be called once before stopping the program
        void              stop           ( void );
        //
        // Return module information as a JSON string
        std::string       specific_info  ( void );
        //
        // Accessors
        int64_t           get_accesstoken_lifetime  (void);
        int64_t           get_refreshtoken_lifetime (void);
        //
        // Called by the bus with a new message
        virtual
        void              bus_event         ( const masnae::bus::Message &  p_message );
        //
        // Find a client, create a session
        create_result     create_session    ( OAuthSession &       p_session,
                                              std::string &        p_session_id,
                                              std::string &        p_details );
        //
        // Create a code to store a newly created Access Token
        std::string       create_code       ( const std::string &  p_session_id,
                                              const std::string &  p_accesstoken,
                                              int64_t              p_at_expire_in,
                                              const std::string &  p_refreshtoken );
        //
        // Find a session
        bool              find_session      ( const std::string &  p_session_id,
                                              OAuthSession &       p_session,
                                              bool                 p_increment_attempt = true );
        //
        // Find an authorization code
        bool              find_code         ( const std::string &  p_code,
                                              OAuthSession &       p_session );
        //
        // Delete a session
        void              delete_session    ( const std::string &  p_session_id );
        //
        // Delete an authorization code
        void              delete_code       ( const std::string &  p_code );
        //
        // Get the HTML code for the Login page
        std::string       get_login_html    ( void );
        //
        // Validate CAPTCH
        bool              check_CAPTCHA     ( const masnae::SingleKey_ValueList &  p_parameters,
                                              masnae::ProcessingResponse &         p_result );
        //
        // Get the corresponding OAuth error tag (empty if no error)
        std::string       get_error_tag     ( create_result        p_error );
        //
    private:
        
        // Configuration
        std::shared_mutex   m_mutex_config;
        int64_t             m_accesstoken_lifetime_s  = 3600;   // range 1-3600-*
        int64_t             m_refreshtoken_lifetime_s = 86400;  // range 1-86400-*
        int64_t             m_code_lifetime_s         = 10;     // range 1-10-3600
        int64_t             m_session_lifetime_s      = 120;    // range 1-120-3600
        int64_t             m_usage_maximum           = 100;    // range 1-100-10000
        int64_t             m_cleaning_probability    = 10;     // range 1-10-10000
        //
        std::string         m_html_login_file;          // HTML login file name
        std::string         m_html_login_code;          // HTML login source
        //
        // CAPTCHA configuration
        enum class captcha_mode { e_none, e_google };
        //
        struct {
            captcha_mode    mode;
            //
            // Data needed by Google
            std::string     google_secret;
            std::string     google_hostname;  // expected hostname
            std::string     google_action;    // v3 only, expected action
            double          google_score;     // v3 only, minimal score
            //
        }                           m_captcha;
        //
        // Cleaning random generatot
        std::default_random_engine  m_generator;
        //
        // Sessions
        std::shared_mutex           m_mutex_sessions;
        std::map< std::string, OAuthSession >
                                    m_sessions;         // indexed by session_id
        std::map< std::string, int64_t >
                                    m_session_usage;    // indexed by client_id
        //
        // Statistics (also protected by m_mutex_sessions)
        adawat::StatsValue          m_stats_session_usage,      // to size m_session_lifetime_s
                                    m_stats_session_lifetime;   // to size m_usage_maximum
        //
        // Parse the configuration
        bool     update_config     ( void );
        //
        // When create_session is called, check if cleaning must be done and do it
        void     maybe_clean       ( void );
        //
        // Called when HTML file changes
        void     monitoring_event  ( const std::string &   p_file );
};

} // namespace urania

#endif
