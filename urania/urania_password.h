// Copyright 2018 Pierre del Perugia
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

//
// Manage existing AccessTokens
//

#ifndef urania_password_h
#define urania_password_h

#include <shared_mutex>

#include "masnae_bus.h"

namespace urania {
    
//-------------------------------------------------------------------------
class PasswordManager : public masnae::bus::Subscriber
{
    public:
        enum class result_type {
            success, no_password, forbidden, banned, weak_password, failure
        };
        //
        // Returns result_type literal
        const char *      to_text          ( result_type p_result ) const;
        //
        // To be called once before any execution
        bool              start            ( void );
        //
        // To be called once before stopping the program
        void              stop             ( void );
        //
        // Return module information as a JSON string
        std::string       specific_info    ( void );
        //
        // Check a password. Update locking time / login date
        result_type       password_check   ( const std::string &           p_user,
                                             const std::string &           p_password,
                                             bool                          p_update_lastlogin,
                                             std::string &                 p_details,
                                             masnae::ProcessingResponse &  p_result );
        //
        // Change user's password. Update locking time
        result_type       password_change  ( const std::string &           p_user,
                                             const std::string &           p_password_old,
                                             const std::string &           p_password_new,
                                             std::string &                 p_details,
                                             masnae::ProcessingResponse &  p_result );
        //
        // Start user's password recovery: generate OTP
        result_type       password_lost_1  ( const std::string &           p_user,
                                             std::string &                 p_otp,
                                             std::string &                 p_details,
                                             masnae::ProcessingResponse &  p_result );
        //
        // Continue user's password recovery: set password
        result_type       password_lost_2  ( const std::string &           p_user,
                                             const std::string &           p_otp,
                                             const std::string &           p_password_new,
                                             std::string &                 p_details,
                                             masnae::ProcessingResponse &  p_result );
        //
        // Set user's password Update locking time
        result_type       password_set     ( const std::string &           p_user,
                                             const std::string &           p_password_new,
                                             masnae::ProcessingResponse &  p_result );
        //
        // Check if the user's password is expired or uses an old hash
        result_type       password_change_needed
                                           ( const std::string &           p_user,
                                             bool &                        p_need_change,
                                             masnae::ProcessingResponse &  p_result );
        //
        // Return a password score (range 0-5)
        unsigned          password_score   ( const std::string &           p_password ) const;
        //
        // Accessors
        unsigned          password_score_minimal ( void );
        //
        // Called by the bus with a new message
        virtual
        void              bus_event        ( const masnae::bus::Message &  p_message );
        //
    private:
        // Configuration
        std::shared_mutex   m_mutex_config;
        int64_t             m_otp_length     = 6;    // range 4-9
        int64_t             m_otp_validity_s = 120;  // in seconds
        int64_t             m_score_minimal  = 3;    // range 0-5
        //
        // Parse the configuration
        bool              update_config    ( void );
        //
        // Calculate the 256 bits (43 characters) digest of a password.
        // Returns an empty string on error
        std::string       password_digest  ( const std::string &  p_password,
                                             const std::string &  p_salt,
                                             const std::string &  p_param );
};

} // namespace urania

#endif
