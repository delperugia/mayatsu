// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

// Copyright 2018 Pierre del Perugia
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <adawat/adawat_crypto.h>
#include <adawat/adawat_encoding.h>
#include <adawat/adawat_string.h>

#include "masnae_global.h"
#include "masnae_misc.h"
#include "urania_common.h"

namespace urania {

//-------------------------------------------------------------------------
// Called by the main to initialize the class
bool AccessTokenManager::start ( void )
{
    masnae::globals::publisher->subscribe( this );
    //
    update_config();
    //
    return true;
}

//-------------------------------------------------------------------------
// Call before program termination
void AccessTokenManager::stop ( void )
{
    if ( masnae::globals::publisher != nullptr )
        masnae::globals::publisher->unsubscribe( this );
}

//-------------------------------------------------------------------------
// Return statistics on the module
std::string AccessTokenManager::specific_info ( void )
{
    // Retrieve data
    masnae::SingleKey_Value  data;
    //
    {
        std::lock_guard< std::mutex > lock( m_stats_mutex );
        //
        masnae::add_stats( "cleaning_count",    m_stats_cleaning_count,    data );
        masnae::add_stats( "cleaning_duration", m_stats_cleaning_duration, data );
    }
    //
    return "{\"stats\": " + masnae::json_convert( data ) +
           "}";
}

//-------------------------------------------------------------------------
// Create an AccessToken on a user
void AccessTokenManager::create_accesstoken ( const std::string &           p_user,
                                              const std::string &           p_scopes, 
                                              int64_t                       p_lifetime,
                                              std::string &                 p_accesstoken,
                                              masnae::ProcessingResponse &  p_result )
{                      
    // Eventually delete old access and refresh token
    g_AccessTokenManager.maybe_clean();
    //
    // Generate a new RefreshToken
    std::string  expiration;
    std::time_t  now;
    //
    time( & now );
    p_accesstoken = adawat::uid_no_ts          ( constants::k_token_size );
    expiration    = masnae::mysql::datetime_utc( now + p_lifetime        );
    //
    if ( p_accesstoken.empty() || expiration.empty() ) {
        p_result.add_error( "create_accesstoken empty accesstoken or expiration" );
        p_accesstoken.erase();
        return;
    }
    //
    // Prepare database connection
    unsigned                   db_result;
    std::string                inserted_id;
    masnae::mysql::Connection  db = masnae::globals::mysql_handler->get_connection( masnae::mysql::writer );
    //
    // Store a new AccessToken
    db_result = db.do_insert(
        "INSERT INTO t_accesstokens "
        "( k_at, k_user, s_scopes, d_expiration ) VALUES ("
        "'" + masnae::mysql::sql_escape( sha2( p_accesstoken ) ) + "',"
        "'" + masnae::mysql::sql_escape( p_user                ) + "',"
        "'" + masnae::mysql::sql_escape( p_scopes              ) + "',"
        "'" + masnae::mysql::sql_escape( expiration            ) + "')",
        inserted_id
    );
    //
    if ( db_result != 0 ) {
        p_result.add_error( "create_accesstoken insert error " + std::to_string( db_result ) );
        p_accesstoken.erase();
        return;
    }
    //
    p_result.add_log( masnae::ProcessingResponse::verbose,
                      "accesstoken created"
                      ", scopes="   + p_scopes +
                      ", lifetime=" + std::to_string( p_lifetime )
    );
}

//-------------------------------------------------------------------------
// Create a RefreshToken for a client on a user
// p_refreshtoken is empty on error
void AccessTokenManager::create_refreshtoken ( const std::string &           p_user,
                                               const std::string &           p_client,
                                               const std::string &           p_scopes, 
                                               int64_t                       p_lifetime,
                                               std::string &                 p_refreshtoken,
                                               masnae::ProcessingResponse &  p_result )
{
    // Generate a new RefreshToken
    std::string  expiration;
    std::time_t  now;
    //
    time( & now );
    p_refreshtoken = adawat::uid_no_ts          ( constants::k_token_size );
    expiration     = masnae::mysql::datetime_utc( now + p_lifetime        );
    //
    if ( p_refreshtoken.empty() || expiration.empty() ) {
        p_result.add_error( "create_refreshtoken empty refreshtoken or expiration" );
        p_refreshtoken.erase();
        return;
    }
    //
    // Prepare database connection
    unsigned                   db_result;
    std::string                inserted_id;
    masnae::mysql::Connection  db = masnae::globals::mysql_handler->get_connection( masnae::mysql::writer );
    //
    // Store a new AccessToken
    db_result = db.do_insert(
        "INSERT INTO t_accesstokens "
        "( k_at, k_user, s_scopes, d_expiration, b_refresh, k_user_client ) VALUES ("
        "'" + masnae::mysql::sql_escape( sha2( p_refreshtoken ) ) + "',"
        "'" + masnae::mysql::sql_escape( p_user                 ) + "',"
        "'" + masnae::mysql::sql_escape( p_scopes               ) + "',"
        "'" + masnae::mysql::sql_escape( expiration             ) + "',"
        "1,"
        "'" + masnae::mysql::sql_escape( p_client               ) + "')",
        inserted_id
    );
    //
    if ( db_result != 0 ) {
        p_result.add_error( "create_refreshtoken insert error " + std::to_string( db_result ) );
        p_refreshtoken.erase();
        return;
    }
    //
    p_result.add_log( masnae::ProcessingResponse::verbose,
                      "refreshtoken created"
                      ", scopes="   + p_scopes +
                      ", lifetime=" + std::to_string( p_lifetime )
    );
}

//-------------------------------------------------------------------------
// Get details on an existing access or refresh token
bool AccessTokenManager::get_token ( const std::string &           p_accesstoken,
                                     AccessTokenDetails &          p_details,
                                     masnae::ProcessingResponse &  p_result )
{
    // Prepare database connection
    unsigned                     db_result;
    masnae::SingleKeyValue_List  tokens;
    masnae::mysql::Connection    db = masnae::globals::mysql_handler->get_connection( masnae::mysql::reader );
    //
    // Ensure that the token will be valid for the next few seconds
    db_result = db.do_select(
        "SELECT k_user,s_scopes,b_refresh,k_user_client,k_peer_at "
        "FROM t_accesstokens "
        "WHERE k_at = '" + masnae::mysql::sql_escape( sha2( p_accesstoken ) ) + "'"
        " AND d_expiration > date_add( utc_timestamp(), interval 5 second )",
        tokens
    );
    //
    if ( db_result != 0 ) {
        p_result.add_error( "get_token select error " + std::to_string( db_result ) );
        return false;
    }
    //
    if ( tokens.empty() )  // not found
        return false;
    //
    p_details = {
        .user           = tokens[ 0 ][ "k_user"        ],
        .scopes         = tokens[ 0 ][ "s_scopes"      ],
        .refresh        = tokens[ 0 ][ "b_refresh"     ] == "1",
        .client         = tokens[ 0 ][ "k_user_client" ],
        .peer_token_sha = tokens[ 0 ][ "k_peer_at"     ]
    };
    //
    return true;
}

//-------------------------------------------------------------------------
// Using given AccessToken, sets the p_security object with
// details/permissions associated
bool AccessTokenManager::get_security ( const std::string &           p_accesstoken,
                                        masnae::SecurityContext &     p_security,
                                        masnae::ProcessingResponse &  p_result )
{
    // Retrieve user's details
    masnae::SingleKeyValue_List  users;
    masnae::mysql::Connection    db = masnae::globals::mysql_handler->get_connection( masnae::mysql::reader );
    //
    unsigned db_result = db.do_select(
        "SELECT u.k_user,k_profile,k_role,s_scopes "
        "FROM t_accesstokens a "
        "JOIN t_users u ON u.k_user=a.k_user "
        "WHERE a.k_at = '" + masnae::mysql::sql_escape( sha2( p_accesstoken ) ) + "'"
        " AND a.d_expiration > utc_timestamp()"
        " AND b_refresh = 0",
        users
    );
    //
    if ( db_result != 0 ) {
        p_result.add_error( "get_security select error " + std::to_string( db_result ) );
        p_result.set_jresp_failed();
        return false;
    }
    //
    if ( users.size() != 1 ) {
        p_result.add_error( "get_security duplicate accesstoken returned" );
        p_result.set_jresp_rejected( "accesstoken_error", "null" );
        return false;
    }
    //
    // Get user's permissions: start by retrieving the permissions of the current role
    // then move to the parent and add its inherited permissions and so on.
    masnae::SingleKey_Value  inherited_permissions;
    std::string              inherited_role = users[0]["k_role"];
    //
    do {
        masnae::SingleKeyValue_List  permissions;
        masnae::SingleKey_Value      role;
        //
        // Role is mandatory and must be present
        if ( ! masnae::globals::ws_caches_handler->find( "t_roles", inherited_role, role ) ) {
            p_result.add_error( "get_security roles inheritance not found" );
            p_result.set_jresp_failed();
            return false;
        }
        //
        // Permissions are optional: a role may have no permission
        masnae::globals::ws_caches_handler->find( "t_roles_permissions", inherited_role, permissions);
        //
        // Add unexisting permissions (existing ones are not changed, ignored by map's insert)
        for ( auto & permission: permissions ) {
            // permission is a row of the cache, like:
            //   { k_permission=urania.password_change  k_role=user_base  s_parameters= }
            inherited_permissions.insert( std::pair( permission["k_permission"],
                                                     permission["s_parameters"] ) );
        }
        //
        // Move to read parent's permissions
        inherited_role = role[ "k_role_parent" ];
    } while ( ! inherited_role.empty() );
    //
    // Create a security context object
    p_security = {
        .user        = users[ 0 ][ "k_user"    ],
        .profile     = users[ 0 ][ "k_profile" ],
        .role        = users[ 0 ][ "k_role"    ],
        .scopes      = users[ 0 ][ "s_scopes"  ],
        .permissions = inherited_permissions
    };
    //
    return true;
}

//-------------------------------------------------------------------------
// Delete an access or refresh token
bool AccessTokenManager::delete_token ( const std::string &           p_accesstoken,
                                        masnae::ProcessingResponse &  p_result )
{
    return delete_token_sha( sha2( p_accesstoken ), p_result );
}

//-------------------------------------------------------------------------
// Delete an access or refresh token
bool AccessTokenManager::delete_token_sha ( const std::string &           p_accesstoken_sha,
                                            masnae::ProcessingResponse &  p_result )
{
    // Prepare database connection
    size_t                     affected;
    unsigned                   db_result;
    masnae::mysql::Connection  db = masnae::globals::mysql_handler->get_connection( masnae::mysql::writer );
    //
    // Delete it from database
    db_result = db.do_delete(
        "DELETE FROM t_accesstokens "
        "WHERE k_at='" + masnae::mysql::sql_escape( p_accesstoken_sha ) + "'",
        affected
    );
    //
    if ( db_result != 0 ) {
        p_result.add_error( "delete_token delete error " + std::to_string( db_result ) );
        return false;
    }
    //
    return affected == 1;
}

//-------------------------------------------------------------------------
// Delete a user's access tokens, eventually keeping one of them
bool AccessTokenManager::delete_user_tokens ( const std::string &           p_user,
                                              masnae::ProcessingResponse &  p_result,
                                              const std::string &           p_except_accesstoken /* = "" */ )
{
    // Prepare database connection
    size_t                     affected;
    unsigned                   db_result;
    masnae::mysql::Connection  db = masnae::globals::mysql_handler->get_connection( masnae::mysql::writer );
    //
    // Delete them from database
    db_result = db.do_delete(
        "DELETE FROM t_accesstokens "
        "WHERE k_user = '" + masnae::mysql::sql_escape( p_user )                       + "'"
        "  AND k_at  <> '" + masnae::mysql::sql_escape( sha2( p_except_accesstoken ) ) + "'",
        affected
    );
    //
    if ( db_result != 0 ) {
        p_result.add_error( "delete_user_tokens delete error " + std::to_string( db_result ) );
        return false;
    }
    //
    return affected == 1;
}

//-------------------------------------------------------------------------
// Make an Access and Refresh tokens point to each other
void AccessTokenManager::update_peer ( const std::string &  p_accesstoken,
                                       const std::string &  p_refreshtoken )
{
    // Prepare database connection
    size_t                     affected_at = 0, affected_rt = 0;
    unsigned                   result_at, result_rt;
    masnae::mysql::Connection  db = masnae::globals::mysql_handler->get_connection( masnae::mysql::writer );
    //
    std::string at = masnae::mysql::sql_escape( sha2( p_accesstoken ) );
    std::string rt = masnae::mysql::sql_escape( sha2( p_refreshtoken ) );
    //
    result_at = db.do_update(
        "UPDATE t_accesstokens"
        " SET   k_peer_at = '" + rt + "'"
        " WHERE k_at      = '" + at + "'",
        affected_at
    );
    //
    result_rt = db.do_update(
        "UPDATE t_accesstokens"
        " SET   k_peer_at = '" + at + "'"
        " WHERE k_at      = '" + rt + "'",
        affected_rt
    );
    //
    if ( affected_at != 1 || affected_rt != 1 )
        masnae::service_error_log( "update_peer update failed "
                                    " at=" + std::to_string( result_at ) +
                                    " rt=" + std::to_string( result_rt ) );
}

//-------------------------------------------------------------------------
void AccessTokenManager::bus_event ( const masnae::bus::Message &  p_message )
{
    switch ( p_message.type )
    {
        case masnae::bus::Message::config_updated:
            update_config();
            break;
        default:
            break;
    }
}

//-------------------------------------------------------------------------
// Reads configuration from global config object.
bool AccessTokenManager::update_config ( void )
{
    bool              ok = true;
    int64_t           new_cleaning_probability;
    //
    //-----
    new_cleaning_probability = 
        masnae::globals::config_handler->get_int64( "/accesstoken/cleaning_probability", 100 );
    //
    ok &= new_cleaning_probability >= 1 && new_cleaning_probability <= 10000;
    //
    //-----
    if ( ok ) {
        std::lock_guard<std::shared_mutex>  lock( m_mutex_config );
        //
        m_cleaning_probability = new_cleaning_probability;
    } else {
        masnae::service_error_log( "AccessTokenManager: invalid config" );
    }
    //
    return ok;
}

//-------------------------------------------------------------------------
// Check if cleaning must be done and do it
void AccessTokenManager::maybe_clean ( void )
{
    int64_t cleaning_probability;
    //
    // One chance out of m_cleaning_probability
    {
        std::shared_lock< std::shared_mutex >  lock( m_mutex_config );
        //
        std::uniform_int_distribution< int64_t > distribution( 0, m_cleaning_probability );
        //
        if ( distribution( m_generator ) != 0 )
            return;
        //
        cleaning_probability = m_cleaning_probability;
    }
    //
    // Delete a limited number of old access and refresh token
    masnae::mysql::Connection  db = masnae::globals::mysql_handler->get_connection( masnae::mysql::writer );
    size_t                     affected;
    //
    auto  start  = std::chrono::system_clock::now();
    //
    db.do_delete( "DELETE FROM t_accesstokens "
                  "WHERE d_expiration < utc_timestamp() "
                  "LIMIT " + std::to_string( 2 * cleaning_probability ),
                  affected );
    //
    auto duration = std::chrono::duration_cast<std::chrono::milliseconds>( std::chrono::system_clock::now() - start );
    // Update stats
    {
        std::lock_guard< std::mutex > lock( m_stats_mutex );
        //
        m_stats_cleaning_count   .add_value( affected        );
        m_stats_cleaning_duration.add_value( duration.count() );
    }
}

//-------------------------------------------------------------------------
// Hash a token and encode the result using base 64
// Resulting string is 44 characters long
std::string AccessTokenManager::sha2 ( const std::string &  p_token )
{
    adawat::Data  hash, data( p_token.begin(), p_token.end());
    //
    if ( ! adawat::crypto_digest( data, hash, EVP_sha256() ) )
        masnae::service_error_log( "AccessTokenManager sha2 crypto_digest failed"
                                    " sz=" + std::to_string( p_token.length() ) +
                                    " ="   + p_token );
    //
    std::string hashed = adawat::encoding_base64_encode( hash, adawat::base64_type::standard, true );
    //
    if ( hashed.empty() )
        masnae::service_error_log( "AccessTokenManager sha2 encoding_base64_encode failed"
                                    " sz=" + std::to_string( p_token.length() ) +
                                    " ="   + p_token );
    //
    return hashed;
}

} // namespace urania
