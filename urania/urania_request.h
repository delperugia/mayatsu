// Copyright 2018 Pierre del Perugia
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

//
// Class called by Masnae to process a request
//

#ifndef urania_request_h
#define urania_request_h

#include "masnae_request_handler.h"

namespace urania {

//-------------------------------------------------------------------------
class UraniaRequestHandler : public masnae::RequestHandler
{
    public:
        UraniaRequestHandler  ();
        //
        // Implementation of the base class pure functions
        //
        // Returns true if request is allowed
        virtual
        bool        check_security  ( const std::string &               p_module,
                                      masnae::Fastcpipp_Path &          p_document,
                                      masnae::ProcessingResponse &      p_result );
        //
        // Returns true if the module/document are found (p_result must be filled then)
        virtual                      
        bool        process_request ( const std::string &               p_module,
                                      masnae::Fastcpipp_Path &          p_document,
                                      masnae::ProcessingResponse &      p_result );
        //
        // Returns information
        // Must return a valid JSON string representing a non-empty object
        virtual
        std::string specific_info   ( void );
        //
    private:
        //
        // OAuth request processing
        bool    process_oauth_request        ( const masnae::Fastcpipp_Path &      p_document,
                                               masnae::ProcessingResponse &        p_result );
        // Authorization Code Grant
        void    process_oauth_authorize      ( const masnae::SingleKey_ValueList & p_parameters,
                                               masnae::ProcessingResponse &        p_result );
        void    process_oauth_login          ( const masnae::SingleKey_ValueList & p_parameters,
                                               masnae::ProcessingResponse &        p_result );
        void    process_oauth_token          ( const masnae::SingleKey_ValueList & p_parameters,
                                               masnae::ProcessingResponse &        p_result );
        //
        //
        // Web request processing
        bool    process_ws_request           ( const masnae::Fastcpipp_Path &      p_document,
                                               masnae::ProcessingResponse &        p_result );
        void    process_ws_signout           ( const masnae::SingleKey_ValueList & p_parameters,
                                               masnae::ProcessingResponse &        p_result );
        void    process_ws_permissions       ( const masnae::SingleKey_ValueList & p_parameters,
                                               masnae::ProcessingResponse &        p_result );
        //
        // Password related requests (body in urania_request_password.cpp)
        void    process_ws_password_change   ( const masnae::SingleKey_ValueList & p_parameters,
                                               masnae::ProcessingResponse &        p_result );
        void    process_ws_password_lost_1   ( const masnae::SingleKey_ValueList & p_parameters,
                                               masnae::ProcessingResponse &        p_result );
        void    process_ws_password_lost_2   ( const masnae::SingleKey_ValueList & p_parameters,
                                               masnae::ProcessingResponse &        p_result );
        void    process_ws_password_score    ( const masnae::SingleKey_ValueList & p_parameters,
                                               masnae::ProcessingResponse &        p_result );
        void    process_ws_password_set      ( const masnae::SingleKey_ValueList & p_parameters,
                                               masnae::ProcessingResponse &        p_result );
        void    process_ws_password_need_change
                                             ( const masnae::SingleKey_ValueList & p_parameters,
                                               masnae::ProcessingResponse &        p_result );
        //
        // Family related requests (body in urania_request_family.cpp)
        void    process_ws_children          ( const masnae::SingleKey_ValueList & p_parameters,
                                               masnae::ProcessingResponse &        p_result );
        void    process_ws_lineage           ( const masnae::SingleKey_ValueList & p_parameters,
                                               masnae::ProcessingResponse &        p_result );
        void    process_ws_siblings          ( const masnae::SingleKey_ValueList & p_parameters,
                                               masnae::ProcessingResponse &        p_result );
        //
        // Find and return a cookie from the request
        bool    get_cookie                  ( const std::string &  p_key,
                                              std::string &        p_value );
};

} // namespace urania

#endif
