// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

// Copyright 2018 Pierre del Perugia
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <shared_mutex>

#include <adawat/adawat_encoding.h>
#include <adawat/adawat_string.h>
#include <adawat/adawat_url.h>

#include "masnae_misc.h"
#include "urania_common.h"

namespace urania {

//-------------------------------------------------------------------------
// Add parameters (and URL encode them) to an existing URL
// with or without existing parameters
std::string add_parameters_to_url ( std::string                      p_url,
                                    bool                             p_as_fragment,
                                    const masnae::SingleKey_Value &  p_parameters )
{
    char separator;
    //
    // The URL must not have a fragment part. If there is one, remove it
    size_t fragment = p_url.find( '#' );
    //
    if ( fragment != std::string::npos )
        p_url.erase( fragment );
    //
    // With implicit Grant, use a fragment; with Authorization Code, use parameters
    if ( p_as_fragment ) {
        separator = '#';
    } else {
        // If there is already some parameters, start with separator '&'
        if ( p_url.find( '?' ) == std::string::npos )
            separator = '?';
        else
            separator = '&';
    }
    //
    return p_url + separator + adawat::url_encode_parameters( p_parameters );
}

//-------------------------------------------------------------------------
// Extract login and password from a basic HTTP authentication
//   Authorization: Basic czZCaGRSa3F0MzpnWDFmQmF0M2JW
bool parse_basic_authentification( const std::string &  p_authorization,
                                   std::string &        p_client_id,
                                   std::string &        p_client_secret )
{
    std::string credential = p_authorization;
    std::string basic      = adawat::str_token( credential );  // basic="Basic", at="czZCaGRSa3F0MzpnWDFmQmF0M2JW"
    //
    if ( adawat::str_upper( basic ) != "BASIC" )  // case insensitive, see RFC1945#11
        return false;
    //
    adawat::Data  data;
    //
    if ( ! adawat::encoding_base64_decode( credential, data ) )
        return false;
    //
    adawat::get_data( p_client_secret, data );  // must be client_id:client_secret
    //
    p_client_id = adawat::str_token( p_client_secret, ":" );
    //
    return ! p_client_id.empty() && ! p_client_secret.empty();
}

} // namespace urania
