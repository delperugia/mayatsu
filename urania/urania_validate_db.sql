use urania;

insert into t_roles (k_role, k_role_parent) values
	('user_base', null),
	('web', null),
	('user_advanced', 'user_base');

insert into t_roles_permissions (k_role, k_permission, s_parameters) values
	('user_advanced', 'dummy', 'trois'),
	('user_base', 'dummy', 'deux'),
    ('user_base', 'thoe.ws', ''),
	('user_base', 'urania.password_change', null),
	('user_base', 'urania.signout', null),
	('web', 'dummy', 'un'),
	('web', 'urania.password_set', 'web,user'),
    ('web', 'urania.password_lost_1', 'user'),
    ('web', 'urania.password_lost_2', 'user');

insert into t_profiles (k_profile) values
	('user'),
	('web');

insert into t_profiles_roles (k_profile, k_role, s_condition) values
	('user', 'user_advanced', null),
	('user', 'user_base', null),
	('web', 'web', null);

insert into t_users (k_user, k_profile, k_role, d_creation, k_user_parent) values
	(1, 'web',  'web',       '2018-10-18 11:08:49', NULL),
	(2, 'user', 'user_base', '2018-10-18 11:09:18', NULL),
	(3, 'user', 'user_advanced', '2018-10-18 13:04:31', NULL),
	(4, 'user', 'user_base', '2018-11-12 13:45:45', 3),
	(5, 'user', 'user_base', '2018-11-12 13:46:13', 3),
	(6, 'user', 'user_base', '2018-11-12 13:46:26', 3),
	(7, 'user', 'user_base', '2018-11-12 13:53:07', 4),
	(8, 'user', 'user_base', '2018-11-12 13:55:11', 4),
	(9, 'user', 'user_base', '2018-11-13 03:30:00', 8);

insert into t_passwords (k_user, s_digest, s_salt, s_hash_param, d_last_update, d_lock_expiration, d_reset_expiration) values
	(1,'Oj6YyOyCnFzyt7L1fujvv4UTidSWatwXuN1cXk/G/ug=','XEFughs3LyBSFeXAbVbZg8','m=8192,t=4,p=2',UTC_TIMESTAMP(),UTC_TIMESTAMP(),UTC_TIMESTAMP()); -- SeKre_789

insert into t_accesstokens (k_at, k_user, d_expiration) values
	(TO_BASE64(UNHEX(SHA2('at_web',256))), 1, '2030-12-31 00:00:00');

insert into t_oauth (k_user, s_name, s_logo, s_allowed_modes, s_redirect_uri) values
	(1, 'Example',    'http://urania.example.com/oauth_logo.png', 'code,token', 'http://urania.example.com/oauth_cb.php'),
    (2, 'Validation', '',                                         'password',   ''                                      );

create user 'urania'@'localhost' identified by 'u7RYYameHx8VTawW';
create user 'urania'@'%'         identified by 'u7RYYameHx8VTawW';
grant all on urania.* to 'urania'@'localhost';
grant all on urania.* to 'urania'@'%';
