create schema if not exists urania;
use urania;

create table t_profiles (
  k_profile             varchar(50) not null,
  i_password_lifetime   int(10) unsigned not null default '0' comment 'Period of time in days before a password should be changed',
  primary key (k_profile)
) engine=innodb default charset=utf8mb4 comment='list available profiles. a profile is a set of roles, only one of them active at a time depending on external conditions.';

create table t_roles (
  k_role                varchar(50) not null,
  k_role_parent         varchar(50) default null,
  primary key (k_role),
  key fk_t_roles_t_roles (k_role_parent),
  constraint fk_t_roles_t_roles foreign key (k_role_parent) references t_roles (k_role) on update cascade
) engine=innodb default charset=utf8mb4 comment='list roles. roles can inherit permissions from another role.';

create table t_profiles_roles (
  k_profile             varchar(50) not null,
  k_role                varchar(50) not null,
  s_condition           varchar(50) default null,
  primary key (k_profile,k_role),
  key fk_t_profiles_roles_t_roles (k_role),
  constraint fk_t_profiles_roles_t_profiles foreign key (k_profile) references t_profiles (k_profile) on update cascade,
  constraint fk_t_profiles_roles_t_roles foreign key (k_role) references t_roles (k_role) on update cascade
) engine=innodb default charset=utf8mb4 comment='list possible roles in a given profile. the active role within a profile is selected using the condition.';

create table t_roles_permissions (
  k_role                varchar(50) not null,
  k_permission          varchar(50) not null,
  s_parameters          varchar(50) default null,
  primary key (k_role,k_permission),
  constraint fk_t_roles_pernissions_t_roles foreign key (k_role) references t_roles (k_role) on delete cascade on update cascade
) engine=innodb default charset=utf8mb4 comment='list permissions associated to a role, each with given parameters. permissions are defined by the module itself, not in the database.';

create table `t_users` (
  `k_user`              varchar(50) not null,
  `k_profile`           varchar(50) default null,
  `k_role`              varchar(50) default null,
  `d_creation`          datetime default null,
  `k_user_parent`       varchar(50) default null,
  primary key (`k_user`),
  key `fk_t_users_t_profiles_roles` (`k_profile`,`k_role`),
  key `fk_t_users_t_users` (`k_user_parent`),
  constraint `fk_t_users_t_profiles_roles` foreign key (`k_profile`, `k_role`) references `t_profiles_roles` (`k_profile`, `k_role`) on update cascade,
  constraint `fk_t_users_t_users` foreign key (`k_user_parent`) references `t_users` (`k_user`) on update cascade
) engine=innodb default charset=utf8mb4 comment='list users. each user has a profile and an active role within this profile.';

create table `t_accesstokens` (
  `k_at`                char(44) NOT NULL,
  `k_user`              varchar(50) default null,
  `d_expiration`        datetime default null,
  `s_scopes`            varchar(50) default null,
  `b_refresh`           tinyint(4) default '0',
  `k_user_client`       int(11) default null,
  `k_peer_at`           char(44) default null comment 'AT for RT, RT for AT',
  primary key (`k_at`),
  key `fk_t_accesstokens_t_users` (`k_user`),
  constraint `fk_t_accesstokens_t_users` foreign key (`k_user`) references `t_users` (`k_user`) on delete cascade on update cascade
) engine=innodb default charset=utf8mb4 comment='list of active at';

create table t_groups (
  k_user                varchar(50) not null,
  k_group               varchar(50) not null,
  primary key (k_user,k_group),
  constraint fk_t_groups_t_users foreign key (k_user) references t_users (k_user) on delete cascade on update cascade
) engine=innodb default charset=utf8mb4 comment='groups of users. each user can be in several groups. group names are defined by the module itself, not in the database.';

create table `t_passwords` (
  `k_user` varchar(50) NOT NULL,
  `s_digest` varchar(50) DEFAULT NULL,
  `s_salt` varchar(50) DEFAULT NULL,
  `d_last_login` datetime DEFAULT NULL,
  `i_lock_period` int(10) unsigned NOT NULL DEFAULT '1',
  `d_lock_expiration` datetime DEFAULT NULL,
  `s_reset_otp` varchar(50) DEFAULT NULL,
  `d_reset_validity` datetime DEFAULT NULL,
  `i_reset_period` int(10) unsigned NOT NULL DEFAULT '1',
  `d_reset_expiration` datetime DEFAULT NULL,
  `s_hash_param` varchar(50) NOT NULL,
  `d_last_update` datetime NOT NULL,
  primary key (`k_user`),
  constraint `fk_t_passwords_t_users` foreign key (`k_user`) references `t_users` (`k_user`) on delete cascade on update cascade
) engine=innodb default charset=utf8mb4 comment='user''s password';

create table `t_oauth` (
  `k_user`              varchar(50) not null,
  `s_name`              varchar(50) not null,
  `s_logo`              varchar(100) not null,
  `s_allowed_modes`     varchar(50) not null,
  `s_redirect_uri`      varchar(500) not null,
  `b_pkce`              tinyint(4) not null default '0',
  primary key (`k_user`),
  constraint `fk_t_oauth_t_users` foreign key (`k_user`) references `t_users` (`k_user`) on delete cascade on update cascade
) engine=innodb default charset=utf8mb4 comment='users acting like oauth client and their configuration';
