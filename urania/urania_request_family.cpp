// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

// Copyright 2018 Pierre del Perugia
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "masnae_mysql.h"
#include "masnae_global.h"
#include "masnae_misc.h"
#include "urania_request.h"

namespace urania {
                      
//-------------------------------------------------------------------------
// Returns all children of a user as an array of user id:
//    ["4","5","6"]
void UraniaRequestHandler::process_ws_children ( const masnae::SingleKey_ValueList & p_parameters,
                                                 masnae::ProcessingResponse &        p_result )
{
    // Retrieve user's details
    masnae::SingleKeyValue_List  users;
    auto                         db = masnae::globals::mysql_handler->get_connection( masnae::mysql::reader );
    //
    unsigned db_result =
        db.do_select( "select k_user "
                      "from t_users "
                      "where k_user_parent = '" + masnae::mysql::sql_escape( m_security.user ) + "'",
                       users );
    //
    if ( db_result != 0 ) {
        p_result.set_jresp_failed();
        return;
    }
    //
    // Build response
    std::string result;
    //
    if ( users.empty() ) {
        result = "[]";
    } else {
        for ( auto & user : users )
            result += ',' + masnae::json_quote_string( user[ "k_user" ] );
        //
        result[0]  = '[';  // replace ',' by '['
        result    += ']';
    }
    //
    p_result.add_log( masnae::ProcessingResponse::regular,
                      "children of '" + m_security.user + "': " + result );
    //
    p_result.set_jresp_success( result );
}

//-------------------------------------------------------------------------
// Returns all ancestors of a user (including himself)
//   ["7","4","3"]
void UraniaRequestHandler::process_ws_lineage ( const masnae::SingleKey_ValueList & p_parameters,
                                                masnae::ProcessingResponse &        p_result )
{
    // Retrieve user's details
    masnae::SingleKeyValue_List  users;
    auto                         db = masnae::globals::mysql_handler->get_connection( masnae::mysql::reader );
    //
    masnae::KeyList lineage;
    std::string     current_user = m_security.user;  // start from self
    //
    lineage.push_back( current_user );
    //
    // Retrieve parents 2 by 2 starting from current (current is already added)
    do {
        unsigned db_result =  // get Father and GrandFather of current_user
            db.do_select( "select u2.k_user as F, u3.k_user as GF "
                          "from t_users u1 "
                          "left outer join t_users u2 on u2.k_user=u1.k_user_parent "
                          "left outer join t_users u3 on u3.k_user=u2.k_user_parent "
                          "where u1.k_user = '" + masnae::mysql::sql_escape( current_user ) + "'",
                          users );
        //
        if ( db_result != 0 || users.size() != 1 ) {
            p_result.set_jresp_failed();
            return;
        }
        //
        const std::string & F  = users[ 0 ][ "F"  ];
        const std::string & GF = users[ 0 ][ "GF" ];
        //
        if ( ! F .empty() )  lineage.push_back( F  );  // push father
        if ( ! GF.empty() )  lineage.push_back( GF );  // push grand-father
        //
        current_user = GF;                             // restart from grand-father
        //
    } while ( ! current_user.empty() );
    //
    std::string lineage_json = masnae::json_convert( lineage );
    //
    p_result.add_log( masnae::ProcessingResponse::regular,
                      "lineage of '" + m_security.user + "': " + lineage_json );
    //
    p_result.set_jresp_success( lineage_json );
}

//-------------------------------------------------------------------------
// Returns all siblings of a user (including himself)
//  ["7","8"]
void UraniaRequestHandler::process_ws_siblings ( const masnae::SingleKey_ValueList & p_parameters,
                                                 masnae::ProcessingResponse &        p_result )
{
    // Retrieve user's details
    masnae::SingleKeyValue_List  users;
    auto                         db = masnae::globals::mysql_handler->get_connection( masnae::mysql::reader );
    //
    unsigned db_result =
        db.do_select( "select k_user "
                      "from t_users "
                      "where k_user_parent in "
                      "  ( select k_user_parent "
                      "    from t_users "
                      "    where k_user = '" + masnae::mysql::sql_escape( m_security.user ) + "')",
                      users );
    //
    if ( db_result != 0 || users.empty() ) {
        p_result.set_jresp_failed();
        return;
    }
    //
    // Build response (users cannot be empty)
    std::string result;
    //
    for ( auto & user : users )
        result += ',' + masnae::json_quote_string( user[ "k_user" ] );
    //
    result[0]  = '[';  // replace ',' by '['
    result    += ']';
    //
    p_result.add_log( masnae::ProcessingResponse::regular,
                      "siblings of '" + m_security.user + "': " + result );
    //
    p_result.set_jresp_success( result );
}

} // namespace urania
