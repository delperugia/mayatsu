// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

// Copyright 2018 Pierre del Perugia
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <argon2.h>
#include <zxcvbn.h>
#include <adawat/adawat_common.h>
#include <adawat/adawat_crypto.h>
#include <adawat/adawat_encoding.h>
#include <adawat/adawat_string.h>
#include <adawat/adawat_uid.h>

#include "masnae_global.h"
#include "masnae_misc.h"

#include "urania_common.h"
#include "urania_password.h"

#ifdef USE_DICT_FILE
#error "ZXCVBN library must include its dictionary data in the source code."
#endif

namespace urania {

//-------------------------------------------------------------------------
// Returns result_type literal
const char * PasswordManager::to_text ( result_type p_result ) const
{
    switch ( p_result )
    {
        case result_type::success:       return "success";
        case result_type::no_password:   return "no_password";
        case result_type::forbidden:     return "forbidden";
        case result_type::banned:        return "banned";
        case result_type::weak_password: return "weak_password";
        case result_type::failure:       return "failure";
        default:                         return "unknown";
    }
}

//-------------------------------------------------------------------------
// Called by the main to initialize the class
bool PasswordManager::start ( void )
{
    masnae::globals::publisher->subscribe( this );
    //
    update_config();
    //
    return true;
}

//-------------------------------------------------------------------------
// Call before program termination
void PasswordManager::stop ( void )
{
    if ( masnae::globals::publisher != nullptr )
        masnae::globals::publisher->unsubscribe( this );
}

//-------------------------------------------------------------------------
// Return statistics on the executor
std::string PasswordManager::specific_info ( void )
{
    return "{\"argon2\":" + adawat::str_sprintf( "\"%d.%d\"",
                                                 ( ARGON2_VERSION_NUMBER >> 4 ) & 0xF,
                                                 ( ARGON2_VERSION_NUMBER      ) & 0xF ) +
           ",\"zxcvbn\":" + "\"-\""  +
           "}";
}

//-------------------------------------------------------------------------
// Check a password. Update locking time / login date
// p_details contains extra data depending on result:
//  banned: ban time in second
PasswordManager::result_type
    PasswordManager::password_check ( const std::string &           p_user,
                                      const std::string &           p_password,
                                      bool                          p_update_lastlogin,
                                      std::string &                 p_details,
                                      masnae::ProcessingResponse &  p_result )
{
    // Prepare database access
    unsigned  db_result;
    size_t    affected;
    auto      db = masnae::globals::mysql_handler->get_connection( masnae::mysql::writer );
    //
    // Retrieve stored salt and digest
    // and date to end of expiration in second (>0 means 'locked')
    masnae::SingleKeyValue_List  passwords;
    //
    db_result = db.do_select(
        "SELECT s_salt, s_digest, i_lock_period, s_hash_param,"
        " COALESCE( TIMESTAMPDIFF( second, UTC_TIMESTAMP(), d_lock_expiration ), 0 ) AS ban_time "
        "FROM  t_passwords "
        "WHERE k_user = '"  + masnae::mysql::sql_escape( p_user ) + "'",
        passwords );
    //
    if ( db_result != 0 || passwords.size() > 1 ) {  // they can be only one password (max) in table
        p_result.add_error( "password_check select error " + std::to_string( db_result ) );
        return result_type::failure;
    }
    //
    if ( passwords.size() == 0 )  // user may exist, but doesn't have a password set
        return result_type::no_password;
    //
    // If the password is banned, exit now
    auto ban_time = atoll( passwords[0]["ban_time"].c_str() );  // conversion cannot fail
    //
    if ( ban_time >= 0 ) {
        p_details = std::to_string( ban_time );
        return result_type::banned;
    }
    //
    // Calculate this password digest
    std::string digest = password_digest( p_password,
                                          passwords[0]["s_salt"],
                                          passwords[0]["s_hash_param"] );
    //
    if ( digest.empty() ) {
        p_result.add_error( "password_check digest failed" );
        return result_type::failure;
    }
    //
    // Compare the stored and calculated digests
    // Time constant comparison is not needed since we calculate the digest of caller OTP
    if ( digest == passwords[0]["s_digest"] ) {
        //
        // Program checks (usually OAuth client requesting an AccessToken)
        // don't update last login, only human login do it.
        // Also reset lock period if it was not reset
        if ( p_update_lastlogin || passwords[0]["i_lock_period"] != "1" ) {
            std::string last_login = p_update_lastlogin ?
                                   " ,d_last_login  = utc_timestamp() " :
                                   "";
            db.do_update(
                "UPDATE t_passwords SET "
                " i_lock_period = 1 " +
                last_login            +
                "WHERE k_user='" + masnae::mysql::sql_escape( p_user ) + "'",
                affected );
        }
        //
        return result_type::success;
    } else {
        // On error, increase ban time and lock password
        db.do_update(
            "UPDATE t_passwords SET "
            " i_lock_period     = greatest( "           + std::to_string( constants::k_locking_period_min ) +
                                            ", least("  + std::to_string( constants::k_locking_period_max ) +
                                                     ", i_lock_period * 2 )), "
            " d_lock_expiration = utc_timestamp() + interval i_lock_period second "
            "WHERE k_user='" + masnae::mysql::sql_escape( p_user ) + "'",
            affected );
        //
        return result_type::forbidden;
    }
}

//-------------------------------------------------------------------------
// Change user's password. Update locking time
// p_details contains extra data depending on result:
//  banned: ban time in second
PasswordManager::result_type
    PasswordManager::password_change ( const std::string &           p_user,
                                       const std::string &           p_password_old,
                                       const std::string &           p_password_new,
                                       std::string &                 p_details,
                                       masnae::ProcessingResponse &  p_result )
{
    // Check user credential, eventually ban him
    auto result = password_check( p_user, p_password_old, false, p_details, p_result );
    if ( result != result_type::success )
        return result;
    //
    return password_set( p_user, p_password_new, p_result );
}

//-------------------------------------------------------------------------
// Start user's password recovery: generate OTP
PasswordManager::result_type
    PasswordManager::password_lost_1 ( const std::string &           p_user,
                                       std::string &                 p_otp,
                                       std::string &                 p_details,
                                       masnae::ProcessingResponse &  p_result )
{
    // Prepare database access
    unsigned  db_result;
    size_t    affected;
    auto      db = masnae::globals::mysql_handler->get_connection( masnae::mysql::writer );
    //
    // Retrieve date to end of expiration in second (>0 means 'locked')
    masnae::SingleKeyValue_List  passwords;
    //
    db_result = db.do_select(
        "SELECT i_reset_period, s_digest, s_hash_param, "
        " coalesce(timestampdiff( second, utc_timestamp(), d_reset_expiration ),0) AS ban_time "
        "FROM  t_passwords "
        "WHERE k_user = '"  + masnae::mysql::sql_escape( p_user ) + "'",
        passwords );
    //
    if ( db_result != 0 || passwords.size() > 1 ) {  // they can be only one password (max) in table
        p_result.add_error( "password_lost_1 select error " + std::to_string( db_result ) );
        return result_type::failure;
    }
    //
    if ( passwords.size() == 0 )  // user may exist, but doesn't have a password set
        return result_type::no_password;
    //
    // Protect against too much attempts: exit now
    auto ban_time = atoll( passwords[0]["ban_time"].c_str() );  // conversion cannot fail
    //
    if ( ban_time >= 0 ) {
        p_details = std::to_string( ban_time );
        return result_type::banned;
    }
    //
    // Retrieve configuration
    int64_t otp_length, otp_validity_s;
    {
        std::shared_lock< std::shared_mutex > lock( m_mutex_config );
        //
        otp_length     = m_otp_length;
        otp_validity_s = m_otp_validity_s;
    }
    //
    // Create OTP
    p_otp = adawat::uid_no_ts( static_cast< size_t >( otp_length), adawat::uid_alphabet::digits );
    //
    // Store a hash of the OTP in the DB. OTP is hashed using the digest as
    // salt: if for some reason the password is (re)set the OTP becomes invalid
    std::string digest = password_digest( p_otp,
                                          passwords[0]["s_digest"],
                                          passwords[0]["s_hash_param"] );
    //
    if ( digest.empty() ) {
        p_result.add_error( "password_lost_1 digest failed" );
        return result_type::failure;
    }
    //
    db.do_update(
        "UPDATE t_passwords SET "
        " s_reset_otp        = '" + digest + "', " +
        " d_reset_validity   = utc_timestamp() + interval " + std::to_string( otp_validity_s ) + " second, "
        " i_reset_period     = greatest( "          + std::to_string( constants::k_reset_period_min ) +
                                        ", least("  + std::to_string( constants::k_reset_period_max ) +
                                                 ", i_reset_period * 2 )), "
        " d_reset_expiration = utc_timestamp() + interval i_reset_period second "
        "WHERE k_user='" + masnae::mysql::sql_escape( p_user ) + "'",
        affected );
    //
    return result_type::success;
}

//-------------------------------------------------------------------------
// Continue user's password recovery: set password
PasswordManager::result_type
    PasswordManager::password_lost_2 ( const std::string &           p_user,
                                       const std::string &           p_otp,
                                       const std::string &           p_password_new,
                                       std::string &                 p_details,
                                       masnae::ProcessingResponse &  p_result )
{
    // Check password now (it will be redo eventually in password_set)
    // This allows to keep the OTP valid if password is weak
    unsigned score = password_score( p_password_new );
    //
    p_result.add_log( masnae::ProcessingResponse::verbose,
                      "new password score: " + std::to_string( score ) );
    //
    if ( score < password_score_minimal() )
        return result_type::weak_password;
    //
    // Prepare database access
    {
        unsigned  db_result;
        size_t    affected;
        auto      db = masnae::globals::mysql_handler->get_connection( masnae::mysql::writer );
        //
        // Retrieve stored salt and digest
        // and date to end of expiration in second (>0 means 'locked')
        masnae::SingleKeyValue_List  passwords;
        //
        db_result = db.do_select(
            "SELECT s_reset_otp, s_digest, s_hash_param, "
            " utc_timestamp() < d_reset_validity AS valid_otp "
            "FROM  t_passwords "
            "WHERE k_user = '"  + masnae::mysql::sql_escape( p_user ) + "'",
            passwords );
        //
        if ( db_result != 0 || passwords.size() > 1 ) {  // they can be only one password (max) in table
            p_result.add_error( "password_lost_2 select error " + std::to_string( db_result ) );
            return result_type::failure;
        }
        //
        if ( passwords.size() == 0 )  // user may exist, but doesn't have a password set
            return result_type::no_password;
        //
        // If OTP is expired (or password_lost_1 was not called), exit now
        if ( passwords[0]["valid_otp"] != "1" )
            return result_type::forbidden;
        //
        // OTP is stored hashed in DB. Use s_digest rather than s_salt
        // so that OTP is linked to the current password value, not to the
        // password DB entries (OTP cannot be used after password is changed)
        std::string digest = password_digest( p_otp,
                                              passwords[0]["s_digest"],
                                              passwords[0]["s_hash_param"] );
        //
        if ( digest.empty() ) {
            p_result.add_error( "password_lost_2 digest failed" );
            return result_type::failure;
        }
        //
        // Time constant comparison is not needed since we calculate the digest of caller OTP
        if ( digest != passwords[0]["s_reset_otp"] ) {
            //
            // Disable current OTP (lost_1 must then be recalled)
            db.do_update(
                    "UPDATE t_passwords SET "
                    " d_reset_validity = utc_timestamp() "
                    "WHERE k_user='" + masnae::mysql::sql_escape( p_user ) + "'",
                    affected );
            //
            return result_type::forbidden;
        }
        //
    }  // delete db: return handle
    //
    return password_set( p_user, p_password_new, p_result );
}

//-------------------------------------------------------------------------
// Change user's password. Update locking time
PasswordManager::result_type
    PasswordManager::password_set ( const std::string &           p_user,
                                    const std::string &           p_password_new,
                                    masnae::ProcessingResponse &  p_result )
{
    // Check parameters
    unsigned score = password_score( p_password_new );
    //
    p_result.add_log( masnae::ProcessingResponse::verbose,
                      "new password score: " + std::to_string( score ) );
    //
    if ( score < password_score_minimal() )
        return result_type::weak_password;
    //
    // Generate new salt, calculate new digest
    std::string  salt, digest;
    std::string  param = constants::k_hash_param;
    //
    salt   = adawat::uid_no_ts( 22                          );  // 22 chars, 131 bits
    digest = password_digest  ( p_password_new, salt, param );  // 43 chars, 256 bits
    //
    if ( salt.empty() || digest.empty() ) {
        p_result.add_error( "password_set empty salt or digest" );
        return result_type::failure;
    }
    //
    // Prepare database connection
    unsigned     db_result;
    std::string  inserted_id;;
    auto         db = masnae::globals::mysql_handler->get_connection( masnae::mysql::writer );
    //
    // Update digest and salt, eventually creating the database record
    // (Re)set locking and reset protections
    db_result = db.do_insert( "INSERT INTO t_passwords "
                              "( k_user, s_salt, s_digest, i_lock_period, d_lock_expiration, i_reset_period, d_reset_expiration, s_hash_param, d_last_update )"
                              " VALUES ("
                              "'" + masnae::mysql::sql_escape( p_user ) + "',"
                              "'" + masnae::mysql::sql_escape( salt   ) + "',"
                              "'" + masnae::mysql::sql_escape( digest ) + "',"
                              "1,"                                      // also locking period
                              "utc_timestamp() - interval 1 second,"    // in case the password is tested in the same second
                              "1,"                                      // also reset period
                              "utc_timestamp() - interval 1 second,"    // in case the password is tested in the same second
                              "'" + masnae::mysql::sql_escape( param ) + "',"
                              "utc_timestamp()"
                              ") ON DUPLICATE KEY UPDATE "
                              " s_salt             = values( s_salt ),"
                              " s_digest           = values( s_digest ),"
                              " i_lock_period      = values( i_lock_period ),"
                              " d_lock_expiration  = values( d_lock_expiration ),"
                              " i_reset_period     = values( i_reset_period ),"
                              " d_reset_expiration = values( d_reset_expiration ),"
                              " s_hash_param       = values( s_hash_param ),"
                              " d_last_update      = values( d_last_update )",
                              inserted_id );
    //
    if ( db_result == 0 ) {
        return result_type::success;
    } else {
        p_result.add_error( "password_set insert error " + std::to_string( db_result ) );
        return result_type::failure;
    }
}

//-------------------------------------------------------------------------
// Check if the password was updated more than the allowed period
// ago (defined per profile), or if the password was encoded with
// parameters different from the currents.
PasswordManager::result_type
    PasswordManager::password_change_needed ( const std::string &           p_user,
                                              bool &                        p_need_change,
                                              masnae::ProcessingResponse &  p_result )
{
    // Prepare database access
    unsigned  db_result;
    auto      db = masnae::globals::mysql_handler->get_connection( masnae::mysql::reader );
    //
    // Retrieves password hashing parameters and checks if it was
    // changed more than the allowed maximum ago.
    masnae::SingleKeyValue_List  passwords;
    //
    db_result = db.do_select(
        "SELECT s_hash_param,"
        " ( TIMESTAMPDIFF( day, d_last_update, UTC_TIMESTAMP() ) > o.i_password_lifetime AND "
        "   o.i_password_lifetime > 0 ) AS need_update "
        "FROM  t_passwords p "
        "JOIN  t_users     u ON u.k_user    = p.k_user "
        "JOIN  t_profiles  o ON o.k_profile = u.k_profile "
        "WHERE p.k_user = '"  + masnae::mysql::sql_escape( p_user ) + "'",
        passwords );
    //
    if ( db_result != 0 || passwords.size() > 1 ) {  // they can be only one password (max) in table
        p_result.add_error( "password_change_needed select error " + std::to_string( db_result ) );
        return result_type::failure;
    }
    //
    if ( passwords.size() == 0 )  // user may exist, but doesn't have a password set
        return result_type::no_password;
    //
    p_need_change = passwords[0]["s_hash_param"] != constants::k_hash_param ||
                    passwords[0]["need_update" ] == "1";
    //
    return result_type::success;
}

//-------------------------------------------------------------------------
// Return a password score (range 0-5)
unsigned PasswordManager::password_score ( const std::string &  p_password ) const
{
    double entropy = ZxcvbnMatch( p_password.c_str(), nullptr, nullptr );
    //
    // See: https://security.stackexchange.com/questions/180115/how-to-convert-zxcvbn-entropy-to-score
    //
    // RFC4086 8.2.1, recommends 90 bits in 1995 + 2/3 bit/year (105 bits
    // in 2018) for very secure passwords. zxcvbn sounds quite low reading
    // this, its top score is at 33 bits.
    //
    // Raising the minimal entropy, with a top target at 64 bits
    //
    unsigned score;
    //
         if ( entropy < 16 ) score = 0;
    else if ( entropy < 28 ) score = 1;
    else if ( entropy < 40 ) score = 2;
    else if ( entropy < 52 ) score = 3;
    else if ( entropy < 64 ) score = 4;
    else                     score = 5;
    //
    return score;
}

//-------------------------------------------------------------------------
// Accessors
unsigned PasswordManager::password_score_minimal ( void )
{
    std::shared_lock< std::shared_mutex > lock( m_mutex_config );
    //
    return m_score_minimal;
}

//-------------------------------------------------------------------------
void PasswordManager::bus_event ( const masnae::bus::Message &  p_message )
{
    switch ( p_message.type )
    {
        case masnae::bus::Message::config_updated:
            update_config();
            break;
        default:
            break;
    }
}

//-------------------------------------------------------------------------
// Reads configuration from global config object.
bool PasswordManager::update_config ( void )
{
    bool     ok = true;
    int64_t  new_otp_length;
    int64_t  new_otp_validity_s;
    int64_t  new_score_minimal;
    //
    //-----
    new_otp_length =
        masnae::globals::config_handler->get_int64( "/password/otp_length",    6 );
    //
    new_otp_validity_s =
        masnae::globals::config_handler->get_int64( "/password/otp_validity",  120 );
    //
    new_score_minimal =
        masnae::globals::config_handler->get_int64( "/password/score_minimal", 3 );
    //
    ok &= new_otp_length    >= 4 && new_otp_length    <= 9;
    ok &= new_score_minimal >= 0 && new_score_minimal <= 5;
    //
    //-----
    if ( ok ) {
        std::lock_guard<std::shared_mutex>  lock( m_mutex_config );
        //
        m_otp_length     = new_otp_length;
        m_otp_validity_s = new_otp_validity_s;
        m_score_minimal  = new_score_minimal;
    } else {
        masnae::service_error_log( "PasswordManager: invalid config" );
    }
    //
    return ok;
}

//-------------------------------------------------------------------------
// Calculate the 256 bits (43 characters) digest of a password.
// Param is the Argon2 configuration, the param part of the PHC
// string format defined in:
//      https://github.com/P-H-C/phc-string-format/blob/master/phc-sf-spec.md
// like:
//      m=4096,t=3,p=2
// Returns an empty string on error
std::string PasswordManager::password_digest( const std::string &  p_password,
                                              const std::string &  p_salt,
                                              const std::string &  p_param )
{
    //----------------------------------------------
    // Extract parameters to use
    uint32_t m_cost, t_cost, parallelism;
    //
    if ( sscanf( p_param.c_str(),
                 "m=%" SCNu32 ",t=%" SCNu32 ",p=%" SCNu32,  // Memory size, Number of iterations, Degree of parallelism
                    & m_cost,     & t_cost,     & parallelism ) != 3 )
        return "";
    //
    //----------------------------------------------
    // Calculate hash
    adawat::Data  password, salt, digest;
    //
    adawat::set_data( password, p_password );
    adawat::set_data( salt,     p_salt     );
    //
    digest.resize( constants::k_digest_size );
    //
    if ( argon2i_hash_raw( t_cost,         // cpu cost
                           m_cost,         // memory cost
                           parallelism,    // 2 threads
                           password.data(),
                           password.size(),
                           salt.data(),
                           salt.size(),
                           digest.data(),
                           digest.size() ) != ARGON2_OK )
        return "";
    //
    return adawat::encoding_base64_encode( digest );
}

} // namespace urania
