// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

// Copyright 2018 Pierre del Perugia
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <adawat/adawat_string.h>

#include "masnae_mysql.h"
#include "masnae_global.h"
#include "masnae_misc.h"

#include "urania_common.h"
#include "urania_password.h"
#include "urania_request.h"

namespace urania {

//-------------------------------------------------------------------------
// Retrieve a user's profile
static
bool get_user_details ( const std::string &           p_user,
                        masnae::SingleKey_Value &     p_user_details,
                        masnae::ProcessingResponse &  p_result )
{
    masnae::SingleKeyValue_List  users;
    auto                         db = masnae::globals::mysql_handler->get_connection( masnae::mysql::reader );
    //
    unsigned db_result =
        db.do_select( "select * "
                      "from t_users "
                      "where k_user = '" + masnae::mysql::sql_escape( p_user ) + "'",
                       users );
    //
    if ( db_result != 0 ) {
        p_result.set_jresp_failed();
        return false;
    }
    //
    if ( users.size() != 1 ) {
        p_result.add_log( masnae::ProcessingResponse::verbose,
                          "user '" + p_user + "' not found" );
        p_result.set_jresp_rejected( "not_found", "\"user\"" );
        return false;
    }
    //
    p_user_details = users[ 0 ];
    return true;
}

//-------------------------------------------------------------------------
// Check that the user's profile is in the allowed list
static
bool user_profile_allowed ( const std::string &           p_user,
                            const std::string &           p_allowed_profiles,
                            masnae::ProcessingResponse &  p_result )
{
    masnae::SingleKey_Value  user_details;
    //
    if ( ! get_user_details( p_user, user_details, p_result ) )
        return false;
    //
    if ( ! adawat::str_csv_has_value( user_details[ "k_profile" ],
                                      p_allowed_profiles ) )
    {
        p_result.add_log( masnae::ProcessingResponse::verbose,
                          "user's profile '" + user_details[ "k_profile" ] + "'not allowed" );
        p_result.set_jresp_rejected( "forbidden", "null" );
        return false;
    }
    //
    return true;
}

//-------------------------------------------------------------------------
// On error, fill p_result and return true
bool handle_password_error ( PasswordManager::result_type    p_password_result,
                             const std::string &             p_details,
                             masnae::ProcessingResponse &    p_result )
{
    switch ( p_password_result )
    {
        case PasswordManager::result_type::success:
            return false;
        //
        case PasswordManager::result_type::banned:
            p_result.set_jresp_rejected( "banned",
                                         "{\"expiration\": " + p_details + "}" );
            return true;
        //    
        case PasswordManager::result_type::no_password:
        case PasswordManager::result_type::forbidden:     
        case PasswordManager::result_type::weak_password:
            p_result.set_jresp_rejected( g_PasswordManager.to_text( p_password_result ),
                                         "null" );
            return true;
        //
        default:          
        case PasswordManager::result_type::failure:
            p_result.set_jresp_failed();
            return true;
    }
}

//-------------------------------------------------------------------------
// Change the current user's password.
// Parameters: password, password_new
void UraniaRequestHandler::process_ws_password_change ( const masnae::SingleKey_ValueList &  p_parameters,
                                                        masnae::ProcessingResponse &         p_result )
{
    // Check that the caller is allowed here
    if ( ! has_permission( "urania.password_change", p_result ) )
        return;
    //
    // Check parameters
    std::string  password, password_new;
    //
    if ( ! masnae::get_parameter( p_parameters, "password",     password,     false, false, p_result ) ||
         ! masnae::get_parameter( p_parameters, "password_new", password_new, false, false, p_result ) )
        return;
    //
    // Change user password
    std::string details;
    auto        result = g_PasswordManager.password_change( m_security.user,
                                                            password,
                                                            password_new,
                                                            details,
                                                            p_result );
    //
    if ( ! handle_password_error( result, details, p_result ) ) {  // handle success case
        std::string current_at;
        //
        // delete all access tokens except the current one
        get_accesstoken( current_at );
        //
        g_AccessTokenManager.delete_user_tokens(
            m_security.user,
            p_result,
            current_at
        );
    }                                             
    //
    p_result.add_log( masnae::ProcessingResponse::regular,
                      "password change for user '" + m_security.user + "': " +
                      g_PasswordManager.to_text( result ) );
}

//-------------------------------------------------------------------------
// Start password recovery: generate OTP
// Parameters: user
void UraniaRequestHandler::process_ws_password_lost_1 ( const masnae::SingleKey_ValueList &  p_parameters,
                                                        masnae::ProcessingResponse &         p_result )
{
    // Check that the caller is allowed here
    std::string  allowed_profiles;
    if ( ! has_permission( "urania.password_lost_1", allowed_profiles, p_result ) )
        return;
    //
    // Check parameters
    std::string  user;
    //
    if ( ! masnae::get_parameter( p_parameters, "user",     user,     false, false, p_result ) )
        return;
    //
    // Check that the user's profile is in the allowed list
    if ( ! user_profile_allowed( user, allowed_profiles, p_result ) )
        return;
    //
    // Generate OTP
    std::string otp, details;
    auto        result = g_PasswordManager.password_lost_1( user,
                                                            otp,
                                                            details,
                                                            p_result );
    //
    if ( ! handle_password_error( result, details, p_result ) ) {  // handle success case
        p_result.set_jresp_success( "{\"otp\": " + masnae::json_quote_string( otp ) + "}" );
    }
    //
    p_result.add_log( masnae::ProcessingResponse::regular,
                      "password lost 1 for user '" + user + "': " + 
                      g_PasswordManager.to_text( result ) + ", " + details );
}

//-------------------------------------------------------------------------
// Continue password recovery: set password
// Parameters: user, otp, password
void UraniaRequestHandler::process_ws_password_lost_2 ( const masnae::SingleKey_ValueList &  p_parameters,
                                                        masnae::ProcessingResponse &         p_result )
{
    // Check that the caller is allowed here
    std::string  allowed_profiles;
    if ( ! has_permission( "urania.password_lost_2", allowed_profiles, p_result ) )
        return;
    //
    // Check parameters
    std::string  user, otp, password;
    //
    if ( ! masnae::get_parameter( p_parameters, "user",     user,     false, false, p_result ) ||
         ! masnae::get_parameter( p_parameters, "otp",      otp,      false, false, p_result ) ||
         ! masnae::get_parameter( p_parameters, "password", password, false, false, p_result ) )
        return;
    //
    // Check that the user's profile is in the allowed list
    if ( ! user_profile_allowed( user, allowed_profiles, p_result ) )
        return;
    //
    // Set password
    std::string details;
    auto        result = g_PasswordManager.password_lost_2( user,
                                                            otp,
                                                            password,
                                                            details,
                                                            p_result );
    //
    if ( ! handle_password_error( result, details, p_result ) ) {  // handle success case
        // delete all access tokens
        g_AccessTokenManager.delete_user_tokens( user, p_result );
    }
    //
    p_result.add_log( masnae::ProcessingResponse::regular,
                      "password lost 2 for user '" + user + "': " +
                      g_PasswordManager.to_text( result ) );
}

//-------------------------------------------------------------------------
// Return a password score (and minimal required value)
void UraniaRequestHandler::process_ws_password_score ( const masnae::SingleKey_ValueList &  p_parameters,
                                                       masnae::ProcessingResponse &         p_result )
{
    // Check parameters
    std::string  password;
    //
    if ( ! masnae::get_parameter( p_parameters, "password", password, false, true, p_result ) )
        return;
    //
    auto score   = std::to_string( g_PasswordManager.password_score( password ) );
    auto minimal = std::to_string( g_PasswordManager.password_score_minimal()   );
    //
    p_result.add_log( masnae::ProcessingResponse::regular,
                      "password score: " + score + "/" + minimal );
    //
    p_result.set_jresp_success( std::move( 
        "{\"score\":"   + score   +
        ",\"minimal\":" + minimal +
        "}"
    ) );
    //
    if ( masnae::globals::logger_handler->quiet() )  // do not generate log for this request
        p_result.disable_log();
}

//-------------------------------------------------------------------------
// Set (forcefully) a user's password.
// Parameters: user, password
void UraniaRequestHandler::process_ws_password_set ( const masnae::SingleKey_ValueList &  p_parameters,
                                                     masnae::ProcessingResponse &         p_result )
{
    // Check that the caller is allowed here
    std::string  allowed_profiles;
    if ( ! has_permission( "urania.password_set", allowed_profiles, p_result ) )
        return;
    //
    // Check parameters
    std::string  user, password;
    //
    if ( ! masnae::get_parameter( p_parameters, "user",     user,     false, false, p_result ) ||
         ! masnae::get_parameter( p_parameters, "password", password, false, false, p_result ) )
        return;
    //
    // Check that the user's profile is in the allowed list
    if ( ! user_profile_allowed( user, allowed_profiles, p_result ) )
        return;
    //
    // Set user password
    auto result = g_PasswordManager.password_set( user,
                                                  password,
                                                  p_result );
    //
    if ( ! handle_password_error( result, "", p_result ) ) {  // handle success case
        // delete all access tokens
        g_AccessTokenManager.delete_user_tokens( user, p_result );
    }
    //
    p_result.add_log( masnae::ProcessingResponse::regular,
                      "password set for user '" + user + "': " +
                      g_PasswordManager.to_text( result ) );
}

//-------------------------------------------------------------------------
// Check if a user's password is expired or uses an old hash
// Parameters: user
void UraniaRequestHandler::process_ws_password_need_change ( const masnae::SingleKey_ValueList &  p_parameters,
                                                             masnae::ProcessingResponse &         p_result )
{
    // Check parameters
    std::string  user;
    //
    if ( ! masnae::get_parameter( p_parameters, "user", user, false, false, p_result ) )
        return;
    //
    // Check password
    bool need_change;
    auto result = g_PasswordManager.password_change_needed( user,
                                                            need_change,
                                                            p_result );
    //
    if ( ! handle_password_error( result, "", p_result ) ) {  // handle success case
        p_result.set_jresp_success(
            need_change ?
                "{\"need_change\": true  }" :
                "{\"need_change\": false }"
        );
    }
    //
    p_result.add_log( masnae::ProcessingResponse::regular,
                      "password need change for user '" + user + "': " +
                      g_PasswordManager.to_text( result ) );
}

} // namespace urania
