
build:
	cd masnae && $(MAKE) build
	cd thoe   && $(MAKE) build
	cd urania && $(MAKE) build
	cd admete && $(MAKE) build

build_test:
	cd masnae && $(MAKE) build_test
	cd thoe   && $(MAKE) build_test
	cd urania && $(MAKE) build_test
	cd admete && $(MAKE) build_test

clean:
	cd masnae && $(MAKE) clean
	cd thoe   && $(MAKE) clean
	cd urania && $(MAKE) clean
	cd admete && $(MAKE) clean

distclean:
	cd masnae && $(MAKE) distclean
	cd thoe   && $(MAKE) distclean
	cd urania && $(MAKE) distclean
	cd admete && $(MAKE) distclean
