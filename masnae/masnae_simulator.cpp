// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

// Copyright 2018 Pierre del Perugia
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <iostream>
#include <regex>
#include <sstream>
#include <adawat/adawat_file.h>
#include <adawat/adawat_uid.h>

#include "masnae_simulator.h"

namespace masnae {

//-------------------------------------------------------------------------
// Parameter starting with @ are file upload. Syntax is:
//   @file.ext[;type=content-type]
void add_file ( std::string              p_key,
                std::string              p_value,
                Fastcpipp_Environment &  p_environment )
{
    p_value.erase( 0, 1 );  // remove @
    //
    std::string  type;
    size_t       semi = p_value.find( ";type=" );  // extract content-type
    //
    if ( semi == std::string::npos ) {
        type = "application/data";
    } else {
        type = p_value.substr( semi + 6);  // skip ";type="
        p_value.erase( semi );
    }
    //
    std::string filename = p_value;
    std::string data;
    //
    if ( adawat::file_read( filename, data ) )
    {
        char * ptr_data = strdup( data.c_str() );  // FastCGI++ uses dynamic allocation to store files
        //
        if ( ptr_data != nullptr ) {
            * ( ptr_data + data.length() ) = 1;  // ensure it is not a null-terminated string to properly test attachment handling
            //
            Fastcgipp::Http::File<char> file;
                file.filename    = filename;
                file.contentType = type;
                file.size        = data.length();
                file.data.reset( ptr_data  );
            p_environment.files.insert( std::pair( p_key, std::move( file ) ) );
        }
    }
}

//-------------------------------------------------------------------------
// Given an URL with optional parameters, extract the parameters,
// add them to the environment
//      /ws/req?a=1&a=3
//
// Script can be executed several times by specifying a multiplier:
//      5*/ws/req?a=1&a=3
//
bool parse_url ( const std::string &        p_url,
                 Fastcpipp_Environment &    p_environment,
                 int &                      p_loop )
{
    p_environment.pathInfo.clear();
    p_environment.posts.clear();
    //                         5    *      /ws/req   ?  a=1&a=3
    //                         [2]  [1]    [3]      [4] [5]
    std::regex  args_regex( "((\\d+)\\*)?/([^\\?]+)(\\?(.*))?" );
    std::smatch args_matches;
    //
    if ( ! std::regex_match( p_url, args_matches, args_regex ) )
        return false;
    //
    std::string loop   = args_matches[2].str();  // multiplier: 5
    std::string script = args_matches[3].str();  // script:     ws/req
    std::string args   = args_matches[5].str();  // arguments:  a=1&a=3
    //
    // Parse multiplier:    5
    p_loop = loop.empty() ? 1 : std::stoi( loop );
    //
    // Parse script:        ws/req
    size_t begin = 0;
    for (;;)
    {
        size_t position = script.find( "/", begin );
        //
        if ( position == std::string::npos )  // last element is pushed after the loop
            break;
        //
        p_environment.pathInfo.push_back( script.substr( begin, position-begin ) );
        //
        begin = position + 1;
    }
    //
    p_environment.pathInfo.push_back( script.substr( begin ) );  // push the rest of the string
    //
    // Parse arguments:     a=1&a=3
    if ( ! args.empty() )  // if there is args
    {
        args += "&";  // add terminator:            a=1&b=2&
        //
        do {
            size_t amp   = args.find( "&" );  // extract next term
            size_t equal = args.find( "=" );  // split name / value
            if ( equal == std::string::npos || amp < equal )
                return false;
            //
            std::string key   = args.substr( 0,         equal );
            std::string value = args.substr( equal + 1, amp - equal - 1 );
            //
            if ( value[0] == '@' )
                add_file( key, value, p_environment );
            else
                p_environment.posts.insert( std::pair( key, value ) );
            //
            args.erase( 0, amp + 1 );   // b=2
        } while ( ! args.empty() );
    }
    //
    return true;
}

//-------------------------------------------------------------------------
// Execute an URL using the given RequestHandler
// p_response contains the last response
bool exec_url ( const std::string &     p_url,
                RequestHandler &        p_handler,
                ProcessingResponse &    p_response,
                bool                    p_debug /* = false */ )
{
    int  nb_loops;
    bool ok = true;
    //
    p_handler.simulation_start();  // switch to our own environment variable
    //
    if ( ! parse_url( p_url, p_handler.simulation_environment(), nb_loops ) ) {
        std::cout << "invalid URL" << p_url << "\n";
        return false;
    }
    //
    if ( p_debug ) {
        std::cout << "\n---------------------------\n";
        std::cout << p_url << "\n";  // display requested URL
    }
    //
    // Set some fake contextual data
    const char * ip_remote = "192.168.0.1";
    const char * ip_local  = "192.168.0.2";
    //
    p_handler.simulation_environment().remoteAddress.assign( ip_remote, ip_remote + strlen(ip_remote) );
    p_handler.simulation_environment().serverAddress.assign( ip_local,  ip_local  + strlen(ip_local)  );
    p_handler.simulation_environment().requestMethod = Fastcgipp::Http::RequestMethod::POST;
    p_handler.simulation_environment().requestUri    = "/masnae/simul?simul=1";
    p_handler.simulation_environment().scriptName    = "/masnae";
    p_handler.simulation_environment().userAgent     = "Masnae Simulator";
    //
    // Retrieve module and path
    std::string  module = p_handler.simulation_environment().pathInfo.at( 0 ); // first folder is the module name
    //
    Fastcpipp_Path  document (  // remaining folders are the document
        p_handler.simulation_environment().pathInfo.begin() + 1,
        p_handler.simulation_environment().pathInfo.end()
    );
    //
    // Execute the requested number of requests
    for ( int n = 0; n < nb_loops; n++ )
    {
        p_response.reset( adawat::uid_ts( adawat::uid_type::long_ts ) );
        //
        if ( ! p_handler.process_internal_request( module, document, p_response ) &&
             ! p_handler.process_request( module, document, p_response ) ) {
            std::cout << "url not found\n";
            return false;
        }
        //
        if ( p_response.has_error() ) {
            std::cout << "error: " << p_response.get_error() << "\n";
            ok = false;
            break;
        }
    }
    //
    if ( p_debug ) {  // display received response
        if ( p_response.is_raw() ) {
            std::string body( reinterpret_cast< const char* >( p_response.get_raw_data() ),
                              p_response.get_raw_size() );
            //
            std::cout << body << "\n";
        } else {
            std::ostringstream  os;
            p_response.send_jresp( os );
            //
            std::cout << os.str() << "\n";
            std::cout << "---------------------------\n";
            std::cout << p_response.get_log() << "\n";
            std::cout << "---------------------------\n";
        }
    }
    //
    return ok;
}

//-------------------------------------------------------------------------
// Prompt the user for an URL to execute, then display the last response
void console ( RequestHandler &  p_handler )
{
    std::string                 line;
    masnae::ProcessingResponse  response;
    bool                        mode_raw_text = false;
    //
    printf( "raw_text....display raw responses as text\n" );
    //
    for (;;) {
        printf( "\nscript (/ws/req?a=1&b=2), enter to exit: " );
        getline( std::cin, line );
        if ( line.empty() )
            break;
        //
        if ( line == "raw_text" ) {
            mode_raw_text = ! mode_raw_text;
            continue;
        }
        //
        if ( ! exec_url( line, p_handler, response ) )
            continue;
        //
        printf( "------------------\n" );
        //
        response.send_headers( std::cout );
        //
        if ( response.is_raw() ) {
            if ( mode_raw_text ) {
                std::string  body( reinterpret_cast< const char * >( response.get_raw_data() ),
                                   response.get_raw_size() );
                //
                printf( "%s\n", body.c_str() );
            } else {
                printf( "body sz: %u\n", static_cast< unsigned >( response.get_raw_size() ) );
                //
                size_t to_display = std::min( size_t{ 16 }, response.get_raw_size() );
                //
                // For raw (binary) response, only display the first 16 bytes
                for ( size_t i = 0; i < to_display; i++ )
                    printf( " %02x", * ( response.get_raw_data() + i ) );
                printf("\n");
            }
        } else {
            response.send_jresp( std::cout );
            printf( "\n" );
        }
        //
        if ( response.has_error() )
            printf( "error:   %s\n", response.get_error().c_str() );
        //
        if ( response.has_log() )
            printf( "log:\n%s\n", response.get_log().c_str() );
        //
        printf( "------------------\n" );
    }
}

} // namespace masnae
