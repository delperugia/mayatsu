// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

// Copyright 2018 Pierre del Perugia
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <curl/curl.h>
#include <fastcgi++/http.hpp>
#include <mariadb/mariadb_version.h>
#include <openssl/opensslv.h>
#include <rapidjson/rapidjson.h>
#include <unistd.h>
#include <adawat/adawat_common.h>
#include <adawat/adawat_string.h>
#include <adawat/adawat_uid.h>

#include "../mayatsu_version.h"
#include "masnae_global.h"
#include "masnae_misc.h"
#include "masnae_manager.hpp"
#include "masnae_request_handler.h"
#include "masnae_ws_tables.h"

namespace masnae {

//-------------------------------------------------------------------------
// Get the maximal document POST size from the manager
size_t RequestHandler::get_max_post_size ( size_t  p_default )
{
    auto manager = static_cast< request::Manager< RequestHandler > * >( globals::request_manager );
    //
    if ( manager != nullptr )
        if ( manager->get_max_post_size() > 0 )
            return static_cast< size_t >( manager->get_max_post_size() );
    //
    return p_default;
}
   
//-------------------------------------------------------------------------
RequestHandler::RequestHandler ( const size_t maxPostSize ) :
    Fastcgipp::Request<char> ( get_max_post_size( maxPostSize ) )
{
}

//-------------------------------------------------------------------------
// Extract AccessToken from Authorization header
bool RequestHandler::get_accesstoken ( std::string &  p_accesstoken )
{
    std::string  bearer;
    ///
    // Header syntax is:
    //  Authorization: Bearer abcd1324
    //
    p_accesstoken  = environment().authorization;
    bearer         = adawat::str_token( p_accesstoken );       // bearer="Bearer", at="abcd1234"
    //
    // Case sensitive like OAuth (not like RFC1945#11 case-insensitive scheme)
    return bearer == "Bearer" && ! p_accesstoken.empty();
}

//-------------------------------------------------------------------------
// Fills m_security by calling Urania with the AccessToken
// to retrieve user's details
// An HTTP module 'urania' must be defined in the configuration
bool RequestHandler::check_accesstoken ( const std::string &  p_accesstoken )
{
    auto manager = static_cast< request::Manager< RequestHandler > * >( globals::request_manager );
    //
    // Try first to retrieve user's detail from cache
    if ( manager != nullptr )
        if ( manager->find_accesstoken_in_cache( p_accesstoken, m_security ) )
            return true;
    //
    // Call Urania
    rapidjson::Document response;
    std::string         response_json, status;
    //
    masnae::globals::http_handler->request_jresp(
        "urania/ws/permissions",
        { { "Authorization", "Bearer " + p_accesstoken } }, {}, {},
        response, response_json
    );
    //
    // Fill m_security with received response, like:
    //    { "status":"success","data": {
    //        "user": "1", "profile": "web", "role": "web", "scopes": "profile",
    //        "permissions": { "dummy":"un",
    //                         "urania.password_set":"",
    //                         "urania.signin":"" } } }
    //
    if ( ! masnae::rapidjson_get_string( response, "/status", status ) ||
         status != "success" )
        return false;
    //
    if ( ! masnae::rapidjson_get_string( response, "/data/user"       , m_security.user        ) ||
         ! masnae::rapidjson_get_string( response, "/data/profile"    , m_security.profile     ) ||
         ! masnae::rapidjson_get_string( response, "/data/role"       , m_security.role        ) ||
         ! masnae::rapidjson_get_string( response, "/data/scopes"     , m_security.scopes      ) ||
         ! masnae::rapidjson_get_kv    ( response, "/data/permissions", m_security.permissions ) )
        return false;
    //
    // Put user's details in cache
    if ( manager != nullptr )
        manager->put_accesstoken_in_cache( p_accesstoken, m_security );
    //
    return true;
}

//-------------------------------------------------------------------------
// Checks that the request caller has a permission
// Set result if not
bool RequestHandler::has_permission ( const std::string &           p_permission,
                                      masnae::ProcessingResponse &  p_result )
{
    if ( m_security.permissions.count( p_permission ) > 0 ) {
        return true;
    } else {
        p_result.set_jresp_rejected( "forbidden", "null" );
        return false;
    }
}

//-------------------------------------------------------------------------
// Checks that the request caller has a permission
// and returns associated parameters
// Set result if not
bool RequestHandler::has_permission ( const std::string &           p_permission,
                                      std::string &                 p_parameter,
                                      masnae::ProcessingResponse &  p_result )
{
    auto iter = m_security.permissions.find( p_permission );
    //
    if ( iter == m_security.permissions.end() ) {
        p_result.set_jresp_rejected( "forbidden", "null" );
        return false;
    } else {
        p_parameter = iter->second;
        return true;
    }
}

//-------------------------------------------------------------------------
// Convenient function to return a status and exit response
bool RequestHandler::die ( const std::string &  p_status )
{
    out << "Status: " << p_status << "\r\n"
        << "\r\n";
    //
    return true;
}

//-------------------------------------------------------------------------
// Output the processed response and send it to Fastcgipp
void RequestHandler::send_response ( ProcessingResponse &  p_response )
{
    p_response.send_headers( out );
    //
    if ( p_response.is_raw() )
        dump( p_response.get_raw_data(), p_response.get_raw_size());
    else
        p_response.send_jresp( out );
}

//-------------------------------------------------------------------------
// Process a received request
bool RequestHandler::response ( void )
{
    auto start = std::chrono::system_clock::now();
    //
    //-------
    // The RequestURI must include at least the module name:
    //    /path/script.fcgi/module[/document]
    //
    if ( environment().pathInfo.size() < 1 )
        return die( "400 Module missing - 1" );
    //
    std::string    module = environment().pathInfo.at( 0 );        // first folder is the module name
    //
    Fastcpipp_Path document ( environment().pathInfo.begin() + 1,  // remaining folders are the document
                              environment().pathInfo.end() );
    //
    //-------
    // Handle the request
    bool handled;
    auto manager = static_cast< request::Manager<RequestHandler> * >( globals::request_manager );
    if ( manager == nullptr )
        return die( "500 Internal Server Error - 3" );
    //
    // Create a new response with a unique id
    ProcessingResponse result( adawat::uid_ts( manager->get_uid_type() ) );
    //
    // Check caller credentials (implemented by derived class)
    if ( ! check_security( module, document, result ) )
        return die( "401 Unauthorized - 2" );
    //
    // If call is made using GET, add headers to disable caching
    if ( environment().requestMethod == Fastcgipp::Http::RequestMethod::GET ||
         environment().requestMethod == Fastcgipp::Http::RequestMethod::HEAD )
    {
        result.add_http_header( "Cache-Control", "no-store, must-revalidate" );
    }
    //
    // First, check if it is an internal one (handled by RequestHandler): mgt, info or echo
    handled = process_internal_request( module, document, result );
    //
    // Then, ask the derived class if it can handle it
    if ( ! handled )
        handled = process_request( module, document, result );
    //
    if ( ! handled )
        return die( "404 API not found - 4" );
    //
    //-------
    // Add processing duration to log
    std::chrono::duration< double > duration = std::chrono::system_clock::now() - start;
    //
    result.add_log( result.debug, "timing: " + std::to_string( duration.count() ) + "s" );
    //
    //-------
    // Sends the response back, store error and log
    send_response( result );
    //
    if ( result.has_error() )       // sends an error to the web server for logging
        err << result.get_id()      // this goes to /var/log/<web_server>/error.log
            << " "
            << result.get_error();
    //
    if ( ! result.is_log_disabled() )  // some (small, read only) requests do not generate log
        store_log( module, document, result );
    //
    return true;
}

//-------------------------------------------------------------------------
// Catch requests to modules handled by RequestHandler
// Returns true if the request is handled (p_result must be filled then)
bool RequestHandler::process_internal_request ( const std::string &     p_module,
                                                const Fastcpipp_Path &  p_document,
                                                ProcessingResponse &    p_result )
{
    bool handled = false;  // if not handled here, will call the derived class
    //
         if ( p_module == "mgt" )   handled = process_management_request ( p_document, p_result );
    else if ( p_module == "info" )  handled = process_info_request       ( p_document, p_result );
    else if ( p_module == "echo" )  handled = process_echo_request       ( p_document, p_result );
    //
    return handled;
}

//-------------------------------------------------------------------------
// Retrieve information on the request received and send it back to the peer
bool RequestHandler::process_echo_request ( const Fastcpipp_Path &  p_document,
                                            ProcessingResponse &    p_result )
{
    // Collect various information on the received request
    std::map< std::string, std::string > data;
    //
    data ["script"]         = environment().scriptName;
    data ["request_uri"]    = environment().requestUri;
    //
    data ["server_address"] = "[" + convert_address( environment().serverAddress ) + "]:" +
                              std::to_string( static_cast< unsigned >( environment().serverPort ) );
    //
    data ["remote_address"] = "[" + convert_address( environment().remoteAddress ) + "]:" +
                              std::to_string( static_cast< unsigned >( environment().remotePort ) );
    //
    // Format them to be sent back
    p_result.set_jresp_success( json_convert( data ) );
    //
    if ( globals::logger_handler->quiet() )  // do not generate log for this request
        p_result.disable_log();
    //
    return true;  // will not call the derived class
}

//-------------------------------------------------------------------------
// Collect information one the system, on this class and the derived one
bool RequestHandler::process_info_request ( const Fastcpipp_Path &  p_document,
                                            ProcessingResponse &    p_result )
{
    auto manager = static_cast< request::Manager< RequestHandler > * >( globals::request_manager );
    //
    // Build masnae JSON info part
    std::map< std::string, std::string >   data;
    std::string                            masnae_json;
    //
    data ["version_adawat"]    = adawat::k_adawat_version;
    data ["version_curl"]      = curl_version();
    data ["version_fastcgipp"] = Fastcgipp::version;
    data ["version_mariadb"]   = MARIADB_PACKAGE_VERSION;
    data ["version_mayatsu"]   = mayatsu::k_version;
    data ["version_openssl"]   = OPENSSL_VERSION_TEXT;
    data ["version_rapidjson"] = RAPIDJSON_VERSION_STRING;
    data ["process_id"]        = std::to_string( getpid() );
    //
    masnae_json  = "\"masnae\":" + json_convert( data );  // just the JSON element
    //
    // Modules' info
    masnae_json += ",\"modules\":";
    masnae_json += "{\"config\":"  + globals::config_handler    ->specific_info();
    masnae_json += ",\"request\":" + manager                    ->specific_info();
    masnae_json += ",\"db\":"      + globals::mysql_handler     ->specific_info();
    masnae_json += ",\"tables\":"  + globals::ws_tables_handler ->specific_info();
    masnae_json += ",\"caches\":"  + globals::ws_caches_handler ->specific_info();
    masnae_json += ",\"http\":"    + globals::http_handler      ->specific_info();
    masnae_json += "}";
    //
    // Get derived class's info
    std::string data_json = specific_info();  // must be a non-empty JSON object:  { "x" :...
    //
    // Merge the two (insert masnae JSON at the beginning)
    auto brace = data_json.find( "{" );
    //
    if ( brace == std::string::npos )  // bad derived class: only return masnae's
        data_json = "{" + masnae_json + "}";
    else
        data_json.insert( brace + 1,
                          masnae_json + "," );  // could produce bad json if derived returns {}
    //
    p_result.set_jresp_success( data_json );
    //
    if ( globals::logger_handler->quiet() )  // do not generate log for this request
        p_result.disable_log();
    //
    return true;  // will not call the derived class
}

//-------------------------------------------------------------------------
// Execute one of the management request. p_document is either:
//  - tables
//  - caches
// Returns true if command was handled. If not, derived class will be called.
bool RequestHandler::process_management_request ( const Fastcpipp_Path &  p_document,
                                                  ProcessingResponse &    p_result )
{
    if ( p_document.size() == 0 ) // this command is not handled here
        return false;
    //
         if ( p_document[ 0 ] == "tables" )  return process_management_tables_request( p_document, p_result );
    else if ( p_document[ 0 ] == "caches" )  return process_management_caches_request( p_document, p_result );
    //
    return false;
}

//-------------------------------------------------------------------------
// Execute the management request. p_document is either:
//  - tables                   list existing tables
//  - tables, <table_name>     operation on the given table
// Returns true if command was handled. If not, derived class will be called.
bool RequestHandler::process_management_tables_request ( const Fastcpipp_Path &  p_document,
                                                         ProcessingResponse &    p_result )
{
    // ws_tables::Handler must be present
    if ( globals::ws_tables_handler == nullptr ) {
        p_result.set_jresp_failed();
        return true;
    }
    //
    // If no table name is specified, returns the list of tables
    if ( p_document.size() < 2 )
    {
        globals::ws_tables_handler->get_tables( p_result );
    }
    else  // execute command on the given table
    {
        // Merge post and get parameters
        auto params = masnae::merge_parameters( environment().posts,
                                                environment().gets );
        //
        // Retrieve parameters
        std::string command, fields, filter, order, range;
        //
        if ( ! masnae::get_parameter( params, "command", command, false, false, p_result ) ||
             ! masnae::get_parameter( params, "fields",  fields,  false, true,  p_result ) ||
             ! masnae::get_parameter( params, "filter",  filter,  false, true,  p_result ) ||
             ! masnae::get_parameter( params, "order",   order,   false, true,  p_result ) ||
             ! masnae::get_parameter( params, "range",   range,   false, true,  p_result ) )
            return true;
        //
        adawat::str_lower( command );
        //
        // Check permissions
        if ( command == "insert" || command == "delete" || command == "update" ) {
            if ( ! has_permission( "masnae.tables.write", p_result ) )
                return true;
        }
        else
        if ( command == "select" ) {
            if ( ! has_permission( "masnae.tables.read", p_result ) )
                return true;
        }
        else {  // unexpected command
            return false;
        }
        //
        // Execute the command
        globals::ws_tables_handler->execute(
            p_document[ 1 ],  // the table name (generate an error if it doesn't exist)
            command,
            fields,
            filter, 
            order,  
            range,  
            p_result
        );
    }
    //
    return true;
}

//-------------------------------------------------------------------------
// Execute the management request. p_document is either:
//  - caches                   list existing tables
//  - caches, <cache_name>     operation on the given cache
// Returns true if command was handled. If not, derived class will be called.
bool RequestHandler::process_management_caches_request ( const Fastcpipp_Path &  p_document,
                                                         ProcessingResponse &    p_result )
{
    // ws_caches::Handler must be present
    if ( globals::ws_caches_handler == nullptr ) {
        p_result.set_jresp_failed();
        //
        return true;
    }
    //
    // If no cache name is specified, returns the list of caches
    if ( p_document.size() < 2 )
    {
        globals::ws_caches_handler->get_caches( p_result );
    }
    else  // execute command on the given cache
    {
        // Merge post and get parameters
        auto params = masnae::merge_parameters( environment().posts,
                                                environment().gets );
        //
        // Retrieve parameters
        std::string command;
        //
        if ( ! masnae::get_parameter( params, "command", command, false, false, p_result ) )
            return true;
        //
        adawat::str_lower( command );
        //
        // Check permissions
        if ( command == "reload" ) {
            if ( ! has_permission( "masnae.caches.write", p_result ) )
                return true;
        }
        else {  // unexpected command
            return false;
        }
        //
        // Execute the command
        globals::ws_caches_handler->execute(
            p_document[ 1 ],  // the table name (generate an error if it doesn't exist)
            command,
            p_result
        );
    }
    //
    return true;
}
//
//-------------------------------------------------------------------------
// Store request log
void RequestHandler::store_log ( const std::string &         p_module,
                                 const Fastcpipp_Path &      p_document,
                                 const ProcessingResponse &  p_result )

{
    std::string log;
    //
    log.reserve( p_result.get_log().length() + 512 );
    //
    log += p_module                             + " " +
           adawat::str_join( p_document, "/" )  + " " +
           p_result.get_http_status()           + "\n";
    //
    log += "peer:     "                                   +
           convert_address( environment().remoteAddress ) + "\n";
    //
    // Keep a trace of the security details
    log += "security: "
           "user "    + m_security.user   + ", "
           "role "    + m_security.role   + ", "
           "scopes "  + m_security.scopes + "\n";
    //
    // The log itself
    if ( p_result.has_log() )
        log += "---\n" + p_result.get_log();
    //
    // The returned result (no data are leaked here)
    if ( p_result.is_raw() )
        log += "---\nraw response\n";
    else
    if ( p_result.is_jresp_error() )
        log += "---\njresp error: " + p_result.get_jresp_message() + "\n";
    else
        log += "---\n" + p_result.get_jresp_status() + "\n";
    //
    globals::logger_handler->store( p_result.get_id(),
                                    log );
}

} // namespace masnae

