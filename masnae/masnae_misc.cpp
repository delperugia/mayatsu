// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

// Copyright 2018 Pierre del Perugia
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <codecvt>
#include <fastcgi++/log.hpp>
#include <locale>
#include <rapidjson/pointer.h>
#include <sstream>
#include <unistd.h>
#include <adawat/adawat_crypto.h>
#include <adawat/adawat_encoding.h>
#include <adawat/adawat_string.h>

#include "masnae_global.h"
#include "masnae_misc.h"

namespace masnae {

//-------------------------------------------------------------------------
// Check if a string is a valid UTF8 string
// See SpiderMonkey: CharacterEncoding.cpp, JS::StringIsUTF8
bool is_utf8 ( const void * p_string, uint32_t p_length )
{
    const uint8_t * s     = reinterpret_cast< const uint8_t * >( p_string );
    const uint8_t * limit = s + p_length;
    //
    while (s < limit) {
        uint32_t len;
        uint32_t min;
        uint32_t n = *s;
        if ((n & 0x80) == 0) {
            len = 1;
            min = 0;
        } else if ((n & 0xE0) == 0xC0) {
            len = 2;
            min = 0x80;
            n &= 0x1F;
        } else if ((n & 0xF0) == 0xE0) {
            len = 3;
            min = 0x800;
            n &= 0x0F;
        } else if ((n & 0xF8) == 0xF0) {
            len = 4;
            min = 0x10000;
            n &= 0x07;
        } else {
            return false;
        }
        if (s + len > limit)
            return false;
        for (uint32_t i = 1; i < len; i++) {
            if ((s[i] & 0xC0) != 0x80)
                return false;
            n = (n << 6) | (s[i] & 0x3F);
        }
        if (n < min || (0xD800 <= n && n < 0xE000) || n >= 0x110000)
            return false;
        s += len;
    }
    return true;
}

//-------------------------------------------------------------------------
// Returns the given string quoted using JavaScript style, enclose in quotes
//    e.g.:    He is called "Johnny"   -->   "He is called \"Johnny\""
std::string json_quote_string ( const std::string &  p_text )
{
    char        hex[8];  // 5 rounded to 8
    std::string result;
    //
    result.reserve( p_text.size() * 2 + 4 );  // +3 in fact, rounded to 4
    //
    result += "\"";
    //
    for ( auto x : p_text ) {
        switch (x) {
            case '\b' : result += "\\b";  break;
            case '\f' : result += "\\f";  break;
            case '\n' : result += "\\n";  break;
            case '\r' : result += "\\r";  break;
            case '\t' : result += "\\t";  break;
            case '"'  : result += "\\\""; break;
            case '\\' : result += "\\\\"; break;
            default   :
                if ( static_cast< unsigned >( x ) < 32 ) {
                    sprintf( hex, "\\u%04x", static_cast<unsigned>(x) );
                    result += hex;
                } else {
                    result += x;
                }
                break;
        }
    }
    //
    result += "\"";
    //
    return result;
}

//-------------------------------------------------------------------------
// Returns the given vector as a JSON array of string
//    e.g.:  ["alpha","beta"]
std::string json_convert ( const KeyList &  p_vector )
{
    if ( p_vector.empty() )
        return "[]";
    //
    //-----------------
    std::string result;
    //
    result.reserve( p_vector.size() * 16 );  // arbitrary average value size
    //
    for ( const auto & x : p_vector )
        result += ',' + json_quote_string( x );
    //
    result[0]  = '[';  // replace ',' by '{'
    result    += ']';
    //
    return result;
}

//-------------------------------------------------------------------------
// Returns the given map as a JSON object of string
//    e.g.:    {"key1":"value1","key2":"value2"}
std::string json_convert ( const SingleKey_Value &  p_map )
{
    if ( p_map.empty() )
        return "{}";
    //
    //-----------------
    std::string result;
    //
    result.reserve( p_map.size() * 32 );  // arbitrary average key+value size
    //
    for ( const auto & x : p_map )
        result += ',' + json_quote_string( x.first ) + ':' + json_quote_string( x.second );
    //
    result[0]  = '{';  // replace ',' by '{'
    result    += '}';
    //
    return result;
}

//-------------------------------------------------------------------------
// Returns the given list of maps as a JSON array of objects
//    e.g.:    [ { "key1": "value1", "key2": "value2" }, { "key1": "value3", "key2": "value4" } ]
std::string json_convert ( const SingleKeyValue_List &  p_rows )
{
    if ( p_rows.empty() )
        return "[]";
    //
    //-----------------
    std::string result;
    //
    result.reserve( p_rows.size() * 256 );  // arbitrary average key+value size
    //
    for ( const auto & row : p_rows )
        result += ',' + json_convert( row );
    //
    result[0]  = '[';  // replace ',' by '['
    result    += ']';
    //
    return result;
}

//-------------------------------------------------------------------------
// Converts a FastCGI++ Address object into a string
std::string convert_address ( const Fastcpipp_Address &  p_address )
{
    std::ostringstream os;
    //
    os << p_address;
    //
    return os.str();
}

//-------------------------------------------------------------------------
// Convert FastCGI++ request method to a string
std::string convert_method ( Fastcgipp::Http::RequestMethod  p_method )
{
    std::ostringstream os;
    //
    os << p_method;
    //
    return os.str();
}

//-------------------------------------------------------------------------
// Merge the received post and get parameters, knowing that:
//  - post has precedence over get
//  - same parameter name can be present several times (hence the vector)
SingleKey_ValueList merge_parameters( const Fastcpipp_Params &  p_post,
                                      const Fastcpipp_Params &  p_get )
{
    // Merge identical keys in multimap in vector ( a:1 b:2 a:3 => a:1,2 b:2 )
    SingleKey_ValueList post, get;
    //
    for (const auto & x: p_post)
        post[ x.first ].push_back( x.second );
    for (const auto & x: p_get)
        get [ x.first ].push_back( x.second );
    //
    // Union the two sets, post has priority if a key is ubiquitous
    // Use std::move (post/get are not re-used after)
    auto                p = post.begin();
    auto                g = get.begin();
    SingleKey_ValueList u;  // result
    //
    for ( ; p!=post.end() && g!=get.end(); ) {
        if ( g->first < p->first ) {         // key in get that is not in post
            u[ g->first ] = std::move( g->second );
            g++;
        } else if ( g->first == p->first ) { // key ubiquitous
            u[ p->first ] = std::move( p->second );
            p++; g++;
        } else {  // g->first > p->first        key in post that is not in get
            u[ p->first ] = std::move( p->second );
            p++;
        }
    }
    //
    // Copy remaining items (only one of post or get may be not empty)
    for ( ; p != post.end(); p++ )
        u[ p->first ] = std::move( p->second );
    for ( ; g != get.end();  g++ )
        u[ g->first ] = std::move( g->second );
    //
    return u;
}

//-------------------------------------------------------------------------
// Retrieve a parameter from the received ones. If it is missing
// set p_result and return false
bool get_parameter( const SingleKey_ValueList &   p_parameters,
                    const std::string &           p_key,
                    std::string &                 p_value,
                    bool                          p_can_be_empty,
                    bool                          p_optional,
                    ProcessingResponse &          p_result )
{
    auto iterator = p_parameters.find( p_key );
    //
    if ( iterator == p_parameters.end() ) {  // parameter not found
        if ( p_optional ) {
            return true;  // p_value is left unchanged then
        } else {
            p_result.set_jresp_error( "Missing parameter "+ p_key );
            return false;
        }
    }
    //
    if ( iterator->second.size() != 1 ) {  // several values for the parameters
        p_result.set_jresp_error( "Multiple values in parameter " + p_key );
        return false;
    }
    //
    p_value = iterator->second[ 0 ];
    //
    if ( p_value.empty() && ! p_can_be_empty ) {  // several values for the parameters
        p_result.set_jresp_error( "Empty parameter " + p_key );
        return false;
    }
    //
    return true;
}

bool get_parameter ( const masnae::SingleKey_ValueList &   p_parameters,
                     const std::string &                   p_key,
                     int64_t &                             p_value,
                     bool                                  p_optional,
                     masnae::ProcessingResponse &          p_result )
{
    std::string  value;
    //
    if ( ! get_parameter( p_parameters, p_key, value, false, p_optional, p_result ) )
        return false;
    //
    if ( p_optional && value.empty() )  // p_value is left unchanged then
        return true;
    //
    char t;
    if ( sscanf( value.c_str(), "%" SCNd64 "%c", & p_value, & t ) != 1 ) {
        p_result.set_jresp_error( "Invalid value " + p_key );
        return false;
    }
    //
    return true;
}

//-------------------------------------------------------------------------
// Two functions to log a message at the service level, accessed
// using journalctl
void service_info_log ( const std::string &  p_message )
{
    std::string message =
        globals::program_name +
        "[" + std::to_string( getpid() ) + "] [inf] "
        + p_message;
    //
    std::wstring_convert< std::codecvt_utf8_utf16< wchar_t > > converter;
    std::wstring wmessage = converter.from_bytes( message );    
    //
    std::lock_guard< std::mutex > lock( ::Fastcgipp::Logging::mutex );
    *::Fastcgipp::Logging::logstream << wmessage << std::endl;
}

void service_error_log( const std::string &  p_message )
{
    std::string message =
        globals::program_name +
        "[" + std::to_string( getpid() ) + "] [err] "
        + p_message;
    //
    std::wstring_convert< std::codecvt_utf8_utf16< wchar_t > > converter;
    std::wstring wmessage = converter.from_bytes( message );    
    //
    std::lock_guard< std::mutex > lock(::Fastcgipp::Logging::mutex);
    *::Fastcgipp::Logging::logstream << wmessage << std::endl;
}

//-------------------------------------------------------------------------
// Convenient functions used to retrieve values in a RapidJSON
// document using JSON Pointer (like "/foo/0"). These are strict
// function: if the pointed value is not of the expected type,
// they return false.

// Pointed element is a string
bool rapidjson_get_string ( rapidjson::Document &  p_document,
                            const std::string &    p_path,
                            std::string &          p_value )
{
    rapidjson::Value * v = rapidjson::Pointer( p_path.c_str() ).Get( p_document );
    //
    if ( v == nullptr || ! v->IsString() )
        return false;
    //
    p_value = v->GetString();
    return true;
}

// Pointed element is a boolean
bool rapidjson_get_bool ( rapidjson::Document &    p_document,
                          const std::string &      p_path,
                          bool &                   p_value )
{
    rapidjson::Value * v = rapidjson::Pointer( p_path.c_str() ).Get( p_document );
    //
    if ( v == nullptr || ! v->IsBool() )
        return false;
    //
    p_value = v->GetBool();
    return true;
}
                         
// Pointed element is a boolean
bool rapidjson_get_double ( rapidjson::Document &    p_document,
                            const std::string &      p_path,
                            double &                 p_value )
{
    rapidjson::Value * v = rapidjson::Pointer( p_path.c_str() ).Get( p_document );
    //
    if ( v == nullptr || ! v->IsDouble() )
        return false;
    //
    p_value = v->GetDouble();
    return true;
}
                         
// Pointed element is an object with only string members
bool rapidjson_get_kv( rapidjson::Document &      p_document,
                       const std::string &        p_path,
                       masnae::SingleKey_Value &  p_kv )
{
    p_kv.clear();
    //
    rapidjson::Value * v = rapidjson::Pointer( p_path.c_str() ).Get( p_document );
    //
    if ( v == nullptr || ! v->IsObject() )
        return false;
    //
    for ( auto & m: v->GetObject() ) {
        if ( ! m.name.IsString() || ! m.value.IsString() )
            return false;
        //
        p_kv.insert( std::pair< std::string, std::string >(
            m.name.GetString(),
            m.value.GetString()
        ) );
    }
    //
    return true;
}

//-------------------------------------------------------------------------
// JSON Web Signature

    // Calculate the signature of headers and payload
    bool jws_calc_signature ( const std::string &  p_headers_b64,
                              const std::string &  p_payload_b64,
                              const std::string &  p_secret,
                              std::string &        p_signature_b64 )
    {
        adawat::Data content, key, signature;
        //
        adawat::set_data( content,
                          p_headers_b64 + "." + p_payload_b64 );
        //
        adawat::set_data( key, p_secret );
        //
        if ( ! adawat::crypto_hmac( key, content, signature, EVP_sha256() ) )
            return false;
        //
        p_signature_b64 =
            adawat::encoding_base64_encode( signature, adawat::base64_type::url, false );
        //
        return true;
    }

    // Parse a JSON document and put fields in a map
    bool jws_json_parse ( const std::string &         p_json,
                           masnae::SingleKey_Value &  p_map )
    {
        if ( p_json.empty() )
            return true;
        //
        // Parse the JSON object
        rapidjson::Document  json;
        //
        json.Parse< rapidjson::kParseFullPrecisionFlag |
                    rapidjson::kParseCommentsFlag >( p_json.c_str() );
        //
        if ( json.HasParseError() || ! json.IsObject() )
            return false;
        //
        // Only collect string and number form object
        for ( const auto & m : json.GetObject() )
        {
            if ( m.name.IsString() )
            {
                if ( m.value.IsInt() )
                    p_map[ m.name.GetString() ] =
                        std::to_string( m.value.GetInt() );
                else
                if ( m.value.IsString() )
                    p_map[ m.name.GetString() ] =
                        m.value.GetString();
            }
        }
        //
        return true;
    }

// Build a JWS signature using given headers and payload
// 'alg' in headers is forced to 'HS256' as this is the only
// supported algorithm
// Returns false on error
bool jws_build ( masnae::SingleKey_Value          p_headers,
                 const masnae::SingleKey_Value &  p_payload,
                 const std::string &              p_secret,
                 std::string &                    p_signature )
{
    // There is only one supported algorithm today
    p_headers[ "alg" ] = "HS256";
    //
    // Build the content to sign
    adawat::Data headers,     payload;
    std::string  headers_b64, payload_b64;
    //
    adawat::set_data( headers, masnae::json_convert( p_headers ) );
    adawat::set_data( payload, masnae::json_convert( p_payload ) );
    //
    headers_b64 = adawat::encoding_base64_encode( headers, adawat::base64_type::url, false );
    payload_b64 = adawat::encoding_base64_encode( payload, adawat::base64_type::url, false );
    //
    // And sign it
    std::string  signature_b64;
    //
    if ( ! jws_calc_signature( headers_b64, payload_b64, p_secret, signature_b64 ) )
        return false;
    //
    p_signature = headers_b64 + "." + payload_b64 + "." + signature_b64;
    //
    return true;
}

// Parse a JWS signature. Assume 'alg' is 'HS256' and extract
// headers and payload
// Returns false on error
bool jws_parse ( std::string                p_signature,
                 const std::string &        p_secret,
                 masnae::SingleKey_Value &  p_headers,
                 masnae::SingleKey_Value &  p_payload )
{
    // Extract headers and payload
    std::string  headers_b64, payload_b64;
    //
    headers_b64   = adawat::str_token( p_signature, "." );
    payload_b64   = adawat::str_token( p_signature, "." );
    // in p_signature remains the signature in base64url
    //
    // Calculate theoretical signature
    std::string  signature_b64;
    //
    if ( ! jws_calc_signature( headers_b64, payload_b64, p_secret, signature_b64 ) )
        return false;
    //
    // Compare signatures
    if ( ! adawat::crypto_equal( signature_b64, p_signature ) )
        return false;
    //
    // Decode headers and payload
    masnae::Data headers, payload;
    //
    if ( ! adawat::encoding_base64_decode( headers_b64, headers ) ||
         ! adawat::encoding_base64_decode( payload_b64, payload ) )
        return false;
    //
    // Parse JSON and extract members
    std::string  headers_json, payload_json;
    //
    adawat::get_data( headers_json, headers );
    adawat::get_data( payload_json, payload );
    //
    return
        jws_json_parse( headers_json, p_headers )
        &&
        jws_json_parse( payload_json, p_payload );
}

//-------------------------------------------------------------------------
// Adds the various stats element in data, to be later
// converted to JSON
void add_stats( const std::string &       p_name,
                const adawat::StatsValue  p_stats,
                SingleKey_Value &         p_data )
{
    p_data [ p_name + ".avg" ]   = std::to_string( p_stats.mean()  );
    p_data [ p_name + ".sd" ]    = std::to_string( p_stats.sd()    );
    p_data [ p_name + ".min" ]   = std::to_string( p_stats.min()   );
    p_data [ p_name + ".max" ]   = std::to_string( p_stats.max()   );
    p_data [ p_name + ".count" ] = std::to_string( p_stats.count() );
}

//-------------------------------------------------------------------------
// Convenient function to implement validators

//--------------------------------------------------------------
// Check that the response is a valid JResp
// Return the parsed RadidJson document
bool is_jresp ( masnae::ProcessingResponse &  p_response,
                rapidjson::Document &         p_jresp )
{
    if ( ! p_response.is_raw() ) {
        std::ostringstream  os;
        p_response.send_jresp( os );
        //
        p_jresp.Parse< rapidjson::kParseFullPrecisionFlag |
                       rapidjson::kParseCommentsFlag >( os.str().c_str() );
        //
        if ( ! p_jresp.HasParseError()     &&
             p_jresp.IsObject()            &&
             p_jresp.HasMember( "status" ) &&
             p_jresp[ "status" ].IsString() )
        {
            return true;
        }
    }
    //
    return false;
}

//--------------------------------------------------------------
// Check that the response is a valid JResp 'success' and if
// path is provided returns it (must be a string)
bool is_jresp_success ( masnae::ProcessingResponse & p_response,
                        const std::string &          p_path,
                        std::string &                p_datum )
{
    rapidjson::Document  jresp;
    //
    if ( is_jresp( p_response, jresp ) &&
         jresp[ "status" ] == "success" )
    {
        if ( p_path.empty() )
            return true;
        //
        return masnae::rapidjson_get_string( jresp,
                                             p_path,
                                             p_datum );
    }
    //
    return false;
}

//--------------------------------------------------------------
// Check that the response is a valid JResp 'success' and if
// path is provided returns it (must be a string)
bool is_jresp_success ( masnae::ProcessingResponse & p_response,
                        const std::string &          p_path,
                        bool &                       p_datum )
{
    rapidjson::Document  jresp;
    //
    if ( is_jresp( p_response, jresp ) &&
         jresp[ "status" ] == "success" )
    {
        if ( p_path.empty() )
            return true;
        //
        return masnae::rapidjson_get_bool( jresp,
                                           p_path,
                                           p_datum );
    }
    //
    return false;
}

//--------------------------------------------------------------
// Check that the response is a valid JResp 'success' and
// set p_data with data field
bool is_jresp_success  ( masnae::ProcessingResponse &                    p_response,
                         rapidjson::GenericValue< rapidjson::UTF8<> > &  p_data )
{
    rapidjson::Document  jresp;
    //
    if ( is_jresp( p_response, jresp ) &&
         jresp[ "status" ] == "success" )
    {
        p_data = jresp[ "data" ];
        return true;
    }
    //
    return false;
}

//--------------------------------------------------------------
// Check that the response is a valid JResp 'error'
bool is_jresp_error ( masnae::ProcessingResponse & p_response )
{
    rapidjson::Document  jresp;
    //
    return is_jresp( p_response, jresp ) &&
           jresp[ "status" ] == "error";
}

//--------------------------------------------------------------
// Check that the response is a valid JResp 'failed'
bool is_jresp_failed ( masnae::ProcessingResponse & p_response )
{
    rapidjson::Document  jresp;
    //
    return is_jresp( p_response, jresp ) &&
           jresp[ "status" ] == "failed";
}

//--------------------------------------------------------------
// Check that the response is a valid JResp 'rejected' and if
// reason and data are provided check them too
bool is_jresp_rejected ( masnae::ProcessingResponse & p_response,
                         const std::string &          p_expected_reason,
                         const std::string &          p_expected_data )
{
    rapidjson::Document  jresp;
    //
    if ( is_jresp( p_response, jresp ) &&
         jresp[ "status" ] == "rejected" )
    {
        if ( ! p_expected_reason.empty() )
            if ( jresp[ "reason" ] != p_expected_reason.c_str() )
                return false;
        //
        if ( ! p_expected_data.empty() )
            if ( jresp[ "data" ] != p_expected_data.c_str() )
                return false;
        //
        return true;
    }
    //
    return false;
}

} // namespace masnae
