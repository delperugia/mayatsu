// Copyright 2018 Pierre del Perugia
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

//
// MySQL manager - pool of MySQL database connections
//

#ifndef masnae_mysql_h
#define masnae_mysql_h

#include <list>
#include <string>
#include <adawat/adawat_stats.h>

#include "masnae_bus.h"
#include "masnae_common.h"
#include "masnae_config.h"

// Avoid the full mariadb/mysql.h inclusion
#ifndef _mysql_h
struct MYSQL;
#endif

namespace masnae {
namespace mysql {

// Masnae specific error codes (MySQL error codes start at 1000)
#define MSN_MYSQL_ERR_MIN              (1)
#define MSN_MYSQL_ERR_NO_HANDLE        (1)
#define MSN_MYSQL_ERR_NOT_WRITER       (2)
#define MSN_MYSQL_ERR_IN_TRANSACTION   (3)
#define MSN_MYSQL_ERR_NO_TRANSACTION   (4)
#define MSN_MYSQL_ERR_MAX              (4)

//-------------------------------------------------------------------------
// Two pools of handles are maintained, when requesting a Connection the
// caller specifies what kind of operations he will make: read only or read/write
enum access_type : int { reader = 0, writer = 1 };

//-------------------------------------------------------------------------
// Escape special characters of a string to be inserted in an SQL statement
std::string sql_escape ( const std::string &  p_string );

//--------------------------------------------------------------------
// Set the JResp answer on DB error: either a fail or an error
void build_resp_error ( unsigned              p_error,
                        ProcessingResponse &  p_result );

//--------------------------------------------------------------------
// Return p_time (now if not passed) as a MySQL DATETIME
// UTC. All dates in database must be UTC (see utc_timestamp())
std::string datetime_utc ( std::time_t  p_time = 0 );

//--------------------------------------------------------------------
// Converts a MySQL DATETIME UTC as time_t or -1 on error
std::time_t datetime_utc ( std::string  p_time );

//--------------------------------------------------------------------
class Handler;

//-------------------------------------------------------------------------
// A connection, allocated to the requester by the manager
class Connection
{
    public:
        Connection( Handler &  p_handler, access_type  p_access, MYSQL *  p_handle );
        //
        ~Connection();
        //
        // Returns a a MySQL result or one of the MSN_MYSQL_ERR_ errors
        unsigned  do_insert   ( const std::string &  p_sql, std::string &          p_id );
        unsigned  do_update   ( const std::string &  p_sql, size_t &               p_affected );
        unsigned  do_select   ( const std::string &  p_sql, SingleKeyValue_List &  p_rows );
        unsigned  do_delete   ( const std::string &  p_sql, size_t &               p_affected );
        //
        // Returns the MySQL text description of the last error 
        std::string
                  native_error         ( void );
        //
        // Return a a MySQL result or one of the MSN_MYSQL_ERR_ errors
        unsigned  start_transaction    ( void );
        unsigned  commit_transaction   ( void );
        unsigned  rollback_transaction ( void );
        //
        // Accessors
        bool      is_valid    ( void ) const  { return m_handle != nullptr; }
        bool      is_writer   ( void ) const  { return m_access == writer;  }
        size_t    max_results ( void ) const  { return m_max_results;       }
        //
        // Mutators
        size_t    set_max_results ( size_t  p_value )
        {
            std::swap( m_max_results, p_value );
            return p_value;  // returns previous value
        }
        //
         //------------------------------------------
        // Prevent copy on the object
        Connection                ( const Connection & ) = delete;
        Connection & operator =   ( const Connection & ) = delete;
        //
    private:
        //
        Handler &   m_handler;
        //
        access_type m_access;
        MYSQL *     m_handle;                  // the allocated MySQL handle (or nullptr if Connection is not valid)
        size_t      m_max_results    = 1000;   // maximum rows allowed in result set
        bool        m_first_use      = true;   // true if the connection has not executed any query
        bool        m_in_transaction = false;  // in a transaction
        //
        // Same as mysql_real_query but retry if server is gone...
        int         query_stat      ( const std::string &  p_sql );  // update statistics
        int         query           ( const std::string &  p_sql );  // retry
        //
        void        flush_results   ( void );   // remove all remaining result sets from MySQL handle
};

//-------------------------------------------------------------------------
class Handler : public bus::Subscriber
{
    friend class  Connection;
        //
    public:
        Handler();
        //
        virtual
        ~Handler();
        //
        // Start and stop the module
        bool            start               ( void );
        void            stop                ( void );
        //
        // Return module information as a JSON string
        std::string     specific_info       ( void );
        //
        // Get a connection from the poll. If none is available,
        // returns an invalid Connection (is_valid() returns false)
        // You need to call thread_begin before.
        Connection      get_connection      ( access_type  p_access );
        //
        // Same but in a dynamic object. Be sure to delete it!
        [[nodiscard]]
        std::unique_ptr< Connection >
                        get_connection_ptr  ( access_type  p_access );
        //
        // Called by the bus with a new message
        virtual
        void            bus_event           ( const bus::Message &  p_message );
        //
        // Prevent copy on the object
        Handler                             ( const Handler & ) = delete;
        Handler & operator =                ( const Handler & ) = delete;
        //
    protected:
        // Each thread must initialize its MySQL context before calling
        // functions. masnae::mysql objects call begin/end and an internal
        // counter avoids extra context creation or untimely destruction
        void        thread_begin    ( void );
        void        thread_end      ( void );
        //
        // Called by Connection destructor to release the handle
        void        return_handle   ( MYSQL *  p_handle, access_type  p_access );
        //
        // Update execution time statistics
        void        update_stats    ( access_type  p_access, int64_t  p_duration_ms );
        //
    private:
        // Configuration from the JSON configuration file
        typedef struct t_config_ {
            // Common
            std::string     user;
            std::string     password;
            std::string     schema;
            int64_t         max_size;
            //
            // Advanced common
            bool            compress;
            int64_t         timeout_ms;
            //
            // Reader
            std::string     reader_host;
            int64_t         reader_port;
            std::string     reader_unix_socket;
            //
            // Writer
            std::string     writer_host;
            int64_t         writer_port;
            std::string     writer_unix_socket;
            //
            // Changes in config require a reconnection
            bool need_reconnect ( const t_config_ &  p_right );
            //
        }  t_config;
        //
        typedef struct {
            MYSQL * handle;     // MySQL handles
            bool    in_use;     // used by a Connection
            bool    to_close;   // to be deleted when returned by Connection
            //
        }  t_entry;
        //
        typedef struct {
            std::mutex              mutex;
            std::condition_variable condition;
            std::list< t_entry >    handles;
            //
            adawat::StatsValue      wait_duration;    // in second
            adawat::StatsValue      exec_duration;    // in second
            uint64_t                no_handle_count;  // unsatisfied handle requests
            //
            // Handle manipulations in the pool
            MYSQL * add_handle_for_new_connection ( size_t  p_max_handles );
            MYSQL * find_handle                   ( void                  );
            void    release_handle                ( MYSQL * p_handle      );
            void    close_handle                  ( MYSQL * p_handle      );
            void    close_all_handles             ( void                  );
            //
        } t_pool;
        //
        static thread_local
        unsigned            m_thread_begin_counter;  // to avoid multiple context creation
        //
        t_pool              m_pools [ 2 ];           // indexed by access_type
        std::shared_mutex   m_mutex_config;
        t_config            m_config;
        //
        // Reads config, clears pools if needed
        void    update_config   ( void );
        //
        // Closes handles from pools
        void    clear_pools     ( void );
        //
        // Creates a new handle
        bool    connect_handle  ( MYSQL *  p_handle, access_type  p_access );
        //
        // Retrieve a handle from the pool or create a new one, nullptr on error
        MYSQL * allocate_handle ( access_type  p_access,
                                  bool         p_allow_extra,  // can exceed maximum configured
                                  bool &       p_is_new );     // returned handle was just allocated
};

} // namespace mysql
} // namespace masnae

#endif
