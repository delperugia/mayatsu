// Copyright 2018 Pierre del Perugia
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

//
// WSTable manager - web service access to internal tables
//

#ifndef masnae_ws_tables_h
#define masnae_ws_tables_h

#include <rapidjson/document.h>
#include <string>

#include "masnae_request_handler.h"
#include "masnae_mysql.h"

namespace masnae {
namespace ws_tables {
namespace jsonsql {

//-------------------------------------------------------------------------
// Class handling MongoDB like JSON queries
class filter
{
    public:
        // Convert a JSON SQL expression into SQL into p_output. On error,
        // returns false and set the reason in p_output.
        bool            convert      ( const std::string &   p_json,
                                       std::string &         p_output );
        //
    protected:
        // Check that a field exist
        virtual
        bool            valid_field  ( const std::string &   p_field  ) { return true; }
        //
        // Check that a field can accept the given type (one of S, U, I, D)
        virtual
        bool            field_accept ( const std::string &   p_field,
                                       char                  p_type   ) { return true; }
        //
    private:
        // The various item parsing functions. All throw an exception
        // on error
        std::string  parse_field  ( const rapidjson::Value &          p_value     );
        std::string  parse_value  ( const std::string &               p_field,
                                    const rapidjson::Value &          p_value     );
        std::string  parse_array  ( const std::string &               p_field,
                                    const rapidjson::Value &          p_value,
                                    const std::string &               p_separator );
        std::string  parse_member ( const std::string &               p_field,
                                    const rapidjson::Value::Member &  p_member    );
        std::string  parse_object ( const std::string &               p_field,
                                    const rapidjson::Value &          p_value     );
};

} // namespace jsonsql

//-------------------------------------------------------------------------
// Definition definition
// Column types to use are:
//  - S for char, varchar, datetime
//  - U for unsigned integer
//  - I for integer
//  - D for number
typedef struct {
    char            type;           // one of S, U, I, D
    bool            in_insert;      // field can be used in INSERT
    bool            in_update;      // field can be used in UPDATE
    bool            required;       // must be set in INSERT, cannot be empty in UPDATE
    const char *    default_insert; // default value in INSERT if missing
    const char *    default_update; // default value on UPDATE if missing
} FieldDescriptor;

typedef std::map< std::string, FieldDescriptor > Fields;  // indexed by field name

//-------------------------------------------------------------------------
// A class describing a database table handled by Masnae
// Derive from this class and define m_table_name and m_fields in
// constructor.
class Definition : jsonsql::filter
{
    public:
        Definition( const std::string &  p_name );
        //
        // Execute the given command
        //  - p_command is one of: INSERT, UPDATE, DELETE, SELECT
        //  - p_fields is:
        //      o an object with fields/values for INSERT and UPDATE
        //           { "name": "D","age": 42 }
        //      o an array with a list of fields for SELECT
        //           [ "name","age" ]
        //  - p_filter is like { "name": "D" }
        //  - p_order          { "name": 1 }
        //  - p_range          [ 5, 10]
        void            execute            ( const std::string &   p_command,
                                             const std::string &   p_fields,
                                             const std::string &   p_filter,
                                             const std::string &   p_order,
                                             const std::string &   p_range,
                                             ProcessingResponse &  p_result );
        //
        // The following function build the SQL request to execute. They
        // throw an std::exception on error.
        // Parameters are:
        //  - p_fields_values   { "name": "D","age": 42 }
        //  - p_fields_list     [ "name","age" ]
        //  - p_filter          { "name": "D" }
        //  - p_order           { "name": 1 }
        //  - p_range           [ 5, 10]
        std::string     build_sql_insert   ( const std::string &   p_fields_values );
        //
        std::string     build_sql_update   ( const std::string &   p_fields_values,
                                             const std::string &   p_filter,
                                             const std::string &   p_order,
                                             const std::string &   p_range );
        //
        std::string     build_sql_select   ( const std::string &   p_fields_list,
                                             const std::string &   p_filter,
                                             const std::string &   p_order,
                                             const std::string &   p_range );
        //
        std::string     build_sql_delete   ( const std::string &   p_filter,
                                             const std::string &   p_order,
                                             const std::string &   p_range );
        //
        // Execute the SQL request and build a JResp response:
        //  - insert:   data: inserted id
        //  - update:   data: affected rows
        //  - select:   data: array of objects, each object being a row
        //  - delete:   data: affected rows
        void            build_resp_insert  ( const std::string &   p_sql,
                                             ProcessingResponse &  p_result );
        void            build_resp_update  ( const std::string &   p_sql,
                                             ProcessingResponse &  p_result );
        void            build_resp_select  ( const std::string &   p_sql,
                                             ProcessingResponse &  p_result );
        void            build_resp_delete  ( const std::string &   p_sql,
                                             ProcessingResponse &  p_result );
        //
        std::string     name ( void)    { return m_table_name; }
        //
    protected:
        // Description of the table
        Fields          m_fields;
        std::string     m_table_name;
        //
    private:
        //
        // Various JSON conversion functions
        SingleKey_Value  build_field_with_value ( const std::string &        p_fields,
                                                  std::set< std::string > &  p_fields_with_value,
                                                  std::set< std::string > &  p_fields_with_null_value );
        KeyList          build_field            ( const std::string &        p_fields );
        std::string      build_filter           ( const std::string &        p_filter );
        std::string      build_order            ( const std::string &        p_order  );
        std::string      build_range            ( const std::string &        p_range  );
        //
        // Field usage check
        bool             valid_field_for_insert ( const std::string &        p_field  );
        bool             valid_field_for_update ( const std::string &        p_field  );
        //
        // These two functions overload filter's virtual functions
        bool             valid_field            ( const std::string &        p_field  );
        bool             field_accept           ( const std::string &        p_field,
                                                  char                       p_type   );
        //
        void             build_resp_error       ( unsigned                   p_error,
                                                  ProcessingResponse &       p_result );
};

//-------------------------------------------------------------------------
// Class managing tables. All tables automatically register here, and this
// class is called by the RequestHandler when a /mgt/tables or /mgt/table/...
// web service call is received
class Handler : public bus::Subscriber
{
    public:
         Handler() = default ;
        ~Handler();
        //
        bool            start              ( void );
        void            stop               ( void );
        //
        // Returns module information as a JSON string
        std::string     specific_info      ( void );
        //
        // Execute the /mgt/tables command
        void            get_tables         ( ProcessingResponse &  p_result );
        //
        // Execute the given command on the requested table
        void            execute            ( const std::string &   p_table,
                                             const std::string &   p_command,
                                             const std::string &   p_fields,
                                             const std::string &   p_filter,
                                             const std::string &   p_order,
                                             const std::string &   p_range,
                                             ProcessingResponse &  p_result );
        //
        // Called by Definition constructor to self register, before starting
        void            add_table          ( Definition *          p_table );
        //
        // Called by the bus with a new message
        virtual
        void            bus_event           ( const bus::Message &  p_message );
        //
        // Disable object copy
        Handler                 ( const Handler & ) = delete;
        void operator=          ( const Handler & ) = delete;
        //
    private:
        //
        bool                                   m_running = false;  // started
        std::map< std::string, Definition *>   m_tables;           // list of managed tables, indexed by their name
};

} // namespace tables
} // namespace masnae

#endif
