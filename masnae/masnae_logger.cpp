// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

// Copyright 2018 Pierre del Perugia
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <fcntl.h>
#include <sys/stat.h>
#include <unistd.h>

#include "masnae_global.h"
#include "masnae_logger.h"
#include "masnae_misc.h"

namespace masnae {
namespace logger {

//-------------------------------------------------------------------------
// Create a directory and returns true if this directory exist
static
bool mkdir_exit ( const std::string &  p_path,
                  mode_t               p_directory_mode )
{
    return mkdir( p_path.c_str(),
                  p_directory_mode ) == 0           // successful...
           ||
           errno == EEXIST;                         // ... or already exist
}

//-------------------------------------------------------------------------
// Recursively create a path, starting from the given position
static
bool create_path ( const std::string &  p_path,
                   size_t               p_from,
                   mode_t               p_directory_mode )
{
    size_t pos = p_path.rfind( '/', p_from );
    //
    if ( pos == std::string::npos || pos == 0 )  // no more previous folder
        return false;
    //
    // Try to create this level of the path
    if ( mkdir_exit( p_path.substr( 0, pos ), p_directory_mode ) )
        return true;
    //
    if ( errno == ENOENT )  // previous level doesn't exist
        return create_path( p_path, pos - 1, p_directory_mode )  // create previous...
               &&
               mkdir_exit( p_path.substr( 0, pos ).c_str(),           // ... and current
                           p_directory_mode );
    else
        return false;  // other error
}

//-------------------------------------------------------------------------
// Open a file, eventually creating the path
static
int open_with_path ( const std::string &  p_name,
                     int                  p_flags,
                     mode_t               p_file_mode,
                     mode_t               p_directory_mode )
{
    int hFile = open( p_name.c_str(), p_flags, p_file_mode );
    //
    if ( hFile == -1 && errno == ENOENT )          // folder doesn't exist
        if ( create_path( p_name.c_str(),          // try to create the path
                          p_name.length() -1,
                          p_directory_mode ) )
            hFile = open( p_name.c_str(),
                          p_flags,
                          p_file_mode );  // then retry to create file
    //
    return hFile;
}

//-------------------------------------------------------------------------
Handler::~Handler ()
{
    stop();
}

//-------------------------------------------------------------------------
// Start, load config
bool Handler::start ( const std::string &  p_default_folder )
{
    m_folder_default = p_default_folder;  // before update_config
    //
    if ( ! update_config() )
        return false;
    //
    globals::publisher->subscribe( this );
    //
    return true;
}

//-------------------------------------------------------------------------
// Stops
void Handler::stop ( void )
{
    if ( globals::publisher != nullptr )
        globals::publisher->unsubscribe( this );
}

//-------------------------------------------------------------------------
// Store the log of the given request
void Handler::store ( const std::string &  p_uid,
                      const std::string &  p_log )
{
    // Retrieve config
    decltype( m_enabled )       enabled;
    decltype( m_file_mode)      file_mode;
    decltype( m_directory_mode) directory_mode;
    decltype( m_folder )        folder;
    //
    {
        std::shared_lock< std::shared_mutex > lock( m_mutex_config );
        //
        enabled         = m_enabled;
        file_mode       = m_file_mode;
        directory_mode  = m_directory_mode;
        folder          = m_folder;
    }
    //
    if ( ! enabled )
        return;
    //
    // Build the fully qualified file name
    std::string  filename = build_filename( folder, p_uid );
    if ( filename.empty() )
        return;
    //
    // Create the file (and eventually needed directories)
    int hFile = open_with_path( filename,
                                O_CREAT | O_WRONLY | O_TRUNC,
                                file_mode,
                                directory_mode );
    if ( hFile < 0 )
        return;
    //
    if ( write( hFile, p_log.c_str(), p_log.length() ) !=
         static_cast< ssize_t >( p_log.length()) ) {
        ;  // ignore error
    }
    //
    close( hFile );
}

//-------------------------------------------------------------------------
// If logger is in quiet mode
// Access is not locked because it returns a bool: worst case
// the previous value is returned (it cannot be a corrupted value)
bool Handler::quiet ( void )
{
    return m_quiet;
};

//-------------------------------------------------------------------------
void Handler::bus_event ( const bus::Message &  p_message )
{
    switch ( p_message.type )
    {
        case masnae::bus::Message::config_updated:
            update_config();
            break;
        default:
            break;
    }
}

//-------------------------------------------------------------------------
// Build log file name with full path
std::string Handler::build_filename ( const std::string &  p_folder,
                                      const std::string &  p_uid )
{
    std::time_t  reference = time( nullptr );
    std::tm      local;
    char         filename[ 64 ];
    //
    // Get the date and time
    if ( localtime_r( & reference, & local ) == nullptr )
        return "";
    //
    if ( strftime( filename,
                   sizeof( filename ),
                   "%Y/%m-%d/%H-%M/",
                   & local ) == 0 )  // overflow
        return "";
    //
    return p_folder + filename + p_uid + ".txt";
}

//-------------------------------------------------------------------------
// Reads configuration from global config object.
bool Handler::update_config ( void )
{
    decltype( m_enabled )        new_enabled;
    decltype( m_file_mode )      new_file_mode;    
    decltype( m_directory_mode ) new_directory_mode;    
    decltype( m_folder )         new_folder;    
    decltype( m_quiet )          new_quiet;
    //
    new_enabled        = globals::config_handler->get_bool  ( "/logger/enabled",         false );
    new_file_mode      = globals::config_handler->get_int64 ( "/logger/file_mode",       0640 );
    new_directory_mode = globals::config_handler->get_int64 ( "/logger/directory_mode",  0750 );
    new_folder         = globals::config_handler->get_string( "/logger/folder",          m_folder_default );
    new_quiet          = globals::config_handler->get_bool  ( "/logger/quiet",           false );
    //
    if ( new_enabled )
    {
        // Check that the folder is writable
        if ( access( new_folder.c_str(), R_OK | W_OK | X_OK ) != 0 ) {
            masnae::service_error_log( "Logger: folder is not writable" );
            return false;
        }
    }
    //
    std::lock_guard<std::shared_mutex>  lock( m_mutex_config );
    //
    m_enabled         = new_enabled;
    m_file_mode       = new_file_mode;
    m_directory_mode  = new_directory_mode;
    m_folder          = new_folder;
    m_quiet           = new_quiet;
    //
    return true;
}

} // namespace logger
} // namespace masnae
