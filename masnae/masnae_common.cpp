// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

// Copyright 2018 Pierre del Perugia
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <ctime>
#include <sstream>

#include <adawat/adawat_common.h>

#include "masnae_common.h"
#include "masnae_misc.h"

namespace masnae {

//-------------------------------------------------------------------------
// By default the result is successful
// The content-type is not set here: since Fastcpipp_Params is a multimap,
// it would be necessary in add_http_header to check if it is already present
// when adding it. Easier to do it at the last moment (in send_header)
ProcessingResponse::ProcessingResponse ( const std::string &  p_id /* = "" */ )
{
    reset( p_id );
}

//-------------------------------------------------------------------------
// Reset everything. If p_id is not set, keep the existing id
void ProcessingResponse::reset ( const std::string &  p_id /* = "" */ )
{
    reset_response();
    //
    m_error_message.erase();
    m_log_message.erase();
    //
    if ( ! p_id.empty() )
        m_id = p_id;
}

//-------------------------------------------------------------------------
// Reset the HTTP response part
void ProcessingResponse::reset_response ( void )
{
    m_http_status   = "200 Ok";
    m_headers.clear();
    //
    m_raw_response  = false;
    //
    m_status        = "success";
    m_data          = "null";
    m_message.erase();
    m_reason.erase();
    //
    m_body.clear();
    //
    m_log_disable = false;
}

//-------------------------------------------------------------------------
// Set the body of the response...

// ... JResp

void ProcessingResponse::set_jresp_success ( const std::string &  p_data )
{
    m_status = "success";
    m_data   = p_data;
}

void ProcessingResponse::set_jresp_success ( std::string &&  p_data )
{
    m_status = "success";
    m_data   = std::move( p_data );
}

void ProcessingResponse::set_jresp_error ( const std::string &  p_message )
{
    m_status  = "error";
    m_message = p_message;
}

void ProcessingResponse::set_jresp_failed ( void )
{
    m_status = "failed";
}

void ProcessingResponse::set_jresp_rejected ( const std::string &  p_reason,
                                              const std::string &  p_data )
{
    m_status = "rejected";
    m_reason = p_reason;
    m_data   = p_data;
}

void ProcessingResponse::set_jresp_continue ( const std::string &  p_data )
{
    m_status = "continue";
    m_data   = p_data;
}

// ... Raw ( add_http_header( "Content-Type", "..."); should be called )

void ProcessingResponse::set_raw ( const Data &  p_body )
{
    m_raw_response = true;
    m_body         = p_body ;
}

void ProcessingResponse::set_raw ( Data &&  p_body )
{
    m_raw_response = true;
    m_body         = std::move( p_body );
}

void ProcessingResponse::set_raw ( const std::string &  p_body )
{
    m_raw_response = true;
    adawat::set_data( m_body, p_body );
}

//-------------------------------------------------------------------------
// Add a header in the response to be sent
void ProcessingResponse::add_http_header ( const std::string &  p_header,
                                           const std::string &  p_value )
{
    m_headers.insert( Fastcpipp_Params::value_type(p_header, p_value) );
}

//-------------------------------------------------------------------------
// Add an error message
void ProcessingResponse::add_error ( const std::string &  p_message )
{
    if ( m_error_message.length() >= 1000000 )  // overflow protection
        return;
    //
    m_error_message += p_message + "\n";
}

//-------------------------------------------------------------------------
// Add a time stamped log line
void ProcessingResponse::add_log ( log_level p_level, const std::string &  p_message )
{
    if ( m_log_message.length() >= 1000000 )  // overflow protection
        return;
    //
    // Add a time stamp like 09:26:22.949494
    struct timespec  ts;
    std::tm          local;
    //
    if ( timespec_get( & ts, TIME_UTC )       != 0        &&
         localtime_r ( & ts.tv_sec, & local ) != nullptr )
    {
        char text[ 32 ];
        //
        snprintf( text, sizeof( text ),
                  "%02d:%02d:%02d.%06ld ",
                  local.tm_hour, 
                  local.tm_min, 
                  local.tm_sec, 
                  ts.tv_nsec / 1000 );
        //
        m_log_message += text;
    }
    //
    m_log_message += static_cast< char >( p_level ) + ( " " + ( p_message + "\n" ) );
}

//-------------------------------------------------------------------------
// Do not store log for this request
void ProcessingResponse::disable_log ( void )
{
    m_log_disable = true;
}

//-------------------------------------------------------------------------
// Format and send the headers
void ProcessingResponse::send_headers ( std::ostream & p_out )
{
    // Send back the response id
    if ( ! m_id.empty() )
        add_http_header( "X-Response-ID", m_id);
    //
    // By default, it is a JResp answer
    if ( ! m_raw_response )
        add_http_header( "Content-Type", "application/json");
    //
    // Response status
    p_out << "Status: " + m_http_status + "\r\n";
    //
    // Headers
    for ( const auto& header: m_headers )
        p_out << header.first + ": " + header.second + "\r\n";
    //
    p_out << "\r\n"; // headers / body separator
}

//-------------------------------------------------------------------------
// Format and send JResp answer
void ProcessingResponse::send_jresp ( std::ostream & p_out )
{
    if ( m_status == "success" || m_status == "continue" )
        p_out << "{\"status\":\"" << m_status << "\""               <<
                 ",\"data\":"     << m_data                         <<
                 "}";
    else if ( m_status == "rejected" )
        p_out << "{\"status\":\"" << m_status << "\""               <<
                 ",\"reason\":"   << json_quote_string( m_reason )  <<
                 ",\"data\":"     << m_data                         <<
                 "}";
    else if ( m_status == "error" )
        p_out << "{\"status\":\"" << m_status << "\""               <<
                 ",\"message\":"  << json_quote_string( m_message ) <<
                 "}";
    else if ( m_status == "failed" )
        p_out << "{\"status\":\"" << m_status << "\""               <<
                 "}";
}

} // namespace masnae
