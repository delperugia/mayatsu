// Copyright 2018 Pierre del Perugia
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

//
// Table cache manager - cache data of SQL queries
//

#ifndef masnae_ws_caches_h
#define masnae_ws_caches_h

#include <shared_mutex>
#include <string>

#include "masnae_common.h"
#include "masnae_request_handler.h"
#include "masnae_mysql.h"

namespace masnae {
namespace ws_caches {

//-------------------------------------------------------------------------
// Cache definition

//-------------------------------------------------------------------------
// A class describing a database cache handled by Masnae
class Cache
{
    public:
        Cache( const std::string &  p_sql_query,
               const std::string &  p_key_name );
        //
        // Returns the database rows corresponding to the requested key
        bool        find    ( const std::string &    p_key,
                              SingleKeyValue_List &  p_rows );
        //
        // Returns the database row corresponding to the requested key in a cache
        // Returns false if the key is not unique (several database rows matching)
        bool        find    ( const std::string &    p_key,
                              SingleKey_Value &      p_row );
        //
        // Execute the given command
        // p_command is one of: reload
        void        execute ( const std::string &    p_command,
                              ProcessingResponse &   p_result );
        //
        // Load cache - returns a db result
        unsigned    load    ( void );
    private:
        std::string                 m_sql_query;    // SQL request to execute to retrieve data to cache
        std::string                 m_key_name;     // name of the database column to use as key
        //
        std::shared_mutex                               m_mutex_cache;
        std::multimap< std::string, SingleKey_Value >   m_cache;        // db rows, indexed by key
};

//-------------------------------------------------------------------------
// Class managing caches
class Handler : public bus::Subscriber
{
    public:
         Handler() = default ;
        ~Handler();
        //
        bool            start              ( void );
        void            stop               ( void );
        //
        // Returns module information as a JSON string
        std::string     specific_info      ( void );
        //
        // Execute the /mgt/caches command
        void            get_caches         ( ProcessingResponse &   p_result );
        //
        // Execute the given command on the requested cache
        void            execute            ( const std::string &    p_cache,
                                             const std::string &    p_command,
                                             ProcessingResponse &   p_result );
        //
        // Returns the database rows corresponding to the requested key in a cache
        bool            find               ( const std::string &    p_cache,
                                             const std::string &    p_key,
                                             SingleKeyValue_List &  p_rows );
        //
        // Returns the database row corresponding to the requested key in a cache
        // Returns false if the key is not unique (several database rows matching)
        bool            find               ( const std::string &    p_cache,
                                             const std::string &    p_key,
                                             SingleKey_Value &      p_row );
        //
        // Add a cache handling. p_sql_query contains the query whose data
        // must be cached. p_key_name is the field name in the results to use
        // as index for lookup, before starting
        void            add_cache          ( const std::string &    p_name,
                                             const std::string &    p_sql_query,
                                             const std::string &    p_key_name );
        //
        // Called by the bus with a new message
        void            bus_event           ( const bus::Message &  p_message );
        //
        // Disable object copy
        Handler                 ( const Handler & ) = delete;
        void operator=          ( const Handler & ) = delete;
        //
    private:
        //
        bool                             m_running = false;  // started
        std::map< std::string, Cache *>  m_caches;           // list of managed caches, indexed by their name
        //
        // Reload all caches
        unsigned    load_all_caches ( void );
};

} // namespace ws_caches
} // namespace masnae

#endif
