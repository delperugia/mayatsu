// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

// Copyright 2018 Pierre del Perugia
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <atomic>
#include <curl/curl.h>
#include <fastcgi++/log.hpp>
#include <iostream>
#include <list>
#include <rapidjson/pointer.h>
#include <sstream>
#include <stdio.h>
#include <adawat/adawat_string.h>
#include <adawat/adawat_threading_pool.hpp>

#include "masnae_config.h"
#include "masnae_global.h"
#include "masnae_http.h"
#include "masnae_logger.h"
#include "masnae_manager.hpp"
#include "masnae_misc.h"
#include "masnae_request_handler.h"
#include "masnae_simulator.h"
#include "masnae_ws_tables.h"

using namespace std::literals::chrono_literals;
using namespace masnae;

//-------------------------------------------------------------------------
/*
    The following table must exist, and the database configured
    accordingly in masnae_config_validate.json:
    
    sudo apt-get install mariadb-server
    sudo mysql_secure_installation
    sudo mariadb
        create user 'masnae'@'localhost' identified by '123456';
        grant all privileges on masnae.* to 'masnae'@'localhost';
        flush privileges;

        create schema masnae;
        use masnae;
        create table tbl ( id      int not null auto_increment,
                           name    varchar(64),
                           age     int(10) unsigned,
                           adr     varchar(128),
                           bd      datetime,
                           primary key (id) );
        insert into tbl (name,age,adr,bd) values
                        ('alpha',1,'un'  ,'2012-12-31T11:30:45'),
                        ('beta' ,2,'deux','2007-05-23T09:15:28');
*/

class dm_tbl : public ws_tables::Definition
{
    public:
        dm_tbl() :
            ws_tables::Definition( "tbl" )
        {
            m_fields = {
                // name     type  ins   upt   req    def.ins                  def.upt
                { "name", { 'S', true, false, true,  nullptr,                 nullptr } },
                { "age",  { 'U', true, true,  false, nullptr,                 nullptr } },
                { "adr",  { 'S', true, true,  true,  nullptr,                 nullptr } },
                { "bd",   { 'S', true, true,  false, "'1999-09-09T12:00:00'", "'1999-09-13T12:00:00'" } }
            };
        }
};

//-------------------------------------------------------------------------
class rh_cfg : public RequestHandler
{
    public:
        rh_cfg () : RequestHandler( 1024*1024 ) {}
        //
        virtual
        ~rh_cfg () {}
    //
    protected:
        bool        check_security  ( const std::string &         p_module,
                                      Fastcpipp_Path &            p_document,
                                      ProcessingResponse &        p_result )
                    {
                        return true;
                    }
        //
        bool        process_request ( const std::string &         p_module,
                                      Fastcpipp_Path &            p_document,
                                      ProcessingResponse &        p_result )
                    {
                        // Implements a webservice expecting two parameters a and b
                        // and returning their concatenation
                        if ( p_module == "ws" &&
                             p_document.size() == 1 && p_document[0] == "fct" )
                        {
                            auto a = environment().posts.find( "a" );
                            auto b = environment().posts.find( "b" );
                            //
                            if ( a != environment().posts.end() && 
                                 b != environment().posts.end() )
                            {
                                p_result.set_jresp_success( a->second + b->second );
                            }
                            //
                            return true;
                        }
                        //
                        return false;
                    }
        //
        void        store_log       ( const std::string &         p_module,
                                      const Fastcpipp_Path &      p_document,
                                      const ProcessingResponse &  p_result )
                    {
                    }
        //
        std::string specific_info   ( void )
                    {
                        return "{\"x\":0}";
                    }
};

//-------------------------------------------------------------------------
masnae::bus::Publisher              g_Bus;
masnae::config::Handler             g_ConfigHandler;
masnae::logger::Handler             g_LoggerHandler;
masnae::mysql::Handler              g_MySQLHandler;
masnae::ws_tables::Handler          g_WsTablesHandler;
masnae::ws_caches::Handler          g_WsCachesHandler;
masnae::http::Handler               g_HttpHandler;
masnae::request::Manager<rh_cfg>    g_Manager;

bool                                g_Verbose = false;

//-------------------------------------------------------------------------
void start (const char * p_cszModule)
{
    printf("%s:", p_cszModule);
}

//-------------------------------------------------------------------------
bool check (const char * p_cszModule, unsigned p_uSection, unsigned p_uTest, bool p_bCondition)
{
    if ( g_Verbose || ! p_bCondition )
        printf("\n   %s.%02u.%02u: %s", p_cszModule, p_uSection, p_uTest, p_bCondition ? "ok" : "error");
    //
    return p_bCondition;
}

//-------------------------------------------------------------------------
bool stop (const char * p_cszModule, bool p_bCondition)
{
    if ( g_Verbose || ! p_bCondition )
        printf("\n%s: %s\n", p_cszModule, p_bCondition ? "ok" : "error");
    else
        printf(" ok\n");
    //
    return p_bCondition;
}

//-------------------------------------------------------------------------
bool validate_misc (void)
{
    std::string             sig;
    masnae::SingleKey_Value headers, payload;
    bool                    ok = true;
    //
    ok &= check( "misc", 1, 1, jws_build( { { "typ", "JWT"    } } ,
                                          { { "iss", "urania" },
                                            { "sub", "test"   } },
                                          "secret",
                                          sig ) &&
                               sig == "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJ1cmFuaWEiLCJzdWIiOiJ0ZXN0In0.3yVODmVoDbh8h7SsqsWsaRnN4VH7ga7ajyKPa0lOIYg" );
    //
    ok &= check( "misc", 2, 1, jws_parse( "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJ1cmFuaWEiLCJzdWIiOiJ0ZXN0In0.3yVODmVoDbh8h7SsqsWsaRnN4VH7ga7ajyKPa0lOIYg",
                                          "secret",
                                          headers,
                                          payload ) );
    ok &= check( "misc", 2, 2, headers[ "typ" ] == "JWT" &&
                               payload[ "iss" ] == "urania" && payload[ "sub" ] == "test" );
    //
    return ok;
}

//-------------------------------------------------------------------------
    bool is_valid_json( const std::string & p_json )
    {
        rapidjson::Document  json;
        //
        // Parse it: it is a JSon object
        json.Parse< rapidjson::kParseFullPrecisionFlag |
                             rapidjson::kParseCommentsFlag >( p_json.c_str() );
        //
        if ( json.HasParseError() || ! json.IsObject() )
            return false;
        //
        return true;
    }

bool validate_json (void)
{
    bool ok = true;
    //
    ok &= check( "json", 1, 1, json_quote_string("abcde")                   == "\"abcde\"" );
    ok &= check( "json", 1, 2, json_quote_string("He is called \"Johnny\"") == "\"He is called \\\"Johnny\\\"\"" );
    //
    ok &= check( "json", 2, 1, json_convert((std::vector<std::string>){})   == "[]" );
    ok &= check( "json", 2, 2, json_convert({"z"})                          == "[\"z\"]" );
    ok &= check( "json", 2, 3, json_convert({"a","b","c"})                  == "[\"a\",\"b\",\"c\"]" );
    //
    ok &= check( "json", 3, 1, json_convert((std::map<std::string,std::string>)
                                            {})                             == "{}" );
    ok &= check( "json", 3, 2, json_convert((std::map<std::string,std::string>)
                                            {{"z","9"}})                    == "{\"z\":\"9\"}" );
    ok &= check( "json", 3, 3, json_convert((std::map<std::string,std::string>)
                                            {{"a","1"},{"b","2"}})          == "{\"a\":\"1\",\"b\":\"2\"}" );
    //
    ok &= check( "json", 4, 1, is_valid_json( json_convert( (std::map<std::string,std::string>)
                                                            {{"a","1"},{"b","2"}}
                                                            )));
    //
    return ok;
}

//-------------------------------------------------------------------------
    std::string remove_response_id_header ( std::string p_text )
    {
        size_t start = p_text.find( "X-Response-ID:" );
        //
        if ( start != p_text.npos ) {
            size_t end = p_text.find( "\r\n", start );
            //
            p_text.erase( start, end - start + 2 );
        }
        //
        return p_text;
    }

bool validate_common (void)
{
    bool ok = true;
    //
    //-------
    {
        ProcessingResponse   R( "test" );
        std::ostringstream   os;
        //
        R.send_headers( os );
        R.send_jresp  ( os );
        //
        ok &= check( "common", 1, 1, remove_response_id_header( os.str() ) == 
            "Status: 200 Ok\r\n"
            "Content-Type: application/json\r\n"
            "\r\n"
            "{\"status\":\"success\",\"data\":null}" );
    }
    //
    //-------
    {
        ProcessingResponse   R( "test" );
        std::ostringstream   os;
        //
        R.add_http_header  ( "X", "1"  );
        R.set_jresp_success( "[1,2,3]" );
        //
        R.send_headers( os );
        R.send_jresp  ( os );
        //
        ok &= check( "common", 1, 2, remove_response_id_header( os.str() ) == 
            "Status: 200 Ok\r\n"
            "Content-Type: application/json\r\n"
            "X: 1\r\n"
            "\r\n"
            "{\"status\":\"success\",\"data\":[1,2,3]}" );
    }
    //
    //-------
    {
        ProcessingResponse   R( "test" );
        std::ostringstream   os;
        //
        R.add_http_header ( "X", "1" );
        R.set_jresp_failed();
        //
        R.send_headers( os );
        R.send_jresp  ( os );
        //
        ok &= check( "common", 1, 3, remove_response_id_header( os.str() ) == 
            "Status: 200 Ok\r\n"
            "Content-Type: application/json\r\n"
            "X: 1\r\n"
            "\r\n"
            "{\"status\":\"failed\"}" );
    }
    //
    //-------
    {
        ProcessingResponse   R( "test" );
        std::ostringstream   os;
        //
        R.set_jresp_error ( "a message" );
        //
        R.send_headers( os );
        R.send_jresp  ( os );
        //
        ok &= check( "common", 1, 4, remove_response_id_header( os.str() ) == 
            "Status: 200 Ok\r\n"
            "Content-Type: application/json\r\n"
            "\r\n"
            "{\"status\":\"error\",\"message\":\"a message\"}" );
    }
    //
    //-------
    {
        ProcessingResponse   R( "test" );
        std::ostringstream   os;
        //
        R.set_jresp_rejected ( "exists", json_quote_string("msisdn") );
        //
        R.send_headers( os );
        R.send_jresp  ( os );
        //
        ok &= check( "common", 1, 5, remove_response_id_header( os.str() ) == 
            "Status: 200 Ok\r\n"
            "Content-Type: application/json\r\n"
            "\r\n"
            "{\"status\":\"rejected\",\"reason\":\"exists\",\"data\":\"msisdn\"}" );
    }
    //
    //-------
    {
        rh_cfg              RH;
        ProcessingResponse  R( "test" );
        std::ostringstream  os;
        //
        if ( exec_url( "/ws/fct?b=2&a=4", RH, R ) )
        {
            R.send_headers( os );
            R.send_jresp  ( os );
            //
            ok &= check( "common", 1, 6, remove_response_id_header( os.str() ) == 
                "Status: 200 Ok\r\n"
                "Content-Type: application/json\r\n"
                "\r\n"
                "{\"status\":\"success\",\"data\":42}" );
        }
        else
        {
            ok &= false;
        }
    }
    //
    return ok;
}

//-------------------------------------------------------------------------
bool validate_config (void)
{
    bool ok = true;
    //
    ok &= check( "config", 2, 1, g_ConfigHandler.get_int64 ( "/int"                   ) == 123456   );
    ok &= check( "config", 2, 2, g_ConfigHandler.get_string( "/string"                ) == "abcdef" );
    ok &= check( "config", 2, 3, g_ConfigHandler.get_bool  ( "/bool"                  ) == true     );
    ok &= check( "config", 2, 4, g_ConfigHandler.get_string( "/object/property"       ) == "value"  );
    //                                                                           
    // Check that previous queries were not destructive                   
    ok &= check( "config", 3, 1, g_ConfigHandler.get_int64 ( "/int"                   ) == 123456   );
    ok &= check( "config", 3, 2, g_ConfigHandler.get_string( "/string"                ) == "abcdef" );
    ok &= check( "config", 3, 3, g_ConfigHandler.get_bool  ( "/bool"                  ) == true     );
    ok &= check( "config", 3, 4, g_ConfigHandler.get_string( "/object/property"       ) == "value"  );
    //
    // Invalid keys
    ok &= check( "config", 4, 1, g_ConfigHandler.get_string( "/_tring",          "1_" ) == "1_"     );
    ok &= check( "config", 4, 2, g_ConfigHandler.get_string( "/object/_roperty", "2_" ) == "2_"     );
    ok &= check( "config", 4, 3, g_ConfigHandler.get_int64 ( "/string",          3    ) == 3        );
    ok &= check( "config", 4, 4, g_ConfigHandler.get_string( "/int",             "4_" ) == "4_"     );
    ok &= check( "config", 4, 5, g_ConfigHandler.get_int64 ( "/bool",            5    ) == 5        );
    ok &= check( "config", 4, 6, g_ConfigHandler.get_bool  ( "/string",          true ) == true     );
    //
    // Array
    ok &= check( "config", 5, 1, g_ConfigHandler.get_size( "/array") == 3 );
    ok &= check( "config", 5, 1, g_ConfigHandler.get_size( "/int")   == 0 );
    ok &= check( "config", 5, 2, g_ConfigHandler.get_int64(  "/array/0/v" ) == 1  );
    ok &= check( "config", 5, 3, g_ConfigHandler.get_int64(  "/array/1/v" ) == 2  );
    ok &= check( "config", 5, 4, g_ConfigHandler.get_int64(  "/array/2/v" ) == 3  );
    ok &= check( "config", 5, 5, g_ConfigHandler.get_string( "/array/0/n" ) == "f1" );
    ok &= check( "config", 5, 6, g_ConfigHandler.get_string( "/array/1/n" ) == "f2" );
    ok &= check( "config", 5, 7, g_ConfigHandler.get_string( "/array/2/n" ) == "f3" );
    ok &= check( "config", 5, 7, g_ConfigHandler.get_string( "/array/3/v" ) == "" );
    //
    // Array
    KeyList A, R;
    //
    A = g_ConfigHandler.get_members( "/object");       R = {"property"}; ok &= check( "config", 6, 1, A == R );
    A = g_ConfigHandler.get_members( "/http/modules");                   ok &= check( "config", 6, 2, A.size() == 8 );
    //
    return ok;
}

//-------------------------------------------------------------------------
bool validate_http ( void )
{
    bool ok = true;
    //
    rapidjson::Document     doc;
    rapidjson::Value *      v;
    //
    std::string    ct;
    Data           b;
    std::string    s;
    //
    //--------------
    ok &= check( "http", 2, 1, g_HttpHandler.request1("adr_html",{},{},{},ct,b)     == 200 &&
                               adawat::str_starts_with(ct, "text/html") );
    ok &= check( "http", 2, 2, g_HttpHandler.request1("adr_invalid",{},{},{},ct,b)  == CURLE_COULDNT_RESOLVE_HOST );
    ok &= check( "http", 2, 3, g_HttpHandler.request1("adr_notfound",{},{},{},ct,b) == 404 );
    //
    //--------------
    ok &= check( "http", 3, 1, g_HttpHandler.request1("adr_post", {},
                                             { {"eleven", "126099586487"},
                                               {"_a",     "a b"},
                                               {"a&b",    " &"} },
                                             {},
                                             ct, b)==200 && ct=="application/json" );
    //
    s.assign ( b.begin(), b.end() );
    //
    doc.Parse< rapidjson::kParseFullPrecisionFlag | rapidjson::kParseCommentsFlag >( s.c_str() );
    ok &= check( "http", 3, 2, !doc.HasParseError() && doc.IsObject() );
    v = rapidjson::Pointer( "/form/_a" ).Get( doc );
    ok &= check( "http", 3, 3, v != nullptr && v->IsString() && v->GetString()==std::string("a b") );
    v = rapidjson::Pointer( "/form/a&b" ).Get( doc );
    ok &= check( "http", 3, 4, v != nullptr && v->IsString() && v->GetString()==std::string(" &") );
    v = rapidjson::Pointer( "/form/eleven" ).Get( doc );
    ok &= check( "http", 3, 5, v != nullptr && v->IsString() && v->GetString()==std::string("126099586487") );
    //
    //--------------
    Data body = { 'o','n','e','=','4','2','&','t','w','o','=','A','%','2','6','+','B' };
    ok &= check( "http", 4, 1, g_HttpHandler.request2("adr_post",
                                             { { "Content-Type", "application/x-www-form-urlencoded" } },
                                             body,
                                             ct, b)==200 && ct=="application/json" );
    //
    s.assign (b.begin(), b.end());
    //
    doc.Parse< rapidjson::kParseFullPrecisionFlag | rapidjson::kParseCommentsFlag >( s.c_str() );
    ok &= check( "http", 4, 2, !doc.HasParseError() && doc.IsObject() );
    v = rapidjson::Pointer( "/form/one" ).Get( doc );
    ok &= check( "http", 4, 3, v != nullptr && v->IsString() && v->GetString()==std::string("42") );
    v = rapidjson::Pointer( "/form/two" ).Get( doc );
    ok &= check( "http", 4, 4, v != nullptr && v->IsString() && v->GetString()==std::string("A& B") );
    //
    //--------------
    ok &= check( "http", 5, 1, g_HttpHandler.request1("adr_notfound",{},{},{},ct,b) == 404 );
    ok &= check( "http", 5, 2, g_HttpHandler.request1("adr_ssl_self",{},{},{},ct,b) == CURLE_SSL_CACERT );
    ok &= check( "http", 5, 3, g_HttpHandler.request1("adr_ssl_exp",{},{},{},ct,b)  == CURLE_SSL_CACERT );
    ok &= check( "http", 5, 4, g_HttpHandler.request1("adr_ssl_bad",{},{},{},ct,b)  == CURLE_PEER_FAILED_VERIFICATION );
    //
    //--------------
    ok &= check( "http", 6, 1, g_HttpHandler.request1("adr_rest/users/1",{},{},{},ct,b) == 200 );
    ok &= check( "http", 6, 2, ( g_HttpHandler.request_json("adr_rest/users/1",{},{},{},doc,s),
                                 !doc.HasParseError() && doc.IsObject() &&
                                 ( v = rapidjson::Pointer( "/data/username" ).Get( doc ),
                                   v != nullptr && v->IsString() && v->GetString()==std::string("Bret") ) ) );
    //
    //--------------
    ok &= check( "http", 7, 1,   g_HttpHandler.is_json( "application/json" ) );
    ok &= check( "http", 7, 2,   g_HttpHandler.is_json( "application/json;" ) );
    ok &= check( "http", 7, 3,   g_HttpHandler.is_json( "application/json; charset=utf-8" ) );
    ok &= check( "http", 7, 4, ! g_HttpHandler.is_json( "application/js" ) );
    ok &= check( "http", 7, 5, ! g_HttpHandler.is_json( "application/js;" ) );
    ok &= check( "http", 7, 6, ! g_HttpHandler.is_json( "application/jsoni" ) );
    ok &= check( "http", 7, 7, ! g_HttpHandler.is_json( "application/jsoni;" ) );
    //
    return ok;
}

//-------------------------------------------------------------------------
bool validate_ws_tables (void)
{
    bool        ok = true;
    unsigned    count;
    std::string work;
    dm_tbl      D;
    //
    SingleKey_Value escape = {
        { "alpha",              "alpha"                 },
        { "o'connor",           "o\\'connor"            },
        { "a\\b\\c",            "a\\\\b\\\\c"           },
        { "john \"p\" john",    "john \\\"p\\\" john"   },
        { "a1\\",               "a1\\\\"                },
        { "'a2",                "\\'a2"                 },
        { "\\a3\"",             "\\\\a3\\\""            },
        { "'é≡",                "\\'é≡"                 }
    };
    //
    SingleKey_Value filter = {
        { "{ \"status\": \"D\" }",                                                                      "`status` = 'D'" },
        { "{ \"status\": { \"$in\": [ \"A\", \"D\" ] } }",                                              "`status` IN ('A','D')" },
        { "{ \"status\": \"A\", \"qty\": { \"$lt\": 30 } } ",                                           "(`status` = 'A' AND `qty` < 30)" },
        { "{ \"status\": null }",                                                                       "`status` IS NULL" },
        { "{ \"status\": { \"$ne\": null } }",                                                          "`status` IS NOT NULL" },
        { "{ \"$or\": [ { \"status\": \"A\" }, { \"qty\": { \"$lt\": 30 } } ] } ",                      "(`status` = 'A' OR `qty` < 30)" },
        { "{ \"status\": \"A\", \"$or\": [ { \"qty\": { \"$lt\": 30 } }, { \"item\": \"x\" } ] } ",     "(`status` = 'A' AND (`qty` < 30 OR `item` = 'x'))" }
    };
    //
    count = 1;
    for ( const auto & text: escape )
         ok &= check( "ws_tables", 1, count++, mysql::sql_escape( text.first ) == text.second );
    //
    ws_tables::jsonsql::filter F;
    count = 1;
    for ( const auto & text: filter )
         ok &= check( "ws_tables", 2, count++, F.convert( text.first, work ) && text.second==work );
    //
    try {
        ok &= check( "ws_tables", 3, 1, D.build_sql_insert( "{ \"name\": \"joe\",\"age\": 42,\"adr\": \"road\" }" ) ==
                     "INSERT INTO tbl ( `adr`,`age`,`name`,`bd`) VALUES ( 'road',42,'joe','1999-09-09T12:00:00')" );
        ok &= check( "ws_tables", 3, 2, D.build_sql_update( "{ \"age\": 42 }", "{ \"age\": null }", "", "" ) ==
                     "UPDATE tbl SET  `age`=42,`bd`='1999-09-13T12:00:00' WHERE `age` IS NULL LIMIT 1000" );
        ok &= check( "ws_tables", 3, 3, D.build_sql_select( "[ \"name\",\"age\" ]", "{ \"name\": null }", "{\"name\":1,\"age\":-1}", "[5,10]" ) ==
                     "SELECT  `name`,`age` FROM tbl WHERE `name` IS NULL ORDER BY `name`,`age` DESC LIMIT 5,5" );
        ok &= check( "ws_tables", 3, 4, D.build_sql_delete( "{ \"name\": null }", "{\"name\":1,\"age\":-1}", "" ) ==
                     "DELETE FROM tbl WHERE `name` IS NULL ORDER BY `name`,`age` DESC LIMIT 1000" );
    } catch ( const std::exception & e ) {
        ok &= check( "ws_tables", 3, 5, false );  // see e.what() on error
    }
    //
    //----
    try {
        D.build_sql_insert( "{ \"name\": null,\"age\": \"42\" }" );
        ok &= check( "ws_tables", 4, 1, false );
    } catch ( const std::exception & e ) {
        ok &= check( "ws_tables", 4, 1, strcmp("Fields: invalid type for 'age'",e.what())==0 );
    }
    //
    try {
        D.build_sql_insert( "{ \"name\": null,\"ag_e\": 42 }" );
        ok &= check( "ws_tables", 4, 2, false );
    } catch ( const std::exception & e ) {
        ok &= check( "ws_tables", 4, 2, strcmp("Fields: unknown field 'ag_e'",e.what())==0 );
    }
    //
    try {
        D.build_sql_insert( "{ \"name\": null,\"age\": 42,\"adr\": \"road\" }" );
        ok &= check( "ws_tables", 4, 3, false );
    } catch ( const std::exception & e ) {
        ok &= check( "ws_tables", 4, 3, strcmp("Fields: 'name' must be set",e.what())==0 );
    }
    //
    try {
        D.build_sql_update( "{ \"name\": null,\"age\": 42 }", "{ \"nam_e\": null }", "", "" );
        ok &= check( "ws_tables", 4, 4, false );
    } catch ( const std::exception & e ) {
        ok &= check( "ws_tables", 4, 4, strcmp("Fields: 'name' invalid for update",e.what())==0 );
    }
    //
    try {
        D.build_sql_update( "{ \"age\": 42 }", "{ \"nam_e\": null }", "", "" );
        ok &= check( "ws_tables", 4, 5, false );
    } catch ( const std::exception & e ) {
        ok &= check( "ws_tables", 4, 5, strcmp("Filter: unknown field 'nam_e'",e.what())==0 );
    }
    //
    try {
        D.build_sql_update( "{ \"age\": 42 }", "", "{\"na_e\":1,\"age\":-1}", "" );
        ok &= check( "ws_tables", 4, 6, false );
    } catch ( const std::exception & e ) {
        ok &= check( "ws_tables", 4, 6, strcmp("Order: unknown field name 'na_e'",e.what())==0 );
    }
    //
    try {
        D.build_sql_update( "{ \"adr\": \"\" }", "", "", "" );
        ok &= check( "ws_tables", 4, 7, false );
    } catch ( const std::exception & e ) {
        ok &= check( "ws_tables", 4, 7, strcmp("Fields: 'adr' must have a value",e.what())==0 );
    }
    //
    try {
        D.build_sql_delete( "", "", "[5,4]" );
        ok &= check( "ws_tables", 4, 8, false );
    } catch ( const std::exception & e ) {
        ok &= check( "ws_tables", 4, 8, strcmp("Range: invalid bounds",e.what())==0 );
    }
    //
    //------
    {
      std::ostringstream   os;
      ProcessingResponse   R( "test51" );
      //
      g_WsTablesHandler.execute( "tbl", "DELETE", "", "", "", "[5,4]", R);
      R.send_jresp  ( os );
      ok &= check( "ws_tables", 5, 1, os.str() == 
          "{\"status\":\"error\",\"message\":\"Range: invalid bounds\"}" );
    }
    //
    {
      std::ostringstream   os;
      ProcessingResponse   R( "test52" );
      //
      g_WsTablesHandler.execute( "xxx", "DELETE", "", "", "", "", R);
      R.send_jresp  ( os );
      ok &= check( "ws_tables", 5, 2, os.str() == 
          "{\"status\":\"error\",\"message\":\"Unknown table\"}" );
    }
    //
    {
      std::ostringstream   os;
      ProcessingResponse   R( "test53" );
      //
      g_WsTablesHandler.execute( "tbl", "update", "", "", "", "", R);
      R.send_jresp  ( os );
      ok &= check( "ws_tables", 5, 3, os.str() == 
          "{\"status\":\"error\",\"message\":\"Fields: no field specified\"}" );
    }
    //
    return ok;
}

//-------------------------------------------------------------------------
bool validate_mysql (void)
{
    bool                 ok = true, found;
    unsigned             r;
    size_t               affected;
    std::string          id;
    SingleKeyValue_List  rows;
    //
    //-------
    {
        mysql::Connection CnxW = g_MySQLHandler.get_connection( mysql::writer );
        //
        r = CnxW.do_insert( "insert into tbl (name,age,adr) values ('gamma',3,'trois')", id );
        ok &= check( "mysql", 1, 1, r == 0 );
        //
        r = CnxW.do_update( "update tbl set age=4 where name='gamma'", affected );
        ok &= check( "mysql", 1, 2, r == 0 && affected == 1 );
        //
        r = CnxW.do_select( "select * from tbl", rows );
        found = false;
        for ( auto & row: rows )
            if ( row["id"] == id )
                found = row["name"]=="gamma" && row["age"]=="4" && row["adr"]=="trois";
        ok &= check( "mysql", 1, 3, r == 0 && found );
        //
        r = CnxW.do_delete( "delete from tbl  where name='gamma'", affected );
        ok &= check( "mysql", 1, 4, r == 0 && affected == 1 );
        //
        //-------
        r = CnxW.start_transaction();
        ok &= check( "mysql", 2, 1, r == 0 );
        //
        r = CnxW.do_insert( "insert into tbl (name,age,adr) values ('delta',4,'quatre')", id );
        ok &= check( "mysql", 2, 2, r == 0 );
        //
        r = CnxW.commit_transaction();
        ok &= check( "mysql", 2, 3, r == 0 );
        //
        r = CnxW.do_delete( "delete from tbl where name='delta'", affected );
        ok &= check( "mysql", 2, 4, r == 0 && affected == 1 );
        //
        //-------
        r = CnxW.start_transaction();
        ok &= check( "mysql", 3, 1, r == 0 );
        //
        r = CnxW.do_insert( "insert into tbl (name,age,adr) values ('delta',4,'quatre')", id );
        ok &= check( "mysql", 3, 2, r == 0 );
        //
        r = CnxW.rollback_transaction();
        ok &= check( "mysql", 3, 3, r == 0 );
        //
        r = CnxW.do_select( "select * from tbl where name='delta'", rows );
        ok &= check( "mysql", 3, 4, r == 0 && rows.size() == 0);
        //
        //-------
        mysql::Connection CnxR = g_MySQLHandler.get_connection( mysql::reader );
        //
        r = CnxR.do_insert( "insert into tbl (name,age,adr) values ('gamma',3,'trois')", id );
        ok &= check( "mysql", 4, 1, r == 2 );
        //
        //------- db.max_size is 4
        mysql::Connection CnxW2 = g_MySQLHandler.get_connection( mysql::writer );
        ok &= check( "mysql", 5, 1,   CnxW2.is_valid() );
        //
        mysql::Connection CnxW3 = g_MySQLHandler.get_connection( mysql::writer );
        ok &= check( "mysql", 5, 2,   CnxW3.is_valid() );
        //
        mysql::Connection CnxW4 = g_MySQLHandler.get_connection( mysql::writer );
        ok &= check( "mysql", 5, 3,   CnxW4.is_valid() );
        //
        mysql::Connection CnxW5 = g_MySQLHandler.get_connection( mysql::writer );
        ok &= check( "mysql", 5, 4, ! CnxW5.is_valid() );
    }
    //
    //-------
    {
        mysql::Connection CnxW3 = g_MySQLHandler.get_connection( mysql::writer );
        ok &= check( "mysql", 6, 1, CnxW3.is_valid() );
        //
        r = CnxW3.do_select( "select * from tbl", rows );
        ok &= check( "mysql", 6, 2, r == 0 && rows.size() == 2 );
    }
    //
    //-------
    class W : public adawat::ThreadPool< bool * >
    {
        protected:
            void execute ( bool * & p_ok ) {
                mysql::Connection       Cnx = g_MySQLHandler.get_connection( mysql::reader );
                SingleKeyValue_List  rows;
                //
                for ( int i = 0; i < 5000 ; i++ ) {
                    unsigned r = Cnx.do_select(
                        adawat::str_sprintf(
                            i == 0 ?
                                "select sleep( 1 )" :
                                "select * from tbl where age < %u",
                            //
                            rand() % 4 // not use for sleep request
                        ),
                        rows
                    );
                    //
                    * p_ok &= r == 0;
                }
            }
    };
    //
    {
        W  w;
        //
        w.set_min_threads( 4 );
        w.set_max_threads( 4 );
        //
        w.push( & ok );
        w.push( & ok );
        w.push( & ok );
        w.push( & ok );
        //
        w.stop ( false );
        //
        ok &= check( "mysql", 7, 1, ok );
    }
    //
    return ok;
}

//-------------------------------------------------------------------------
bool validate_ws_caches (void)
{
    bool                 ok = true;
    SingleKeyValue_List  rows;
    ProcessingResponse   R;
    //
    //-------
    g_WsCachesHandler.execute( "name", "reload", R );
    ok &= check( "ws_caches", 1, 1, R.is_jresp_success() );
    //
    g_WsCachesHandler.execute( "age",  "reload", R );
    ok &= check( "ws_caches", 1, 2, R.is_jresp_success() );
    //
    //-------
    ok &= check( "ws_caches", 1, 3, g_WsCachesHandler.find( "name", "alpha", rows ) &&
                                   rows.size()==1 &&
                                   rows[0]["age"]=="1" && rows[0]["adr"]=="un" );
    //
    ok &= check( "ws_caches", 1, 4, g_WsCachesHandler.find( "age", "2", rows ) &&
                                   rows.size()==1 &&
                                   rows[0]["name"]=="beta" && rows[0]["adr"]=="deux" );
    //
    return ok;
}

//-------------------------------------------------------------------------
bool validate_info (void)
{
    bool                    ok = true;
    rapidjson::Document     doc;
    //
    doc.Parse< rapidjson::kParseFullPrecisionFlag | rapidjson::kParseCommentsFlag >(
        g_ConfigHandler.specific_info().c_str() );
    ok &= check( "info", 1, 1, !doc.HasParseError() && doc.IsObject() );
    //
    doc.Parse< rapidjson::kParseFullPrecisionFlag | rapidjson::kParseCommentsFlag >(
        g_MySQLHandler.specific_info().c_str() );
    ok &= check( "info", 1, 2, !doc.HasParseError() && doc.IsObject() );
    //
    doc.Parse< rapidjson::kParseFullPrecisionFlag | rapidjson::kParseCommentsFlag >(
        g_WsTablesHandler.specific_info().c_str() );
    ok &= check( "info", 1, 3, !doc.HasParseError() && doc.IsObject() );
    //
    doc.Parse< rapidjson::kParseFullPrecisionFlag | rapidjson::kParseCommentsFlag >(
        g_WsCachesHandler.specific_info().c_str() );
    ok &= check( "info", 1, 4, !doc.HasParseError() && doc.IsObject() );
    //
    doc.Parse< rapidjson::kParseFullPrecisionFlag | rapidjson::kParseCommentsFlag >(
        g_HttpHandler.specific_info().c_str() );
    ok &= check( "info", 1, 5, !doc.HasParseError() && doc.IsObject() );
    //
    doc.Parse< rapidjson::kParseFullPrecisionFlag | rapidjson::kParseCommentsFlag >(
        g_Manager.specific_info().c_str() );
    ok &= check( "info", 1, 6, !doc.HasParseError() && doc.IsObject() );
    //
    return ok;
}

//-------------------------------------------------------------------------
int main (int argc, char * argv [])
{
    if ( argc != 2 ) {
        printf( "syntax: %s <config file>\n", argv[0] );
        return -1;
    }
    //
    bool ok = true;
    //
    masnae::globals::init( "masnae",
                           & g_Bus,
                           & g_ConfigHandler,
                           & g_LoggerHandler,
                           & g_MySQLHandler,
                           & g_WsTablesHandler,
                           & g_WsCachesHandler,
                           & g_HttpHandler,
                           & g_Manager );
    //
    //-----
    dm_tbl  g_DbTblDm;
    //
    g_WsCachesHandler.add_cache( "name", "select * from tbl", "name" );
    g_WsCachesHandler.add_cache( "age",  "select * from tbl", "age"  );
    //
    //-----
    //
    if ( ! g_ConfigHandler.start( argv[1] ) ) {  // masnae_validate_config.json
        printf( "failed to load configuration file\n" );
        return 2;
    }
    //
    if ( ! g_LoggerHandler.start( "/tmp/" ) ) {
        printf( "failed to start HttpLogger\n" );
        return 2;
    }
    //
    if ( ! g_MySQLHandler.start() ) {
        printf( "failed to start DbHandler\n" );
        return 2;
    }
    //
    if ( ! g_WsTablesHandler.start() ) {
        printf( "failed to start DbTableHandler\n" );
        return 2;
    }
    //
    if ( ! g_WsCachesHandler.start() )
         {
        printf( "failed to start g_WsCachesHandler\n" );
        return 2;
    }
    //
    if ( ! g_HttpHandler.start() ) {
        printf( "failed to start HttpHandler\n" );
        return 2;
    }
    //
    start ("misc       ");
    ok &= stop ("misc",      validate_misc() );
    //
    start ("json       ");
    ok &= stop ("json",      validate_json() );
    //
    start ("common     ");
    ok &= stop ("common",    validate_common() );
    //
    start ("config     ");
    ok &= stop ("config",    validate_config() );
    //
    start ("http       ");
    ok &= stop ("http",      validate_http() );
    //
    start ("ws_tables  ");
    ok &= stop ("ws_tables", validate_ws_tables() );
    //
    start ("mysql      ");
    ok &= stop ("mysql",     validate_mysql() );
    //
    start ("ws_caches  ");
    ok &= stop ("ws_caches", validate_ws_caches() );
    //
    start ("info       ");
    ok &= stop ("info",      validate_info() );
    //
    printf ("-------\n");
    printf ("validation : %s\n", ok ? "ok" : "error");
    //
    g_HttpHandler.stop();
    g_WsCachesHandler.stop();
    g_WsTablesHandler.stop();
    g_MySQLHandler.stop();
    g_LoggerHandler.stop();
    g_ConfigHandler.stop();
    //
    exit  (ok ? 0 : 1);
    return ok ? 0 : 1;
}
