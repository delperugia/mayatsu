// Copyright 2018 Pierre del Perugia
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

//
// Config management - monitor and read configuration file
//

#ifndef masnae_config_h
#define masnae_config_h

#include <rapidjson/document.h>
#include <shared_mutex>
#include <string>
#include <vector>

#include "masnae_bus.h"
#include "masnae_monitor_files.h"

namespace masnae {
namespace config {

//-------------------------------------------------------------------------
class Handler : public  bus::Subscriber,
                private MonitorFiles
{
    public:
        Handler  ();
        ~Handler ();
        //
        // Start and stop monitoring on the given configuration file
        bool        start          ( const std::string &   p_config_file );
        void        stop           ( void );
        //
        // Return module information as a JSON string
        std::string specific_info  ( void );
        //
        // Retrieve from the configuration the requested key. p_key represents
        // the path to the key, e.g.: in
        //    { "settings": { "max": 5 }, "prefix": "f_" }
        // paths are:
        //    /settings/max   is 5
        //    /prefix         is "f_"
        //
        // Return p_default if the key is not found or does not have
        // the proper type
        bool        get_bool    ( const std::string &   p_key,
                                  bool                  p_default = false );
        int64_t     get_int64   ( const std::string &   p_key,
                                  int64_t               p_default = -1 );
        std::string get_string  ( const std::string &   p_key,
                                  const std::string &   p_default = "" );
        double      get_double  ( const std::string &   p_key,
                                  double                p_default = 0.0 );
        //
        // Get an array size
        size_t      get_size    ( const std::string &   p_key );
        //
        // Get all members of a class (no value then) or array (only strings)
        std::vector< std::string >
                    get_members ( const std::string &   p_key );
        //
        // Check if a key exists in the configuration
        bool        key_exist   ( const std::string &   p_key );
        //
        // Called by the bus with a new message
        void        bus_event   ( const bus::Message &  p_message );
        //
    private:
        //
        std::shared_mutex          m_mutex_configuration;
        rapidjson::Document        m_configuration;        // config as JSON object
        //
        std::string                m_config_file;          // configuration file
        //
        // Load and parse the configuration file
        bool     read_config       ( bool  p_notify );
        //
        // Called when configuration file changes
        void     monitoring_event  ( const std::string &   p_file );
};

} // namespace config
} // namespace masnae

#endif
