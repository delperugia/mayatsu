// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

// Copyright 2018 Pierre del Perugia
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "masnae_bus.h"

namespace masnae {
    
namespace bus {

    //-------------------------------------------------------------------------
    // Add a subscriber to the audience
    void Publisher::subscribe ( Subscriber *  p_subscriber )
    {
        std::lock_guard< std::mutex > lock( m_mutex );
        //
        m_subscribers.insert( p_subscriber );
    }

    //-------------------------------------------------------------------------
    // Remove a subscriber
    void Publisher::unsubscribe ( Subscriber *  p_subscriber )
    {
        std::lock_guard< std::mutex > lock( m_mutex );
        //
        m_subscribers.erase( p_subscriber );
    }

    //-------------------------------------------------------------------------
    // Send a message to all subscribers
    void Publisher::publish ( const Message & p_message )
    {
        std::lock_guard< std::mutex > lock( m_mutex );
        //
        for ( auto subscriber : m_subscribers )
            subscriber->bus_event( p_message );
    }

} // namespace bus

} // namespace masnae
