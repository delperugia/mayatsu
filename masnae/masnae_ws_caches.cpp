// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

// Copyright 2018 Pierre del Perugia
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <mutex>
#include <rapidjson/document.h>

#include "masnae_global.h"
#include "masnae_misc.h"
#include "masnae_ws_caches.h"

//-------------------------------------------------------------------------
namespace masnae {
namespace ws_caches {

//-------------------------------------------------------------------------
// Cache
//-------------------------------------------------------------------------

//-------------------------------------------------------------------------
Cache::Cache( const std::string &  p_sql_query,
              const std::string &  p_key_name ) :
     m_sql_query( p_sql_query ),
     m_key_name(  p_key_name  )
{
}

//-------------------------------------------------------------------------
// Returns the database rows corresponding to the requested key
bool Cache::find ( const std::string &    p_key,
                   SingleKeyValue_List &  p_rows )
{
    p_rows.clear();
    //
    std::shared_lock< std::shared_mutex > lock( m_mutex_cache );
    //
    auto iter = m_cache.find( p_key );  // move to the beginning of the list of rows
    //
    while ( iter != m_cache.end() &&
            iter->first == p_key )      // copy them until encountering the next key
    {
        p_rows.push_back( iter->second );
        iter++;
    }
    //
    return p_rows.size() != 0;
}

//-------------------------------------------------------------------------
// Returns the database row corresponding to the requested key in a cache
// Returns false if the key is not unique (several database rows matching)
bool Cache::find ( const std::string &    p_key,
                   SingleKey_Value &      p_row )
{
    std::shared_lock< std::shared_mutex > lock( m_mutex_cache );
    //
    auto iter = m_cache.find( p_key );  // move to the beginning of the list of rows
    //
    if ( iter != m_cache.end() ) {
        p_row = iter->second;           // copy the 1st row found
        iter++;
        //
        // The following row must not have the same key
        if ( iter == m_cache.end() || iter->first != p_key )
            return true;
    }
    //
    return false;
}

//-------------------------------------------------------------------------
void Cache::execute ( const std::string &   p_command,
                      ProcessingResponse &  p_result )
{
    if ( p_command == "reload" )
    {
        unsigned db_result = load();
        //
        if ( db_result != 0 )
            mysql::build_resp_error( db_result, p_result );
    }
    else
        p_result.set_jresp_error( "Unknown operation" );
}

//-------------------------------------------------------------------------
// Load cache - returns a MySQL result
unsigned Cache::load ( void )
{
    masnae::SingleKeyValue_List  rows;
    masnae::mysql::Connection    db = masnae::globals::mysql_handler->get_connection( masnae::mysql::reader );
    //
    unsigned db_result = db.do_select( m_sql_query, rows );
    //
    if ( db_result == 0 ) {
        decltype( m_cache ) new_cache;
        //
        // Insert database rows in a multimap, using the configure key as index:
        //     a  b  c         first  second
        //    ---------
        //     1  2  3          [1] . { a=1, b=2, c=3 }
        //     1  4  5      =>  [1] . { a=1, b=4, c=5 }
        //     2  6  7          [1] . { a=2, b=6, c=7 }
        for ( auto & row: rows )
            new_cache.insert( std::pair( row[ m_key_name ], row ));
        //
        //---
        std::lock_guard< std::shared_mutex > lock( m_mutex_cache );
        //
        m_cache = std::move( new_cache );
    }
    //
    return db_result;
}

//-------------------------------------------------------------------------
// Handler
//-------------------------------------------------------------------------

//-------------------------------------------------------------------------
Handler::~Handler()
{
    stop();
    //
    for ( const auto & cache: m_caches )
        delete cache.second;
}

//-------------------------------------------------------------------------
bool Handler::start ( void )
{
    if ( m_running )
        return false;
    //
    globals::publisher->subscribe( this );
    //
    unsigned db_result = load_all_caches();
    if ( db_result != 0 ) {
        service_error_log( "Cache failed to load, mysql_errno=" +
                           std::to_string( db_result ) );
        return false;
    }
    //
    m_running = true;
    return true;
}

//-------------------------------------------------------------------------
void Handler::stop ( void )
{
    if ( globals::publisher != nullptr )
        globals::publisher->unsubscribe( this );
    //
    m_running = false;
}

//-------------------------------------------------------------------------
// Return statistics on the module
std::string Handler::specific_info ( void )
{
    KeyList  tables;
    //
    for ( const auto & cache: m_caches )
        tables.push_back( cache.first );
    //
    return "{\"tables\":" + json_convert( tables ) +
           "}";
}

//-------------------------------------------------------------------------
// Execute the /mgt/caches command:
// returns the list of caches
void Handler::get_caches ( ProcessingResponse &  p_result )
{
    KeyList  tables;
    //
    for ( const auto & cache: m_caches )
        tables.push_back( cache.first );
    //
    p_result.set_jresp_success( json_convert( tables ) );
}

//-------------------------------------------------------------------------
// Execute the given command on the requested cache
void Handler::execute ( const std::string &   p_cache,
                        const std::string &   p_command,
                        ProcessingResponse &  p_result )
{
    if ( m_caches.count( p_cache ) > 0 ) {
        m_caches[ p_cache ]->execute( p_command,
                                      p_result );
        return;
    }
    //
    p_result.set_jresp_error( "Unknown cache" );
}

//-------------------------------------------------------------------------
// Returns the database row corresponding to the requested key in a cache
bool Handler::find ( const std::string &    p_cache,
                     const std::string &    p_key,
                     SingleKeyValue_List &  p_rows )
{
    if ( m_caches.count( p_cache ) > 0 ) {
        return m_caches[ p_cache ]->find( p_key,
                                          p_rows );
    }
    //
    return false;
}

//-------------------------------------------------------------------------
// Returns the database row corresponding to the requested key in a cache
// Returns false if the key is not unique (several database rows matching)
bool Handler::find ( const std::string &  p_cache,
                     const std::string &  p_key,
                     SingleKey_Value &    p_row )
{
    if ( m_caches.count( p_cache ) > 0 ) {
        return m_caches[ p_cache ]->find( p_key,
                                          p_row );
    }
    //
    return false;
}

//-------------------------------------------------------------------------
// Add a cache handling
void Handler::add_cache ( const std::string &  p_name,
                          const std::string &  p_sql_query,
                          const std::string &  p_key_name )
{
    if ( m_running )
        return;
    //
    auto cache = new Cache( p_sql_query, p_key_name );
    //
    m_caches.insert( std::pair( p_name, cache ) );
}

//-------------------------------------------------------------------------
void Handler::bus_event ( const bus::Message &  p_message )
{
    switch ( p_message.type )
    {
        case masnae::bus::Message::config_updated:
            // nothing to do
            break;
        default:
            break;
    }
}

//-------------------------------------------------------------------------
// Load all caches
unsigned Handler::load_all_caches ( void )
{
    for ( const auto & cache: m_caches ) {
        unsigned db_result = cache.second->load();
        //
        if ( db_result != 0 )
            return db_result;
    }
    //
    return 0;
}

} // namespace tables
} // namespace masnae
