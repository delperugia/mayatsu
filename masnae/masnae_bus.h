// Copyright 2018 Pierre del Perugia
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

//
// Internal bus - used to send notifications between modules
//

#ifndef masnae_bus_h
#define masnae_bus_h

#include <mutex>
#include <string>
#include <set>

namespace masnae {
namespace bus {

//-------------------------------------------------------------------------
struct Message
{
    enum { config_updated     // configuration file has changed
    }  type;
};

//-------------------------------------------------------------------------
// The subscriber base class: inherit from this class, register into a
// publisher to receive events
class Subscriber
{
    public:
        virtual
        ~Subscriber() {}
        //
        virtual
        void    bus_event ( const Message &  p_message ) = 0;
};

//-------------------------------------------------------------------------
// The bus class. Create a global instance of this class.
class Publisher
{
    public:
        Publisher() {}
        //
        virtual
        ~Publisher() {}
        //
        // Add a subscriber to the audience
        void    subscribe    ( Subscriber *     p_subscriber );
        //
        // Remove a subscriber
        void    unsubscribe  ( Subscriber *     p_subscriber );
        //
        // Send a message to all subscribers
        void    publish      ( const Message &  p_message );
        //
        // Disable object copy
        Publisher      ( const Publisher & ) = delete;
        void operator= ( const Publisher & ) = delete;
        //
    private:
        std::mutex                 m_mutex;
        std::set< Subscriber * >   m_subscribers;
};

} // namespace bus
} // namespace masnae

#endif
