// Copyright 2018 Pierre del Perugia
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

//
// Requests' log logger
//

#ifndef masnae_logger_h
#define masnae_logger_h

#include <shared_mutex>
#include <string>

#include "masnae_bus.h"
#include "masnae_request_handler.h"

namespace masnae {
namespace logger {

//-------------------------------------------------------------------------
class Handler : public bus::Subscriber
{
    public:
        ~Handler ();
        //
        // Start and stop the module
        bool        start       ( const std::string &  p_default_folder );
        void        stop        ( void );
        //
        // Store the log of the given request
        void        store       ( const std::string &  p_uid,
                                  const std::string &  p_log );
        //
        // If logger is in quiet mode
        bool        quiet       ( void );
        //
        // Called by the bus with a new message
        void        bus_event   ( const bus::Message &  p_message );
        //
    private:
        std::string         m_folder_default;
        //
        std::shared_mutex   m_mutex_config;
        bool                m_enabled        = false;
        mode_t              m_file_mode      = 0640;
        mode_t              m_directory_mode = 0750;
        std::string         m_folder;                   // must end with /
        bool                m_quiet          = false;
        //
        // Build log file name with full path
        std::string     build_filename ( const std::string &  p_folder,
                                         const std::string &  p_uid );
        //
        // Reads configuration from global config object.
        bool            update_config  ( void );
};

} // namespace logger
} // namespace masnae

#endif

