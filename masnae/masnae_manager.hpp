// Copyright 2018 Pierre del Perugia
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

//
// HTTP requests manager
//

#ifndef masnae_manager_h
#define masnae_manager_h

#include <fastcgi++/manager.hpp>
#include <string>
#include <adawat/adawat_uid.h>

#include "masnae_bus.h"
#include "masnae_global.h"
#include "masnae_misc.h"

namespace masnae {
namespace request {

//-------------------------------------------------------------------------
// A wrapper around the FastCGI++ manager
template< class RH >
class Manager : public Fastcgipp::Manager< RH >,
                public bus::Subscriber
{
    public:
        Manager ( unsigned p_threads_per_cpu = 2 ) :
            Fastcgipp::Manager< RH >    ( p_threads_per_cpu * std::thread::hardware_concurrency() ),
            m_urania_cache_last_cleaning( 0 ),
            m_urania_cache_lifetime     ( 30 ),
            m_uid_type                  ( adawat::uid_type::short_ts ),
            m_nb_threads                ( -1 ),
            m_max_post_size_b           ( 0 )  // disable upload
        {
        }
        //
        virtual
        ~Manager ()
        {
        }
        //
        // Start the module
        bool init ( void )
        {
            globals::publisher->subscribe( this );
            //
            // Update load config
            update_config();
            //
            return true;
        }
        //
        // Main loop, wait for stop() call
        bool run ( void )
        {
            if ( m_nb_threads > 0 )
                Fastcgipp::Manager_base::resizeThreads( m_nb_threads );
            //
            Fastcgipp::Manager_base::setupSignals();
            //
            if ( ! Fastcgipp::Manager_base::listen() )
                return false;
            //
            Fastcgipp::Manager_base::start();
            Fastcgipp::Manager_base::join();
            //
            return true;
        }
        //
        // Accessors
        adawat::uid_type get_uid_type (void)
        {
            std::shared_lock <std::shared_mutex> lock( m_mutex_config );
            //
            return m_uid_type;
        }
        //
        int64_t get_max_post_size (void)  // in bytes
        {
            std::shared_lock <std::shared_mutex> lock( m_mutex_config );
            //
            return m_max_post_size_b;
        }
        //
        // Return module information as a JSON string
        std::string specific_info ( void )
        {
            clean_urania_cache();  // delete expired cache entries
            //
            //---
            size_t entries;
            //
            {
                std::shared_lock <std::shared_mutex> lock( m_urania_cache_mutex );
                //
                entries = m_urania_cache_data.size();
            }
            //
            return "{\"accesstoken_in_cache\":" + std::to_string( entries ) +
                   "}";
        }
        //
        // To speed up RequestHandler security checks, cache calls to Urania
        // made by check_accesstoken
        bool find_accesstoken_in_cache ( const std::string &      p_accesstoken,
                                         SecurityContext &        p_security )
        {
            clean_urania_cache();  // delete expired cache entries
            //
            //---
            std::shared_lock <std::shared_mutex> lock( m_urania_cache_mutex );
            //
            if ( m_urania_cache_lifetime == 0 )  // caching is disabled
                return false;
            //
            auto context = m_urania_cache_data.find( p_accesstoken );
            if ( context == m_urania_cache_data.end() )
                return false;
            //
            p_security = context->second;  // if found, return it
            return true;
        }
        //
        // Store the security context of an access token in the cache
        void put_accesstoken_in_cache  ( const std::string &      p_accesstoken,
                                         const SecurityContext &  p_security )
        {
            time_t now = time( nullptr );
            //
            std::lock_guard <std::shared_mutex> lock( m_urania_cache_mutex );
            //
            if ( m_urania_cache_lifetime == 0 )  // caching is disabled
                return;
            //
            m_urania_cache_data.insert   ( { p_accesstoken, p_security } );  // data
            m_urania_cache_keys.push_back( { p_accesstoken, now        } );  // expiration
        }
        //
        // Called by the bus with a new message
        virtual
        void bus_event ( const bus::Message &  p_message )
        {
            switch ( p_message.type )
            {
                case masnae::bus::Message::config_updated:
                    update_config();
                    break;
                default:
                    break;
            }
        }
        //
        // Disable object copy
        Manager                 ( const Manager & ) = delete;
        void operator=          ( const Manager & ) = delete;
        //
    private:
        //
        // Urania cache
        std::shared_mutex   m_urania_cache_mutex;
        time_t              m_urania_cache_last_cleaning;
        int64_t             m_urania_cache_lifetime;
        //
        std::unordered_map< std::string, SecurityContext >
                            m_urania_cache_data;  // cached data indexed by access token
        //
        std::deque< std::pair< std::string, time_t > >
                            m_urania_cache_keys;  // insertion time per access token, ordered
        //
        // Delete old entries from cache
        void clean_urania_cache ( void )
        {
            time_t now = time( nullptr );
            //
            std::lock_guard <std::shared_mutex> lock( m_urania_cache_mutex );
            //
            // Limit to one purge per second
            if ( now <= m_urania_cache_last_cleaning )
                return;
            //
            m_urania_cache_last_cleaning = now;
            //
            // Delete entries older than 30s
            time_t limit = now - m_urania_cache_lifetime;
            //
            while ( ! m_urania_cache_keys.empty() )
                if ( m_urania_cache_keys.front().second <= limit ) { // creation before limit
                    m_urania_cache_data.erase    ( m_urania_cache_keys.front().first );
                    m_urania_cache_keys.pop_front();
                } else {
                    break;
                }
        }
        //
        // Configuration
        std::shared_mutex   m_mutex_config;
        adawat::uid_type    m_uid_type;         // UId type used tor each received request identification
        int64_t             m_nb_threads;       // number of threads to start to handle requests
        int64_t             m_max_post_size_b;  // maximal accepted upload size in bytes
        //
        // Read config from the global config object
        void update_config ( void )
        {
            bool              ok = true;
            adawat::uid_type  new_uid_type;
            int64_t           new_nb_threads;             // -1 means Not set
            int64_t           new_max_post_size_b;        // -1 means Not set
            int64_t           new_urania_cache_lifetime;
            //
            new_nb_threads      = globals::config_handler->get_int64( "/request/nb_threads",      -1 );
            new_max_post_size_b = globals::config_handler->get_int64( "/request/max_post_size_b", -1 );
            //
            new_urania_cache_lifetime
                                = globals::config_handler->get_int64( "/request/urania_cache_lifetime", 30 );
            //
            ok &= adawat::uid_type_cvt( globals::config_handler->get_string( "/request/uid_type" ),
                                        new_uid_type );
            //
            ok &= new_nb_threads == -1 ||
                  ( new_nb_threads      >= 1 && new_nb_threads      <= 128 );
            //
            ok &= new_max_post_size_b == -1 ||
                  ( new_max_post_size_b >= 0 && new_max_post_size_b <= 1000000000 );
            //
            ok &= new_urania_cache_lifetime >= 0 && new_urania_cache_lifetime <= 3600;
            //
            if ( ok ) {
                {
                    std::lock_guard <std::shared_mutex> lock( m_mutex_config );
                    //
                    m_uid_type        = new_uid_type;
                    m_nb_threads      = new_nb_threads;
                    m_max_post_size_b = new_max_post_size_b;
                }
                {
                    std::lock_guard <std::shared_mutex> lock( m_urania_cache_mutex );
                    //
                    m_urania_cache_lifetime = new_urania_cache_lifetime;
                }
            } else {
                masnae::service_error_log( "Manager: invalid config" );
            }
        }
};

} // namespace request
} // namespace masnae

#endif
