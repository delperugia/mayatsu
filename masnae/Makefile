#------------------------------
ifeq ($(CXX),)
    CXX := g++
endif
ifeq ($(AR),)
    AR := ar
endif

#------------------------------
TEST_PRG=		validate
TEST_SRCS=		masnae_validate.cpp
LIBRARY=		libmasnae.a
LIBRARY_SRCS=	masnae_bus.cpp \
				masnae_common.cpp \
				masnae_config.cpp \
                masnae_mysql.cpp \
				masnae_global.cpp \
				masnae_http.cpp \
				masnae_http_openssl.cpp \
				masnae_logger.cpp \
				masnae_misc.cpp \
				masnae_monitor_files.cpp \
				masnae_request_handler.cpp \
				masnae_simulator.cpp \
				masnae_ws_caches.cpp \
				masnae_ws_tables.cpp
CPPFLAGS=		
CXXFLAGS=		-g -std=c++17 -O2
CXXWARN= 		-Wall -Wextra -Wcast-align -Wcast-qual -Wctor-dtor-privacy \
				-Wdisabled-optimization -Wformat=2 -Winit-self -Wmissing-include-dirs \
				-Wold-style-cast -Woverloaded-virtual -Wredundant-decls -Wshadow \
				-Wsign-conversion -Wsign-promo -Wstrict-overflow=5 \
				-Wswitch-default -Wundef -Werror -Wno-unused-function -Wno-unused-parameter
LDFLAGS=		-g
LDLIBS=			-ladawat -lfastcgipp -lcurl -lmariadb -lcrypto -lpthread
ARFLAGS=		rcs

# if using G++, add some warning (ZZ is a prefix, to match g++, g++-6...)
ifeq (ZZg++,$(findstring ZZg++,ZZ$(CXX)))
	CXXWARN += -Wlogical-op -Wstrict-null-sentinel -Wnoexcept
endif

#------------------------------
TEST_OBJS=		$(patsubst  %.cpp,objs/%.o,$(TEST_SRCS))
LIBRARY_OBJS=	$(patsubst  %.cpp,objs/%.o,$(LIBRARY_SRCS))

build_test: $(LIBRARY) $(TEST_PRG)
	@echo Testing $(TEST_PRG) ...
	@"`pwd`/$(TEST_PRG)" ./masnae_validate_config.json 2>/dev/null

gitlab: $(LIBRARY) $(TEST_PRG) 
	@echo Testing $(TEST_PRG) ...
	@"`pwd`/$(TEST_PRG)" ./masnae_validate_config_gitlab.json 2>/dev/null

build: $(LIBRARY)

$(TEST_PRG): $(LIBRARY) $(TEST_OBJS)
	@echo Linking $(TEST_PRG) ...
	@$(CXX) $(LDFLAGS) -o $(TEST_PRG) $(TEST_OBJS) $(LIBRARY) $(LDLIBS) 

$(LIBRARY): $(LIBRARY_OBJS)
	@echo Building $(LIBRARY) ...
	@$(AR) $(ARFLAGS) -o $(LIBRARY) $(LIBRARY_OBJS)

objs/%.o: %.cpp
	@mkdir -p objs/
	@echo " " $<
	@$(CXX) $(CPPFLAGS) $(CXXFLAGS) $(CXXWARN) -c -o $@ $<

depend: _depend.inc

_depend.inc: $(LIBRARY_SRCS) $(TEST_SRCS)
	@echo Building dependencies...
	@rm -f                                ./_depend.inc
	@$(CXX) $(CPPFLAGS) -M $^          >> ./_depend.inc
	@sed -ri 's/^(.*\.o: *)/objs\/\1/'    ./_depend.inc

clean:
	@echo Cleaning...
	@rm -f $(TEST_PRG) $(TEST_OBJS) $(LIBRARY_OBJS) _depend.inc
	@rm -f _pvs.log _pvs.err strace_out

distclean: clean
	@echo Cleaning for distribution...
	@rm -f $(LIBRARY)

include _depend.inc
