// Copyright 2018 Pierre del Perugia
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

//
// HTTP client - pool of cUrl connections
//

#ifndef masnae_http_h
#define masnae_http_h

#include <deque>
#include <memory>
#include <mutex>
#include <shared_mutex>
#include <unordered_map>
#include <adawat/adawat_stats.h>

#include "masnae_bus.h"
#include "masnae_common.h"

// Avoid the full curl/curl.h inclusion
#ifndef __CURL_CURL_H
struct CURLSH;
struct CURL;
struct curl_mime;
#endif

namespace masnae {
namespace http {

// Masnae specific error codes (libcurl uses positive numbers)
#define MSN_HTTP_ERR_NO_HANDLE        (-1)
#define MSN_HTTP_ERR_NOT_CONFIGURE    (-2)
#define MSN_HTTP_ERR_NAME_NOT_FOUND   (-3)
#define MSN_HTTP_ERR_BAD_RESPONSE     (-4)

//-------------------------------------------------------------------------
class Handler : public bus::Subscriber
{
    public:
         Handler() = default;
        ~Handler ();
        //
        // Start and stop the module
        bool        start       ( void );
        void        stop        ( void );
        //
        // Return module information as a JSON string
        std::string specific_info  ( void );
        //
        // Variants to call URL...
        //
        // p_module_document contains the module name, eventually followed
        // by a document (with path):
        //   e.g.:   "module/path/to/document"
        // If module is configured as "https://example.com/" the
        // URL called will be:
        //  "https://example.com/path/to/document"
        //
        // Positive returned codes are cURl's ones.
        // Negative codes are:
        //     -1  failed to allocated a cURL handle
        //     -2  failed to configured cURL handle
        //     -3  module name not found
        //     -4  call was successfully done, but failed to retrieve response
        //
        // ... passing parameters (function will use either x-www-form-urlencoded
        // or multipart/form-data). p_attachments contains the data and must have
        // the same key in p_parameters containing the content type
        int   request1      ( const std::string &     p_module_document,
                              const MultiKey_Value &  p_headers,
                              const MultiKey_Value &  p_parameters,
                              const KeyData &         p_attachments,
                              std::string &           p_content_type,
                              Data &                  p_response );
        //   
        // ... passing body  
        int   request2      ( const std::string &     p_module_document,
                              const MultiKey_Value &  p_headers,
                              const Data &            p_body,
                              std::string &           p_content_type,
                              Data &                  p_response );
        //
        // ... using JResp protocol. Always return a JResp response
        // as a string and as a parsed RadpiJSON object
        void  request_jresp ( const std::string &     p_module_document,
                              const MultiKey_Value &  p_headers,
                              const MultiKey_Value &  p_parameters,
                              const KeyData &         p_attachments,
                              rapidjson::Document &   p_response,
                              std::string  &          p_response_jresp );
        //
        // ... using JSon protocol. Always return a JResp response
        // as a string and as a parsed RadpiJSON object. Received
        // response is in the data field of the JResp object, JResp
        // format is used to convey the error status
        void  request_json  ( const std::string &     p_module_document,
                              const MultiKey_Value &  p_headers,
                              const MultiKey_Value &  p_parameters,
                              const KeyData &         p_attachments,
                              rapidjson::Document &   p_response,
                              std::string  &          p_response_jresp );
        //
        // Called by the bus with a new message
        void        bus_event   ( const bus::Message &  p_message );
        //
        // Disable object copy
        Handler                 ( const Handler & ) = delete;
        void operator=          ( const Handler & ) = delete;
        //
    public:  // defined public in order to be tested by the validation program
        //
        // Check that the Content-Type is JSON
        bool    is_json ( const std::string &  p_content_type ) const;
        //
    private:
        //
        std::mutex                    m_mutex_handles;
        CURLSH *                      m_share_handle = nullptr;  // the share handle
        std::deque< CURL * >          m_curl_handles;            // pool of curl handles
        //
        std::unordered_map< int, std::mutex >                    // indexed by shared data type CURL_LOCK_DATA_...
                                      m_share_mutexes;           // (see cb_share_lock)
        //
        // Modules' configuration
        typedef struct {
            std::string         url;
            long                timeout;          // in second
            long                connect_timeout;  // in second
            adawat::StatsValue  call_duration;    // in second
        }  t_module_config;
        //
        std::shared_mutex             m_mutex_modules;
        std::map< std::string,
                  t_module_config >   m_modules;           // indexed by module name
        //
        // Get a new cURL handle from the pool. Before using a returned handle,
        // the following option must be set:
        //    CURLOPT_URL          URL to call
        //    CURLOPT_WRITEDATA    pointer to a std::string receiving the body
        [[nodiscard]]
        CURL *      borrow_curl_handler  ( void );
        //
        // Return a new cURL handle into the pool
        void        return_curl_handler  ( CURL * &                p_curl );
        // 
        // Convert the key values into a x-www-form-urlencoded 
        bool        urlencode_kv         ( CURL *                  p_curl,
                                           const MultiKey_Value &  p_kv,
                                           std::string &           p_encoded );
        // 
        // Fill the cURL handle with POST parameters   
        bool        fill_post_parameters ( CURL *                  p_curl,
                                           const MultiKey_Value &  p_parameters,
                                           std::string &           p_body );
        // 
        // Fill the cURL handle with a MIME document   
        bool        fill_post_mime       ( CURL *                  p_curl,
                                           const MultiKey_Value &  p_parameters,
                                           const KeyData &         p_attachments,
                                           curl_mime *             p_mime );
        //
        // Continue the URL call  
        int         request_continue     ( CURL *                  p_curl,
                                           const std::string &     p_module_document,
                                           const MultiKey_Value &  p_headers,
                                           std::string &           p_content_type,
                                           Data &                  p_response );
        // 
        // Finish the URL call     
        int         request_call         ( CURL *                  p_curl,
                                           const std::string &     p_module_document,
                                           std::string &           p_content_type,
                                           Data &                  p_response );
        //
        // Read config from the global config object
        void        update_config        ( void );
        //
        // Update call statistics for given module
        void        update_stats         ( CURL *                 p_curl,
                                           const std::string &    p_module,
                                           long                   p_result );
        //
        // Retrieve the configuration of a module, false if not found
        bool        get_module_config    ( const std::string &    p_name,
                                           t_module_config &      p_config );
        //
        // Check that the string is a valid JResp response and returns
        // the corresponding RapidJSON document
        bool        is_valid_jresp       ( const std::string &    p_jresp,
                                           rapidjson::Document &  p_response ) const;
};

} // namespace http
} // namespace masnae

#endif
