// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

// Copyright 2018 Pierre del Perugia
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <fastcgi++/log.hpp>
#include <mariadb/errmsg.h>         // ER_ERROR_FIRST, ER_ERROR_LAST
#include <mariadb/mysql.h>
#include <mariadb/mysqld_error.h>   // CR_MIN_ERROR, CER_MIN_ERROR, CR_MAX_ERROR, CER_MAX_ERROR
#include <rapidjson/document.h>
#include <adawat/adawat_string.h>
#include <adawat/adawat_time.h>

#include "masnae_mysql.h"
#include "masnae_global.h"
#include "masnae_misc.h"

// Ensure there is no overlap between our error codes and MySQL
#if ! (                                                                                \
        ( MSN_MYSQL_ERR_MIN > ER_ERROR_LAST || MSN_MYSQL_ERR_MAX < ER_ERROR_FIRST ) && \
        ( MSN_MYSQL_ERR_MIN > CR_MAX_ERROR  || MSN_MYSQL_ERR_MAX < CR_MIN_ERROR   ) && \
        ( MSN_MYSQL_ERR_MIN > CER_MAX_ERROR || MSN_MYSQL_ERR_MAX < CER_MIN_ERROR  )    \
      )
    #error Masnae error codes overlap MySQL codes
#endif

namespace masnae {
namespace mysql {

using namespace std::literals::chrono_literals;

//--------------------------------------------------------------------
// Escape string to insert in an SQL query
// Since only UTF-8 is use, this is not prone to the injection
// after changing default-character-set vulnerability
// Since most strings don't have special characters, it is faster
// to insert rather than creating a new string
// Tested at 0.787us on:    abcdefghij'lmnopqrst
std::string sql_escape ( const std::string &  p_string )
{
    static
    std::string sql_escaped_char( "\0\\'\"\r\n\x1a", 7);  // length need to be set because there is a 0
    //
    std::string escaped = p_string;
    //
    for ( size_t position = 0; ; ) {
        position = escaped.find_first_of( sql_escaped_char, position );
        if ( position  == std::string::npos )
            break;
        //
        escaped.insert( position , 1, '\\' );
        position += 2;
    }
    //
    return escaped;
}

//--------------------------------------------------------------------
// Set the JResp answer on MySQL error: either a fail or an error
void build_resp_error ( unsigned              p_error,
                        ProcessingResponse &  p_result )
{
    switch ( p_error ) {
        case 1062:  // ER_DUP_ENTRY
                    p_result.set_jresp_rejected( "db_exists",      "null" );
                    break;
        case 1048:  // ER_BAD_NULL_ERROR
                    p_result.set_jresp_rejected( "db_not_null",    "null" );
                    break;
        case 1205:  // ER_LOCK_WAIT_TIMEOUT
        case 1206:  // ER_LOCK_TABLE_FULL
        case 1213:  // ER_LOCK_DEADLOCK
                    p_result.set_jresp_rejected( "db_locked",      "null" );
                    break;
        case 1406:  // ER_DATA_TOO_LONG
                    p_result.set_jresp_rejected( "db_too_long",    "null" );
                    break;
        case 1451:  // ER_ROW_IS_REFERENCED_2
        case 1452:  // ER_NO_REFERENCED_ROW_2
                    p_result.set_jresp_rejected( "db_foreign_key", "null" );
                    break;
        default:
                    p_result.set_jresp_rejected( "db_other",       std::to_string( p_error ) );
                    break;
    }
}

//--------------------------------------------------------------------
// Return p_time (now if not passed) as a MySQL DATETIME
// either using local or UTC timezone
std::string datetime_utc ( std::time_t p_time /* = 0 */ )
{
    std::string datetime = adawat::time_format_rfc3339_utc( p_time );
    //
    // datetime format is: 1985-04-12T23:20:50Z
    if ( ! datetime.empty() )
        datetime.erase( 19 );  // remove Z
    //
    return datetime;
}

//--------------------------------------------------------------------
// Converts a MySQL DATETIME UTC as time_t or -1 on error
//  2019-04-10 07:47:04   -->  1554882424
std::time_t datetime_utc ( std::string  p_time )
{
    if ( p_time.length() != 19 )
        return -1;
    //
    // RFC format is:   1985-04-12T23:20:50Z
    p_time[ 10 ] = 'T';
    p_time += 'Z';
    //
    return adawat::time_parse_rfc3339( p_time );
}

//-------------------------------------------------------------------------
// Connection
//-------------------------------------------------------------------------
Connection::Connection ( Handler &    p_handler,
                         access_type  p_access,
                         MYSQL *      p_handle ) :
    m_handler       ( p_handler ),
    m_access        ( p_access  ),
    m_handle        ( p_handle  )
{
    m_handler.thread_begin();
}

//-------------------------------------------------------------------------
// Return the connection to the manager's pool
// Roll back the current transaction if any
Connection::~Connection()
{
    if ( m_handle != nullptr ) {
        m_handler.return_handle( m_handle, m_access );
        //
        m_handle = nullptr;
    }
    //
    m_handler.thread_end();
}

//-------------------------------------------------------------------------
// Execute a SQL INSERT, sets the generated id if any
// Return a a MySQL result or one of the MSN_MYSQL_ERR_ errors
unsigned Connection::do_insert ( const std::string &  p_sql, std::string &  p_id )
{
    if ( ! is_valid() )
        return MSN_MYSQL_ERR_NO_HANDLE;
    if ( ! is_writer() )
        return MSN_MYSQL_ERR_NOT_WRITER;
    //
    int result = query( p_sql );
    //
    if ( result == 0 ) {
        p_id = std::to_string( mysql_insert_id( m_handle ) );
        //
        flush_results();  // if MySQL returns some data, ignore them
        //
        return 0;
    } else {
        return mysql_errno( m_handle );
    }
}

//-------------------------------------------------------------------------
// Execute a SQL UPDATE, sets the number of affected rows
// Return a a MySQL result or one of the MSN_MYSQL_ERR_ errors
unsigned Connection::do_update ( const std::string &  p_sql, size_t &  p_affected )
{
    if ( ! is_valid() )
        return MSN_MYSQL_ERR_NO_HANDLE;
    if ( ! is_writer() )
        return MSN_MYSQL_ERR_NOT_WRITER;
    //
    int result = query( p_sql );
    //
    if ( result == 0 ) {
        p_affected = mysql_affected_rows( m_handle );
        //
        flush_results();  // if MySQL returns some data, ignore them
        //
        return 0;
    } else {
        return mysql_errno( m_handle );
    }
}

//-------------------------------------------------------------------------
// Execute a SQL SELECT, sets resulting rows
// Return a a MySQL result or one of the MSN_MYSQL_ERR_ errors
unsigned Connection::do_select ( const std::string &  p_sql, SingleKeyValue_List &  p_rows )
{
    p_rows.clear();
    //
    if ( ! is_valid() )
        return MSN_MYSQL_ERR_NO_HANDLE;
    //
    int result = query( p_sql );
    //
    if ( result == 0 ) {
        // Use mysql_use_result instead of mysql_store_result since
        // it is faster and use less memory, and we are not going
        // to do a lot of processing nor locking
        //
        MYSQL_RES * data = mysql_use_result( m_handle );
        if ( data ) {
            // Retrieve the list of field names
            KeyList        fields;
            MYSQL_FIELD *  db_field;
            while ( ( db_field = mysql_fetch_field( data ) ) )
                fields.push_back( db_field->name );
            //
            // Browse all rows
            MYSQL_ROW  db_row;
            //
            while ( ( db_row = mysql_fetch_row( data ) ) ) {
                SingleKey_Value  row;
                unsigned long *  lengths = mysql_fetch_lengths( data );  // array of length of each field
                //
                // Add all fields data in a row
                for (unsigned i = 0; i < fields.size(); i++ )
                    row[ fields[i] ] = db_row[i] == nullptr ?  // a SQL NULL
                                            "" :
                                            std::string( db_row[i], lengths[i] );
                //
                p_rows.push_back( row );  // add the new row to the result set
                //
                if ( p_rows.size() >= m_max_results )  // protection: limit result size
                    break;
            }
            //
            mysql_free_result( data );
            //
            // If MySQL returns more data, ignore them
            if ( mysql_next_result( m_handle ) == 0 )
                flush_results();
        } else {
            // No result set, check if we should have had one
            if ( mysql_field_count( m_handle ) > 0 )  // we should have
                return mysql_errno( m_handle );
        }
        //
        return 0;
    } else {
        return mysql_errno( m_handle );
    }
}

//-------------------------------------------------------------------------
// Execute a SQL DELETE, sets the number of affected rows
// Return a a MySQL result or one of the MSN_MYSQL_ERR_ errors
unsigned Connection::do_delete ( const std::string &  p_sql, size_t &  p_affected )
{
    if ( ! is_valid() )
        return MSN_MYSQL_ERR_NO_HANDLE;
    if ( ! is_writer() )
        return MSN_MYSQL_ERR_NOT_WRITER;
    //
    int result = query( p_sql );
    //
    if ( result == 0 ) {
        p_affected = mysql_affected_rows( m_handle );
        //
        flush_results();  // if MySQL returns some data, ignore them
        //
        return 0;
    } else {
        return mysql_errno( m_handle );
    }
}

//-------------------------------------------------------------------------
// Returns the MySQL text description of the last error 
std::string Connection::native_error ( void )
{
    return is_valid() ?
                mysql_error( m_handle ) :
                "ERROR: Not Connected";
}

//-------------------------------------------------------------------------
// Start a transaction on the current connection
// Actually, it just disable autocommit mode
unsigned Connection::start_transaction ( void )
{
    if ( ! is_valid() )
        return MSN_MYSQL_ERR_NO_HANDLE;
    if ( ! is_writer() )
        return MSN_MYSQL_ERR_NOT_WRITER;
    if ( m_in_transaction )
        return MSN_MYSQL_ERR_IN_TRANSACTION;
    //
    if ( mysql_autocommit( m_handle, 0 ) == 0 ) {
        m_in_transaction = true;
        return 0;
    } else {
        return mysql_errno( m_handle );
    }
}

//-------------------------------------------------------------------------
// Commit the current transaction on the connection
// Return to autocommit mode
unsigned Connection::commit_transaction ( void )
{
    if ( ! is_valid() )
        return MSN_MYSQL_ERR_NO_HANDLE;
    if ( ! is_writer() )
        return MSN_MYSQL_ERR_NOT_WRITER;
    if ( ! m_in_transaction )
        return MSN_MYSQL_ERR_NO_TRANSACTION;
    //
    unsigned result = mysql_commit( m_handle ) == 0 ?
                        0 :
                        mysql_errno( m_handle );
    //
    mysql_autocommit( m_handle, 1 );
    //
    m_in_transaction = false;
    //
    return result;
}

//-------------------------------------------------------------------------
// Roll back the current transaction on the connection
// Return to autocommit mode
unsigned Connection::rollback_transaction ( void )
{
    if ( ! is_valid() )
        return MSN_MYSQL_ERR_NO_HANDLE;
    if ( ! is_writer() )
        return MSN_MYSQL_ERR_NOT_WRITER;
    if ( ! m_in_transaction )
        return MSN_MYSQL_ERR_NO_TRANSACTION;
    //
    unsigned result = mysql_rollback( m_handle ) == 0 ?
                        0 :
                        mysql_errno( m_handle );
    //
    mysql_autocommit( m_handle, 1 );
    //
    m_in_transaction = false;
    //
    return result;
}

//-------------------------------------------------------------------------
// Same as mysql_real_query but retry if server is gone

// Update statistics for successful requests
int Connection::query_stat ( const std::string &  p_sql )
{
    auto  start  = std::chrono::system_clock::now();
    //
    int   result = mysql_real_query( m_handle,
                                     p_sql.c_str(), p_sql.length() );
    //
    if ( result == 0 ) {
        auto duration = std::chrono::duration_cast
                            < std::chrono::milliseconds >
                            ( std::chrono::system_clock::now() - start );
        //
        m_handler.update_stats( m_access, duration.count() );
    }
    //
    return result;
}

// The retry loop
int Connection::query ( const std::string &  p_sql )
{
    int result = query_stat( p_sql );
    //
    // If there is an error 'Server is gone', but not if there
    // is a transaction (we don't want to run some queries
    // on one server and the remaining ones on another), try
    // to execute the query on another connection.
    //
    if ( m_in_transaction )  // stop now (use the result, successful or not)
        return result;
    //
    while ( result != 0 &&
            mysql_errno( m_handle ) == 2006 )  // 'Server is gone'
    {
        // Try to allocate a new handle
        bool                 is_new;
        decltype( m_handle ) new_handle;
        //
        // Since a handle will be released just after, maximum will not be enforced
        new_handle = m_handler.allocate_handle( m_access, true, is_new );
        //
        if ( new_handle == nullptr )  // stop if it is not possible
            break;
        //
        // Return current handle (it will be deleted). Do it now and not 
        // before allocate_handle because we must ensure that m_handle
        // remains valid when leaving this function 
        m_handler.return_handle( m_handle, m_access );
        //
        m_handle = new_handle;
        //
        // And re-run the query
        result   = query_stat( p_sql );
        //
        if ( is_new )  // whatever the result, when using a new connection,
            break;     // do not try to create another (it can be better that this one)
    }
    //
    return result;
}

//-------------------------------------------------------------------------
// Remove all remaining result sets from MySQL handle
// Even if not requested the server sometime send several result sets
// If they are not purged, next query fails
void Connection::flush_results ( void )
{
    MYSQL_RES * data;
    //
    do {
        data = mysql_store_result( m_handle );
        if ( data )
            mysql_free_result( data );
    } while(  mysql_next_result( m_handle ) == 0 );
}

//-------------------------------------------------------------------------
// Handler
//-------------------------------------------------------------------------

//-------------------------------------------------------------------------
// No initialization done here to avoid any problem since this object
// is created as a global variable

thread_local
unsigned Handler::m_thread_begin_counter = 0;

Handler::Handler() :
    m_pools(),
    m_config()
{
}

//-------------------------------------------------------------------------
Handler::~Handler()
{
    stop();
}

//-------------------------------------------------------------------------
// Start, initialize the library
bool Handler::start ( void )
{
    //---------------------------
    if ( mysql_thread_safe() != 1 ) {  // binary must be linked with a threaded library
        service_error_log( "MySQL not thread safe" );
        return false;
    }
    //
    thread_begin();
    //
    if ( mysql_library_init( 0, nullptr, nullptr ) != 0 ) {
        service_error_log( "MySQL library_init failed" );
        return false;
    }
    //
    //---------------------------
    // Finally load config
    update_config();
    //
    globals::publisher->subscribe( this );
    //
    return true;
}

//-------------------------------------------------------------------------
// Stops
// If for some reason a handle is still in use, keep it:
// return_handle will later be called and the handle will be closed then.
// mysql_library_end() is not called for the same reason.
void Handler::stop ( void )
{
    if ( globals::publisher != nullptr )
        globals::publisher->unsubscribe( this );
    //
    clear_pools();
    //
    thread_end();
}

//-------------------------------------------------------------------------
// Return statistics on the module
std::string Handler::specific_info ( void )
{
    std::string      schema, tables_size;
    SingleKey_Value  data[ 2 ];
    //
    {
        std::shared_lock< std::shared_mutex > lock( m_mutex_config );
        //
        data[ reader ][ "host" ] = m_config.reader_host;
        data[ writer ][ "host" ] = m_config.writer_host;
        schema                   = m_config.schema;
    }
    //
    for ( access_type access : { reader, writer } )
    {
        std::lock_guard< std::mutex > lock ( m_pools[ access ].mutex );
        //
        auto in_use = std::count_if( m_pools[ access ].handles.begin(),
                                     m_pools[ access ].handles.end(),
                                     [](auto item){ return item.in_use; } );
        //
        data[ access ][ "in_use" ]         = std::to_string( in_use );
        data[ access ][ "size"]            = std::to_string( m_pools[ access ].handles.size() );
        data[ access ][ "no_handle_count"] =
            std::to_string( m_pools[ access ].no_handle_count );
        //
        masnae::add_stats( "wait_duration", m_pools[ access ].wait_duration, data[ access ] );
        masnae::add_stats( "exec_duration", m_pools[ access ].exec_duration, data[ access ] );
    }
    //
    {
        SingleKeyValue_List  tables;
        auto                 db = get_connection( reader );
        //
        unsigned db_result =
            db.do_select( "SELECT"
                          " TABLE_NAME AS `name`,"
                          " TABLE_ROWS AS `rows`,"
                          " DATA_LENGTH AS `data`,"
                          " INDEX_LENGTH AS `index` "
                          "FROM information_schema.tables "
                          "WHERE TABLE_SCHEMA = '" + sql_escape( schema ) + "'",
                          tables );
        //
        if ( db_result == 0 )
            tables_size = json_convert( tables );
        else
            tables_size = "null";
    }
    //
    return "{\"reader\":" + json_convert( data[ reader ] ) +
           ",\"writer\":" + json_convert( data[ writer ] ) +
           ",\"tables\":" + tables_size                    +
           "}";
}

//-------------------------------------------------------------------------
// Each thread must initialize its MySQL context before calling
// functions. masnae::mysql objects call begin/end and an internal
// counter avoid extra context creation or untimely destruction
//
void Handler::thread_begin( void )
{
    if ( m_thread_begin_counter == 0 )  // first time
        mysql_thread_init();
    //
    m_thread_begin_counter++;
}

void Handler::thread_end  ( void )
{
    if ( m_thread_begin_counter > 0 )  // protection if thread_end is called too much
    {
        m_thread_begin_counter--;
        //
        if ( m_thread_begin_counter == 0 )  // last time
            mysql_thread_end();
    }
}

//-------------------------------------------------------------------------
// Create a new Connection, retrieved if possible from the pool
// If not found, return an invalid Connection
Connection Handler::get_connection ( access_type  p_access )
{
    bool    is_new;
    MYSQL * handle = allocate_handle( p_access, false, is_new );
    //
    return Connection( * this, p_access, handle );
}

//-------------------------------------------------------------------------
// Same but in a dynamic object. Be sure to delete it!
std::unique_ptr< Connection >
Handler::get_connection_ptr ( access_type  p_access )
{
    bool    is_new;
    MYSQL * handle = allocate_handle( p_access, false, is_new );
    //
    return
        std::unique_ptr< Connection >(
            new (std::nothrow) Connection( * this, p_access, handle )
        );
}

//-------------------------------------------------------------------------
void Handler::bus_event ( const bus::Message &  p_message )
{
    switch ( p_message.type )
    {
        case masnae::bus::Message::config_updated:
            update_config();
            break;
        default:
            break;
    }
}

//-------------------------------------------------------------------------
// Called when Connection is destroyed: returns the handle to
// the pool, or close it if marked so
void Handler::return_handle( MYSQL *  p_handle, access_type  p_access )
{
    // Try to detect if a connection is gone
    bool dead_connection = false;
    auto last_error      = mysql_errno( p_handle );
    //
    if ( last_error != 0 )  {                   // to reduce overload, only if there was an error
        if ( mysql_ping( p_handle ) != 0 ) {    // query the server
            dead_connection = true;
            //
            service_error_log(
                adawat::str_sprintf( "MySQL connection %p is dead, mysql_errno=%u",
                                     p_handle,
                                     mysql_errno( p_handle ) ) );
        }
    }
    //
    // Find the handle, release (or close) it
    std::lock_guard< std::mutex > lock ( m_pools[ p_access ].mutex );
    //
    if ( dead_connection )
        m_pools[ p_access ].close_handle( p_handle );
    else
        m_pools[ p_access ].release_handle( p_handle );
    //
    // Inform waiting process that either a handle is ready or
    // that they can try to create a new one
    m_pools[ p_access ].condition.notify_one();
}

//-------------------------------------------------------------------------
// Update execution time statistics
void Handler::update_stats ( access_type  p_access, int64_t  p_duration_ms )
{
    std::lock_guard< std::mutex > lock ( m_pools[ p_access ].mutex );
    //
    m_pools[ p_access ].exec_duration.add_value( p_duration_ms / 1000.0 );
}

//-------------------------------------------------------------------------
// Reads configuration from global config object. If config has changed,
// clear existing handles from pools
void Handler::update_config ( void )
{
    // Read the configuration file
    t_config  new_config;
    //
    new_config.user                 = globals::config_handler->get_string( "/mysql/user"               , ""          );
    new_config.password             = globals::config_handler->get_string( "/mysql/password"             , ""          );
    new_config.schema               = globals::config_handler->get_string( "/mysql/schema"             , ""          );
    new_config.max_size             = globals::config_handler->get_int64(  "/mysql/max_size"           , 2           );
    //
    new_config.compress             = globals::config_handler->get_bool(   "/mysql/compress"           , false       );
    new_config.timeout_ms           = globals::config_handler->get_int64(  "/mysql/timeout_ms"         , 1000        );
    //
    new_config.reader_host          = globals::config_handler->get_string( "/mysql/reader/host"        , "localhost" );
    new_config.reader_port          = globals::config_handler->get_int64(  "/mysql/reader/port"        , 0           );
    new_config.reader_unix_socket   = globals::config_handler->get_string( "/mysql/reader/unix_socket" , ""          );
    //
    new_config.writer_host          = globals::config_handler->get_string( "/mysql/reader/host"        , "localhost" );
    new_config.writer_port          = globals::config_handler->get_int64(  "/mysql/reader/port"        , 0           );
    new_config.writer_unix_socket   = globals::config_handler->get_string( "/mysql/reader/unix_socket" , ""          );
    //
    // Copy the new config, check if it has changed
    bool need_reconnection;
    //
    {
        std::lock_guard< std::shared_mutex > lock( m_mutex_config );
        //
        need_reconnection = m_config.need_reconnect( new_config );
        m_config          = std::move( new_config );
    }
    //
    // If config has changed, close existing handles
    if ( need_reconnection )
        clear_pools();
}

//-------------------------------------------------------------------------
// Close existing handles, or mark then if they are in use
void Handler::clear_pools ( void )
{
    for ( access_type access : { reader, writer } )
    {
        std::lock_guard< std::mutex > lock ( m_pools[ access ].mutex );
        //
        m_pools[ access ].close_all_handles();
        //
        // Reset pool's stats
        m_pools[ access ].wait_duration.clear();
        m_pools[ access ].exec_duration.clear();
        m_pools[ access ].no_handle_count = 0;
    }
}

//-------------------------------------------------------------------------
// Connect a previously created MySQL handle to a server
// Return false if connection failed
bool Handler::connect_handle ( MYSQL *  p_handle, access_type  p_access )
{
    //---------------------------
    t_config  config;
    //
    {
        std::shared_lock< std::shared_mutex > lock( m_mutex_config );
        //
        config = m_config;
    }
    //
    //---------------------------
    //
    // Build flags
    // MYSQL_OPT_RECONNECT is not used because of transactions
    unsigned long flags = CLIENT_FOUND_ROWS | CLIENT_IGNORE_SIGPIPE;
    //
    if ( config.compress )
        flags |= CLIENT_COMPRESS;
    //
    // Connect
    MYSQL * connection = mysql_real_connect(
        p_handle,
        p_access == writer ?
            config.writer_host.c_str() :
            config.reader_host.c_str(),
        config.user.c_str(),
        config.password.c_str(),
        config.schema.c_str(),
        p_access == writer ?
            config.writer_port :
            config.reader_port,
        p_access == writer ?
            ( config.writer_unix_socket.empty() ?
                nullptr :
                config.writer_unix_socket.c_str() ) :
            ( config.reader_unix_socket.empty() ?
                nullptr :
                config.reader_unix_socket.c_str() ),
        flags
    );
    //
    // If mysql_real_connect fails, connection is nullptr but
    // the handle created by mysql_init is still there
    if ( connection == nullptr ) {
        service_error_log( "MySQL failed to connect, mysql_errno=" +
                           std::to_string( mysql_errno( p_handle ) ) );
        return false;
    }
    //
    // If mysql_real_connect succeeds, connection equals p_handle
    //
    int result = 0;
    //
    result |= mysql_set_server_option( connection, MYSQL_OPTION_MULTI_STATEMENTS_OFF );
    result |= mysql_set_character_set( connection, "utf8" );
    result |= mysql_optionsv         ( connection, MYSQL_SET_CHARSET_NAME, "utf8");
    //
    result |= mysql_autocommit       ( connection, 1 );
    //
    result |= mysql_query            ( connection, "SET SESSION sql_mode='STRICT_ALL_TABLES'" );
    //
    if ( result == 0 ) {
        service_info_log( adawat::str_sprintf( "MySQL new connection %p ",
                                               connection ) );
        return true;
    } else {
        service_error_log( "MySQL failed to configure connection" );
        return false;
    }
}

//-------------------------------------------------------------------------
// Retrieve a handle from the pool or create a new one
// Returns nullptr on error
MYSQL * Handler::allocate_handle ( access_type  p_access,
                                   bool         p_allow_extra,  // can exceed maximum configured
                                   bool &       p_is_new )      // returned handle was just allocated
{
    MYSQL * handle = nullptr;
    auto    start  = std::chrono::system_clock::now();
    //
    //---------------------------
    t_config  config;
    //
    {
        std::shared_lock< std::shared_mutex > lock( m_mutex_config );
        //
        config = m_config;
    }
    //
    //---------------------------
    {
        std::unique_lock< std::mutex > lock ( m_pools[ p_access ].mutex );
        //
        for (;;)
        {
            // Search in pool if there is an available handle
            handle = m_pools[ p_access ].find_handle();
            if ( handle != nullptr ) {
                p_is_new = false;
                //
                auto duration = std::chrono::duration_cast<std::chrono::milliseconds>( std::chrono::system_clock::now() - start );
                m_pools[ p_access ].wait_duration.add_value( duration.count() / 1000.0 );
                //
                return handle;
            }
            //
            // If possible add a new one
            handle = m_pools[ p_access ].add_handle_for_new_connection(
                        static_cast< size_t >( config.max_size ) +
                        ( p_allow_extra ? 1 : 0 )
            );
            if ( handle != nullptr )
                break;  // and try to create one
            //
            // Exit if trying for too long
            auto duration = std::chrono::duration_cast<std::chrono::milliseconds>( std::chrono::system_clock::now() - start );
            if ( duration.count() > config.timeout_ms ) {
                m_pools[ p_access ].no_handle_count ++;
                //
                return nullptr;
            }
            //
            // The mutex is unlocked while in wait_for
            m_pools[ p_access ].condition.wait_for( lock, 100ms );
            // The mutex is relocked when exiting wait_for
        }
    }
    //
    // Try to connect to the MySQL server using the new handle
    if ( connect_handle( handle, p_access ) )
    {
        p_is_new = true;
        //
        auto duration = std::chrono::duration_cast<std::chrono::milliseconds>( std::chrono::system_clock::now() - start );
        //
        std::lock_guard< std::mutex > lock ( m_pools[ p_access ].mutex );
        //
        m_pools[ p_access ].wait_duration.add_value( duration.count() / 1000.0 );
        //
        return handle;
    }
    //
    // Failed to get a connection: remove allocated handle
    std::lock_guard< std::mutex > lock ( m_pools[ p_access ].mutex );
    //
    m_pools[ p_access ].close_handle( handle );
    //
    m_pools[ p_access ].no_handle_count ++;
    //
    return nullptr;
}

//-------------------------------------------------------------------------
// Returns true if the changes in config require a reconnection
bool Handler::t_config::need_reconnect ( const t_config &  p_right )
{
    return
        user                 !=  p_right.user                ||
        password             !=  p_right.password            ||
        schema               !=  p_right.schema              ||
        //
        compress             !=  p_right.compress            ||
        //
        reader_host          !=  p_right.reader_host         ||
        reader_port          !=  p_right.reader_port         ||
        reader_unix_socket   !=  p_right.reader_unix_socket  ||
        //
        writer_host          !=  p_right.writer_host         ||
        writer_port          !=  p_right.writer_port         ||
        writer_unix_socket   !=  p_right.writer_unix_socket;
}

//-------------------------------------------------------------------------
// If the pool is not too big, allocates and returns a new MySQL handle
// Pool must be locked before
MYSQL * Handler::t_pool::add_handle_for_new_connection( size_t p_max_handles )
{
    if ( handles.size() >= p_max_handles )
        return nullptr;
    //
    MYSQL * handle = mysql_init( nullptr );
    //
    if ( handle != nullptr ) {
        t_entry entry = {
            .handle   = handle,
            .in_use   = true,
            .to_close = false
        };
        //
        handles.push_back( entry );
    }
    //
    return handle;
}

//-------------------------------------------------------------------------
// In the pool, search for a handle available and not marked to_close
// (config has not changed since it was created)
// Marks the found handle in_use and returns it
// Pool must be locked before
MYSQL * Handler::t_pool::find_handle( void )
{
    for ( auto & entry : handles ) {
        if ( ! entry.in_use && ! entry.to_close ) {
            entry.in_use = true;
            return entry.handle;
        }
    }
    //
    return nullptr;
}

//-------------------------------------------------------------------------
// Release a handle from the pool. If the handle was marked to_close (configuration
// has changed), close and remove it.
// Pool must be locked before
void Handler::t_pool::release_handle( MYSQL * p_handle )
{
    for ( auto iter = handles.begin(); iter != handles.end(); iter++ ) {
        if ( iter->handle == p_handle ) {
            if ( iter->to_close ) {
                handles.erase( iter );
                break;
            } else {
                iter->in_use = false;
                return;
            }
        }
    }
    //
    mysql_close( p_handle );
}

//-------------------------------------------------------------------------
// Removes and closes a handle from the from the pool
// The handle may be connected or not, but it is not in use
// (even if the flag in_use may still be set)
// Pool must be locked before
void Handler::t_pool::close_handle( MYSQL * p_handle )
{
    for ( auto iter = handles.begin(); iter != handles.end(); iter++ ) {
        if ( iter->handle == p_handle ) {
            handles.erase( iter );
            break;
        }
    }
    //
    mysql_close( p_handle );
}

//-------------------------------------------------------------------------
// Removes and closes all handles from the pool
// If a handle is still in_use, just mark it and it will be closed
// later by release_handle
// Pool must be locked before
void Handler::t_pool::close_all_handles( void )
{
    for ( auto iter = handles.begin(); iter != handles.end(); ) {
        if ( iter->in_use ) {
            iter->to_close = true;
            iter++;
        } else {
            mysql_close( iter->handle );
            iter = handles.erase( iter );
        }
    }
}

} // namespace mysql
} // namespace masnae
