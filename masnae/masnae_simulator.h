// Copyright 2018 Pierre del Perugia
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

//
// HTTP request call simulation
//

#ifndef masnae_simulator_h
#define masnae_simulator_h

#include "masnae_common.h"
#include "masnae_request_handler.h"

namespace masnae {

//-------------------------------------------------------------------------
// Execute an URL using the given RequestHandler
// p_response contains the last response
// URL must have the form:  /ws/req?a=1&a=3
// Script can be executed several times by specifying a multiplier:
//      5*/ws/req?a=1&a=3
// Parameter starting with @ are file upload. Syntax is:
//   @file.ext[;type=content-type]
bool exec_url ( const std::string &     p_url,
                RequestHandler &        p_handler,
                ProcessingResponse &    p_response,
                bool                    p_debug = false );

//-------------------------------------------------------------------------
// Interactive console session executing the request using the given
// RequestHandler
void console  ( RequestHandler &        p_handler );

} // namespace masnae

#endif
