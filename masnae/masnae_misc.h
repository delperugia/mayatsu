// Copyright 2018 Pierre del Perugia
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

//
// Miscellaneous functions
//

#ifndef masnae_misc_h
#define masnae_misc_h

#include <map>
#include <rapidjson/document.h>
#include <string>
#include <vector>
#include <adawat/adawat_stats.h>

#include "masnae_common.h"

namespace masnae {

//-------------------------------------------------------------------------
// Check if a string is a valid UTF8 string
bool is_utf8 ( const void * p_string, uint32_t p_length );

//-------------------------------------------------------------------------
// Returns the given string quoted using JavaScript style, enclose in quotes
//    e.g.:    He is called "Johnny"   -->   "He is called \"Johnny\""
std::string json_quote_string ( const std::string &  p_text );

//-------------------------------------------------------------------------
// Returns the given vector as a JSON array of string
//    e.g.:    [ "alpha", "beta" ]
std::string json_convert ( const KeyList &  p_vector );

//-------------------------------------------------------------------------
// Returns the given map as a JSON object of string
//    e.g.:    { "key1": "value1", "key2": "value2" }
std::string json_convert ( const SingleKey_Value &  p_map );

//-------------------------------------------------------------------------
// Returns the given list of maps as a JSON array of objects
//    e.g.:    [ { "key1": "value1", "key2": "value2" }, { "key1": "value3", "key2": "value4" } ]
std::string json_convert ( const SingleKeyValue_List &  p_rows );

//-------------------------------------------------------------------------
// Converts a FastCGI++ Address object into a string
std::string convert_address( const Fastcpipp_Address &  p_address );

//-------------------------------------------------------------------------
// Convert FastCGI++ request method to a string
std::string convert_method( Fastcgipp::Http::RequestMethod  p_method );

//-------------------------------------------------------------------------
// Merge the received post and get parameters, knowing that:
//  - post has precedence over get
//  - same parameter name can be present several times (hence the vector)
SingleKey_ValueList merge_parameters( const Fastcpipp_Params &  p_post,
                                      const Fastcpipp_Params &  p_get );

//-------------------------------------------------------------------------
// Retrieve a parameter from the received ones. If it is missing or invalid,
// set p_result and return false
bool get_parameter ( const masnae::SingleKey_ValueList &   p_parameters,
                     const std::string &                   p_key,
                     std::string &                         p_value,
                     bool                                  p_can_be_empty,
                     bool                                  p_optional,
                     masnae::ProcessingResponse &          p_result );

bool get_parameter ( const masnae::SingleKey_ValueList &   p_parameters,
                     const std::string &                   p_key,
                     int64_t &                             p_value,
                     bool                                  p_optional,
                     masnae::ProcessingResponse &          p_result );

//-------------------------------------------------------------------------
// Two functions to log a message at the service level, accessed
// using journalctl
void service_info_log  ( const std::string &  p_message );
void service_error_log ( const std::string &  p_message );

//-------------------------------------------------------------------------
// Convenient functions used to retrieve values in a RapidJSON
// document using JSON Pointer (like "/foo/0"). These are strict
// functions: if the pointed value is not of the expected type,
// they return false.

// Pointed element is a string
bool rapidjson_get_string ( rapidjson::Document &  p_document,
                            const std::string &    p_path,
                            std::string &          p_value );

// Pointed element is a boolean
bool rapidjson_get_bool   ( rapidjson::Document &    p_document,
                            const std::string &      p_path,
                            bool &                   p_value );

// Pointed element is a double
bool rapidjson_get_double ( rapidjson::Document &    p_document,
                            const std::string &      p_path,
                            double &                 p_value );

// Pointed element is an object with only string members
bool rapidjson_get_kv     ( rapidjson::Document &      p_document,
                            const std::string &        p_path,
                            masnae::SingleKey_Value &  p_kv );

//-------------------------------------------------------------------------
// JSON Web Signature

// Build a JWS signature using given headers and payload
// 'alg' in headers is forced to 'HS256' as this is the only
// supported algorithm
// Returns false on error
bool jws_build ( masnae::SingleKey_Value          p_headers,
                 const masnae::SingleKey_Value &  p_payload,
                 const std::string &              p_secret,
                 std::string &                    p_signature );

// Parse a JWS signature. Assume 'alg' is 'HS256' and extract
// headers and payload
// Returns false on error
bool jws_parse ( std::string                p_signature,
                 const std::string &        p_secret,
                 masnae::SingleKey_Value &  p_headers,
                 masnae::SingleKey_Value &  p_payload );

//-------------------------------------------------------------------------
// Adds the various stats element in data, to be later
// converted to JSON
void add_stats( const std::string &       p_name,
                const adawat::StatsValue  p_stats,
                SingleKey_Value &         p_data );

//-------------------------------------------------------------------------
// Convenient function to implement validators

// Check that the response is a valid JResp 'success' and if
// path is provided returns it (must be a string)
bool is_jresp_success  ( masnae::ProcessingResponse & p_response,
                         const std::string &          p_path,
                         std::string &                p_datum );

// Check that the response is a valid JResp 'success' and if
// path is provided returns it (must be a boolean)
bool is_jresp_success  ( masnae::ProcessingResponse & p_response,
                         const std::string &          p_path,
                         bool &                       p_datum );

// Check that the response is a valid JResp 'success' and
// set p_data with data field
bool is_jresp_success  ( masnae::ProcessingResponse &                    p_response,
                         rapidjson::GenericValue< rapidjson::UTF8<> > &  p_data );

// Check that the response is a valid JResp 'error'
bool is_jresp_error    ( masnae::ProcessingResponse & p_response );

// Check that the response is a valid JResp 'failed'
bool is_jresp_failed   ( masnae::ProcessingResponse & p_response );

// Check that the response is a valid JResp 'rejected' and if
// reason and data are provided check them too
bool is_jresp_rejected ( masnae::ProcessingResponse & p_response,
                         const std::string &          p_expected_reason,
                         const std::string &          p_expected_data );

} // namespace masnae

#endif
