// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

// Copyright 2018 Pierre del Perugia
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <sys/inotify.h>
#include <sys/stat.h>
#include <unistd.h>

#include "masnae_global.h"
#include "masnae_misc.h"
#include "masnae_monitor_files.h"

namespace masnae {
    
//-------------------------------------------------------------------------
MonitorFiles::MonitorFiles ( const char *  p_name) :
    m_monitor_name( p_name )
{
}

//-------------------------------------------------------------------------
bool MonitorFiles::start_monitoring ( const std::string &  p_path,
                                      bool                 p_is_folder )
{
    masnae::service_info_log( m_monitor_name +
                              ( p_is_folder ?
                                    " monitoring folder: " :
                                    " monitoring file: " ) +
                              p_path );
    //
    if ( p_is_folder )
    {
        m_monitoring_folder = p_is_folder;
        m_monitored_folder  = p_path;
    }
    else
    {
        // Instead of monitoring the file itself, the whole folder is monitored
        // because some editor like vi create a new file when saving. The initial
        // watch would then stop when the original file would have been deleted.
        // Extract the folder and file name.
        size_t pos = p_path.find_last_of( '/' );
        if ( pos == std::string::npos ) {
            service_error_log( m_monitor_name + ": file has no path");
            return false;
        }
        //
        m_monitoring_folder = p_is_folder;
        m_monitored_folder  = p_path.substr( 0,    pos );
        m_monitored_file    = p_path.substr( pos+1 );
    }
    //
    // Start by checking that the folder holding scripts is readable
    if ( access( m_monitored_folder.c_str(), R_OK ) != 0 ) {
        masnae::service_error_log( m_monitor_name + ": folder is not readable" );
        return false;
    }
    //
    // Start monitoring engine
    m_notify_handle = inotify_init();
    if ( m_notify_handle < 0 ) {
        masnae::service_error_log( m_monitor_name + ": inotify_init failed, errno=" +
                                   std::to_string(errno) );
        return false;
    }
    //
    // Monitor the folder for any file update
    m_notify_watch = inotify_add_watch( m_notify_handle,
                                        m_monitored_folder.c_str(),
                                        IN_CLOSE_WRITE | IN_DELETE );
    if ( m_notify_watch < 0 ) {
        masnae::service_error_log( m_monitor_name + ": inotify_add_watch failed, errno=" +
                                   std::to_string( errno ) );
        close( m_notify_handle );
        m_notify_handle = -1;
        return false;
    }
    //
    // Start the monitoring thread
    m_monitoring_thread = std::thread( thread_monitoring, this );
    //
    return true;
}

//-------------------------------------------------------------------------
void MonitorFiles::stop_monitoring ( void )
{
    if ( m_notify_watch > 0 ) {
        inotify_rm_watch( m_notify_handle,
                          m_notify_watch );  // will generate a IN_IGNORED...
        m_notify_watch = -1;
    }
    //
    if ( m_monitoring_thread.joinable() )    // ... and the thread will exit
        m_monitoring_thread.join();
    //
    if ( m_notify_handle > 0 ) {
        close( m_notify_handle );
        m_notify_handle = -1;
    }
}

//-------------------------------------------------------------------------
// Wait on the monitored folder for:
//   IN_CLOSE_WRITE is notified when a file is written
//   IN_IGNORED when monitoring stops
void MonitorFiles::thread_monitoring ( MonitorFiles *  p_self )
{
    char buffer[4096]
        __attribute__ ((aligned(__alignof__(struct inotify_event))));
    //
    for ( bool stop = false; ! stop; ) {
        // Read a buffer containing several size-variable events
        ssize_t nb_read = read( p_self->m_notify_handle,
                                & buffer,
                                sizeof(buffer) );
        if ( nb_read <= 0 ) {
            masnae::service_error_log( p_self->m_monitor_name + ": thread_monitoring read error, errno=" +
                                       std::to_string( errno ) );
            masnae::globals::request_manager->stop();
            break;
        }
        //
        // For each received event
        for (const char * p = buffer; p < buffer + nb_read; ) {
            auto notification = reinterpret_cast<const struct inotify_event *>( p );
            //
            if ( notification->mask & IN_IGNORED ) {  // stop request received
                masnae::service_info_log( p_self->m_monitor_name + " thread_monitoring stopped" );
                stop = true;
                break;
            }
            //
            // Remove file from cache
            if ( notification->mask & IN_CLOSE_WRITE )  // only possible event
                p_self->monitor_file_written( notification->name );
            //
            // Move to next event
            p += sizeof(struct inotify_event) + notification->len;
        }
    }
}

//-------------------------------------------------------------------------
// Called when a file is written in the monitored folder
void MonitorFiles::monitor_file_written ( const char *  p_file )
{
    // When monitoring a single file, ignore other files
    if ( ! m_monitoring_folder && m_monitored_file != p_file )
        return;
    //
    monitoring_event( p_file );
}

} // namespace masnae
