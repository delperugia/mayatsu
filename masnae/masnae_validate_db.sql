create schema if not exists masnae;
use masnae;
create table tbl ( id      int not null auto_increment,
                   name    varchar(64),
                   age     int(10) unsigned,
                   adr     varchar(128),
                   bd      datetime,
                   primary key (id) );
insert into tbl (name,age,adr,bd) values
                ('alpha',1,'un'  ,'2012-12-31T11:30:45'),
                ('beta' ,2,'deux','2007-05-23T09:15:28');

create user 'masnae'@'localhost' identified by 'z6dXuce7cu2XX9UB';
create user 'masnae'@'%'         identified by 'z6dXuce7cu2XX9UB';
grant all on masnae.* to 'masnae'@'localhost';
grant all on masnae.* to 'masnae'@'%';
