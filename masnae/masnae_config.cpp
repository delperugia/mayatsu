// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

// Copyright 2018 Pierre del Perugia
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <fastcgi++/log.hpp>
#include <fastcgi++/manager.hpp>
#include <rapidjson/pointer.h>
#include <sys/inotify.h>
#include <sys/stat.h>
#include <unistd.h>

#include "masnae_config.h"
#include "masnae_global.h"
#include "masnae_misc.h"

namespace masnae {
namespace config {
    
//-------------------------------------------------------------------------
Handler::Handler () :
    MonitorFiles( "Config" )
{
}

//-------------------------------------------------------------------------
Handler::~Handler ()
{
    stop();
}

//-------------------------------------------------------------------------
// Starts, monitoring the given configuration file
bool Handler::start ( const std::string &  p_config_file )
{
    globals::publisher->subscribe( this );
    //
    m_config_file = p_config_file;
    //
    if ( start_monitoring( p_config_file, false ) ) {
        if ( read_config( false ) )  // read but don't notify
            return true;
        else
            stop_monitoring();
    }
    //
    return true;
}

//-------------------------------------------------------------------------
// Stops
void Handler::stop ( void )
{
    stop_monitoring();
    //
    if ( globals::publisher != nullptr )
        globals::publisher->unsubscribe( this );
}

//-------------------------------------------------------------------------
// Return statistics on the module
std::string Handler::specific_info ( void )
{
    return "{\"config_file\":" + json_quote_string( m_config_file ) +
           "}";
}

//-------------------------------------------------------------------------
bool Handler::get_bool ( const std::string &  p_key,
                         bool                 p_default /* = false */ )
{
    std::shared_lock< std::shared_mutex > lock( m_mutex_configuration );
    //
    rapidjson::Value * value = rapidjson::Pointer( p_key.c_str() ).Get( m_configuration );
    //
    if ( value != nullptr && value->IsBool() )
        return value->GetBool();
    //
    return p_default;
}

//-------------------------------------------------------------------------
// Retrieve from the configuration the given key as an integer
// Return p_default if the key doesn't exist or is not an int
int64_t Handler::get_int64 ( const std::string &  p_key,
                             int64_t              p_default /* = -1 */ )
{
    std::shared_lock< std::shared_mutex > lock( m_mutex_configuration );
    //
    rapidjson::Value * value = rapidjson::Pointer( p_key.c_str() ).Get( m_configuration );
    //
    if ( value != nullptr && value->IsInt64() )
        return value->GetInt64();
    //
    return p_default;
}

//-------------------------------------------------------------------------
// Retrieve from the configuration the given key as an integer
// Return p_default if the key doesn't exist or is not an int
double Handler::get_double ( const std::string &  p_key,
                             double                p_default /* = 0.0 */ )
{
    std::shared_lock< std::shared_mutex > lock( m_mutex_configuration );
    //
    rapidjson::Value * value = rapidjson::Pointer( p_key.c_str() ).Get( m_configuration );
    //
    if ( value != nullptr && value->IsDouble() )
        return value->GetDouble();
    //
    return p_default;
}

//-------------------------------------------------------------------------
// Retrieve from the configuration the given key as a string
// Return p_default if the key doesn't exist or is not a string
std::string Handler::get_string ( const std::string &  p_key,
                                  const std::string &  p_default /* = "" */ )
{
    std::shared_lock< std::shared_mutex > lock( m_mutex_configuration );
    //
    rapidjson::Value * value = rapidjson::Pointer( p_key.c_str() ).Get( m_configuration );
    //
    if ( value != nullptr && value->IsString() )
        return value->GetString();
    //
    return p_default;
}

//-------------------------------------------------------------------------
size_t Handler::get_size( const std::string &  p_key )
{
    std::shared_lock< std::shared_mutex > lock( m_mutex_configuration );
    //
    rapidjson::Value * value = rapidjson::Pointer( p_key.c_str() ).Get( m_configuration );
    //
    if ( value != nullptr && value->IsArray() )
        return value->Size();
    //
    return 0;
}

//-------------------------------------------------------------------------
// Get all members of a class (no value then) or array (only strings)
std::vector< std::string >
Handler::get_members ( const std::string &  p_key )
{
    std::vector< std::string > members;
    //
    std::shared_lock< std::shared_mutex > lock( m_mutex_configuration );
    //
    rapidjson::Value * value = rapidjson::Pointer( p_key.c_str() ).Get( m_configuration );
    //
    if ( value != nullptr )
    {
        if ( value->IsObject() )
            for ( const auto & m : value->GetObject() )
                members.push_back( m.name.GetString() );
        else
        if ( value->IsArray() )
            for ( const auto & e : value->GetArray() )
                if ( e.IsString() )
                    members.push_back( e.GetString() );
    }
    //
    return members;
}

//-------------------------------------------------------------------------
// Check if a key exists in the configuration and is either an int or a string
bool Handler::key_exist ( const std::string &  p_key )
{
    std::shared_lock< std::shared_mutex > lock( m_mutex_configuration );
    //
    rapidjson::Value * value = rapidjson::Pointer( p_key.c_str() ).Get( m_configuration );
    //
    return value != nullptr &&
           ( value->IsInt64() || value->IsString() );
}

//-------------------------------------------------------------------------
void Handler::bus_event ( const bus::Message &  p_message )
{
    switch ( p_message.type )
    {
        case masnae::bus::Message::config_updated:
            // nothing to do
            break;
        default:
            break;
    }
}

//-------------------------------------------------------------------------
// Read the configuration file
// If successful, send and event on the bus to inform everyone
bool Handler::read_config ( bool  p_notify )
{
    // Open config file
    FILE * file = fopen( m_config_file.c_str(), "rt" );
    //
    if ( file == nullptr ) {
        service_error_log( "Config: failed to open file" );
        return false;
    }
    //
    // Now read it
    std::string config;
    char        buffer[4096];
    size_t      length;
    //
    while ( (length = fread( buffer, 1, sizeof(buffer), file )) > 0 )
        config.insert( config.length(), buffer, length );
    //
    fclose( file );
    //
    // Parse it
    rapidjson::Document  configuration;
    //
    configuration.Parse< rapidjson::kParseFullPrecisionFlag |
                         rapidjson::kParseCommentsFlag >( config.c_str() );
    //
    if ( configuration.HasParseError() ) {
        service_error_log( "Config: failed to parse file" );
        return false;
    } else if ( ! configuration.IsObject() ) {
        service_error_log( "Config: json is not an object" );
        return false;
    }
    //
    // Start by updating the current configuration
    {
        std::lock_guard< std::shared_mutex > lock( m_mutex_configuration );
        //
        m_configuration = std::move( configuration );
    }
    //
    service_info_log( "Config: configuration updated" );
    //
    // Then eventually inform everyone (including ourself)
    if ( p_notify )
        globals::publisher->publish( { bus::Message::config_updated } );
    //
    return true;
}

//-------------------------------------------------------------------------
// Called when configuration file changes
void Handler::monitoring_event ( const std::string &   p_file )
{
    read_config( true );
}

} // namespace config
} // namespace masnae
