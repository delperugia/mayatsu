// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

// Copyright 2018 Pierre del Perugia
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <curl/curl.h>

#include <adawat/adawat_string.h>

#include "masnae_global.h"
#include "masnae_http.h"
#include "masnae_misc.h"

namespace masnae {
namespace http {

//-------------------------------------------------------------------------
// Functions defined in masnae_http_openssl.cpp used to configure
// OpenSSL in a threaded environment
void http_open_ssl_start_locks ( void );
void http_open_ssl_stop_locks  ( void );

//-------------------------------------------------------------------------
// Function called by cURL to lock a data of the shared zone
// access could be one of CURL_LOCK_ACCESS_SHARED 1 / CURL_LOCK_ACCESS_SINGLE 2
// but we always do a full lock (and cURL doesn't use shared in 7.55 anyway)
// data typically range from 1 to 5 (CURL_LOCK_DATA_)
void cb_share_lock ( CURL * handle, curl_lock_data data, curl_lock_access access, void * useptr )
{
    auto * mutex = static_cast< std::unordered_map< int, std::mutex > * >( useptr );
    //
    ( * mutex ) [ data ].lock();
}

//-------------------------------------------------------------------------
// See cb_share_lock
void cb_share_unlock ( CURL * handle, curl_lock_data data, void * useptr )
{
    auto * mutex = static_cast< std::unordered_map< int, std::mutex > * >( useptr );
    //
    ( * mutex ) [ data ].unlock();
} 

//-------------------------------------------------------------------------
// Called by cURL when receiving the body. The user data is a std::string
// set before calling curl_easy_perform
size_t cb_write_callback ( char * ptr, size_t size, size_t nmemb, void * userdata )
{
    Data * output = static_cast< Data * >( userdata );
    //
    size_t  current_size = output->size();
    size_t  to_add       = size * nmemb;
    //
    output->resize( current_size + to_add );
    memcpy ( output->data() + current_size, ptr, to_add );
    //
    return size * nmemb;
}

//-------------------------------------------------------------------------
// Called by cURL when sending binary body. The user data is a ReadContext
// set before calling curl_easy_perform

    // This structure is passed to the read callback
    typedef struct { const Data *   data;   // data to send
                     size_t         ptr;    // current read pointer
                   } ReadContext;

size_t cb_read_callback ( char * ptr, size_t size, size_t nmemb, void * userdata )
{
    ReadContext * context = static_cast< ReadContext * >( userdata );
    //
    size_t  to_read    = size * nmemb;
    size_t  available  = context->data->size() - context->ptr;
    size_t  to_send    = std::min( to_read, available );
    //
    memcpy( ptr, context->data->data() + context->ptr, to_send );
    context->ptr += to_send;
    //
    return to_send;
}

//-------------------------------------------------------------------------
// Called by cURL when sending the binary body. The user data is a ReadContext
// set before calling curl_easy_perform
size_t cb_seek_callback ( void *userp, curl_off_t offset, int origin )
{
    ReadContext * context = static_cast< ReadContext * >( userp );
    //
    switch ( origin ) {
        case SEEK_SET:
            context->ptr  = static_cast<size_t>( offset );
            break;
        case SEEK_CUR:
            context->ptr  = static_cast<size_t>( 
                                static_cast<curl_off_t>( context->ptr ) + offset
                            );
            break;
        case SEEK_END:
            context->ptr  = static_cast<size_t>( 
                                static_cast<curl_off_t>( context->data->size() ) + offset
                            );
            break;
        default:
            return CURL_SEEKFUNC_FAIL;
    }
    //
    return CURL_SEEKFUNC_OK;
}

//-------------------------------------------------------------------------
Handler::~Handler ()
{
    stop();
}

//-------------------------------------------------------------------------
// Start, initialize cURL and the shared object
bool Handler::start ( void )
{
    //---------------------------
    const curl_ssl_backend ** availables = nullptr;
    //
    if ( curl_global_sslset( CURLSSLBACKEND_OPENSSL,
                             NULL,
                             & availables ) != CURLSSLSET_OK )
    {
        std::string text;
        //
        if ( availables != nullptr )
            for ( int i = 0; availables[i] != nullptr; i++ )
                text += availables[i]->name + std::string{" "};
        //
        if ( text.empty() )
            text = "none";
        //
        service_error_log( "HTTP OpenSSL backend not found. Availables: " + text );
        return false;
    }
    //
    http_open_ssl_start_locks();
    //
    //---------------------------
    if ( curl_global_init( CURL_GLOBAL_ALL ) != CURLE_OK )
    {
        http_open_ssl_stop_locks();
        service_error_log( "HTTP cURL global initialization failed" );
        return false;
    }
    //
    // Init the share object
    m_share_handle = curl_share_init();
    if ( m_share_handle == nullptr )
    {
        curl_global_cleanup();
        //
        http_open_ssl_stop_locks();
        //
        service_error_log( "HTTP cURL share initialization failed" );
        return false;
    }
    //
    // CURL_LOCK_DATA_CONNECT is not active before 7.57
    // Cookies are not shared since curl handler are shared between
    // threads doing different things
    bool ok = true;
    ok &= CURLSHE_OK == curl_share_setopt( m_share_handle, CURLSHOPT_SHARE,      CURL_LOCK_DATA_DNS         );
    ok &= CURLSHE_OK == curl_share_setopt( m_share_handle, CURLSHOPT_SHARE,      CURL_LOCK_DATA_SSL_SESSION );
    ok &= CURLSHE_OK == curl_share_setopt( m_share_handle, CURLSHOPT_SHARE,      CURL_LOCK_DATA_CONNECT     );
    ok &= CURLSHE_OK == curl_share_setopt( m_share_handle, CURLSHOPT_LOCKFUNC,   cb_share_lock              ); 
    ok &= CURLSHE_OK == curl_share_setopt( m_share_handle, CURLSHOPT_UNLOCKFUNC, cb_share_unlock            ); 
    ok &= CURLSHE_OK == curl_share_setopt( m_share_handle, CURLSHOPT_USERDATA,   & m_share_mutexes          );
    //
    if ( ! ok ) {
        curl_share_cleanup( m_share_handle );
        m_share_handle = nullptr;
        //
        curl_global_cleanup();
        //
        http_open_ssl_stop_locks();
        //
        service_error_log( "HTTP cURL share options failed" );
        return false;
    }
    //
    //---------------------------
    // Finally load config
    update_config();
    //
    globals::publisher->subscribe( this );
    //
    return true;
}

//-------------------------------------------------------------------------
// Stops
void Handler::stop ( void )
{
    if ( globals::publisher != nullptr )
        globals::publisher->unsubscribe( this );
    //
    if ( m_share_handle != nullptr ) {
        curl_share_cleanup( m_share_handle );
        m_share_handle = nullptr;
    }
    //
    curl_global_cleanup();
    //
    http_open_ssl_stop_locks();
}

//-------------------------------------------------------------------------
// Return statistics on the module
std::string Handler::specific_info ( void )
{
    uint64_t             nb_curl_handlers;
    SingleKeyValue_List  modules_data;
    //
    {
        std::lock_guard< std::mutex > lock (m_mutex_handles);
        //
        nb_curl_handlers = m_curl_handles.size();
    }
    //
    {
        std::shared_lock< std::shared_mutex > lock( m_mutex_modules );
        //
        for ( const auto & module: m_modules )
        {
            SingleKey_Value  data;
            //
            data [ "module" ] = module.first;
            //
            masnae::add_stats( "call_duration", module.second.call_duration, data );
            //
            modules_data.push_back( data );
        }
    }
    //
    return "{\"nb_curl_handlers\":" + std::to_string( nb_curl_handlers ) +
           ",\"modules\":"          + json_convert  ( modules_data     ) +
           "}";
}

//-------------------------------------------------------------------------
// Call URL passing parameters and attachments. Function will use either
// x-www-form-urlencoded or multipart/form-data
int Handler::request1 ( const std::string &     p_module_document,
                        const MultiKey_Value &  p_headers,
                        const MultiKey_Value &  p_parameters,
                        const KeyData &         p_attachments,
                        std::string &           p_content_type,
                        Data &                  p_response )
{
    // Get a handle from the pool
    CURL * curl = borrow_curl_handler();
    if ( curl == nullptr )
        return MSN_HTTP_ERR_NO_HANDLE;
    //
    // Note: these variables must persist until the end of the request
    std::string  parameters;
    curl_mime *  mime = nullptr;
    //
    // Configure body
    bool ok = true;
    int  result;
    //
    if ( p_attachments.empty() ) {
        if ( ! p_parameters.empty() ) {
            // The parameters are URL encoded and set as the body.
            // cURL will use POST and x-www-form-urlencoded
            ok &= fill_post_parameters( curl, p_parameters, parameters );
        } // else cURL will use GET
    } else {
        // We need to build a Mime document
        //
        if ( ( mime = curl_mime_init( curl ) ) == nullptr )
            ok &= false;  //-V753 VPS founds suspicious that the value is set that way to false
        else
            ok &= fill_post_mime( curl, p_parameters, p_attachments, mime );
    }
    //
    if ( ok )  // continue configuration and call the URL
        result = request_continue( curl, p_module_document, p_headers, p_content_type, p_response );
    else
        result = MSN_HTTP_ERR_NOT_CONFIGURE;
    //
    if ( mime != nullptr )
        curl_mime_free( mime );
    //
    return_curl_handler( curl );
    //
    return result;
}

//-------------------------------------------------------------------------
// Call URL passing the body. A header with the Content-Type should
// also be present.
int Handler::request2 ( const std::string &     p_module_document,
                        const MultiKey_Value &  p_headers,
                        const Data &            p_body,
                        std::string &           p_content_type,
                        Data &                  p_response )
{
    // Get a handle from the pool
    CURL * curl = borrow_curl_handler();
    if ( curl == nullptr )
        return MSN_HTTP_ERR_NO_HANDLE;
    //
    // This context variable will be passed to the read callback
    ReadContext context;
    //
    context.data = & p_body;    // data to send
    context.ptr  = 0;           // starting from offset 0
    //
    // Configure body
    bool ok = true;
    ok &= CURLE_OK == curl_easy_setopt( curl, CURLOPT_READFUNCTION, cb_read_callback );
    ok &= CURLE_OK == curl_easy_setopt( curl, CURLOPT_READDATA,     & context );
    ok &= CURLE_OK == curl_easy_setopt( curl, CURLOPT_SEEKFUNCTION, cb_seek_callback );
    ok &= CURLE_OK == curl_easy_setopt( curl, CURLOPT_SEEKDATA,     & context );
    ok &= CURLE_OK == curl_easy_setopt( curl, CURLOPT_POST,         1L);
    //
    int result;
    //
    if ( ok ) {
        // The Content-Length header is not set by cURL, it is added here
        MultiKey_Value headers = p_headers;                         // copy received headers
        headers.insert( std::pair( "Content-Length",                // add Content-Length
                                   std::to_string( p_body.size() )
                                 ) );
        //
        // Continue configuration and call URL
        result = request_continue( curl, p_module_document, headers, p_content_type, p_response );
    } else {
        result = MSN_HTTP_ERR_NOT_CONFIGURE;
    }
    //
    return_curl_handler( curl );
    //
    return result;
}

//-------------------------------------------------------------------------
// Call URL passing parameters and attachments. Function will use either
// x-www-form-urlencoded or multipart/form-data. Called server must
// return a valid JResp response. This function always return a JResp
// response (as a string and as a RapidJSON document), with status
// rejected if the call failed
void Handler::request_jresp ( const std::string &     p_module_document,
                              const MultiKey_Value &  p_headers,
                              const MultiKey_Value &  p_parameters,
                              const KeyData &         p_attachments,
                              rapidjson::Document &   p_response,
                              std::string  &          p_response_jresp )
{
    // Start by calling the URL
    std::string content_type;
    Data        body;
    //
    int result = request1 ( p_module_document,
                            p_headers,
                            p_parameters,
                            p_attachments,
                            content_type,
                            body );
    //
    // Build a JResp response if there is none
    if ( result == 200 ) {
        if ( is_json( content_type ) ) {
            p_response_jresp.assign( body.begin(), body.end() );
            //
            // It must be an UTF-8 string
            if ( ! is_utf8( p_response_jresp.c_str(), p_response_jresp.length() ) )
                p_response_jresp = "{\"status\":\"rejected\",\"reason\":\"invalid_utf8\",\"data\":null}";
            else
            if ( ! is_valid_jresp( p_response_jresp, p_response ) )  // set p_response
                p_response_jresp = "{\"status\":\"rejected\",\"reason\":\"invalid_jresp\",\"data\":null}";
            else
                return;
        } else
            // It was not a JSON response
            p_response_jresp = "{\"status\":\"rejected\",\"reason\":\"not_json\",\"data\":null}";
    }
    else
        // Request failed
        p_response_jresp = "{\"status\":\"rejected\",\"reason\":\"http\",\"data\":" +
                std::to_string( result ) + "}";
    //
    // Parse the built-in response
    p_response.Parse< rapidjson::kParseFullPrecisionFlag |
                      rapidjson::kParseCommentsFlag >( p_response_jresp.c_str() );
}

//-------------------------------------------------------------------------
// Call URL passing parameters and attachments. Function will use either
// x-www-form-urlencoded or multipart/form-data. Called server must
// return a valid JSON response. This function always return a JResp
// response (as a string and as a RapidJSON document), with status
// rejected if the call failed and JSON response in data field
void Handler::request_json ( const std::string &     p_module_document,
                             const MultiKey_Value &  p_headers,
                             const MultiKey_Value &  p_parameters,
                             const KeyData &         p_attachments,
                             rapidjson::Document &   p_response,
                             std::string  &          p_response_jresp )
{
    // Start by calling the URL
    std::string content_type;
    Data        body;
    //
    int result = request1 ( p_module_document,
                            p_headers,
                            p_parameters,
                            p_attachments,
                            content_type,
                            body );
    //
    // Build a JResp response if there is none
    if ( result == 200 ) {
        if ( is_json( content_type ) ) {
            p_response_jresp.assign( body.begin(), body.end() );
            //
            // It must be an UTF-8 string
            if ( ! is_utf8( p_response_jresp.c_str(), p_response_jresp.length() ) ) {
                p_response_jresp = "{\"status\":\"rejected\",\"reason\":\"invalid_utf8\",\"data\":null}";
            } else {
                // Build a JResp encapsulating the JSON response
                p_response_jresp = "{\"status\":\"success\",\"data\":" + p_response_jresp + "}";
                //
                // Parse it and exit on success
                p_response.Parse< rapidjson::kParseFullPrecisionFlag |
                                     rapidjson::kParseCommentsFlag >( p_response_jresp.c_str() );
                //
                if ( ! p_response.HasParseError() && p_response.IsObject() )
                    return;
                //
                // It was not a valid JSON document
                p_response_jresp = "{\"status\":\"rejected\",\"reason\":\"invalid_json\",\"data\":null}";
            }
        } else
            // It was not a JSON response
            p_response_jresp = "{\"status\":\"rejected\",\"reason\":\"not_json\",\"data\":null}";
    }
    else
        // Request failed
        p_response_jresp = "{\"status\":\"rejected\",\"reason\":\"http\",\"data\":" +
                std::to_string( result ) + "}";
    //
    // Parse the built-in response
    p_response.Parse< rapidjson::kParseFullPrecisionFlag |
                      rapidjson::kParseCommentsFlag >( p_response_jresp.c_str() );
}

//-------------------------------------------------------------------------
void Handler::bus_event ( const bus::Message &  p_message )
{
    switch ( p_message.type )
    {
        case masnae::bus::Message::config_updated:
            update_config();
            break;
        default:
            break;
    }
}

//-------------------------------------------------------------------------
// Get a new cURL handle from the pool. Before using a returned handle,
// the following option must be set:
//    CURLOPT_URL          URL to call
//    CURLOPT_WRITEDATA    pointer to a std::string receiving the body
CURL * Handler::borrow_curl_handler ( void )
{
    CURL * curl = nullptr;
    //
    // Try to find an available cURL handle
    {
        std::lock_guard< std::mutex > lock ( m_mutex_handles );
        //
        if ( ! m_curl_handles.empty() ) {
            curl = m_curl_handles.front();
            m_curl_handles.pop_front();
        }
    }
    //
    // Create a new one if needed
    if ( curl == nullptr )
        curl = curl_easy_init();
    //
    if ( curl == nullptr )
        return nullptr;
    //
    // Configure default option
    //
    curl_easy_reset( curl );  // clear any previous options
    //
    bool ok = true;
    //
    ok &= CURLE_OK == curl_easy_setopt( curl, CURLOPT_WRITEFUNCTION, cb_write_callback );
    ok &= CURLE_OK == curl_easy_setopt( curl, CURLOPT_SHARE,         m_share_handle );
    ok &= CURLE_OK == curl_easy_setopt( curl, CURLOPT_MAXCONNECTS,   5L );
    ok &= CURLE_OK == curl_easy_setopt( curl, CURLOPT_NOSIGNAL,      1L );
    //
    if ( ! ok ) {
        curl_easy_cleanup( curl );
        return nullptr;
    }
    //
    return curl;
}

//-------------------------------------------------------------------------
// Return a new cURL handle into the pool
void Handler::return_curl_handler ( CURL * &  p_curl )
{
    if ( p_curl != nullptr ) {
        std::lock_guard< std::mutex > lock ( m_mutex_handles );
        //
        m_curl_handles.push_back( p_curl );
        //
        p_curl = nullptr;
    }
}

//-------------------------------------------------------------------------
// Convert the key values into a x-www-form-urlencoded
// Return false on error
bool Handler::urlencode_kv ( CURL *                  p_curl,
                             const MultiKey_Value &  p_kv,
                             std::string &           p_encoded )
{
    p_encoded.clear();
    p_encoded.reserve( p_kv.size() * ( 18 + 1 + 10 ) );  // arbitrary average key+value size
    //
    for ( const auto kv: p_kv ) {
        char * key   = curl_easy_escape( p_curl,
                                         kv.first.c_str(),
                                         static_cast< int >( kv.first.length() ) );
        char * value = curl_easy_escape( p_curl,
                                         kv.second.c_str(),
                                         static_cast< int >( kv.second.length() ) );
        //
        if ( key != nullptr && value != nullptr ) {               // can only fail on memory error
            ( ( ( p_encoded += key ) += "=" ) += value ) += "&";  // add: key=value& to p_encoded
            //
            curl_free( key );
            curl_free( value );
        } else {
            if ( key != nullptr )   curl_free( key );
            if ( value != nullptr ) curl_free( value );
            //
            return false;
        }
    }
    //
    if ( p_encoded.length() > 0 )  // remove trailing &
        p_encoded.pop_back();
    //
    return true;
}

//-------------------------------------------------------------------------
// Encodes the parameters and set them in cURL. p_body is not a local
// variable because it needs to be persistent during cURL call
// Return false on error
bool Handler::fill_post_parameters ( CURL *                  p_curl,
                                     const MultiKey_Value &  p_parameters,
                                     std::string &           p_body )
{
    if ( ! urlencode_kv( p_curl, p_parameters, p_body ) )
        return false;
    //
    bool       ok   = true;
    curl_off_t size = static_cast< curl_off_t >( p_body.length() );
    //
    ok &= CURLE_OK == curl_easy_setopt( p_curl, CURLOPT_POSTFIELDS,           p_body.c_str() );
    ok &= CURLE_OK == curl_easy_setopt( p_curl, CURLOPT_POSTFIELDSIZE_LARGE,  size );
    //
    return ok;
}

//-------------------------------------------------------------------------
// Encodes the parameters and attachments in cURL. p_mime is not a local
// variable because it needs to be persistent during cURL call
// Return false on error
bool Handler::fill_post_mime ( CURL *                  p_curl,
                               const MultiKey_Value &  p_parameters,
                               const KeyData &         p_attachments,
                               curl_mime *             p_mime )
{
    bool            ok = true;
    curl_mimepart * part;
    //
    for ( const auto & parameter: p_parameters ) {
        if ( ( part = curl_mime_addpart( p_mime ) ) == nullptr )
            return false;
        //
        if ( p_attachments.count( parameter.first ) > 0 ) {
            const Data & data = p_attachments.at( parameter.first );
            //
            curl_mime_name( part, parameter.first.c_str() );
            curl_mime_type( part, parameter.second.c_str() );
            curl_mime_data( part,
                            reinterpret_cast< const char * >( data.data() ),
                            data.size() );
        } else {
            curl_mime_name( part, parameter.first.c_str() );
            curl_mime_data( part,
                            parameter.second.c_str(),
                            parameter.second.length() );
        }
    }
    //
    ok &= CURLE_OK == curl_easy_setopt( p_curl, CURLOPT_MIMEPOST, p_mime );
    //
    return ok;
}

//-------------------------------------------------------------------------
// Add the headers and make the call
int Handler::request_continue ( CURL *                  p_curl,
                                const std::string &     p_module_document,
                                const MultiKey_Value &  p_headers,
                                std::string &           p_content_type,
                                Data &                  p_response )
{
    struct  curl_slist * chunk = nullptr;
    bool                 ok    = true;
    //
    // Always add this header to disable cURL from sending Expect
    chunk = curl_slist_append( chunk, "Expect:" );
    ok   &= chunk != nullptr;
    //
    // Then add custom headers if any
    if ( ! p_headers.empty() ) {
        for ( const auto & header: p_headers ) {
            chunk = curl_slist_append( chunk, (header.first + ": " + header.second).c_str() );
            ok   &= chunk != nullptr;
        }
    }
    //
    // Apply headers
    ok &= CURLE_OK == curl_easy_setopt( p_curl, CURLOPT_HTTPHEADER, chunk );
    //
    if ( ! ok ) {
        if ( chunk != nullptr )
            curl_slist_free_all( chunk );
        //
        return MSN_HTTP_ERR_NOT_CONFIGURE;
    }
    //
    // Finish configuration and call URL
    int result = request_call( p_curl, p_module_document, p_content_type, p_response );
    //
    curl_slist_free_all( chunk );  // ok on nullptr
    return result;
}

//-------------------------------------------------------------------------
// The public 'request' functions above configure the request, this function
// executes it ans retrieves the result
int Handler::request_call ( CURL *               p_curl,
                            const std::string &  p_module_document,
                            std::string &        p_content_type,
                            Data &               p_response )
{
    // From "module/path/to/document" extract...
    std::string document = p_module_document;                   // path/to/document
    std::string module   = adawat::str_token( document, "/" );  // module
    //
    // Get the module configuration
    t_module_config module_config;
    //
    if ( ! get_module_config( module, module_config ) )
        return MSN_HTTP_ERR_NAME_NOT_FOUND;
    //
    // And finish cURL configuration
    bool ok = true;
    //
    ok &= CURLE_OK == curl_easy_setopt( p_curl, CURLOPT_WRITEDATA,      & p_response );          // body will be written there
    ok &= CURLE_OK == curl_easy_setopt( p_curl, CURLOPT_TIMEOUT,        module_config.timeout );
    ok &= CURLE_OK == curl_easy_setopt( p_curl, CURLOPT_CONNECTTIMEOUT, module_config.connect_timeout );
    ok &= CURLE_OK == curl_easy_setopt( p_curl, CURLOPT_URL,           ( module_config.url +
                                                                         document ).c_str() );  // no separator added, more flexible for user
    //
    if ( ! ok )
        return MSN_HTTP_ERR_NOT_CONFIGURE;
    //
    // Since the callback add to p_response, it is needed to clear it 
    p_content_type.clear();
    p_response.clear();
    //
    // Call the URL
    CURLcode result = curl_easy_perform( p_curl );
    long     code;
    //
    if ( result == CURLE_OK ) {
        char *      content_type     = nullptr;
        curl_off_t  downloaded_bytes = 0;
        //
        // Get the content type
        if ( curl_easy_getinfo( p_curl, CURLINFO_CONTENT_TYPE, & content_type )        == CURLE_OK  &&
             content_type != nullptr )
        {
            p_content_type = content_type;  // no error if content-type is missing
        }
        //
        // Get the HTTP code, check that the body received is complete
        if ( curl_easy_getinfo( p_curl, CURLINFO_RESPONSE_CODE,   & code )             != CURLE_OK  ||
             curl_easy_getinfo( p_curl, CURLINFO_SIZE_DOWNLOAD_T, & downloaded_bytes ) != CURLE_OK  ||
             p_response.size() != static_cast< size_t >( downloaded_bytes ) )
        {
            code = MSN_HTTP_ERR_BAD_RESPONSE;
        }
    } else {  // return the native cURL error (range 1-99)
        code = result;
    }
    //
    update_stats( p_curl, module, result );
    //
    return code;
}

//-------------------------------------------------------------------------
// Read config from config object
void Handler::update_config ( void )
{
    auto names = globals::config_handler->get_members( "/http/modules" ); // get all modules name
    //
    // New configuration
    decltype( m_modules )  new_configuration;
    //
    // Create a new entry per module
    for ( const auto & name: names ) {
        t_module_config config;
        //
        config.url =
            globals::config_handler->get_string( "/http/modules/" + name + "/url" );
        //
        config.timeout =
            globals::config_handler->get_int64( "/http/modules/" + name + "/timeout", 30 );
        //
        config.connect_timeout =
            globals::config_handler->get_int64( "/http/modules/" + name + "/connect_timeout", 10 );
        //
        new_configuration[ name ] = std::move( config );
    }
    //
    // And update the configuration
    {
        std::lock_guard< std::shared_mutex > lock( m_mutex_modules );
        //
        m_modules = std::move( new_configuration );
    }
}

//-------------------------------------------------------------------------
// Update call statistics for given module
void Handler::update_stats ( CURL *                 p_curl,
                             const std::string &    p_module,
                             long                   p_result )
{
    if ( p_result == CURLE_OK ) {
        double duration;
        //
        if ( curl_easy_getinfo( p_curl, CURLINFO_TOTAL_TIME, & duration ) == CURLE_OK  )
        {
            std::lock_guard< std::shared_mutex > lock( m_mutex_modules );
            //
            auto & module = m_modules[ p_module ];
            //
            module.call_duration.add_value( duration );
        }
    }
}

//-------------------------------------------------------------------------
// Retrieve the configuration of a module, false if not found
bool Handler::get_module_config ( const std::string &   p_name,
                                  t_module_config &     p_config )
{
    std::shared_lock< std::shared_mutex > lock( m_mutex_modules );
    //
    auto iter = m_modules.find( p_name );
    //
    if ( iter == m_modules.end() )
        return false;
    //
    p_config = iter->second;
    return true;
}

//-------------------------------------------------------------------------
// Check that the string is a valid JResp response and returns
// the corresponding RapidJSON document
bool Handler::is_valid_jresp ( const std::string &    p_jresp,
                               rapidjson::Document &  p_response ) const
{
    // Parse it: it is a JSon object
    p_response.Parse< rapidjson::kParseFullPrecisionFlag |
                         rapidjson::kParseCommentsFlag >( p_jresp.c_str() );
    //
    if ( p_response.HasParseError() || ! p_response.IsObject() )
        return false;
    //
    // The status is mandatory
    if ( ! p_response.HasMember("status") || ! p_response["status"].IsString() )
        return false;
    //
    // Then it depends on the status
    if ( p_response["status"] == "success" || p_response["status"] == "continue" )
    {
        if ( ! p_response.HasMember("data") )  // data must be present
            return false;
    }
    else if ( p_response["status"] == "error" )
    {
        if ( ! p_response.HasMember("message") )  // message must be present
            return false;
    }
    else if ( p_response["status"] == "rejected" )
    {
        if ( ! p_response.HasMember("reason") )  // message must be present
            return false;
        if ( ! p_response.HasMember("data") )  // data must be present
            return false;
    }
    else if ( p_response["status"] == "failed" )
    {
        // nothing to check
    }
    else
        return false; // unknown status
    //
    return true;
}

//-------------------------------------------------------------------------
// Check that the Content-Type is JSON
// p_content_type can be "application/json", "application/json; charset=utf-8"...
bool Handler::is_json ( const std::string &  p_content_type ) const
{
    constexpr
    const std::string_view  k_content_type_json = "application/json";
    //
    if ( ! adawat::str_starts_with( p_content_type, k_content_type_json ) )
        return false;
    //
    if ( p_content_type.length() == k_content_type_json.length() )
        return true;
    //
    return p_content_type[ k_content_type_json.length() ] == ';';
}

} // namespace http
} // namespace masnae
