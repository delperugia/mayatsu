// Copyright 2018 Pierre del Perugia
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

//
// Global variables
//

#ifndef masnae_global_h
#define masnae_global_h

#include <fastcgi++/manager.hpp>

#include "masnae_bus.h"
#include "masnae_config.h"
#include "masnae_mysql.h"
#include "masnae_http.h"
#include "masnae_logger.h"
#include "masnae_ws_caches.h"
#include "masnae_ws_tables.h"

namespace masnae {
namespace globals {

//-------------------------------------------------------------------------
// Before anything else the library must be initialized: the global
// variables must be instantiated in the main program and their addresses
// passed to the library.

// They are stored there
extern
std::string                   program_name;
//
extern
bus::Publisher *              publisher;
//
extern
Fastcgipp::Manager_base *     request_manager;
//
extern
config::Handler *             config_handler;
//
extern
mysql::Handler *              mysql_handler;
//
extern
ws_caches::Handler *          ws_caches_handler;
//
extern
ws_tables::Handler *          ws_tables_handler;
//
extern
http::Handler *               http_handler;
//
extern
logger::Handler *             logger_handler;

void init( const std::string &          p_program_name,
           bus::Publisher *             p_publisher,
           config::Handler *            p_config_handler,
           logger::Handler *            p_logger_handler,
           mysql::Handler *             p_mysql_handler,
           ws_tables::Handler *         p_ws_tables_handler,
           ws_caches::Handler *         p_ws_caches_handler,
           http::Handler *              p_http_handler,
           Fastcgipp::Manager_base *    p_request_manager );

} // namespace globals
} // namespace masnae

#endif
