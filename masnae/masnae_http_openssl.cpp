// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

// Copyright 2018 Pierre del Perugia
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <openssl/crypto.h>
#include <pthread.h>

namespace masnae {
namespace http {

//-------------------------------------------------------------------------
static
pthread_mutex_t *  g_HttpOpensslLocks = nullptr;

//-------------------------------------------------------------------------
static
void http_open_ssl_lock_callback ( int mode, int n, const char * file, int line )
{
    if ( mode & CRYPTO_LOCK )
        pthread_mutex_lock  ( & g_HttpOpensslLocks[ n ] );
    else
        pthread_mutex_unlock( & g_HttpOpensslLocks[ n ] );
}

//-------------------------------------------------------------------------
static
unsigned long http_open_ssl_thread_id ( void )
{
    return static_cast< unsigned long > ( pthread_self() );
}

//-------------------------------------------------------------------------
void http_open_ssl_start_locks ( void )
{
    g_HttpOpensslLocks = static_cast< pthread_mutex_t * > (
            malloc( static_cast< size_t >( CRYPTO_num_locks() )
                    *
                    sizeof( pthread_mutex_t ) ) );
    //
    if ( g_HttpOpensslLocks != nullptr ) {
        for ( int i = 0; i < CRYPTO_num_locks(); i++ )
            pthread_mutex_init( & g_HttpOpensslLocks[i], nullptr );
        //
        CRYPTO_set_id_callback      ( & http_open_ssl_thread_id );
        CRYPTO_set_locking_callback ( & http_open_ssl_lock_callback );
    }
}

//-------------------------------------------------------------------------
void http_open_ssl_stop_locks ( void )
{
    CRYPTO_set_id_callback     ( nullptr );
    CRYPTO_set_locking_callback( nullptr );
    //
    if ( g_HttpOpensslLocks != nullptr ) {
        for ( int i = 0; i < CRYPTO_num_locks(); i++ )
            pthread_mutex_destroy ( & g_HttpOpensslLocks[ i ] );
        //
        free( g_HttpOpensslLocks );
        g_HttpOpensslLocks = nullptr;
    }
}

} // namespace http
} // namespace masnae
