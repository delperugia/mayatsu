// Copyright 2018 Pierre del Perugia
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

//
// Common constants and types
//

#ifndef masnae_common_h
#define masnae_common_h

#include <fastcgi++/address.hpp>
#include <fastcgi++/fcgistreambuf.hpp>
#include <fastcgi++/request.hpp>
#include <ostream>
#include <string>

namespace masnae {

//-------------------------------------------------------------------------
// Alias for the FastCGI++ types used by derived classes
typedef Fastcgipp::Http::Environment< char >        Fastcpipp_Environment;
typedef Fastcgipp::FcgiStreambuf< char >            Fastcpipp_Stream;
typedef Fastcgipp::Address                          Fastcpipp_Address;
typedef std::multimap< std::string, std::string >   Fastcpipp_Params;  // headers, get, post, cookies...
typedef std::vector< std::string >                  Fastcpipp_Path;    // path, acceptLanguages

//-------------------------------------------------------------------------
// All data manipulations are done using this vector
typedef std::vector<uint8_t>                        Data;

// Convenient key-value containers...
typedef std::vector< std::string >                  KeyList;
typedef std::vector< std::string >                  ValueList;
typedef std::map< std::string, std::string >        SingleKey_Value;
typedef std::multimap< std::string, std::string >   MultiKey_Value;
typedef std::vector< SingleKey_Value >              SingleKeyValue_List;
typedef std::map< std::string, ValueList >          SingleKey_ValueList;
typedef std::map< std::string, SingleKey_Value >    SingleKey_SingleKey_Value;

// ... and a key-data map
typedef std::map< std::string, Data >               KeyData;

//-------------------------------------------------------------------------
// User identification and permissions associated to an execution context
struct SecurityContext
{
    // In Urania:                     table          field
    std::string      user;         // t_users        k_user
    std::string      profile;      //                k_profile
    std::string      role;         //                k_role
    //
    std::string      scopes;       // t_accesstokesn s_scopes
    //
    SingleKey_Value  permissions;  // indexed by permission, parameters
};

//-------------------------------------------------------------------------
// Contains the response to send back to the caller
class ProcessingResponse {
    public:
        // Constructed as a successful, empty, JResp answer
        ProcessingResponse          ( const std::string &  p_id = "" );
        //
        // Reset everything. If p_id is not set, keep the existing id
        void        reset           ( const std::string &  p_id = "" );
        //
        // Reset the HTTP response part
        void        reset_response  ( void );
        //
        //--------------
        // Depending on the type of response...
        //
        // ... JResp
        void            set_jresp_success  ( const std::string &  p_data );
        void            set_jresp_success  ( std::string &&       p_data );
        void            set_jresp_error    ( const std::string &  p_message );
        void            set_jresp_failed   ( void );
        void            set_jresp_rejected ( const std::string &  p_reason, 
                                             const std::string &  p_data );
        void            set_jresp_continue ( const std::string &  p_data );
        //
        bool            is_jresp_success   ( void ) const { return m_status == "success";  };
        bool            is_jresp_rejected  ( void ) const { return m_status == "rejected"; };
        bool            is_jresp_error     ( void ) const { return m_status == "error";    };
        const
        std::string &   get_jresp_status   ( void ) const { return m_status;  }  // if ! is_raw
        const
        std::string &   get_jresp_message  ( void ) const { return m_message; }  // if is_error
        const
        std::string &   get_jresp_reason   ( void ) const { return m_reason;  }  // if is_rejected
        const
        std::string &   get_jresp_data     ( void ) const { return m_data;    }  // if is_jresp_success or is_rejected
        //
        // ... raw
        void            set_raw            ( const Data &         p_body );  // add_header( "Content-Type", "..."); should be called
        void            set_raw            ( Data &&              p_body );
        void            set_raw            ( const std::string &  p_body );
        bool            is_raw             ( void ) const { return m_raw_response; }
        const      
        uint8_t *       get_raw_data       ( void ) const { return m_body.data(); }
        size_t          get_raw_size       ( void ) const { return m_body.size(); }
        //
        // ... and at the HTTP level
        void            set_http_status    ( const std::string &  p_status ) { m_http_status = p_status; }
        void            add_http_header    ( const std::string &  p_header,
                                             const std::string &  p_value );
        const  
        std::string &   get_http_status    ( void ) const                    { return m_http_status; }
        //
        //--------------
        // Error reporting to the web server log (not sent to caller)
        void            add_error ( const std::string &  p_message );
        bool            has_error ( void ) const { return ! m_error_message.empty(); }
        const                                    
        std::string &   get_error ( void ) const { return m_error_message; }
        //
        //--------------
        // Logs of the request (not sent to caller)
        enum log_level : char { regular = 'R', verbose = 'V', debug = 'D' };
        //
        void            add_log         ( log_level p_level, const std::string &  p_message );
        bool            has_log         ( void ) const { return ! m_log_message.empty(); }
        const   
        std::string &   get_log         ( void ) const { return m_log_message; }
        //
        void            disable_log     ( void );  // do not store log for this request
        bool            is_log_disabled ( void ) const { return m_log_disable; }
        //
        //--------------
        // Returns the response id
        std::string     get_id    ( void ) const { return m_id; }
        //
        //--------------
        // Functions called by RequestHandler to send generate the response
        //
        // Format and send the headers
        void    send_headers    ( std::ostream &    p_out );
        // 
        // Format and send JResp answer
        void    send_jresp      ( std::ostream &    p_out );
        //
        //------------------------------------------
        // Prevent copy on the object
        ProcessingResponse                ( const ProcessingResponse & ) = delete;
        ProcessingResponse & operator =   ( const ProcessingResponse & ) = delete;
        //
    private:
        std::string       m_http_status;    // HTTP response status 
        Fastcpipp_Params  m_headers;        // headers to send in response
        //
        bool              m_raw_response;   // not an encapsulated JResp answer
        //
        // JResp answer
        std::string       m_status;         // success, error, failed, rejected, continue
        std::string       m_data;           //  x                       x         x         json string
        std::string       m_message;        //           x                                  text message
        std::string       m_reason;         //                          x                   text code
        // 
        // Raw answer
        Data              m_body;           // the body
        //
        std::string       m_id;             // the request id associated to this response
        std::string       m_error_message;  // error message to store in web server error log
        std::string       m_log_message;    // request processing to log
        bool              m_log_disable;    // do not store log for this request
};

} // namespace masnae

#endif
