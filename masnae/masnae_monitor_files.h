// Copyright 2018 Pierre del Perugia
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef masnae_monitor_files_h
#define masnae_monitor_files_h

#include <string>
#include <thread>

namespace masnae {

class MonitorFiles
{
    public:
        MonitorFiles             ( const char *          p_name);
        //
        // Start and stop monitoring on the given file / folder
        bool    start_monitoring ( const std::string &   p_path,
                                   bool                  p_is_folder );
        void    stop_monitoring  ( void );
        //
        // Function called when a file is written
        virtual
        void    monitoring_event ( const std::string &   p_file ) = 0;
        //
        // Disable object copy
        MonitorFiles             ( const MonitorFiles & ) = delete;
        void operator=           ( const MonitorFiles & ) = delete;
        //
    private:
        std::string     m_monitor_name;               // module name
        //
        bool            m_monitoring_folder = false;  // monitoring file or folder
        std::string     m_monitored_folder;           // folder monitored
        std::string     m_monitored_file;             // file name if monitoring file
        //
        int             m_notify_handle     = -1;     // handle of the inotify monitor
        int             m_notify_watch      = -1;     // programmed watch descriptor
        std::thread     m_monitoring_thread;          // thread monitoring script folder

        // The thread doing the polling on inotify
        static
        void     thread_monitoring      ( MonitorFiles *  p_self );
        //
        // Called when a file is written in the monitored folder
        void     monitor_file_written   ( const char *    p_file );
};

} // namespace masnae

#endif
