// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

// Copyright 2018 Pierre del Perugia
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "masnae_global.h"

namespace masnae {
namespace globals {

//-------------------------------------------------------------------------
// The global vars where addresses are stored
std::string                 program_name;
bus::Publisher *            publisher         = nullptr;
Fastcgipp::Manager_base *   request_manager   = nullptr;
config::Handler *           config_handler    = nullptr;
mysql::Handler *            mysql_handler     = nullptr;
ws_caches::Handler *        ws_caches_handler = nullptr;
ws_tables::Handler *        ws_tables_handler = nullptr;
http::Handler *             http_handler      = nullptr;
logger::Handler *           logger_handler    = nullptr;

//-------------------------------------------------------------------------
// Before anything else the library must be initialized: the global
// variables must be instantiated in the main program and their addresses
// passed to the library.
void init( const std::string &          p_program_name,
           bus::Publisher *             p_publisher,
           config::Handler *            p_config_handler,
           logger::Handler *            p_logger_handler,
           mysql::Handler *             p_mysql_handler,
           ws_tables::Handler *         p_ws_tables_handler,
           ws_caches::Handler *         p_ws_caches_handler,
           http::Handler *              p_http_handler,
           Fastcgipp::Manager_base *    p_request_manager )
{
    globals::program_name       = p_program_name;
    globals::publisher          = p_publisher;
    globals::config_handler     = p_config_handler;
    globals::logger_handler     = p_logger_handler;
    globals::mysql_handler      = p_mysql_handler;
    globals::ws_tables_handler  = p_ws_tables_handler;
    globals::ws_caches_handler  = p_ws_caches_handler;
    globals::http_handler       = p_http_handler;
    globals::request_manager    = p_request_manager;
}

} // namespace globals
} // namespace masnae

