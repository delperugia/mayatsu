// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

// Copyright 2018 Pierre del Perugia
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <rapidjson/document.h>

#include "masnae_global.h"
#include "masnae_misc.h"
#include "masnae_ws_tables.h"

//-------------------------------------------------------------------------
namespace masnae {
namespace ws_tables {

constexpr size_t k_max_rows = 1000;  // maximum number of rows in a SLEECT result

namespace jsonsql {
    
//-------------------------------------------------------------------------
// jsonsql::filter
//-------------------------------------------------------------------------

//--------------------------------------------------------------------
// Convert a JSON SQL expression into SQL in p_output. On error,
// returns false and set the reason
bool filter::convert( const std::string &  p_json,
                      std::string &        p_output )
{
    if ( p_json.empty() ) {
        p_output = "Empty JSON expression";
        return false;
    }
    //
    rapidjson::Document  json;
    //
    json.Parse< rapidjson::kParseFullPrecisionFlag |
                rapidjson::kParseCommentsFlag >( p_json.c_str() );
    //
    try {
        if ( json.HasParseError() )
            throw std::runtime_error( "Invalid JSON expression" );
        //
        p_output = parse_object( "", json );
        return true;
    }
    catch ( const std::exception & e ) {
        p_output = e.what();
        return false;
    }
}

//--------------------------------------------------------------------
// ... a field name
std::string filter::parse_field( const rapidjson::Value &  p_value )
{
    if ( ! p_value.IsString() )
        throw std::runtime_error( "invalid field name" );
    //
    std::string field = p_value.GetString();
    //
    if ( ! valid_field( field ) )
        throw std::runtime_error( "unknown field '" + field + "'" );
    //
    return "`" + mysql::sql_escape( field ) + "`";
}

//--------------------------------------------------------------------
// ... a value
std::string filter::parse_value( const std::string &       p_field,
                                 const rapidjson::Value &  p_value )
{
    std::string  value;
    char         type;
    //
         if ( p_value.IsNull()   )   type = '?', value = "NULL";
    else if ( p_value.IsString() )   type = 'S', value = "'" + mysql::sql_escape( p_value.GetString() ) + "'";
    else if ( p_value.IsUint64()  )  type = 'U', value = std::to_string  ( p_value.GetUint64()  );
    else if ( p_value.IsInt64()  )   type = 'I', value = std::to_string  ( p_value.GetInt64()  );
    else if ( p_value.IsDouble() )   type = 'D', value = std::to_string  ( p_value.GetDouble() );
    else if ( p_value.IsObject() )   type = 'O', value = parse_object    ( p_field, p_value    );
    else
        throw std::runtime_error( "Invalid value" );
    //
    if ( ! field_accept( p_field, type ) )
        throw std::runtime_error( "Invalid type for '" + p_field + "'" );
    //
    return value;
}

//--------------------------------------------------------------------
// ... an array of items. The delimiter inserted between items
// is context dependent. Note: if spaces are needed around the
// separator, the caller must set them.
std::string filter::parse_array( const std::string &       p_field,
                                 const rapidjson::Value &  p_value,
                                 const std::string &       p_separator )
{
    if ( ! p_value.IsArray() )
        throw std::runtime_error( "Array of items expected" );
    //
    std::string result;
    //
    for ( const auto & item: p_value.GetArray() ) {
        if ( ! result.empty() )
            result += p_separator;
        //
        result += parse_value( p_field, item );
    }
    //
    return result;
}

//--------------------------------------------------------------------
// ... the member of an object: either a field / value, a comparison
// or an OR
std::string filter::parse_member( const std::string &               p_field,
                                  const rapidjson::Value::Member &  p_member )
{
    if ( ! p_member.name.IsString() )
        throw std::runtime_error( "Invalid object property name" );
    //
    std::string operand = p_member.name.GetString();
    //
    // Comparisons...
         if ( operand == "$in"  )       return " IN ("     + parse_array( p_field, p_member.value, "," )     + ")";
    else if ( operand == "$nin" )       return " NOT IN (" + parse_array( p_field, p_member.value, "," )     + ")";
    else if ( operand == "$gt"  )       return " > "       + parse_value( p_field, p_member.value );
    else if ( operand == "$gte" )       return " >= "      + parse_value( p_field, p_member.value );
    else if ( operand == "$lt"  )       return " < "       + parse_value( p_field, p_member.value );
    else if ( operand == "$lte" )       return " <= "      + parse_value( p_field, p_member.value );
    //
    // ... but SQL does not use = to compare NULL
    else if ( operand == "$eq"  ) {
        if ( p_member.value.IsNull() )  return " IS "      + parse_value( p_field, p_member.value );
        else                            return " = "       + parse_value( p_field, p_member.value );        
    } else if ( operand == "$ne" ) {
        if ( p_member.value.IsNull() )  return " IS NOT "  + parse_value( p_field, p_member.value );
        else                            return " <> "      + parse_value( p_field, p_member.value );
    }
    //
    // Logical OR conjunction
    else if ( operand == "$or" )        return "("         + parse_array( p_field, p_member.value, " OR " )  + ")";
    //
    // If value is neither a comparison nor a OR, it must be a plain
    // value, to compare with a regular "=" (implicit { \"$eq\": X })
    else {
        std::string result;
        //
        result += parse_field( p_member.name );
        //
        // Implicit $eq if the value is not an object. If it is an object,
        // it must be a comparison and parse_value will add the operand
        if ( p_member.value.IsNull() )
            result += " IS ";
        else if ( ! p_member.value.IsObject() )
            result += " = ";
        //
        result += parse_value( p_member.name.GetString(), p_member.value );  // p_member.name was checked as string by parse_field above
        //
        return result;
    }
}

//--------------------------------------------------------------------
// ... an object: either a comparison, or the main expression
std::string filter::parse_object( const std::string &       p_field,
                                  const rapidjson::Value &  p_value )
{
    if ( ! p_value.IsObject() )
        throw std::runtime_error( "Object expected" );
    //
    std::string result;
    //
    for ( const auto & m : p_value.GetObject() ) {
        if ( ! result.empty() )
            result += " AND ";
        //
        result += parse_member( p_field, m );
    }
    //
    // Comparison must not have parenthesis (it would generated:
    //    a (= 1)
    // and parenthesis are not a problem for the main expression
    if ( p_value.GetObject().MemberCount() > 1 ) 
        result = "(" + result + ")";
    //
    return result;
}

} // namespace jsonsql

//-------------------------------------------------------------------------
// Definition
//-------------------------------------------------------------------------

//--------------------------------------------------------------------
Definition::Definition( const std::string &  p_name ) :
    m_table_name( p_name )
{
    if ( globals::ws_tables_handler != nullptr )
        globals::ws_tables_handler->add_table( this );
}

//--------------------------------------------------------------------
void Definition::execute ( const std::string &   p_command,
                           const std::string &   p_fields,
                           const std::string &   p_filter,
                           const std::string &   p_order,
                           const std::string &   p_range,
                           ProcessingResponse &  p_result )
{
    std::string sql;
    //
    try {
        //
        // Build the SQL query then
        // Execute the query, build and set JSON response in p_result
        //
        if ( p_command == "INSERT" ||
             p_command == "insert" )  build_resp_insert( build_sql_insert( p_fields ),
                                                         p_result );
        else
        if ( p_command == "UPDATE" ||
             p_command == "update" )  build_resp_update( build_sql_update( p_fields, p_filter, p_order, p_range ),
                                                         p_result );
        else
        if ( p_command == "SELECT" ||
             p_command == "select" )  build_resp_select( build_sql_select( p_fields, p_filter, p_order, p_range ),
                                                         p_result );
        else
        if ( p_command == "DELETE" ||
             p_command == "delete" )  build_resp_delete( build_sql_delete( p_filter,           p_order, p_range ),
                                                         p_result );
        else
            throw std::runtime_error( "Unknown operation" );
        //
    } catch ( const std::exception & e ) {
        p_result.set_jresp_error( e.what() );
    }
}

//--------------------------------------------------------------------
// The following functions build the SQL request to execute. They
// throw an std::exception on error...

// ... for INSERT
std::string Definition::build_sql_insert ( const std::string &  p_fields_values )
{
    std::set< std::string > fields_with_value, fields_with_null_value;
    SingleKey_Value         kv_fields = build_field_with_value( p_fields_values,
                                                                fields_with_value,
                                                                fields_with_null_value );
    //
    std::string sql_fields, sql_values;
    //
    // Builds the two SQL strings: list of fields and list of values
    if ( kv_fields.empty() )
        throw std::runtime_error( "Fields: no field specified" );
    //
    for ( const auto & field: kv_fields ) {
        if ( ! valid_field_for_insert( field.first ) )
            throw std::runtime_error( "Fields: '" + field.first + "' invalid for insert" );
        //
        sql_fields += ",`" + mysql::sql_escape( field.first ) + "`";
        sql_values += ","  + field.second;  // already escaped/quoted by build_field_with_value
    }
    //
    // Check that all required fields are present and set to non-empty/NULL value
    for ( const auto & field: m_fields )
        if ( field.second.required && fields_with_value.count( field.first ) == 0 )
            throw std::runtime_error( "Fields: '" + field.first + "' must be set" );
    //
    // If a field is missing, uses its default value (if any)
    for ( const auto & field: m_fields )
        if ( field.second.default_insert != nullptr         &&   // has a default
             fields_with_value.count( field.first ) == 0    &&   // but no value
             fields_with_null_value.count( field.first ) == 0 )  // (null or not)
        {
            sql_fields += ",`" + mysql::sql_escape( field.first ) + "`";
            sql_values += ","  + std::string( field.second.default_insert );  // default is a raw SQL text
        }
    //
    // Replace leading commas by space
    sql_fields[0] = ' ';
    sql_values[0] = ' ';
    //
    std::string sql = "INSERT INTO " + m_table_name +
                      " (" + sql_fields + ") VALUES (" + sql_values + ")";
    //
    return sql;
}

// ... for UPDATE
std::string Definition::build_sql_update ( const std::string &  p_fields_values,
                                           const std::string &  p_filter,
                                           const std::string &  p_order,
                                           const std::string &  p_range )
{
    std::set< std::string > fields_with_value, fields_with_null_value;
    SingleKey_Value         kv_fields = build_field_with_value( p_fields_values,
                                                                fields_with_value,
                                                                fields_with_null_value );
    //
    std::string sql_assignements;
    //
    // Builds the SQL string with the list of field=value
    if ( kv_fields.empty() )
        throw std::runtime_error( "Fields: no field specified" );
    //
    for ( const auto & field: kv_fields ) {
        if ( ! valid_field_for_update( field.first ) )
            throw std::runtime_error( "Fields: '" + field.first + "' invalid for update" );
        //
        sql_assignements += ",`" + mysql::sql_escape( field.first ) + "`=" +
                            field.second;  // already escaped/quoted by build_field_with_value
    }
    //
    // Check that all required fields are set to non-empty/NULL value
    for ( const auto & field: m_fields )
        if ( field.second.required && fields_with_null_value.count( field.first ) == 1 )
            throw std::runtime_error( "Fields: '" + field.first + "' must have a value" );
    //
    // If a field is missing, uses its default value (if any)
    for ( const auto & field: m_fields )
        if ( field.second.default_update != nullptr         &&   // has a default
             fields_with_value.count( field.first ) == 0    &&   // but no value
             fields_with_null_value.count( field.first ) == 0 )  // (null or not)
        {
            sql_assignements += ",`" + mysql::sql_escape( field.first ) + "`=" +
                                field.second.default_update;  // default is a raw SQL text
        }
    //
    // Replace leading comma with a space
    sql_assignements[0] = ' ';
    //
    std::string sql = "UPDATE " + m_table_name + " SET " + sql_assignements;
    //
    if ( ! p_filter.empty() )  sql += " WHERE "    + build_filter  ( p_filter   );
    if ( ! p_order.empty() )   sql += " ORDER BY " + build_order   ( p_order    );
    if (   p_range.empty() )   sql += " LIMIT "    + std::to_string( k_max_rows );
    else                       sql += " LIMIT "    + build_range   ( p_range    );
    //
    return sql;
}

// ... for SELECT
std::string Definition::build_sql_select ( const std::string &  p_fields_list,
                                           const std::string &  p_filter,
                                           const std::string &  p_order,
                                           const std::string &  p_range )
{
    KeyList k_fields = build_field( p_fields_list );
    //
    std::string sql_fields;
    //
    // Builds the SQL string with the list of field=value
    if ( k_fields.empty() )
        throw std::runtime_error( "Fields: no field specified" );
    //
    for ( const auto & field: k_fields ) {
        if ( ! valid_field( field ) )
            throw std::runtime_error( "Fields: unknown field '" + field + "'" );
        //
        sql_fields += ",`" + mysql::sql_escape( field ) + "`";
    }
    //
    // Replace leading comma with a space
    sql_fields[0] = ' ';
    //
    std::string sql = "SELECT " + sql_fields + " FROM " + m_table_name;
    //
    if ( ! p_filter.empty() )  sql += " WHERE "    + build_filter  ( p_filter   );
    if ( ! p_order.empty() )   sql += " ORDER BY " + build_order   ( p_order    );
    if (   p_range.empty() )   sql += " LIMIT "    + std::to_string( k_max_rows );
    else                       sql += " LIMIT "    + build_range   ( p_range    );
    //
    return sql;
}

// ... for DELETE
std::string Definition::build_sql_delete ( const std::string &   p_filter,
                                           const std::string &   p_order,
                                           const std::string &   p_range )
{
    std::string sql = "DELETE FROM " + m_table_name;
    //
    if ( ! p_filter.empty() )  sql += " WHERE "    + build_filter  ( p_filter   );
    if ( ! p_order.empty() )   sql += " ORDER BY " + build_order   ( p_order    );
    if (   p_range.empty() )   sql += " LIMIT "    + std::to_string( k_max_rows );
    else                       sql += " LIMIT "    + build_range   ( p_range    );
    //
    return sql;
}

//--------------------------------------------------------------------
// The following functions execute the SQL request and build
// a JResp response...

// ... for INSERT
void Definition::build_resp_insert ( const std::string &   p_sql,
                                     ProcessingResponse &  p_result )
{
    std::string        id;
    mysql::Connection  db = globals::mysql_handler->get_connection( mysql::writer );
    //
    unsigned db_result = db.do_insert( p_sql, id );
    if ( db_result == 0 )
        p_result.set_jresp_success( json_quote_string( id ) );
    else
        mysql::build_resp_error( db_result, p_result );
}

// ... for UPDATE
void Definition::build_resp_update ( const std::string &   p_sql,
                                     ProcessingResponse &  p_result )
{
    size_t             affected;
    mysql::Connection  db = globals::mysql_handler->get_connection( mysql::writer );
    //
    unsigned db_result = db.do_update( p_sql, affected );
    if ( db_result == 0 )
        p_result.set_jresp_success( std::to_string( affected ) );
    else
        mysql::build_resp_error( db_result, p_result );
}

// ... for SELECT 
void Definition::build_resp_select ( const std::string &   p_sql,
                                     ProcessingResponse &  p_result )
{
    SingleKeyValue_List  rows;
    mysql::Connection    db = globals::mysql_handler->get_connection( mysql::reader );
    //
    // Reconfigure connection limit with our own limit
    auto previous_max = db.set_max_results( k_max_rows );
    //
    // Execute the SELECT
    unsigned db_result = db.do_select( p_sql, rows );
    if ( db_result == 0 )
        p_result.set_jresp_success( json_convert( rows ) );
    else
        mysql::build_resp_error( db_result, p_result );
    //
    // Restore connection configuration
    db.set_max_results( previous_max );
}

// ... for DELETE
void Definition::build_resp_delete ( const std::string &   p_sql,
                                     ProcessingResponse &  p_result )
{
    size_t             affected;
    mysql::Connection  db = globals::mysql_handler->get_connection( mysql::writer );
    //
    unsigned db_result = db.do_delete( p_sql, affected );
    if ( db_result == 0 )
        p_result.set_jresp_success( std::to_string( affected ) );
    else
        mysql::build_resp_error( db_result, p_result );
}

//--------------------------------------------------------------------
// Extracts from a JSON object two lists (where properties representing fields):
//   - properties with value
//   - properties with value null or empty
// and returns the map of field / value (in SingleKey_Value)
// Field names are copied as they are (not escaped, not quoted)
// Values are escaped and quoted if needed
// e.g.:  {"name":"joe","age":42}
SingleKey_Value Definition::build_field_with_value ( const std::string &        p_fields,
                                                     std::set< std::string > &  p_fields_with_value,
                                                     std::set< std::string > &  p_fields_with_null_value )
{
    SingleKey_Value  result;
    //
    if ( p_fields.empty() )
        return result;
    //
    // Parse the JSON object
    rapidjson::Document  json;
    //
    json.Parse< rapidjson::kParseFullPrecisionFlag |
                rapidjson::kParseCommentsFlag >( p_fields.c_str() );
    //
    if ( json.HasParseError()  || ! json.IsObject() )
        throw std::runtime_error( "Fields: invalid JSON expression" );
    //
    // Collect all members, escape texts
    std::string      field, value;
    char             type;
    //
    for ( const auto & m : json.GetObject() )
    {
        // Name must be string
        if ( m.name.IsString() )
            field = m.name.GetString();
        else
            throw std::runtime_error( "Fields: invalid name" );
        //
        // Value type is detected:
             if ( m.value.IsNull()   )  type = '?', value = "NULL";
        else if ( m.value.IsString() )  type = 'S', value = "'" + mysql::sql_escape( m.value.GetString() ) + "'";
        else if ( m.value.IsUint64() )  type = 'U', value = std::to_string( m.value.GetUint64()  );
        else if ( m.value.IsInt64()  )  type = 'I', value = std::to_string( m.value.GetInt64()   );
        else if ( m.value.IsDouble() )  type = 'D', value = std::to_string( m.value.GetDouble()  );
        else
            throw std::runtime_error( "Fields: invalid value for '" + field + "'" );
        //
        // Checks
        if ( ! valid_field( field ) )
            throw std::runtime_error( "Fields: unknown field '" + field + "'" );
        //
        if ( ! field_accept( field, type ) )
            throw std::runtime_error( "Fields: invalid type for '" + field + "'" );
        //
        // Keep the list of all non-empty fields. build_sql_insert/update will check the 'required' flag
        // Note: number cannot be empty
        if ( ( m.value.IsNull() )
             ||
             ( m.value.IsString() && m.value.GetStringLength() == 0 ) )
            p_fields_with_null_value.insert( field );
        else
            p_fields_with_value.insert( field );
        //
        result.insert( std::pair( field, value) );
    }
    //
    return result;
}

//--------------------------------------------------------------------
// Converts a JSON array of field names into a SQL list of fields
// Field names are copied as they are (not escaped, not quoted)
// e.g.: ["name","age"]
KeyList Definition::build_field ( const std::string &  p_fields )
{
    KeyList  result;
    //
    if ( p_fields.empty() )
        return result;
    //
    // Parse the JSON object
    rapidjson::Document  json;
    //
    json.Parse< rapidjson::kParseFullPrecisionFlag |
                rapidjson::kParseCommentsFlag >( p_fields.c_str() );
    //
    if ( json.HasParseError() || ! json.IsArray() )
        throw std::runtime_error( "Fields: invalid JSON expression" );
    //
    // Collect field names
    std::string field_name;
    //
    for ( const auto & field: json.GetArray() )
    {
        if ( field.IsString() )
            field_name = field.GetString();
        else
            throw std::runtime_error( "Fields: invalid name" );
        //
        result.push_back( field_name );
    }
    //
    return result;
}

//--------------------------------------------------------------------
// Convert a JSON filter into a SQL WHERE
// See jsonsql::filter
// E.g.:    { \"status\": { \"$in\": [ \"A\", \"D\" ] } }
std::string Definition::build_filter ( const std::string &  p_filter )
{
    std::string result;
    //
    if ( convert( p_filter, result ) )
        return result;
    //
    throw std::runtime_error( "Filter: " + result );
}

//--------------------------------------------------------------------
// Convert a JSON order into an SQL expression
// To each field to sort on, a positive or negative number is set
// Positive means Ascending, negative means Descending
// JSON example:     {"name":1,"age":-1}
std::string Definition::build_order ( const std::string &  p_order )
{
    if ( p_order.empty() )
        return "";
    //
    // Parse the JSON object
    rapidjson::Document  json;
    //
    json.Parse< rapidjson::kParseFullPrecisionFlag |
                rapidjson::kParseCommentsFlag >( p_order.c_str() );
    //
    if ( json.HasParseError() || ! json.IsObject() )
        throw std::runtime_error( "Order: invalid JSON expression" );
    //
    // Collect all members
    std::string sql, field;
    int         direction;
    //
    for ( const auto & m : json.GetObject() )
    {
        if ( m.name.IsString() )
            field = m.name.GetString();
        else
            throw std::runtime_error( "Order: invalid field name" );
        //
        if ( m.value.IsInt() )
            direction = m.value.GetInt();
        else
            throw std::runtime_error( "Order: invalid value" );
        //
        if ( ! valid_field( field ) )
            throw std::runtime_error( "Order: unknown field name '" + field + "'" );
        //
        if ( ! sql.empty() )
            sql += ",";
        //
        sql += "`" + mysql::sql_escape( field ) + "`";
        //
        if ( direction < 0 )  // Note: 0 is ASC
            sql += " DESC";
    }
    //
    return sql;
}

//--------------------------------------------------------------------
// Convert a JSON range into an SQL LIMIT (MySQL syntax)
// Upper bound is excluded
// JSON example: [ 0, 5 ]
std::string Definition::build_range ( const std::string &  p_range )
{
    if ( p_range.empty() )
        return "";
    //
    // Parse the JSON object
    rapidjson::Document  json;
    //
    json.Parse< rapidjson::kParseFullPrecisionFlag |
                rapidjson::kParseCommentsFlag >( p_range.c_str() );
    //
    if ( json.HasParseError() || ! json.IsArray() )
        throw std::runtime_error( "Range: invalid JSON expression" );
    //
    // Collect bounds
    if ( json.GetArray().Size() != 2 )
        throw std::runtime_error( "Range: invalid size" );
    //
    for ( const auto & item: json.GetArray() )
        if ( ! item.IsUint() )
            throw std::runtime_error( "Range: invalid value" );
    //
    size_t from = json.GetArray()[ 0 ].GetUint();
    size_t to   = json.GetArray()[ 1 ].GetUint();
    //
    if ( from >= to )
        throw std::runtime_error( "Range: invalid bounds" );
    //
    if ( ( to - from ) > k_max_rows )
        throw std::runtime_error( "Range: too large" );
    //
    // MySQL syntax is: LIMIT offset, row_count
    return std::to_string( from ) + "," + std::to_string( to - from );
}

//--------------------------------------------------------------------
// Check that the field can be used in INSERT
bool Definition::valid_field_for_insert ( const std::string &  p_field )
{
    return m_fields.count( p_field ) == 1 &&
           m_fields[ p_field ].in_insert;
}

//--------------------------------------------------------------------
// Check that the field can be used in UPDATE
bool Definition::valid_field_for_update ( const std::string &  p_field )
{
    return m_fields.count( p_field ) == 1 &&
           m_fields[ p_field ].in_update;
}

//--------------------------------------------------------------------
// Check that the field exists
bool Definition::valid_field ( const std::string &  p_field  )
{
    return m_fields.count( p_field ) == 1;
}

//--------------------------------------------------------------------
// Check that the given type is compatible with the type of a column
// p_type is one of: ? S U I D (? is compatible with all types)
bool Definition::field_accept ( const std::string &   p_field,
                                char                  p_type )
{
    if ( m_fields.count( p_field ) == 1 ) {
        switch ( m_fields[ p_field ].type ) {
            case 'S': return p_type == '?' || p_type == 'S';
            case 'U': return p_type == '?' || p_type == 'U';
            case 'I': return p_type == '?' || p_type == 'U' || p_type == 'I';  // do not check overflow, let the SQL sever do it
            case 'D': return p_type == '?' || p_type == 'U' || p_type == 'I' || p_type == 'D';
            default : return false;
        }
    } else {
        return false;
    }
}

//-------------------------------------------------------------------------
// Handler
//-------------------------------------------------------------------------

//-------------------------------------------------------------------------
Handler::~Handler()
{
    stop();
}

//-------------------------------------------------------------------------
bool Handler::start ( void )
{
    if ( m_running )
        return false;
    //
    globals::publisher->subscribe( this );
    //
    m_running = true;
    return true;
}

//-------------------------------------------------------------------------
void Handler::stop ( void )
{
    if ( globals::publisher != nullptr )
        globals::publisher->unsubscribe( this );
    //
    m_running = false;
}

//-------------------------------------------------------------------------
// Return statistics on the module
std::string Handler::specific_info ( void )
{
    KeyList  tables;
    //
    for ( const auto & table: m_tables )
        tables.push_back( table.first );
    //
    return "{\"tables\":" + json_convert( tables ) +
           "}";
}

//-------------------------------------------------------------------------
// Execute the /mgt/tables command:
// returns the list of tables
void Handler::get_tables ( ProcessingResponse &  p_result )
{
    KeyList  tables;
    //
    for ( const auto & table: m_tables )
        tables.push_back( table.first );
    //
    p_result.set_jresp_success( json_convert( tables ) );
}

//-------------------------------------------------------------------------
// Execute the given command on the requested table
void Handler::execute ( const std::string &   p_table,
                        const std::string &   p_command,
                        const std::string &   p_fields,
                        const std::string &   p_filter,
                        const std::string &   p_order,
                        const std::string &   p_range,
                        ProcessingResponse &  p_result )
{
    if ( m_tables.count( p_table ) > 0 ) {
        m_tables[ p_table ]->execute ( p_command,
                                       p_fields,
                                       p_filter,
                                       p_order,
                                       p_range,
                                       p_result );
        return;
    }
    //
    p_result.set_jresp_error( "Unknown table" );
}

//-------------------------------------------------------------------------
// Register a table in the handler
void Handler::add_table ( Definition *  p_table )
{
    if ( m_running )
        return;
    //
    m_tables.insert( std::pair( p_table->name(), p_table ) );
}

//-------------------------------------------------------------------------
void Handler::bus_event ( const bus::Message &  p_message )
{
    switch ( p_message.type )
    {
        case masnae::bus::Message::config_updated:
            // nothing to do
            break;
        default:
            break;
    }
}

} // namespace tables
} // namespace masnae
