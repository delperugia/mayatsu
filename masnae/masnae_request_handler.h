// Copyright 2018 Pierre del Perugia
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

//
// Request handler - handle HTTP request
//

#ifndef masnae_request_handler_h
#define masnae_request_handler_h

#include <fastcgi++/request.hpp>

#include "masnae_common.h"

namespace masnae {

//-------------------------------------------------------------------------
class RequestHandler : public Fastcgipp::Request<char>
{
    public:
        RequestHandler  ( const size_t maxPostSize );
        //
        //-----------------------------------------------------------------
        // Simulation functions
        //
        void    simulation_start        ( void ) { m_in_simulation = true;  }
        void    simulation_stop         ( void ) { m_in_simulation = false; }
        bool    in_simulation           ( void ) { return m_in_simulation;  }
        //
        // Set simulation environment before calling process_request
        Fastcpipp_Environment &
                simulation_environment  ( void ) { return m_environment_simulation; }
        //
        // Overload of the base function to switch between the two environments
        const Fastcpipp_Environment & environment() const
        {
            if ( m_in_simulation )
                return m_environment_simulation;
            //
            return Fastcgipp::Request<char>::environment();
        }
        //
        //-----------------------------------------------------------------
        // Function implemented by derived class to process HTTP requests
        //
        // Returns true if request is allowed
        virtual
        bool        check_security       ( const std::string &      p_module,
                                           Fastcpipp_Path &         p_document,
                                           ProcessingResponse &     p_result ) = 0;
        //
        // Returns true if the module/document are found (p_result must be filled then)
        virtual
        bool        process_request      ( const std::string &      p_module,
                                           Fastcpipp_Path &         p_document,
                                           ProcessingResponse &     p_result ) = 0;
        //
        // Function implemented by derived class to return his information
        // Must return a valid JSON string representing a non-empty object
        virtual
        std::string specific_info        ( void ) = 0;
        //
    protected:
        // Security context, as defined by Urania
        SecurityContext         m_security;
        //
        // Extract AccessToken from Authorization header
        bool        get_accesstoken      ( std::string &                 p_accesstoken );
        //                                                               
        // Fills m_security by calling Urania with the AccessToken       
        // to retrieve user's details                                    
        // An HTTP module 'urania' must be defined in the configuration  
        bool        check_accesstoken    ( const std::string &           p_accesstoken );
        //
        // Checks in m_security that the request caller has a permission.
        // Set p_result and returns false if not. Second form set the
        // parameter associated to the permission
        bool        has_permission       ( const std::string &           p_permission,
                                           masnae::ProcessingResponse &  p_result );
        //                                                               
        bool        has_permission       ( const std::string &           p_permission,
                                           std::string &                 p_parameter,
                                           masnae::ProcessingResponse &  p_result );
        //
    private:
        //
        // Get the maximal document POST size from the manager
        size_t      get_max_post_size    ( size_t                        p_default );
        //
        // Convenient function to return a status and exit response
        bool        die                  ( const std::string &           p_status );
        //
        // Output the processed response and send it to Fastcgipp
        void        send_response        ( ProcessingResponse &          p_response );
        //
        // Called by Fastcgipp to handle an HTTP request
        bool        response             ( void );
        //
        // Handle requests to this object
            // The simulator can call it
            friend bool exec_url ( const std::string &   p_url,
                                   RequestHandler &      p_handler,
                                   ProcessingResponse &  p_response,
                                   bool                  p_debug );
            //
        bool        process_internal_request          ( const std::string &      p_module,
                                                        const Fastcpipp_Path &   p_document,
                                                        ProcessingResponse &     p_result );
        //  
        // Function called by process_internal_request...   
        //  
        // ... the Echo service 
        bool        process_echo_request              ( const Fastcpipp_Path &   p_document,
                                                        ProcessingResponse &     p_result );
        // ... the Info service         
        bool        process_info_request              ( const Fastcpipp_Path &   p_document,
                                                        ProcessingResponse &     p_result );
        // ... the Management service           
        bool        process_management_request        ( const Fastcpipp_Path &   p_document,
                                                        ProcessingResponse &     p_result );
        bool        process_management_tables_request ( const Fastcpipp_Path &   p_document,
                                                        ProcessingResponse &     p_result );
        bool        process_management_caches_request ( const Fastcpipp_Path &   p_document,
                                                        ProcessingResponse &     p_result );
        //
        // Store request log
        void        store_log ( const std::string &         p_module,
                                const Fastcpipp_Path &      p_document,
                                const ProcessingResponse &  p_result );
        //
        // To run simulation, we need to fill the environment. Since the one
        // in Fastcgipp::Request is not accessible, we have our own copy here
        // and switch between the two.
        bool                    m_in_simulation = false;
        Fastcpipp_Environment   m_environment_simulation;
};

} // namespace masnae

#endif
