# Mayatsu

Mayatsu is an software suite designed to facilitate and speed up
back office web service development, using C++ for specific, highly
specialized, time critical functions, and JavaScript for easily
customized, businesses oriented, services.

It offers the following modules:

  - **Masnae**: the base library to build web services using C++
  - **Urania**: a user manager using RBAC and OAuth 2.0
  - **Thoe**:   a JavaScript server side server
  - **Admete**: an accounting module

# Masnae

[Masnae](_docs/masnae_readme.md) uses
[FastCGI++](https://github.com/eddic/fastcgipp)
to handle web service requests. It adds:

  - dynamic configuration handling
  - HTTP client
  - MySQL client
  - generic web service for database tables and caches handling

Web services natively support parameters using standard HTTP methods
(query string, x-www-form-urlencoded, form-data...). Handling different
types is possible but has to be implemented.

The [JResp](https://gitlab.com/delperugia/papers/blob/master/jresp.md)
format is the default format used to return results, but any format
can be used, including binary ones.

# Urania

[Urania](_docs/urania_readme.md) is in charge of authenticating
users, managing roles and permissions, and delivering and
controlling AccessTokens. It provides:

  - RBAC
  - OAuth 2.0

# Thoe

[Thoe](_docs/thoe_readme.md) uses
[SpiderMonkey](https://developer.mozilla.org/en-US/docs/Mozilla/Projects/SpiderMonkey)
to execute JavaScript written web service. It adds to JavaScript the following
features:

  - Blob manipulation
  - cryptographic functions
  - HTTP client
  - MySQL client
  - money calculation
  - libraries using 'require' keyword

Received request is available in a global object.

Since Thoe derives from Masnae, it is also possible to simultaneously
implement WebServices written in C++.

# Admete

[Admete](_docs/admete_readme.md) is an accounting module,
using double entry accounting.
Accounts can be created in multiple currencies. It supports
reservations and reversal.

# Performances

  In order to evaluate the overhead introduce by FastCGI,
  the various context creations and SpiderMonkey, we made
  some load tests with an Intel(R) Xeon(R) CPU E5-1650 0 @ 3.20GHz,
  using Apache Bench and the following command line:

```bash
    ab -k -c 12 -n 10000 *url*
```

  and comparing with the following product versions:
  
    PHP 7.2.5, Node 8.10.0, lighttpd 1.4.45
    
  Masnae/Thoe and PHP were configured in lighttpd with 2
  processes, each having 12 threads.

  First with the simplest application possible returning
  and empty document, tested with 4, 12 then 24 simultaneous
  connections:
    
  | Server       | 4     | 12    | 24    |
  |--------------|------:|------:|------:|
  | Lighttpd     | 28756 | 28961 | 29428 |
  | Masnae       | 17337 | 18966 | 19522 |
  | PHP          | 17833 | 18050 | 18703 |
  | Node.js      |  8457 |  8627 |  8747 |
  | Thoe         |  3318 |  5833 |  7830 |
  
  Then with an application doing some loops and string
  concatenations.
  
  | Server       | 4     | 12    | 24    |
  |--------------|------:|------:|------:|
  | Masnae       | 16669 | 18329 | 18549 |
  | PHP          |  8724 | 13175 | 13800 |
  | Node.js      |  4663 |  4786 |  4726 |
  | Thoe         |  1869 |  2764 |  3000 |

# Installation

Mayatsu is designed and tested on Ubuntu 18.04 using
GCC 7.4, and Ubuntu 19.04 using GCC 8.3.

Mayatsu was tested on Fedora 29, Clang 6.0 and SpiderMonkey 60.

The following packages must be installed using apt-get:
  - libargon2-0-dev         0~20161029-1.1
  - libbfd-dev              2.30-21
  - libcurl4-openssl-dev    7.58.0
  - libmariadb-dev          3.0.3
  - libmozjs-52-dev         52.9.1
  - libssl-dev              1.1.1
  - libxml2-dev             2.9.4
  - libzxcvbn-dev           2.3
  - rapidjson-dev           1.1.0

And these packages manually:
  - Adawat                  1.6
  - FastCGI++               3.1

Packages can be installed using:
```bash
    sudo apt-get install make cmake g++ \
                         libargon2-0-dev libbfd-dev libcurl4-openssl-dev \
                         libmariadb-dev libmozjs-52-dev libssl-dev libxml2-dev \
                         libzxcvbn-dev rapidjson-dev
```

Adawat is available using:
```bash
    git clone https://gitlab.com/delperugia/adawat.git
```

To retrieve the compatible version of FastCGI++, run in a console:
```bash
    git clone https://github.com/delperugia/fastcgipp.git
```

Licenses of these modules are available here:
  - Adawat              https://www.apache.org/licenses/LICENSE-2.0.txt
  - Argon2              https://www.apache.org/licenses/LICENSE-2.0.txt
  - BFD                 https://www.gnu.org/licenses/gpl-3.0.en.html
  - libcurl             https://curl.haxx.se/docs/copyright.html (MIT/X)
  - libxml2	            https://www.opensource.org/licenses/mit-license.html
  - MariaDB Connector   https://www.gnu.org/licenses/lgpl-2.1.en.html
  - SpiderMonkey        https://www.mozilla.org/en-US/MPL/2.0/
  - OpenSSL             https://www.apache.org/licenses/LICENSE-2.0.txt
  - RapidJSON           https://github.com/Tencent/rapidjson/blob/master/license.txt (MIT)
  - FastCGI++           https://www.gnu.org/licenses/lgpl-3.0.en.html
  - zxcvbn-c            https://www.opensource.org/licenses/mit-license.html

Since BFD (used by the crashhandler in Adawat) is GPL v3 licensed, it can only
be used if the resulting software is also licensed under GPL v3.
If not, BFD must be disabled from Adawat.

# Building the modules

In a console, run:

```bash
    make
```

# License

Copyright 2018,2019 Pierre del Perugia

[Apache license v2](https://www.apache.org/licenses/LICENSE-2.0.txt)
