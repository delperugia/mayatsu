create schema if not exists admete;
use admete;

CREATE TABLE IF NOT EXISTS `t_accounts` (
  `k_account` bigint(20) unsigned NOT NULL,
  `s_currency` char(3) NOT NULL,
  `f_balance` decimal(24,8) NOT NULL DEFAULT '0.00000000',
  `f_overdraft` decimal(24,8) NOT NULL DEFAULT '0.00000000',
  `f_ceiling` decimal(24,8) NOT NULL DEFAULT '0.00000000',
  PRIMARY KEY (`k_account`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE IF NOT EXISTS `t_journal` (
  `k_journal` bigint(20) unsigned NOT NULL,
  `i_period` mediumint(6) unsigned NOT NULL,
  `d_date` datetime NOT NULL,
  `s_type` varchar(32) DEFAULT NULL,
  `s_category` varchar(32) DEFAULT NULL,
  `s_narration` varchar(128) DEFAULT NULL,
  `s_user` varchar(50) DEFAULT NULL,
  `s_user_reference` varchar(50) DEFAULT NULL,
  `k_journal_reversal` bigint(20) unsigned DEFAULT NULL,
  PRIMARY KEY (`k_journal`,`i_period`),
  UNIQUE KEY `UK_user_reference` (`s_user_reference`),
  KEY `FK_reversal` (`k_journal_reversal`),
  CONSTRAINT `FK_reversal` FOREIGN KEY (`k_journal_reversal`) REFERENCES `t_journal` (`k_journal`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE IF NOT EXISTS `t_posting` (
  `k_post` bigint(20) unsigned NOT NULL,
  `i_period` mediumint(6) unsigned NOT NULL,
  `k_journal` bigint(20) unsigned NOT NULL,
  `k_account` bigint(20) unsigned NOT NULL,
  `f_amount` decimal(24,8) NOT NULL,
  `s_currency` char(3) NOT NULL,
  PRIMARY KEY (`k_account`,`k_post`,`i_period`),
  KEY `FK_journal` (`k_journal`),
  CONSTRAINT `FK_accounts` FOREIGN KEY (`k_account`) REFERENCES `t_accounts` (`k_account`),
  CONSTRAINT `FK_journal` FOREIGN KEY (`k_journal`) REFERENCES `t_journal` (`k_journal`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE IF NOT EXISTS `t_reservations` (
  `k_account` BIGINT(20) UNSIGNED NOT NULL,
  `k_reservation` CHAR(6) NOT NULL,
  `f_amount` DECIMAL(24,8) NOT NULL COMMENT 'always positive or null',
  `d_expiration` DATETIME NOT NULL,
  `s_narration` VARCHAR(128) NULL DEFAULT NULL,
  PRIMARY KEY (`k_account`, `k_reservation`),
  CONSTRAINT `FK_account` FOREIGN KEY (`k_account`) REFERENCES `t_accounts` (`k_account`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE IF NOT EXISTS `t_sequencer` (
  `i_next_journal` bigint(20) unsigned NOT NULL DEFAULT '1',
  `i_next_posting` bigint(20) unsigned NOT NULL DEFAULT '1',
  `i_next_account` bigint(20) unsigned NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE OR REPLACE
    ALGORITHM = UNDEFINED
    DEFINER = CURRENT_USER
    SQL SECURITY INVOKER
    VIEW `v_ledger` AS
    select `p`.`k_account` AS `k_account`,`j`.`k_journal` AS `k_journal`,`p`.`k_post` AS `k_post`,
           `j`.`d_date` AS `d_date`,`p`.`f_amount` AS `f_amount`,`p`.`s_currency` AS `s_currency`,
           `j`.`s_type` AS `s_type`,`j`.`s_category` AS `s_category`,`j`.`s_narration` AS `s_narration`,
           `j`.`s_user_reference` AS `s_reference`,`j`.`k_journal_reversal` AS `k_journal_reversal`
    from (`t_journal` `j` join `t_posting` `p`
          on(((`p`.`i_period` = `j`.`i_period`) and (`p`.`k_journal` = `j`.`k_journal`))));

delete from t_reservations;
delete from t_posting;
delete from t_journal;
delete from t_accounts;
delete from t_sequencer;
insert into t_sequencer () values ();

create user 'admete'@'localhost' identified by '82nQQn0uUTOJO1NP';
create user 'admete'@'%'         identified by '82nQQn0uUTOJO1NP';
grant all on admete.* to 'admete'@'localhost';
grant all on admete.* to 'admete'@'%';
