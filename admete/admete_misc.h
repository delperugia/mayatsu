// Copyright 2019 Pierre del Perugia
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

//
// Miscellaneous functions
//

#ifndef admete_misc_h
#define admete_misc_h

#include <string>
#include <utility>
#include <vector>

#include <adawat/adawat_money.h>

#include "masnae_mysql.h"

namespace admete {

//-------------------------------------------------------------------------
// An account loaded in memory: amount fields are parsed
// into Adawat Amount objects
struct Account
{
    std::string     account;
    adawat::Amount  balance;
    adawat::Amount  overdraft;
    adawat::Amount  ceiling;
    adawat::Amount  reserved;
    //
    // Check if balance is in [overdraft+reserved,ceiling]
    // Returns:
    //      success
    //      failed
    //      rejected    overflow
    //                  incompatible_currency
    //                  overdraft
    //                  ceiling
    bool check_limits ( masnae::ProcessingResponse &  p_result ) const;
};

typedef std::map < std::string, Account >
    SingleKey_Accounts;  // indexed by account id

//-------------------------------------------------------------------------
// A reservation loaded in memory: amount field is parsed
// into an Adawat Amount object
struct Reservation
{
    std::string     account;
    std::string     id;
    adawat::Amount  amount;
};

typedef std::map < std::string, Reservation >
    SingleKey_Reservation;  // indexed by account:id

//-------------------------------------------------------------------------
// A list of operation in a transaction: amount field is parsed
// into an Adawat Amount object
struct Record
{
    std::string     account;      // account number
    adawat::Amount  amount;       // value of the operation (positive or negative)
    std::string     reservation;  // optional reservation id
};

typedef std::vector< Record >
    List_Record;

//-------------------------------------------------------------------------
// Parse a list of records as a JSON expression
// p_text is an array of objects with a single member, like:
//    [ {"a":"-2"}, {"b":"1"}, {"a":"1"} ]
// Returns:
//      success
//      error
//      rejected    invalid_currency
//                  invalid_value           amount
bool parse_records ( const std::string &           p_text,
                     const std::string &           p_currency,
                     List_Record &                 p_records,
                     masnae::ProcessingResponse &  p_result );

//-------------------------------------------------------------------------
// Parse JSON to retrieve records, build SQL WHERE clause to retrieve
// accounts, check that sum of records is zero. Set p_result on error
// JSON format: array of single member objects with account : amount
//          [ {"10001":"-5"}, {"10002":"5"} ]
// Returns:
//      success
//      error
//      rejected    overflow                account
//                  invalid_currency
//                  not_null
//                  reservation_credited    reservation
//                  invalid_value           amount
bool prepare_records( const std::string &           p_records_json,
                      const std::string &           p_currency,
                      List_Record  &                p_records,
                      std::string &                 p_accounts_where,
                      masnae::ProcessingResponse &  p_result );

//-------------------------------------------------------------------------
// Period is the UTC year / month
std::string get_period ( time_t  p_now );

//-------------------------------------------------------------------------
// Get next ids for journal and posting (locking)
// Returns:
//      success
//      failed
bool get_next_sequence( masnae::mysql::Connection &   p_db,
                        uint64_t &                    p_next_journal,
                        uint64_t &                    p_next_posting,
                        masnae::ProcessingResponse &  p_result );

//-------------------------------------------------------------------------
// Set JResp data with the result of a journal transaction / reversal
bool fill_successful_result ( const std::string &           p_date,
                              uint64_t                      p_next_journal,
                              uint64_t                      p_next_posting,
                              size_t                        p_nb_posting,
                              masnae::ProcessingResponse &  p_result );

//-------------------------------------------------------------------------
// Get accounts details (locking them and reservation)
// p_accounts_where is a CSV (escaped) of accounts
// Returns:
//      success
//      failed
bool get_accounts( masnae::mysql::Connection &    p_db,
                   const std::string &            p_accounts_where,
                   SingleKey_Accounts &           p_accounts_mem,
                   masnae::ProcessingResponse &   p_result );

//-------------------------------------------------------------------------
// Get reservation details (locking them and accounts)
// p_accounts_where is a CSV (escaped) of accounts
// Returns:
//      success
//      failed
bool get_reservations( masnae::mysql::Connection &    p_db,
                       const std::string &            p_accounts_where,
                       SingleKey_Reservation &        p_reservation_mem,
                       masnae::ProcessingResponse &   p_result );

//-------------------------------------------------------------------------
// Update reservations
// Returns:
//      success
//      failed
bool write_reservations ( masnae::mysql::Connection &    p_db,
                          const SingleKey_Reservation &  p_reservation_mem,
                          masnae::ProcessingResponse &   p_result );

//-------------------------------------------------------------------------
// Applies records to balances, check account overflows / currencies
// Returns:
//      success
//      failed
//      rejected    overdraft               account
//                  ceiling                 account
//                  overflow                account
//                  incompatible_currency   account
//                  not_found               account
//                  reservation_not_found   reservation
//                  reservation_too_low     reservation
bool apply_records ( const List_Record &                  p_records,
                     SingleKey_Accounts &                 p_accounts_mem,
                     SingleKey_Reservation &              p_reservation_mem,
                     masnae::ProcessingResponse &         p_result );

//-------------------------------------------------------------------------
// Write journal
// Returns:
//      success
//      failed
bool write_journal( masnae::mysql::Connection &   p_db,
                    const std::string &           p_period,
                    uint64_t                      p_next_journal,
                    const std::string &           p_date,
                    const std::string &           p_type,
                    const std::string &           p_category,
                    const std::string &           p_narration,
                    const std::string &           p_user,
                    const std::string &           p_user_reference,
                    masnae::ProcessingResponse &  p_result );

//-------------------------------------------------------------------------
// Write posting, update accounts
// Update p_next_journal / p_next_posting with the next vales to use
// Returns:
//      success
//      failed
bool write_posting( masnae::mysql::Connection &   p_db,
                    const std::string &           p_period,
                    uint64_t &                    p_next_journal,
                    uint64_t &                    p_next_posting,
                    const List_Record &           p_records,
                    const SingleKey_Accounts &    p_accounts_mem,
                    masnae::ProcessingResponse &  p_result );

//-------------------------------------------------------------------------
// Write next sequence values to use
// Returns:
//      success
//      failed
bool update_next_sequence( masnae::mysql::Connection &   p_db,
                           uint64_t                      p_next_journal,
                           uint64_t                      p_next_posting,
                           masnae::ProcessingResponse &  p_result );

} // namespace admete

#endif
