// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

// Copyright 2019 Pierre del Perugia
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <curl/curl.h>
#include <fastcgi++/manager.hpp>
#include <iostream>
#include <mariadb/mariadb_version.h>
#include <rapidjson/rapidjson.h>
#include <unistd.h>
#include <adawat/adawat_common.h>
#include <adawat/adawat_crashhandler.h>

#include "../mayatsu_version.h"
#include "masnae_bus.h"
#include "masnae_config.h"
#include "masnae_manager.hpp"
#include "masnae_misc.h"
#include "masnae_global.h"
#include "masnae_simulator.h"
#include "masnae_ws_tables.h"

#include "admete_account.h"
#include "admete_closing.h"
#include "admete_common.h"
#include "admete_reservation.h"
#include "admete_trial.h"
#include "admete_validate.h"

using namespace std::string_literals;

//-------------------------------------------------------------------------
// Global variables
bool                            s_RunClosing    = false;
bool                            s_RunConsole    = false;
bool                            s_RunSelfTests  = false;
bool                            s_RunTrial      = false;
bool                            s_Succeeded     = false;
std::string                     s_ConfigFile;
std::string                     s_ClosingPeriod;
//
masnae::bus::Publisher          g_Bus;
masnae::config::Handler         g_ConfigHandler;
masnae::logger::Handler         g_LoggerHandler;
masnae::mysql::Handler          g_MySQLHandler;
masnae::ws_tables::Handler      g_WsTablesHandler;
masnae::ws_caches::Handler      g_WsCachesHandler;
masnae::http::Handler           g_HttpHandler;
masnae::request::Manager<admete::AdmeteRequestHandler>
                                g_Manager( admete::constants::k_threads_per_cpu );
admete::AccountManager          g_AccountManager;
admete::ReservationManager      g_ReservationManager;

//-------------------------------------------------------------------------
// Parse command line, returns false if program must exit
bool parse_command_line ( int argc, char * argv [] )
{
    int c;
    //
    while ( (c = getopt( argc, argv, "ctp:ivf:h")) != -1 ) {
        switch (c) {
            case 'c':   s_RunConsole    = true;
                        break;
            case 't':   s_RunSelfTests  = true;
                        break;
            case 'p':   s_RunClosing    = true;
                        s_ClosingPeriod = optarg;
                        break;
            case 'i':   s_RunTrial = true;
                        break;
            case 'v':   printf( "Adawat:       %s\n",    adawat::k_adawat_version );
                        printf( "FastCGI++:    %s\n",    Fastcgipp::version );
                        printf( "Mayatsu:      %s\n",    mayatsu::k_version );
                        printf( "cURL:         %s\n",    curl_version() );
                        printf( "MariaDb:      %s\n",    MARIADB_PACKAGE_VERSION );
                        printf( "RapidJSON :   %s\n",    RAPIDJSON_VERSION_STRING );
                        return false;
            case 'f':   s_ConfigFile = optarg;
                        break;
            case 'h':
            default:    printf( "-c console\n" );
                        printf( "-i trial balance\n" );
                        printf( "-f <config>\n" );
                        printf( "-t <test_url>\n" );
                        printf( "-v version\n" );
                        return false;
        }
    }
    //
    return true;
}

//-------------------------------------------------------------------------
int main ( int argc, char * argv [] )
{
    //------------------------
    if ( ! adawat::crashhandler_setup() )
        return EXIT_FAILURE;
    //
    //------------------------
    masnae::globals::init( "Admete",
                           & g_Bus,
                           & g_ConfigHandler,
                           & g_LoggerHandler,
                           & g_MySQLHandler,
                           & g_WsTablesHandler,
                           & g_WsCachesHandler,
                           & g_HttpHandler,
                           & g_Manager );
    //
    //------------------------
    // The web server should define something like:
    //    "ADMETE_CONFIG" => "/var/www/cgi-config/admete.json"
    const char * config_file = getenv( admete::constants::k_env_var );
    if ( config_file == NULL )
        s_ConfigFile = admete::constants::k_default_config_file;
    else
        s_ConfigFile = config_file;
    //
    //------------------------
    if ( ! parse_command_line( argc, argv ) )  // returns false if program must exit
        return EXIT_SUCCESS;
    //
    //------------------------
    masnae::service_info_log( "Admete starting, "s + mayatsu::k_version );
    //
    //------------------------
    // Start all modules
    if ( g_ConfigHandler.start( s_ConfigFile )  &&
         g_LoggerHandler.start( admete::constants::k_default_log_folder ) &&
         g_MySQLHandler.start()                 && 
         g_WsTablesHandler.start()              && 
         g_WsCachesHandler.start()              &&
         g_HttpHandler.start()                  &&
         g_AccountManager.start()               &&
         g_ReservationManager.start()           &&
         g_Manager.init() )
    {
        // Execute as a console or as FastCGI
        if ( s_RunConsole ) {
            s_Succeeded = true;
            //
            std::thread t1( admete::console );
            t1.join();
        } else if ( s_RunSelfTests ) {
            std::thread t1( admete::validate, & s_Succeeded );
            t1.join();
        } else if ( s_RunTrial ) {
            s_Succeeded = admete::trial();
        } else if ( s_RunClosing ) {
            s_Succeeded = admete::closing( s_ClosingPeriod );
        } else {
            if ( g_Manager.run() )  // wait for http server stop
                s_Succeeded = true;
        }
    }
    //
    //------------------------
    // Stop modules
    g_ReservationManager.stop();
    g_AccountManager.stop();
    g_HttpHandler.stop();
    g_WsCachesHandler.stop();
    g_WsTablesHandler.stop();
    g_MySQLHandler.stop();
    g_LoggerHandler.stop();
    g_ConfigHandler.stop();
    //
    //------------------------
    return s_Succeeded ? EXIT_SUCCESS : EXIT_FAILURE;
}
