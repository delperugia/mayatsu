// Copyright 2019 Pierre del Perugia
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

//
// Definitions of global elements.
//

#ifndef admete_common_h
#define admete_common_h

#include <fastcgi++/manager.hpp>

#include "masnae_bus.h"
#include "masnae_config.h"
#include "masnae_http.h"

#include "admete_account.h"
#include "admete_request.h"
#include "admete_reservation.h"

//-------------------------------------------------------------------------
namespace admete {
namespace constants {

constexpr size_t    k_post_max_size         = 1024;
constexpr unsigned  k_threads_per_cpu       = 2;
constexpr char      k_env_var[]             = "ADMETE_CONFIG";
constexpr char      k_default_config_file[] = "/var/www/cgi-config/admete.json";
constexpr char      k_default_log_folder[]  = "/var/log/admete/";

// admete_closing.cpp
constexpr char      k_closing_prefix[]         = "Closing period ";  // closing's narration (followed by period)

// admete_account.cpp
constexpr unsigned  k_usage_period_max         = 31;                 // maximal period in days (max. 20K)

// admete_request_reservation.cpp
constexpr unsigned  k_reservation_lifetime_max = 86400*90;           // maximal reservation lifetime in seconds

// admete_request_reverse.cpp
constexpr char      k_reverse_prefix[]         = "X: ";              // prefix added to reverse's narration

} // namespace constants
} // namespace admete

//-------------------------------------------------------------------------
// Global variables
extern admete::AccountManager       g_AccountManager;
extern admete::ReservationManager   g_ReservationManager;

#endif
