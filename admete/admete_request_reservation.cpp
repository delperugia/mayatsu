// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

// Copyright 2019 Pierre del Perugia
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <adawat/adawat_money.h>

#include "masnae_global.h"
#include "masnae_misc.h"

#include "admete_common.h"
#include "admete_request.h"

namespace admete {

//-------------------------------------------------------------------------
bool AdmeteRequestHandler::process_ws_reservation ( const masnae::Fastcpipp_Path &       p_document,
                                                    const masnae::SingleKey_ValueList &  p_parameters,
                                                    masnae::ProcessingResponse &         p_result )
{
    if ( p_document.size() < 2 )
        return false;
    //
         if ( p_document[ 1 ] == "select" ) process_ws_reservation_select( p_parameters, p_result );
    else if ( p_document[ 1 ] == "create" ) process_ws_reservation_create( p_parameters, p_result );
    else if ( p_document[ 1 ] == "delete" ) process_ws_reservation_delete( p_parameters, p_result );
    else return false;
    //
    return true;
}

//-------------------------------------------------------------------------
// Search and return reservations
// Use the same syntax as the /mgt/table/ syntax, with 
// parameters: fields
//             filter
//             order
//             range
void AdmeteRequestHandler::process_ws_reservation_select ( const masnae::SingleKey_ValueList &  p_parameters,
                                                           masnae::ProcessingResponse &         p_result )
{
    // Check that the caller is allowed here
    if ( ! has_permission( "admete.reservation_select", p_result ) )
        return;
    //
    // Retrieve parameters
    std::string fields, filter, order, range;
    //
    if ( ! masnae::get_parameter( p_parameters, "fields", fields,  false, true, p_result ) ||
         ! masnae::get_parameter( p_parameters, "filter", filter,  false, true, p_result ) ||
         ! masnae::get_parameter( p_parameters, "order",  order,   false, true, p_result ) ||
         ! masnae::get_parameter( p_parameters, "range",  range,   false, true, p_result ) )
        return;
    //
    g_ReservationManager.select( fields, filter, order, range, p_result );
}

//-------------------------------------------------------------------------
// Create a new reservation
// parameters: account
//             limit
//             lifetime
//             narration
void AdmeteRequestHandler::process_ws_reservation_create ( const masnae::SingleKey_ValueList &  p_parameters,
                                                           masnae::ProcessingResponse &         p_result )
{
    // Check that the caller is allowed here
    if ( ! has_permission( "admete.reservation_create", p_result ) )
        return;
    //
    // Retrieve parameters
    std::string account, limit, narration;
    int64_t     lifetime;
    //
    if ( ! masnae::get_parameter( p_parameters, "account",    account,    false, true, p_result ) ||
         ! masnae::get_parameter( p_parameters, "limit",      limit,      false, true, p_result ) ||
         ! masnae::get_parameter( p_parameters, "lifetime",   lifetime,   false,       p_result ) ||
         ! masnae::get_parameter( p_parameters, "narration",  narration,  false, true, p_result ) )
        return;
    //
    if ( lifetime < 1 ||
         lifetime > constants::k_reservation_lifetime_max )
    {
        p_result.set_jresp_rejected( "invalid_lifetime", "null" );
        return;
    }
    //
    g_ReservationManager.create( account, limit, lifetime, narration, p_result );
}

//-------------------------------------------------------------------------
// Delete reservations
// Use the same syntax as the /mgt/table/ syntax, with 
// parameters: filter
//             order
//             range
void AdmeteRequestHandler::process_ws_reservation_delete ( const masnae::SingleKey_ValueList &  p_parameters,
                                                           masnae::ProcessingResponse &         p_result )
{
    // Check that the caller is allowed here
    if ( ! has_permission( "admete.reservation_delete", p_result ) )
        return;
    //
    // Retrieve parameters
    std::string filter, order, range;
    //
    if ( ! masnae::get_parameter( p_parameters, "filter", filter,  false, true, p_result ) ||
         ! masnae::get_parameter( p_parameters, "order",  order,   false, true, p_result ) ||
         ! masnae::get_parameter( p_parameters, "range",  range,   false, true, p_result ) )
        return;
    //
    g_ReservationManager.close( filter, order, range, p_result );
}

} // namespace admete
