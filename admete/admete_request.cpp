// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

// Copyright 2019 Pierre del Perugia
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "masnae_misc.h"
#include "admete_common.h"
#include "admete_request.h"

namespace admete {

//-------------------------------------------------------------------------
AdmeteRequestHandler::AdmeteRequestHandler() :
    masnae::RequestHandler( admete::constants::k_post_max_size )
{
}

//-------------------------------------------------------------------------
// Returns true if the peer is allowed to call this web service
bool AdmeteRequestHandler::check_security ( const std::string &           p_module,
                                            masnae::Fastcpipp_Path &      p_document,
                                            masnae::ProcessingResponse &  p_result )
{
    // When executed in simulation mode (auto-test or console) use
    // the following security context:
    if ( m_security_simulate_uriana )
    {
        m_security = {
            .user        = "111",
            .profile     = "profile_test",
            .role        = "role_test",
            .scopes      = "refresh profile",
            .permissions = {
                { "admete.ws",                 "" },
                { "admete.exchange",           "" },
                { "admete.transfer",           "" },
                { "admete.reverse",            "" },
                { "admete.account_create",     "" },
                { "admete.account_delete",     "" },
                { "admete.account_select",     "" },
                { "admete.account_ledger",     "" },
                { "admete.account_usage",     "" },
                { "admete.reservation_create", "" },
                { "admete.reservation_delete", "" },
                { "admete.reservation_select", "" }
            }
        };
    }
    else // call Uriana to validate the received AccessToken
    {
        std::string accesstoken;
        //
        if ( ! get_accesstoken( accesstoken ) )  // retrieve AT from Authorization header
            return false;
        //
        // Calls Urania, fills security context (m_security)
        if ( ! check_accesstoken( accesstoken ) )
            return false;
    }
    //
    return true;
}

//-------------------------------------------------------------------------
// Returns true if the module/document are found (p_result must be filled then)
bool AdmeteRequestHandler::process_request ( const std::string &           p_module,
                                             masnae::Fastcpipp_Path &      p_document,
                                             masnae::ProcessingResponse &  p_result )
{
    bool handled = false;  // if not handled here, will call the derived class (if any)
    //
    if ( p_module == "ws" )  handled = process_ws_request( p_document, p_result );
    //
    return handled;
}

//-------------------------------------------------------------------------
// Returns true if the module/document are found (p_result is already be filled)
bool AdmeteRequestHandler::process_ws_request ( const masnae::Fastcpipp_Path &  p_document,
                                                masnae::ProcessingResponse &    p_result )
{
    if ( p_document.size() < 1 )
        return false;
    //
    // Merge post and get parameters
    auto params = masnae::merge_parameters( environment().posts,
                                            environment().gets );
    //
    // Function bodies located in admete_request_account.cpp
         if ( p_document[ 0 ] == "account" )        process_ws_account    ( p_document, params, p_result );
    //
    // Function bodies located in admete_request_exchange.cpp
    else if ( p_document[ 0 ] == "exchange" )       process_ws_exchange   ( params, p_result );
    //
    // Function bodies located in admete_request_reservation.cpp
    else if ( p_document[ 0 ] == "reservation" )    process_ws_reservation( p_document, params, p_result );
    //
    // Function bodies located in admete_request_reverse.cpp
    else if ( p_document[ 0 ] == "reverse" )        process_ws_reverse    ( params, p_result );
    //
    // Function bodies located in admete_request_transaction.cpp
    else if ( p_document[ 0 ] == "transfer" )       process_ws_transfer   ( params, p_result );
    //
    else return false;
    //
    return true;
}

//-------------------------------------------------------------------------
// Returns information
// Must return a valid JSON string representing a non-empty object
std::string AdmeteRequestHandler::specific_info ( void )
{
    return "";  // nothing returned: result will be ignored
}

} // namespace admete
