// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

// Copyright 2019 Pierre del Perugia
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <adawat/adawat_time.h>

#include "masnae_common.h"
#include "masnae_mysql.h"
#include "masnae_global.h"

namespace admete {

//-------------------------------------------------------------------------
bool sum_of_accounts ( void )
{
    printf( "    %s sum_of_accounts:       ", adawat::time_format_rfc3339_local().c_str() );
    //
    masnae::SingleKeyValue_List  rows;
    masnae::mysql::Connection    db = masnae::globals::mysql_handler->get_connection( masnae::mysql::reader );
    //    
    unsigned db_result = db.do_select(
        "SELECT s_currency, SUM(f_balance) as balance"
        " FROM t_accounts"
        " GROUP BY s_currency"
        " HAVING balance != 0",
        rows );
    //
    if ( db_result != 0 ) {
        printf( "database error %u\n", db_result );
        return false;
    }
    //
    if ( rows.empty() ) {
        printf( "ok\n" );
        return true;
    }
    //
    printf( "%zu error(s)\n", rows.size() );
    for ( auto & row: rows )
        printf( "      currency %s, balance %s\n",
                row[ "s_currency" ].c_str(),
                row[ "balance"    ].c_str() );
    //
    return false;
}

//-------------------------------------------------------------------------
bool sum_of_transactions ( void )
{
    printf( "    %s sum_of_transactions:   ", adawat::time_format_rfc3339_local().c_str() );
    //
    masnae::SingleKeyValue_List  rows;
    masnae::mysql::Connection    db = masnae::globals::mysql_handler->get_connection( masnae::mysql::reader );
    //
    unsigned db_result = db.do_select(
        "SELECT k_journal, s_currency, SUM(f_amount) AS balance"
        " FROM t_posting"
        " GROUP BY k_journal, s_currency"
        " HAVING balance != 0",
        rows );
    //    
    if ( db_result != 0 ) {
        printf( "database error %u\n", db_result );
        return false;
    }
    //
    if ( rows.empty() ) {
        printf( "ok\n" );
        return true;
    }
    //
    printf( "%zu error(s)\n", rows.size() );
    for ( auto & row: rows )
        printf( "      journal %s currency %s, balance %s\n",
                row[ "k_journal"  ].c_str(),
                row[ "s_currency" ].c_str(),
                row[ "balance"    ].c_str() );
    //
    return false;
}

//-------------------------------------------------------------------------
bool account_limits ( void )
{
    printf( "    %s account_limits:        ", adawat::time_format_rfc3339_local().c_str() );
    //
    masnae::SingleKeyValue_List  rows;
    masnae::mysql::Connection    db = masnae::globals::mysql_handler->get_connection( masnae::mysql::reader );
    //
    unsigned db_result = db.do_select(
        "SELECT k_account, f_balance, f_overdraft, f_ceiling"
        " FROM t_accounts"
        " WHERE f_balance < f_overdraft OR f_balance > f_ceiling",
        rows );
    //    
    if ( rows.empty() ) {
        printf( "ok\n" );
        return true;
    }
    //
    if ( db_result != 0 ) {
        printf( "database error %u\n", db_result );
        return false;
    }
    //
    printf( "%zu error(s)\n", rows.size() );
    for ( auto & row: rows )
        printf( "      account %s, %s <= %s <= %s\n",
                row[ "k_account"   ].c_str(),
                row[ "f_overdraft" ].c_str(),
                row[ "f_balance"   ].c_str(),
                row[ "f_ceiling"   ].c_str() );
    //
    return false;
}

//-------------------------------------------------------------------------
bool account_match_posting ( void )
{
    printf( "    %s account_match_posting: ", adawat::time_format_rfc3339_local().c_str() );
    //
    //
    masnae::SingleKeyValue_List  rows;
    masnae::mysql::Connection    db = masnae::globals::mysql_handler->get_connection( masnae::mysql::reader );
    //
    unsigned db_result = db.do_select(
        "SELECT p.k_account, a.f_balance, SUM(f_amount) AS balance"
        " FROM t_posting p"
        " JOIN t_accounts a ON a.k_account=p.k_account"
        " GROUP BY k_account"
        " HAVING f_balance != balance",
        rows
    );
    //    
    if ( rows.empty() ) {
        printf( "ok\n" );
        return true;
    }
    //
    if ( db_result != 0 ) {
        printf( "database error %u\n", db_result );
        return false;
    }
    //
    printf( "%zu error(s)\n", rows.size() );
    for ( auto & row: rows )
        printf( "      account %s, %s <> %s\n",
                row[ "k_account" ].c_str(),
                row[ "f_balance" ].c_str(),
                row[ "balance"   ].c_str() );
    //
    return false;
}

//-------------------------------------------------------------------------
// Run integrity checks on the database
// With 10K accounts, 2M journal entries:
//   Starting trial balance integrity check
//       2019-05-06T00:00:00+02:00 starting
//       2019-05-06T00:00:09+02:00 sum_of_accounts:       ok
//       2019-05-06T00:00:09+02:00 sum_of_transactions:   ok
//       2019-05-06T00:01:17+02:00 account_limits:        ok
//       2019-05-06T00:01:17+02:00 account_match_posting: ok
//   Trial balance finished
//   
bool trial ( void)
{
    // Pre-allocate a DB handle
    {
        masnae::globals::mysql_handler->get_connection( masnae::mysql::reader );
    }
    //
    printf( "Starting trial balance integrity check\n" );
    //
    printf( "    %s starting\n", adawat::time_format_rfc3339_local().c_str() );
    //
    bool ok = true;
    //
    ok &= sum_of_accounts();
    ok &= sum_of_transactions();
    ok &= account_limits();
    ok &= account_match_posting();
    //
    printf( "Trial balance finished\n" );
    //
    return ok;
}

} // namespace admete
