// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

// Copyright 2019 Pierre del Perugia
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <algorithm>
#include <adawat/adawat_string.h>

#include "masnae_global.h"
#include "masnae_misc.h"

#include "admete_account.h"
#include "admete_common.h"
#include "admete_misc.h"

namespace admete {

//-------------------------------------------------------------------------
// Use the Masnae ws_tables engine to access to the t_accounts and
// t_posting table
// but don't expose it directly for seurity reason
class TableAccounts : public masnae::ws_tables::Definition
{
    public:
        TableAccounts() :
            masnae::ws_tables::Definition( "t_accounts" )
        {
            m_fields = {
                // name           type ins   upt   req   def.ins  def.upt
                { "k_account",  { 'S', true, true, true, nullptr, nullptr } },
                { "s_currency", { 'S', true, true, true, nullptr, nullptr } },
                { "f_balance",  { 'D', true, true, true, nullptr, nullptr } },
                { "f_overdraft",{ 'D', true, true, true, nullptr, nullptr } },
                { "f_ceiling",  { 'D', true, true, true, nullptr, nullptr } }
            };
        }
};

// Ledger is a view on t_journal joined to t_posting
class TableLedger : public masnae::ws_tables::Definition
{
    public:
        TableLedger() :
            masnae::ws_tables::Definition( "v_ledger" )
        {
            m_fields = {
                // name                   type ins   upt   req   def.ins  def.upt
                { "k_account",          { 'S', true, true, true, nullptr, nullptr } },
                { "k_journal",          { 'S', true, true, true, nullptr, nullptr } },
                { "k_post",             { 'S', true, true, true, nullptr, nullptr } },
                { "d_date",             { 'S', true, true, true, nullptr, nullptr } },
                { "f_amount",           { 'D', true, true, true, nullptr, nullptr } },
                { "s_currency",         { 'S', true, true, true, nullptr, nullptr } },
                { "s_type",             { 'S', true, true, true, nullptr, nullptr } },
                { "s_category",         { 'S', true, true, true, nullptr, nullptr } },
                { "s_narration",        { 'S', true, true, true, nullptr, nullptr } },
                { "s_reference",        { 'S', true, true, true, nullptr, nullptr } },
                { "k_journal_reversal", { 'S', true, true, true, nullptr, nullptr } }
            };
        }
};

//-------------------------------------------------------------------------
// Called by the main to initialize the class
bool AccountManager::start ( void )
{
    masnae::globals::publisher->subscribe( this );
    //
    if ( ! update_config() )
        return false;
    //
    // Check that timezones are configured in MySQL
    {
        auto                         db = masnae::globals::mysql_handler->get_connection( masnae::mysql::reader );
        unsigned                     db_result;
        masnae::SingleKeyValue_List  rows;
        //
        db_result = db.do_select(
                "SELECT "
                " CONVERT_TZ('2007-03-11 2:00:00','US/Eastern','US/Central') AS time1,"
                " CONVERT_TZ('2007-03-11 3:00:00','US/Eastern','US/Central') AS time2 ",
                rows
            );
        //
        if ( db_result != 0 ) {
            masnae::service_error_log( "AccountManager timezone query failed: " + std::to_string( db_result ) );
            return false;
        }
        //
        if ( rows[0]["time1"] != "2007-03-11 01:00:00" ||
             rows[0]["time2"] != "2007-03-11 01:00:00" )
        {
            masnae::service_error_log( "AccountManager timezone not configured" );
            return false;
        }
    }
    //
    return true;
}

//-------------------------------------------------------------------------
// Call before program termination
void AccountManager::stop ( void )
{
    if ( masnae::globals::publisher != nullptr )
        masnae::globals::publisher->unsubscribe( this );
}

//-------------------------------------------------------------------------
// Search and return accounts
void AccountManager::select ( const std::string &           p_fields_list,
                              const std::string &           p_filter,
                              const std::string &           p_order,
                              const std::string &           p_range,
                              masnae::ProcessingResponse &  p_result )
{
    TableAccounts  table;
    //
    table.execute( "SELECT", p_fields_list, p_filter, p_order, p_range, p_result );
}

//-------------------------------------------------------------------------
// Search and return journal and posting entries
void AccountManager::ledger ( const std::string &           p_fields_list,
                              const std::string &           p_filter,
                              const std::string &           p_order,
                              const std::string &           p_range,
                              masnae::ProcessingResponse &  p_result )
{
    TableLedger  table;
    //
    table.execute( "SELECT", p_fields_list, p_filter, p_order, p_range, p_result );
}

//-------------------------------------------------------------------------
// Create a new account
// 100tps (@ 10K account, MariaDB defaults, 2 cores)
// Returns:
//      success
//      failed
//      rejected    prefix_length   null
void AccountManager::create ( const std::string &           p_currency,
                              const std::string &           p_overdraft,
                              const std::string &           p_ceiling,
                              const std::string &           p_prefix,
                              masnae::ProcessingResponse &  p_result )
{
    uint64_t     next_account;
    std::string  account;
    //
    auto      db     = masnae::globals::mysql_handler->get_connection( masnae::mysql::writer );
    unsigned  result = db.start_transaction();
    //
    if ( result == 0 ) {
        bool ok = true;
        //
        // Get next id for account (locking)
        ok = ok && get_next_account( db, next_account, p_result );
        //
        // From the next sequence build an account number (adding prefix, padding and key)
        ok = ok && build_account_from_sequence( p_prefix, next_account, account, p_result  );  // next_... changes here
        //
        // Write account to database
        ok = ok && write_account( db, account, p_currency, p_overdraft, p_ceiling, p_result );
        //
        // Fill the result
        ok = ok && fill_successful_result( account, p_overdraft, p_ceiling, p_result );
        //
        // Increase journal and posting ids
        ok = ok && update_next_account( db, next_account, p_result );
        //
        // Finaly, commit all
        if ( ok ) {
            result = db.commit_transaction();
            //
            if ( result != 0 ) {
                masnae::mysql::build_resp_error( result, p_result );
                ok = false;
            }
        }
        //
        if ( ! ok )
            db.rollback_transaction();
    } else {
        masnae::mysql::build_resp_error( result, p_result );
    }
}

//-------------------------------------------------------------------------
// Returns the count of transactions per period for an account
// Parameters
//      p_periods               periods requested (e.g.: 1,7,30)
//      p_timezone              like Europe/Paris
//      p_type & p_category     optional
// 350tps (@ 10K account, 1M journal over one year, periods 30/7/1, MariaDB defaults, 2 cores)
// Returns:
//      success
//      error
//      rejected    invalid_mode

    // Add two Usage objects. If the object is not defined (automaticaly
    // created by UsagePer[x]), the object is ignored (any of the two added
    // objects can be undefined)
    bool AccountManager::Usage::add ( const Usage &  p_usage )
    {
        if ( p_usage.count == 0 )
            return true;
        //
        if ( count == 0 ) {
            count  = p_usage.count;
            amount = p_usage.amount;
        } else {
            count  += p_usage.count;
            if ( amount.add( p_usage.amount ) != adawat::Amount::result::ok )
                return false;
        }
        //
        return true;
    }

    // Given a CSV of periods (in days), checks that all values are
    // integer in the allowed range ([1..constants::k_usage_period_max])
    // and sort the list
    // Returns:
    //      error
    bool AccountManager::get_usage_periods ( const std::string &           p_periods_csv,
                                             std::vector< unsigned > &     p_periods,
                                             unsigned &                    p_period_max,
                                             masnae::ProcessingResponse &  p_result )
    {
        p_result.set_jresp_error( "invalid periods" );
        p_period_max = 0;
        p_periods.clear();
        //
        auto      periods    = adawat::str_split( p_periods_csv, "," );
        unsigned  period_min = constants::k_usage_period_max + 1;
        //
        for ( const auto & period: periods ) {
            try {
                unsigned p = std::stoul( period );
                //
                if ( p > p_period_max ) p_period_max = p;
                if ( p < period_min )   period_min   = p;
                //
                p_periods.push_back( p );
            }
            catch( ... ) {
                return false;
            }
        }
        //
        if ( period_min   < 1                             ||
             p_period_max > constants::k_usage_period_max ||
             period_min   > p_period_max )
            return false;
        //
        // Sort and remove duplicates
        std::sort( p_periods.begin(), p_periods.end() );
        p_periods.erase( std::unique( p_periods.begin(), p_periods.end() ), p_periods.end() );
        //
        return true;
    }
    
    // Depending on the mode, build the amount agregation and
    // the where clause to apply
    // Returns:
    //      rejected    invalid_mode
    bool AccountManager::get_usage_mode ( const std::string &           p_mode,
                                          std::string &                 p_mode_amount,
                                          std::string &                 p_mode_where,
                                          masnae::ProcessingResponse &  p_result )
    {
        if ( p_mode == "negative" ) {
            p_mode_amount = "SUM(ABS(p.f_amount))";
            p_mode_where  = " p.f_amount <= 0 AND ";
            return true;
        }
        //
        if ( p_mode == "positive" ) {
            p_mode_amount = "SUM(p.f_amount)";
            p_mode_where  = " p.f_amount >= 0 AND ";
            return true;
        }
        //
        if ( p_mode == "sum" ) {
            p_mode_amount = "SUM(p.f_amount)";
            p_mode_where  = "";
            return true;
        }
        //
        if ( p_mode == "sum_absolute" ) {
            p_mode_amount = "SUM(ABS(p.f_amount))";
            p_mode_where  = "";
            return true;
        }
        //
        p_result.set_jresp_rejected( "invalid_mode", "null" );
        return false;
    }

    // Returns the WHERE clause to select period(s) needed to fill
    // the requested usage periods (p_period_max in days)
    std::string AccountManager::get_db_periods ( unsigned  p_period_max )
    {
        time_t   now  = time( nullptr ),
                 then = now - static_cast< int >( p_period_max * 86400 );
        std::tm  utc_from, utc_to;
        //
        if ( then >= now                               ||  // protect against overflow in cast above
             gmtime_r( & then, & utc_from ) == nullptr ||
             gmtime_r( & now,  & utc_to )   == nullptr )
            return "";
        //
        std::string list;
        //
        for (;;) {  // start from utc_from
            list += adawat::str_sprintf( ",%4u%02d",
                                         utc_from.tm_year + 1900,
                                         utc_from.tm_mon  + 1 );
            //
            // until reaching utc_to
            if ( utc_from.tm_mon == utc_to.tm_mon && utc_from.tm_year == utc_to.tm_year )
                break;
            //
            // move to following period
            if ( ++utc_from.tm_mon == 12 ) {
                utc_from.tm_mon = 0;
                utc_from.tm_year++;
            }
        }
        //
        list[ 0 ] = ' ';  // remove leading ,
        //
        return " p.i_period IN (" + list + ") AND ";
    }

    // Extract account's data
    // Returns:
    //      error
    //
    // SELECT datediff(CONVERT_TZ(UTC_TIMESTAMP(),'UTC','Europe/Paris'),local_date) AS days_ago,
    //        f_amount, s_currency, i_count
    // FROM (
    //    SELECT DATE(CONVERT_TZ(j.d_date,'UTC','Europe/Paris')) AS local_date,
    //           SUM(ABS(p.f_amount)) AS f_amount, p.s_currency, COUNT(k_account) AS i_count
    //    FROM t_posting p
    //    JOIN t_journal j ON j.k_journal=p.k_journal AND j.i_period=p.i_period
    //    WHERE p.k_account='55000865984' AND
    //          p.i_period IN ( 201904,201905) AND
    //          p.f_amount <= 0 AND
    //          j.d_date > DATE_SUB(UTC_TIMESTAMP(), INTERVAL 30 DAY) AND
    //          j.k_journal_reversal IS NULL AND
    //          ( j.s_type<>'closing' || j.s_category<>'system' )
    //    GROUP BY local_date
    // ) subquery;
    //
    bool AccountManager::get_usage_data( const std::string &            p_mode_amount,
                                         const std::string &            p_mode_where,
                                         const std::string &            p_account,
                                         const std::string &            p_timezone,
                                         const std::string &            p_type,
                                         const std::string &            p_category,
                                         unsigned                       p_period_max,
                                         UsagePer &                     p_usage_per_day,
                                         masnae::ProcessingResponse &   p_result )
    {
        auto                         db = masnae::globals::mysql_handler->get_connection( masnae::mysql::reader );
        unsigned                     db_result;
        masnae::SingleKeyValue_List  rows;
        //
        // Aggregates the journal entries of the account per LOCAL (to the user)
        // day: date are in UTC in the database. Collects all figures for the
        // maximal (longest) period. Ignore reversed journal entries and closing
        // entries.
        // In resulting set, days_ago starts at 0 (today) and can have an extra
        // row (days_ago=30 when period_max is 30).
        db_result = db.do_select(
            "SELECT datediff(CONVERT_TZ(UTC_TIMESTAMP(),'UTC','" + p_timezone + "'),local_date) AS days_ago,"
            "       f_amount, s_currency, i_count "
            "FROM ("
            "  SELECT DATE(CONVERT_TZ(j.d_date,'UTC','" + p_timezone + "')) AS local_date,"
            "     " + p_mode_amount + " AS f_amount, p.s_currency, COUNT(k_account) AS i_count"
            "  FROM t_posting p"
            "  JOIN t_journal j ON j.k_journal=p.k_journal AND j.i_period=p.i_period "
            "  WHERE p.k_account='" + p_account + "' AND " +
                 get_db_periods( p_period_max ) +    // p.i_period IN ( ... ) AND
                 p_mode_where +                      // p.f_amount <= 0 AND
                 ( p_type.empty() ? p_type :
            "        j.s_type='" + p_type + "' AND" ) +
                 ( p_category.empty() ? p_category :
            "        j.s_category='" + p_category + "' AND" ) +
            "    j.d_date > DATE_SUB(UTC_TIMESTAMP(), INTERVAL " + std::to_string( p_period_max ) + " DAY) AND"
            "    j.k_journal_reversal IS NULL AND "
            "    ( j.s_type<>'closing' || j.s_category<>'system' )"
            "  GROUP BY local_date"
            ") subquery",
            rows
        );
        //
        if ( db_result != 0 ) {
            p_result.set_jresp_failed();
            p_result.add_error( "usage select error: " + std::to_string( db_result ) );
            return false;
        }
        //
        // Convert from database string resuls to a UsagePer, indexed by
        // the day from today (today is 0)
        p_usage_per_day.clear();
        //
        for ( auto & row: rows ) {
            unsigned days_ago;
            Usage    new_usage;
            //
            auto r = new_usage.amount.set( row["f_amount"], row["s_currency"] );
            //
            if ( r != adawat::Amount::result::ok ) {
                p_result.set_jresp_failed();
                p_result.add_error( "usage amount invalid: " + adawat::Amount::to_string( r ) );
                return false;
            }
            //
            try {
                days_ago        = std::stoul( row["days_ago"] );
                new_usage.count = std::stoul( row["i_count"] );
            }
            catch( ... ) {
                p_result.set_jresp_failed();
                p_result.add_error( "usage stoul error " + row["days_ago"] + "," + row["i_count"] );
                return false;
            }
            //
            p_usage_per_day[ days_ago ] = new_usage;
        }
        //
        return true;
    }
    //
    // Aggregate table with usage per day into a table with usage per requested periods:
    //  p_periods:        [1,7,30]
    //                                                                 in aggregates:
    //  p_usage_per_day:  days_ago  f_amount  s_currency  i_count
    //                    0         "1"       "EUR"       "1"       ->  1, 7, 30
    //                    5         "2"       "EUR"       "2"       ->  7, 30
    //                    8         "4"       "EUR"       "4"       ->  30
    //                    15        "8"       "EUR"       "8"       ->  30
    // Returns:
    //      error
    //                
    bool AccountManager::aggregate_periods( const UsagePer &                 p_usage_per_day,
                                            const std::vector< unsigned > &  p_periods,
                                            UsagePer &                       p_usage_per_period,
                                            masnae::ProcessingResponse &     p_result )
    {
        p_usage_per_period.clear();
        //
        // Add to a period the days exclusively to this period
        // (add day 5 to period 7 but not 30)
        {
            auto p = p_periods.begin();
            //
            for ( const auto u: p_usage_per_day ) {
                for (; p != p_periods.end(); ) {                    // no more period to store u
                    if ( u.first < *p ) {                           // add to current period
                        p_usage_per_period[ *p ].add( u.second );   // 1:#1 7:#2 30:#8,#15
                        break;                                      // move to next u
                    }
                    //
                    p++;                                            // move to next period
                }
            }
        }
        //
        // Add to a period the previous period (add 1 to 7, 7 to 30)
        for ( auto p = p_periods.begin() + 1; p != p_periods.end(); p++ )    // 7,30
            if ( ! p_usage_per_period[ *p ].add( p_usage_per_period[ *(p - 1) ] ) ) {  // 1:#1 7:#2,#1 30:#8,#11,#2,#1
                p_result.set_jresp_failed();
                p_result.add_error( "aggregate_periods add" );
                return false;
            }
        //
        return true;
    }

void AccountManager::usage ( const std::string &           p_mode,
                             const std::string &           p_account,
                             const std::string &           p_periods,
                             const std::string &           p_timezone,
                             const std::string &           p_type,
                             const std::string &           p_category,
                             masnae::ProcessingResponse &  p_result )
{
    // Start by protecting inputs
    std::string account  = masnae::mysql::sql_escape( p_account );
    std::string timezone = masnae::mysql::sql_escape( p_timezone );
    std::string type     = masnae::mysql::sql_escape( p_type );
    std::string category = masnae::mysql::sql_escape( p_category );
    //
    // Preprocess and check periods
    std::vector< unsigned >  periods;
    unsigned                 period_max;
    if ( ! get_usage_periods( p_periods, periods, period_max, p_result ) )
        return;
    //
    // Define which operations to take into account and how to count them
    std::string mode_amount, mode_where;
    //
    if ( ! get_usage_mode( p_mode, mode_amount, mode_where, p_result ) )
        return;
    //
    // Extract usages
    UsagePer usage_per_day;
    //
    if ( ! get_usage_data( mode_amount, mode_where, account, timezone, type, category, period_max,
                           usage_per_day, p_result ) )
        return;
    //
    // Aggregates usage per day into usage per (requested) period
    UsagePer usage_per_period;
    //
    if ( ! aggregate_periods( usage_per_day, periods, usage_per_period, p_result ) )
        return;
    //
    // Build result
    std::string json;
    //
    for ( auto & usage: usage_per_period ) {
        json += ",\"" + std::to_string( usage.first ) + "\":" +
                    masnae::json_convert( {
                    { "amount",   usage.second.amount.value()          },
                    { "currency", usage.second.amount.currency()       },
                    { "count",    std::to_string( usage.second.count ) }
                } );
    }
    //
    json[0] = '{';  // replace leading ',' by {
    json   += "}";
    //
    p_result.set_jresp_success( json );
}

//-------------------------------------------------------------------------
// Delete accounts. Their balance must be null and they must
// not be present in t_posting
// Returns:
//      success
//      error
//      failed
//      rejected    not_zero                account
//      rejected    used
void AccountManager::close ( const std::string &           p_accounts,
                             masnae::ProcessingResponse &  p_result )
{
    // Parse a list of accounts, quote them
    std::string accounts_where;
    //
    if ( ! parse_account_list ( p_accounts, accounts_where ) ) {
        p_result.set_jresp_error( "Invalid parameter accounts" );
        return;
    }
    //
    // Transaction
    SingleKey_Accounts  accounts_mem;
    //
    auto     db     = masnae::globals::mysql_handler->get_connection( masnae::mysql::writer );
    unsigned result = db.start_transaction();
    //
    if ( result == 0 ) {
        bool ok = true;
        //
        // Get accounts details (locking)
        ok = ok && get_accounts( db, accounts_where, accounts_mem, p_result );
        //
        // Check that balances are null
        ok = ok && check_null( accounts_mem, p_result );
        //
        // Try to delete them (fail is referenced in t_posting)
        ok = ok && delete_account( db, accounts_where, p_result );
        //
        // Finaly, commit all
        if ( ok ) {
            result = db.commit_transaction();
            //
            if ( result != 0 ) {
                masnae::mysql::build_resp_error( result, p_result );
                ok = false;
            }
        }
        //
        if ( ! ok )
            db.rollback_transaction();
    } else {
        masnae::mysql::build_resp_error( result, p_result );
    }
}

//-------------------------------------------------------------------------
void AccountManager::bus_event ( const masnae::bus::Message &  p_message )
{
    switch ( p_message.type )
    {
        case masnae::bus::Message::config_updated:
            update_config();
            break;
        default:
            break;
    }
}

//-------------------------------------------------------------------------
// Reads configuration from global config object.
bool AccountManager::update_config ( void )
{
    bool         ok = true;
    std::string  new_key;
    //
    decltype( m_control_key_enabled ) new_control_key_enabled;
    decltype( m_control_key_type    ) new_control_key_type;
    int64_t                           new_length;
    decltype( m_prefix              ) new_prefix;                        
    //
    //-----
    new_key =
        masnae::globals::config_handler->get_string( "/account/key",    "none" );
    //
    new_prefix =
        masnae::globals::config_handler->get_string( "/account/prefix", "1" );
    //
    new_length =
        masnae::globals::config_handler->get_int64 ( "/account/length", 9 );
    //
    if ( new_key == "none" ) {
        new_control_key_enabled = false;
    } else {
        new_control_key_enabled = true;
        ok = ok && adawat::error_control_type_cvt( new_key, new_control_key_type );
    }
    //
    // Total length of prefix + length must fit in a uint64_t
    //
    ok =  ok &&
          new_prefix.length() >= 1 && new_prefix.length() < 18           && 
          std::all_of( new_prefix.begin(), new_prefix.end(), ::isdigit ) &&
          new_prefix[ 0 ] != '0';
    //
    ok =  ok &&
          new_length > 0 &&
          ( new_prefix.length() + static_cast< size_t >( new_length ) ) <= 19;
    //
    //-----
    if ( ok ) {
        std::lock_guard<std::shared_mutex>  lock( m_mutex_config );
        //
        m_control_key_enabled  = new_control_key_enabled;
        m_control_key_type     = new_control_key_type;
        m_length               = static_cast< decltype( m_length ) >( new_length );
        m_prefix               = new_prefix;
    } else {
        masnae::service_error_log( "AccountManager: invalid config" );
    }
    //
    return ok;
}

//-------------------------------------------------------------------------
// Get next ids for account (locking)
// Returns:
//      success
//      failed
bool AccountManager::get_next_account( masnae::mysql::Connection &   p_db,
                                       uint64_t &                    p_next_account,
                                       masnae::ProcessingResponse &  p_result )
{
    masnae::SingleKeyValue_List  rows;
    unsigned                     db_result;
    //
    db_result = p_db.do_select( "SELECT i_next_account FROM t_sequencer FOR UPDATE",
                                rows );
    //
    if ( db_result != 0 ) {
        p_result.set_jresp_failed();
        p_result.add_error( "get_next_account select error " + std::to_string( db_result ) );
        return false;
    }
    //
    if ( rows.size() != 1 ) {
        p_result.set_jresp_failed();
        p_result.add_error( "get_next_account not single row" );
        return false;
    }
    //
    char t;
    //
    if ( sscanf( rows[0]["i_next_account"].c_str(), "%" SCNu64 "%c", & p_next_account, & t ) != 1 ) {
        p_result.set_jresp_failed();
        p_result.add_error( "get_next_account invalid i_next_account" );
        return false;
    }
    //
    p_result.add_log( masnae::ProcessingResponse::verbose,
                      "Sequence: " + rows[0]["i_next_account"] );
    //
    return true;
}

//-------------------------------------------------------------------------
// From the next sequence build an account number (adding prefix, padding and key)
// Change next_account to next value
// Returns:
//      failed
//      rejected    prefix_length   null
bool AccountManager::build_account_from_sequence( const std::string &           p_prefix,
                                                  uint64_t &                    p_next_account,
                                                  std::string &                 p_account,
                                                  masnae::ProcessingResponse &  p_result )
{
    // Get config
    decltype( m_control_key_enabled ) control_key_enabled;
    decltype( m_control_key_type    ) control_key_type;
    decltype( m_length              ) length;
    decltype( m_prefix              ) prefix;
    //
    {
        std::shared_lock< std::shared_mutex > lock( m_mutex_config );
        //
        control_key_enabled = m_control_key_enabled;
        control_key_type    = m_control_key_type;
        length              = m_length;
        prefix              = m_prefix;
    }
    //
    // If a prefix is specified, use it. If not use default prefix
    if ( ! p_prefix.empty() )
        prefix = p_prefix;
    //
    // The new account number as it is
    p_account = std::to_string( p_next_account );
    //
    // Reject the request if the requested length is too short
    // (or the sequence too large)
    if ( ( prefix.length() + p_account.length() ) > length ) {
        // If the prefix is specified by the caller, assume it is too long
        // If the default prefix is used, it must be the server fault
        if ( p_prefix.empty() ) {
            p_result.add_error( "build_account_from_sequence length too short or system full" );
            p_result.set_jresp_failed();
        } else {
            p_result.set_jresp_rejected( "prefix_length", "null" );
        }
        return false;
    }
    //
    // Pad (to the left) with 0 and add prefix
    p_account.insert( 0, length - ( prefix.length() + p_account.length() ), '0' );
    p_account.insert( 0, prefix );
    //
    // If requested, add a control signature to the account number
    if ( control_key_enabled ) {
        if ( ! adawat::error_control_sign( p_account, control_key_type ) ) {
            p_result.add_error( "build_account_from_sequence adding control key on " + p_account );
            p_result.set_jresp_failed();
            return false;
        }
    }
    //
    // Move to next account number
    p_next_account++;
    //
    return true;
}

//-------------------------------------------------------------------------
// Write account to database
// Returns:
//      success
//      failed
bool AccountManager::write_account( masnae::mysql::Connection &   p_db,
                                    const std::string &           p_account,
                                    const std::string &           p_currency,
                                    const std::string &           p_overdraft,
                                    const std::string &           p_ceiling,
                                    masnae::ProcessingResponse &  p_result )
{
    p_result.add_log( masnae::ProcessingResponse::regular,
                      "Adding account " + p_account +
                      " [" + p_overdraft + "," + p_ceiling + "] " +
                      p_currency );
    //
    unsigned     db_result;
    std::string  id;
    //
    db_result = p_db.do_insert(
        "INSERT INTO t_accounts (k_account,s_currency,f_overdraft,f_ceiling) VALUES ("
        "'" + masnae::mysql::sql_escape( p_account   ) + "',"
        "'" + masnae::mysql::sql_escape( p_currency  ) + "',"
        "'" + masnae::mysql::sql_escape( p_overdraft ) + "',"
        "'" + masnae::mysql::sql_escape( p_ceiling   ) + "')",
        id
    );
    //
    if ( db_result != 0 ) {
        p_result.set_jresp_failed();
        p_result.add_error( "write_account insert error " + std::to_string( db_result ) );
        return false;;
    }
    //
    return true;
}

//-------------------------------------------------------------------------
// Set JResp data with the result of a account creation
bool AccountManager::fill_successful_result ( const std::string &           p_account,
                                              const std::string &           p_overdraft,
                                              const std::string &           p_ceiling,
                                              masnae::ProcessingResponse &  p_result )
{
    p_result.set_jresp_success( masnae::json_convert( {
        { "account",   p_account   },
        { "overdraft", p_overdraft },
        { "ceiling",   p_ceiling   }
    } ) );
    //
    return true;
}

//-------------------------------------------------------------------------
// Write next sequence value to use
// Returns:
//      success
//      failed
bool AccountManager::update_next_account( masnae::mysql::Connection &   p_db,
                                          uint64_t                      p_next_account,
                                          masnae::ProcessingResponse &  p_result )
{
    size_t    affected;
    unsigned  db_result;
    //
    db_result = p_db.do_update(
        "UPDATE t_sequencer SET"
        " i_next_account = " + std::to_string( p_next_account ),
        affected );
    //
    if ( db_result != 0 ) {
        p_result.set_jresp_failed();
        p_result.add_error( "update_next_account update error " + std::to_string( db_result ) );
        return false;
    }
    //
    if ( affected != 1 ) {
        p_result.set_jresp_failed();
        p_result.add_error( "update_next_account update changed " + std::to_string( affected ) + " rows" );
        return false;
    }
    //
    return true;
}

//-------------------------------------------------------------------------
// Parse a CSV list of account, quote them, check
// that it is not empty
bool AccountManager::parse_account_list ( const std::string &  p_accounts,
                                          std::string &        p_accounts_where )
{
    // Parse CSV
    auto set = adawat::str_split( p_accounts, "," );
    //
    if ( set.empty() )
        return false;
    //
    // Build WHERE clause
    for ( const auto & a: set )
        p_accounts_where += ",'" + masnae::mysql::sql_escape( a ) + "'";
    //
    p_accounts_where[ 0 ] = ' ';  // replace leading comma by space
    //
    return true;
}

//-------------------------------------------------------------------------
// Check that all balance are null
// Returns:
//      rejected    not_zero                account
bool AccountManager::check_null( const SingleKey_Accounts &    p_accounts_mem,
                                 masnae::ProcessingResponse &  p_result )
{
    for ( const auto & account: p_accounts_mem ) {
        if ( ! account.second.balance.is_zero() ) {
            p_result.set_jresp_rejected(
                "not_zero",
                masnae::json_quote_string( account.first )
            );
            //
            return false;
        }
    }
    //
    return true;
}

//-------------------------------------------------------------------------
// Delete accounts
// Returns:
//      failed
//      rejected    used
bool AccountManager::delete_account( masnae::mysql::Connection &   p_db,
                                     const std::string &           p_accounts_where,
                                     masnae::ProcessingResponse &  p_result )
{
    unsigned     db_result;
    size_t       affected;
    //
    db_result = p_db.do_delete(
        "DELETE FROM t_accounts WHERE k_account IN (" + p_accounts_where + ")",
        affected
    );
    //
    // Even if the account's balance is null, it cannot be deleted
    // if it is still referenced in a posting: FK error
    if ( db_result == 1451 ) {  // Cannot delete or update a parent row: a foreign key constraint fails
        p_result.set_jresp_rejected( "used", "null" );
        return false;
    }
    //
    if ( db_result != 0 ) {
        p_result.set_jresp_failed();
        p_result.add_error( "delete_account error " + std::to_string( db_result ) );
        return false;
    }        //
    return true;
}

} // namespace admete
