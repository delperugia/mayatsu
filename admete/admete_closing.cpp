// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

// Copyright 2019 Pierre del Perugia
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <adawat/adawat_string.h>
#include <adawat/adawat_time.h>

#include "masnae_common.h"
#include "masnae_mysql.h"
#include "masnae_global.h"

#include "admete_common.h"
#include "admete_misc.h"

namespace admete {

//-------------------------------------------------------------------------
// Extract all accounts with non-null forward balance, along with their currency
bool extract_forwarded_account ( masnae::mysql::Connection &   p_db,
                                 const std::string &           p_table,
                                 const std::string &           p_period,
                                 masnae::ProcessingResponse &  p_result )
{
    size_t    affected;
    unsigned  db_result;
    //
    // This of course is not needed
    db_result = p_db.do_update(
        "DROP TEMPORARY TABLE IF EXISTS " + p_table,
        affected );
    //
    if ( db_result != 0 ) {
        p_result.set_jresp_failed();
        p_result.add_error( "extract_forwarded_account drop error " + std::to_string( db_result ) );
        return false;
    }
    //
    // Temporary table is only visible to this session and is dropped automatically
    db_result = p_db.do_update(
        "CREATE TEMPORARY TABLE " + p_table + " AS ("
        "  SELECT k_account, SUM(f_amount) AS f_amount, s_currency"
        "  FROM t_posting"
        "  WHERE i_period <= " + p_period +
        "  GROUP BY k_account"
        "  HAVING f_amount <> 0"
        ")",
        affected );
    //
    if ( db_result != 0 ) {
        p_result.set_jresp_failed();
        p_result.add_error( "extract_forwarded_account select error " + std::to_string( db_result ) );
        return false;
    }
    //
    printf( "    %s %zu accounts to forward\n",
                 adawat::time_format_rfc3339_local().c_str(),
                 affected );
    //
    return true;
}

//-------------------------------------------------------------------------
// Retrieve currencies from the extract
bool get_currencies_forwarded ( masnae::mysql::Connection &   p_db,
                                const std::string &           p_table, 
                                masnae::ValueList &           p_currencies,
                                masnae::ProcessingResponse &  p_result )
{
    masnae::SingleKeyValue_List  rows;
    unsigned                     db_result;
    //
    db_result = p_db.do_select(
        "SELECT DISTINCT s_currency"
        " FROM " + p_table,
        rows );
    //
    if ( db_result != 0 ) {
        p_result.set_jresp_failed();
        p_result.add_error( "get_currencies_forwarded error " + std::to_string( db_result ) );
        return false;
    }
    //
    for ( auto & row : rows )
        p_currencies.push_back( row[ "s_currency" ] );
    //
    printf( "    %s currencies: %s\n",
                 adawat::time_format_rfc3339_local().c_str(),
                 adawat::str_join( p_currencies ).c_str() );
    //
    return true;
}

//-------------------------------------------------------------------------
// Delete all references to this period from the tables
// Deleting journal entries in a transaction is slow due
// to the FK_journal foreign key. So this function
// temporary disable FK checks.
//    
//     j.FK_reversal | p.FK_accounts | p.FK_journal | DELETE p + j
//    ----------------------------------------------|------------
//           -       |      -        |       -      | 1:22 +  1:24
//           -       |      -        |       x      | 2:16 + 11:08
//           -       |      x        |       -      | 2:13 +  1:32
//           -       |      x        |       x      | 1:30 +  9:55
//           x       |      -        |       -      | 2:16 +  1:40
//           x       |      -        |       x      | 1:38 + 10:27
//           x       |      x        |       -      | 1:30 +  1:46
//           x       |      x        |       x      | 2:18 + 10:51
//    

// There is no real benefit doing chunked deletes here: they are
// anyway done in the same transaction. It does however have a
// significant impact on duration (183s vs 147s with two 1M loops
// vs one 2M). Code kept if ever needed
constexpr size_t  k_delete_by = 100000000;

bool delete_period ( masnae::mysql::Connection &   p_db,
                     const std::string &           p_period,
                     masnae::ProcessingResponse &  p_result )
{
    size_t    affected, total_affected = 0;
    unsigned  db_result;
    //
    //------------
    // Disable FK enforcement
    db_result = p_db.do_update( "SET FOREIGN_KEY_CHECKS=0", affected );
    //
    if ( db_result != 0 ) {
        p_result.set_jresp_failed();
        p_result.add_error( "delete_period disable FK error " + std::to_string( db_result ) );
        return false;
    }
    //
    //------------
    // Delete posting entries for the period
    total_affected = 0;
    //
    do {
        db_result = p_db.do_update(
            "DELETE FROM t_posting"
            " WHERE i_period <= " + p_period +
            " LIMIT " + std::to_string( k_delete_by ),
            affected );
        //
        if ( db_result != 0 ) {
            p_result.set_jresp_failed();
            p_result.add_error( "delete_period t_posting error " + std::to_string( db_result ) );
            return false;
        }
        //
        total_affected += affected;
    } while ( affected == k_delete_by );
    //
    printf( "    %s %zu postings deleted\n",
                 adawat::time_format_rfc3339_local().c_str(),
                 total_affected );
    //
    //------------
    // Delete journal entries for the period
    total_affected = 0;
    //
    do {
        db_result = p_db.do_update(
            "DELETE FROM t_journal"
            " WHERE i_period <= " + p_period +
            " LIMIT " + std::to_string( k_delete_by ),
            affected );
        //
        if ( db_result != 0 ) {
            p_result.set_jresp_failed();
            p_result.add_error( "delete_period t_journal error " + std::to_string( db_result ) );
            return false;
        }
        //
        total_affected += affected;
    } while ( affected == k_delete_by );
    //
    printf( "    %s %zu journals deleted\n",
                 adawat::time_format_rfc3339_local().c_str(),
                 total_affected );
    //
    //------------
    // Re-enable FK enforcement
    db_result = p_db.do_update( "SET FOREIGN_KEY_CHECKS=1", affected );
    //
    if ( db_result != 0 ) {
        p_result.set_jresp_failed();
        p_result.add_error( "delete_period enable FK error " + std::to_string( db_result ) );
        return false;
    }
    //
    //------------
    return true;
}

//-------------------------------------------------------------------------
// Copy extract for a currency to posting
// next_.. change here
bool copy_posting ( masnae::mysql::Connection &   p_db,
                    const std::string &           p_table, 
                    const std::string &           p_currency,
                    uint64_t &                    p_next_journal, 
                    uint64_t &                    p_next_posting, 
                    const std::string &           p_period,
                    masnae::ProcessingResponse &  p_result )
{
    size_t    affected;
    unsigned  db_result;
    //
    // Use a variable to generate posting numbers
    db_result = p_db.do_update(
        "INSERT INTO t_posting"
        " SELECT k_post,"
        "  	     " + p_period                         + " AS i_period,"
        "        " + std::to_string( p_next_journal ) + " AS k_journal,"
        "        k_account, f_amount, s_currency"
        " FROM"
        " ("
        "     SELECT @post:=" + std::to_string( p_next_posting - 1) +  // following select does +1 first
        " ) init,"
        " ("
        "     SELECT @post:=@post+1 AS k_post,k_account,f_amount,s_currency"
        "     FROM " + p_table +
        "     WHERE s_currency = '" + p_currency + "'"
        " ) main",
        affected );
    //
    if ( db_result != 0 ) {
        p_result.set_jresp_failed();
        p_result.add_error( "copy_posting error " + std::to_string( db_result ) );
        return false;
    }
    //
    printf( "    %s %zu new posting inserted for %s\n",
                 adawat::time_format_rfc3339_local().c_str(),
                 affected, p_currency.c_str() );
    //
    p_next_journal += 1;
    p_next_posting += affected;
    //
    return true;
}

//-------------------------------------------------------------------------
// Delete forwarded extract
bool delete_extract ( masnae::mysql::Connection &   p_db,
                      const std::string &           p_table,
                      masnae::ProcessingResponse &  p_result )
{
    size_t    affected;
    unsigned  db_result;
    //
    db_result = p_db.do_update(
        "DROP TEMPORARY TABLE IF EXISTS " + p_table,
        affected );
    //
    if ( db_result != 0 ) {
        p_result.set_jresp_failed();
        p_result.add_error( "delete_extract error " + std::to_string( db_result ) );
        return false;
    }
    //
    printf( "    %s temporary table deleted\n",
                 adawat::time_format_rfc3339_local().c_str() );
    //
    return true;
}

//-------------------------------------------------------------------------
// Returns true if the given period is a valid one
// Note: no control is made on the date itslef: it is possible
// to close the current period if needed
bool valid_period ( const std::string &  p_period )
{
    // Check first that it is a valid number
    if ( p_period.length() != 6                                       ||
         ! std::all_of( p_period.begin(), p_period.end(), ::isdigit ) )
    {
        return false;
    }
    //
    return true;
}

//-------------------------------------------------------------------------
// Close an accounting period
// With 10K accounts, 2M journal entries:
//   Starting period closing 201904
//       2019-05-07T00:00:00+02:00 starting transaction
//       2019-05-07T00:00:10+02:00 9741 accounts to forward
//       2019-05-07T00:00:10+02:00 currencies: EUR
//       2019-05-07T00:02:37+02:00 2000000 postings deleted
//       2019-05-07T00:04:19+02:00 1000000 journals deleted
//       2019-05-07T00:04:19+02:00 EUR: journal #4369741, posting #8749547
//       2019-05-07T00:04:29+02:00 9741 new posting inserted for EUR
//       2019-05-07T00:04:29+02:00 temporary table deleted
//       2019-05-07T00:04:29+02:00 transaction commited
//   Period closing finished
//
bool closing ( const std::string &  p_period )
{
    // Pre-allocate a DB handle
    {
        masnae::globals::mysql_handler->get_connection( masnae::mysql::writer );
    }
    //
    printf( "Starting period closing %s\n", p_period.c_str() );
    //
    // Check parameter
    if ( ! valid_period( p_period ) ) {
        printf( "invalid period\n" );
        return false;
    }
    //
    // Transacion timestamp
    time_t      now            = time( nullptr );
    std::string date           = masnae::mysql::datetime_utc( now );
    std::string narration      = constants::k_closing_prefix + p_period;
    std::string extract_table  = "closing_"        + p_period;  // temporary table name
    std::string current_period = get_period( now );
    //
    // Transaction
    masnae::ValueList           currencies;
    uint64_t                    next_journal, next_posting;
    masnae::ProcessingResponse  result;
    //
    printf( "    %s starting transaction\n",
                 adawat::time_format_rfc3339_local().c_str() );
    //
    bool     ok        = true;
    auto     db        = masnae::globals::mysql_handler->get_connection( masnae::mysql::writer );
    unsigned db_result = db.start_transaction();
    //
    if ( db_result == 0 ) {
        //
        // Extract all accounts with non-zero balance, along with their currency
        ok = ok && extract_forwarded_account( db, extract_table, p_period, result );
        //
        // Retrieve currencies from this extract
        ok = ok && get_currencies_forwarded( db, extract_table, currencies, result );
        //
        // Delete all references to this period from the tables
        ok = ok && delete_period( db, p_period, result );
        //
        // Get next ids for journal and posting (locking)
        ok = ok && get_next_sequence( db, next_journal, next_posting, result );
        //
        // Create a journal entry per currency
        for ( auto currency  = currencies.begin();
                   currency != currencies.end() && ok;
                   currency++  )
        {
            printf( "    %s %s: journal #%" PRIu64 ", posting #%" PRIu64 "\n",
                    adawat::time_format_rfc3339_local().c_str(),
                    (*currency).c_str(), next_journal, next_posting );
            //
            // Write journal
            ok = write_journal( db, current_period, next_journal, date,
                                "closing", "system", narration,
                                "", "",
                                result );
            //
            // Copy accounts with non-zero balance for this currency to posting
            ok = ok && copy_posting( db, extract_table, *currency,
                                     next_journal, next_posting, current_period,
                                     result );  // next_.. change here
        }
        //
        // Increase journal and posting ids
        ok = ok && update_next_sequence( db, next_journal, next_posting, result );
        //
        // Delete forwarded extract
        // Note: even on error try to drop the table
        ok = delete_extract( db, extract_table, result ) && ok;
        //
        // Finaly, commit all
        if ( ok ) {
            db_result = db.commit_transaction();
            //
            if ( db_result == 0 ) {
                printf( "    %s transaction commited\n",
                             adawat::time_format_rfc3339_local().c_str() );
            } else {
                printf( "database commit error %u\n", db_result );
                ok = false;
            }
        } else {
            printf( "error: %s\n", result.get_error().c_str() );
        }
        //
        if ( ! ok )
            db.rollback_transaction();
    } else {
        printf( "database transaction error %u\n", db_result );
        ok = false;
    }
    //
    printf( "Period closing finished\n" );
    //
    return ok;
}

} // namespace admete
