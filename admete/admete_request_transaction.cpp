// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

// Copyright 2019 Pierre del Perugia
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <adawat/adawat_money.h>
#include <adawat/adawat_string.h>

#include "masnae_global.h"
#include "masnae_misc.h"
#include "masnae_mysql.h"

#include "admete_misc.h"
#include "admete_request.h"

namespace admete {

//-------------------------------------------------------------------------
// Records a list of operations on accounts. The sum of all amounts must
// be null, all accounts must use the same currency as the transaction.
// The final balance of each account must be in the overdraft / ceiling range.
// Parameter:
//  - records       [{"1":"-2"},{"2":"1"},{"3":"1"}]
//  - currency      EUR
//  - type
//  - category
//  - narration     'first example'
// Returns:
//      success     {"date":"2019-02-07T13:54:36","sequence":"60-3","transaction":"439"}
//      failed
//      error
//      rejected    overflow                account
//                  invalid_currency
//                  not_zero
//                  reservation_credited    reservation
//                  overdraft               account
//                  ceiling                 account
//                  incompatible_currency   account
//                  not_found               account
//                  reservation_not_found   reservation
//                  reservation_too_low     reservation
//                  invalid_value           amount
//                  duplicate_reference
//
// 50tps (@ 10K account, 1M journal, MariaDB defaults, 2 cores)
//
void AdmeteRequestHandler::process_ws_transfer ( const masnae::SingleKey_ValueList &  p_parameters,
                                                 masnae::ProcessingResponse &         p_result )
{
    // Check that the caller is allowed here
    if ( ! has_permission( "admete.transfer", p_result ) )
        return;
    //
    // Check parameters
    std::string  records_json, currency, type, category, narration, user_reference;
    //
    if ( ! masnae::get_parameter( p_parameters, "records",   records_json,   false, false, p_result ) ||
         ! masnae::get_parameter( p_parameters, "currency",  currency,       false, false, p_result ) ||
         ! masnae::get_parameter( p_parameters, "type",      type,           false, true,  p_result ) ||
         ! masnae::get_parameter( p_parameters, "category",  category,       false, true,  p_result ) ||
         ! masnae::get_parameter( p_parameters, "narration", narration,      false, true,  p_result ) ||
         ! masnae::get_parameter( p_parameters, "reference", user_reference, false, true,  p_result ) )
        return;
    //
    // Transacion timestamp
    time_t      now     = time( nullptr );
    std::string period  = get_period( now );
    std::string date    = masnae::mysql::datetime_utc( now );
    //
    p_result.add_log( masnae::ProcessingResponse::regular,
                      "Transfering on " + date );
    //
    // Parse JSON to retrieve records, build SQL WHERE clause to retrieve
    // accounts, check that sum of records is zero. Set p_result on error
    List_Record  records;
    std::string  accounts_where;
    //
    if ( ! prepare_records( records_json, currency,
                            records, accounts_where,
                            p_result ) )
        return;
    //
    // Transaction
    uint64_t                     next_journal, next_posting;
    SingleKey_Accounts           accounts_mem;
    SingleKey_Reservation        reservation_mem;
    //
    auto     db     = masnae::globals::mysql_handler->get_connection( masnae::mysql::writer );
    unsigned result = db.start_transaction();
    //
    if ( result == 0 ) {
        bool ok = true;
        //
        // Get accounts details (locking them and reservation)
        ok = ok && get_accounts( db, accounts_where, accounts_mem, p_result );
        //
        // Get reservations (locking them and accounts)
        ok = ok && get_reservations( db, accounts_where, reservation_mem, p_result );
        //
        // Applies records to balances, check account overflows / currencies
        ok = ok && apply_records( records, accounts_mem, reservation_mem, p_result );
        //
        // Get next ids for journal and posting (locking)
        ok = ok && get_next_sequence( db, next_journal, next_posting, p_result );
        //
        // If everything goes well, this will be our result (before next_.. change)
        ok = ok && fill_successful_result( date, next_journal, next_posting, records.size(), p_result );
        //
        // Write journal
        ok = ok && write_journal( db, period, next_journal, date,
                                  type, category, narration,
                                  m_security.user, user_reference,
                                  p_result );
        //
        // Write posting, update accounts' balance
        ok = ok && write_posting( db, period, next_journal, next_posting, records, accounts_mem, p_result );  // next_.. change here
        //
        // Update reservations
        ok = ok && write_reservations( db, reservation_mem, p_result );
        //
        // Increase journal and posting ids
        ok = ok && update_next_sequence( db, next_journal, next_posting, p_result );
        //
        // Finaly, commit all
        if ( ok ) {
            result = db.commit_transaction();
            //
            if ( result != 0 ) {
                masnae::mysql::build_resp_error( result, p_result );
                ok = false;
            }
        }
        //
        if ( ! ok )
            db.rollback_transaction();
    } else {
        masnae::mysql::build_resp_error( result, p_result );
    }
}

} // namespace admete
