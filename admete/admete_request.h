// Copyright 2019 Pierre del Perugia
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

//
// Class called by Masnae to process a request
//

#ifndef admete_request_h
#define admete_request_h

#include "masnae_request_handler.h"

namespace admete {

//-------------------------------------------------------------------------
class AdmeteRequestHandler : public masnae::RequestHandler
{
    public:
        AdmeteRequestHandler ();
        //
        // Implementation of the base class pure functions
        //
        // Returns true if request is allowed
        virtual
        bool        check_security  ( const std::string &           p_module,
                                      masnae::Fastcpipp_Path &      p_document,
                                      masnae::ProcessingResponse &  p_result );
        //
        // Returns true if the module/document are found (p_result must be filled then)
        virtual                      
        bool        process_request ( const std::string &           p_module,
                                      masnae::Fastcpipp_Path &      p_document,
                                      masnae::ProcessingResponse &  p_result );
        //
        // Returns information
        // Must return a valid JSON string representing a non-empty object
        virtual
        std::string specific_info   ( void );
        //
        // If invoked, check_security will not call Uriana but use
        // a default built-in context
        void        security_disable_uriana ( void )  { m_security_simulate_uriana = true; }
        //
    private:
        //
        bool    process_ws_request        ( const masnae::Fastcpipp_Path &       p_document,
                                            masnae::ProcessingResponse &         p_result );
        //
        // In admete_admete_request_account.cpp
        bool    process_ws_account        ( const masnae::Fastcpipp_Path &       p_document,
                                            const masnae::SingleKey_ValueList &  p_parameters,
                                            masnae::ProcessingResponse &         p_result );
        //
        void    process_ws_account_select ( const masnae::SingleKey_ValueList &  p_parameters,
                                            masnae::ProcessingResponse &         p_result );
        void    process_ws_account_ledger ( const masnae::SingleKey_ValueList &  p_parameters,
                                            masnae::ProcessingResponse &         p_result );
        void    process_ws_account_create ( const masnae::SingleKey_ValueList &  p_parameters,
                                            masnae::ProcessingResponse &         p_result );
        void    process_ws_account_delete ( const masnae::SingleKey_ValueList &  p_parameters,
                                            masnae::ProcessingResponse &         p_result );
        void    process_ws_account_usage  ( const masnae::SingleKey_ValueList &  p_parameters,
                                            masnae::ProcessingResponse &         p_result );
        //
        // In admete_request_exchange.cpp
        void    process_ws_exchange       ( const masnae::SingleKey_ValueList &  p_parameters,
                                            masnae::ProcessingResponse &         p_result );
        //
        // In admete_request_reservation.cpp
        bool    process_ws_reservation    ( const masnae::Fastcpipp_Path &       p_document,
                                            const masnae::SingleKey_ValueList &  p_parameters,
                                            masnae::ProcessingResponse &         p_result );
        //
        void    process_ws_reservation_select ( const masnae::SingleKey_ValueList &  p_parameters,
                                                masnae::ProcessingResponse &         p_result );
        void    process_ws_reservation_create ( const masnae::SingleKey_ValueList &  p_parameters,
                                                masnae::ProcessingResponse &         p_result );
        void    process_ws_reservation_delete ( const masnae::SingleKey_ValueList &  p_parameters,
                                                masnae::ProcessingResponse &         p_result );
        //
        // In admete_request_reverse.cpp
        void    process_ws_reverse        ( const masnae::SingleKey_ValueList &  p_parameters,
                                            masnae::ProcessingResponse &         p_result );
        //
        // In admete_request_transaction.cpp
        void    process_ws_transfer       ( const masnae::SingleKey_ValueList &  p_parameters,
                                            masnae::ProcessingResponse &         p_result );
        //
        // Security
        bool    m_security_simulate_uriana = false;  // simulate Uriana call when checking security
};

} // namespace admete

#endif
