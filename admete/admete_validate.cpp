// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

// Copyright 2019 Pierre del Perugia
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <iostream>
#include <rapidjson/document.h>
#include <rapidjson/pointer.h>
#include <sstream>
#include <adawat/adawat_string.h>
#include <adawat/adawat_time.h>
#include <adawat/adawat_uid.h>

#include "masnae_common.h"
#include "masnae_misc.h"
#include "masnae_mysql.h"
#include "masnae_global.h"
#include "masnae_simulator.h"

#include "admete_common.h"
#include "admete_misc.h"
#include "admete_request.h"

namespace admete {

bool g_Verbose = false;

//-------------------------------------------------------------------------
void console ( void )
{
    admete::AdmeteRequestHandler  handler;
    //
    handler.security_disable_uriana();
    //
    masnae::Fastcpipp_Path      document;
    masnae::ProcessingResponse  response;
    handler.check_security( "/ws", document, response );
    //
    masnae::console( handler );
}

//-------------------------------------------------------------------------
void start (const char * p_cszModule)
{
    printf("%s:", p_cszModule);
}

//-------------------------------------------------------------------------
bool check (const char * p_cszModule, unsigned p_uSection, unsigned p_uTest, bool p_bCondition)
{
    if ( g_Verbose || ! p_bCondition )
        printf("\n   %s.%02u.%02u: %s", p_cszModule, p_uSection, p_uTest, p_bCondition ? "ok" : "error");
    //
    return p_bCondition;
}

//-------------------------------------------------------------------------
bool stop (const char * p_cszModule, bool p_bCondition)
{
    if ( g_Verbose || ! p_bCondition )
        printf("\n%s: %s\n", p_cszModule, p_bCondition ? "ok" : "error");
    else
        printf(" ok\n");
    //
    return p_bCondition;
}

//-------------------------------------------------------------------------
bool validate_misc ( void )
{
    bool         ok = true;
    //
    List_Record                mvt;
    masnae::ProcessingResponse result;
    //
    parse_records( "[ {\"a\":\"a\",\"m\":\"-2\"}, {\"a\":\"b\",\"m\":\"1\"}, {\"a\":\"a\",\"m\":\"1\"} ]",
                   "EUR",
                   mvt,
                   result );
    //
    ok = ok && check( "misc", 1, 1, result.is_jresp_success() &&
                               mvt[0].account=="a" && mvt[0].amount.value()=="-2" &&
                               mvt[1].account=="b" && mvt[1].amount.value()== "1" &&
                               mvt[2].account=="a" && mvt[2].amount.value()== "1" );
    //
    return ok;
}

//-------------------------------------------------------------------------
std::string make_mvt ( const std::string & p_1,
                       const std::string & p_2,
                       const std::string & p_3,
                       const std::string & p_4,
                       const std::string & p_5,
                       const std::string & p_6 )
{
   std::string json;
   //
   if ( ! p_1.empty() ) json += ",{\"a\":\"1\",\"m\":\"" + p_1 + "\"}";
   if ( ! p_2.empty() ) json += ",{\"a\":\"2\",\"m\":\"" + p_2 + "\"}";
   if ( ! p_3.empty() ) json += ",{\"a\":\"3\",\"m\":\"" + p_3 + "\"}";
   if ( ! p_4.empty() ) json += ",{\"a\":\"4\",\"m\":\"" + p_4 + "\"}";
   if ( ! p_5.empty() ) json += ",{\"a\":\"5\",\"m\":\"" + p_5 + "\"}";
   if ( ! p_6.empty() ) json += ",{\"a\":\"6\",\"m\":\"" + p_6 + "\"}";
   //
   if ( json.empty() )
       json = "[";
   else
       json[ 0 ] = '[';
   //
   json += "]";
   //
   return json;
}

bool check_bal ( std::vector< std::string > p_balances )
{
    masnae::SingleKeyValue_List  rows;
    masnae::mysql::Connection    db = masnae::globals::mysql_handler->get_connection( masnae::mysql::writer );
    //
    db.do_select( "SELECT k_account,f_balance FROM t_accounts "
                  "WHERE k_account IN (1,2,3,4,5,6) ORDER BY k_account",
                  rows );
    //
    size_t max = std::max( p_balances.size(), rows.size() );
    //
    rows.resize      ( max );
    p_balances.resize( max );
    //
    for ( size_t i = 0; i < max; i++ )
        if ( rows[ i ][ "f_balance" ] != p_balances[ i ] )
            return false;
    //
    return true;
}

//-------------------------------------------------------------------------
bool validate_ws_transfer ( void )
{
    struct { std::string currency;
             std::string mvt_1;
             std::string mvt_2;
             std::string mvt_3;
             std::string mvt_4;
             std::string mvt_5;
             std::string mvt_6;
             std::string exp_1;
             std::string exp_2;
             std::string exp_3;
             std::string exp_4;
             std::string exp_5;
             std::string exp_6;
             std::string exp_status;
             std::string exp_reason;  // if status is rejected
             std::string exp_data;    // if status is rejected
    } tests [] =
    {
        /*
        Account 1, 2, 3, 4, 5:
            overdraft: -10, ceiling: 10, initilal: 0
            currency: 1-3: EUR, 4-5: USD
        Account 6 doesn't exit

                1                2              3               4               5             6       status      reason                      data
        ------------------------------------------------------------------------------------------------------------ */
    {  "EUR",   "",              "",            "",             "",             "",           "",         
                "0.00000000",    "0.00000000",  "0.00000000",   "0.00000000",   "0.00000000", "",     "error",    "",                         "" },
                                                                                              
    {  "EUR",   "-6",           "-6",           "12",           "",             "",           "",         
                "0.00000000",    "0.00000000",  "0.00000000",   "0.00000000",   "0.00000000", "",     "rejected", "ceiling",                  "\"3\"" },
                                                                                              
    {  "EUR",   "-5",           "5",            "",             "",             "",           "",         
                "-5.00000000",   "5.00000000",  "0.00000000",   "0.00000000",   "0.00000000", "",     "success",  "",                         "" },
                                                                                              
    {  "EUR",   "",             "6",            "-6",           "",             "",           "",             
                "-5.00000000",   "5.00000000",  "0.00000000",   "0.00000000",   "0.00000000", "",     "rejected",  "ceiling",                 "\"2\"" },
                                                                                              
    {  "EUR",   "6",            "",             "-6",           "",             "",           "",             
                "1.00000000",    "5.00000000",  "-6.00000000",  "0.00000000",   "0.00000000", "",     "success",  "",                         "" },
                                                                                              
    {  "EUR",   "",             "5",            "-5",           "",             "",           "",             
                "1.00000000",   "5.00000000",   "-6.00000000",  "0.00000000",   "0.00000000", "",     "rejected", "overdraft",                "\"3\"" },
                                                                                              
    {  "EUR",   "-1",           "",             "",             "1",            "",           "",
                "1.00000000",   "5.00000000",   "-6.00000000",  "0.00000000",   "0.00000000", "",     "rejected", "incompatible_currency",    "\"4\"" },
                                                                                              
    {  "USD",   "-1",           "",             "",             "1",            "",           "",
                "1.00000000",   "5.00000000",   "-6.00000000",  "0.00000000",   "0.00000000", "",     "rejected", "incompatible_currency",    "\"1\"" },
                                                                                              
    {  "EUR",   "-1",           "2",            "",             "",             "",           "",
                "1.00000000",   "5.00000000",   "-6.00000000",  "0.00000000",   "0.00000000", "",     "rejected", "not_zero",                 "null" },
                                                                                              
    {  "EUR",   "-1",           "1",            "1",            "",             "",           "",                 
                "1.00000000",   "5.00000000",   "-6.00000000",  "0.00000000",   "0.00000000", "",     "rejected", "not_zero",                 "null" },
                                                                                              
    {  "E",     "-1",           "1",            "",             "",             "",           "",                 
                "1.00000000",   "5.00000000",   "-6.00000000",  "0.00000000",   "0.00000000", "",     "rejected", "invalid_currency",         "null" },
                                                                                              
    {  "EUR",   "-1",           "",             "",             "",             "",          "1",                 
                "1.00000000",   "5.00000000",   "-6.00000000",  "0.00000000",   "0.00000000", "",     "rejected", "not_found",                "\"6\"" },
                                                                                              
    {  "EUR",   "-0.1",         "0.05",         "0.05",         "",             "",           "",             
                "0.90000000",   "5.05000000",   "-5.95000000",  "0.00000000",   "0.00000000", "",     "success",  "",                         "" },
                                                                                              
    {  "USD",   "",             "",             "",             "-1",           "1",          "",             
                "0.90000000",   "5.05000000",   "-5.95000000",  "-1.00000000",  "1.00000000", "",     "success",  "",                         "" },

    // resa 3, bal 0.9, over -10
    {  "EUR",   "-8",           "8",            "",             "",             "",           "",             
                "0.90000000",   "5.05000000",   "-5.95000000",  "-1.00000000",  "1.00000000", "",     "rejected", "overdraft",                "\"1\"" },

    {  "",      "",             "",             "",             "",             "",           "",                 
                "",             "",             "",             "",             "",           "",     "",         "",                         "" }
    };
    //
    {
        size_t                     affected;
        masnae::mysql::Connection  db = masnae::globals::mysql_handler->get_connection( masnae::mysql::writer );
        //
        db.do_update( "INSERT INTO t_accounts (k_account,s_currency,f_balance,f_overdraft,f_ceiling) "
                      "VALUES (1,'EUR','0','-10','10'),(2,'EUR','0','-10','10'),(3,'EUR','0','-10','10'),(4,'USD','0','-10','10'),(5,'USD','0','-10','10') "
                      "ON DUPLICATE KEY UPDATE s_currency=VALUES(s_currency),f_balance=VALUES(f_balance),f_overdraft=VALUES(f_overdraft),f_ceiling=VALUES(f_ceiling)",
                      affected );
        db.do_delete( "DELETE FROM t_accounts WHERE k_account=6",
                      affected );
        //
        db.do_delete( "DELETE FROM t_reservations WHERE k_account IN (1,2,3,4,5,6)",
                      affected );
        db.do_update( "INSERT INTO t_reservations (k_account,k_reservation,f_amount,d_expiration) "
                      "VALUES (1,1,1,utc_timestamp() + interval 10 second),"
                             "(1,2,2,utc_timestamp() + interval 10 second),"
                             "(1,3,9,utc_timestamp() - interval 10 second)",  // expired
                      affected );
        //
        masnae::SingleKeyValue_List  rows;
        masnae::ValueList            journals;
        //
        db.do_select( "SELECT k_journal FROM t_posting WHERE k_account IN (1,2,3,4,5,6)",
                      rows );
        //
        for ( auto & row : rows )
            journals.push_back( row[ "k_journal" ] );
        //
        db.do_delete( "DELETE FROM t_journal WHERE k_journal IN (" + adawat::str_join( journals ) + ")",
                      affected );
        db.do_delete( "DELETE FROM t_posting WHERE k_journal IN (" + adawat::str_join( journals ) + ")",
                      affected );
    }
    //
    bool ok = true;
    //
    AdmeteRequestHandler          handler;
    masnae::Fastcpipp_Path        document;
    masnae::ProcessingResponse    response;
    //
    handler.security_disable_uriana();
    handler.check_security( "/ws", document, response );
    //
    //--------------
    for ( unsigned i = 0; ! tests[ i ].currency.empty(); i++ )
    {
        if (ok) exec_url( "/ws/transfer?currency=" + tests[ i ].currency +
                              "&reference=" + adawat::uid_ts( adawat::uid_type::medium_ts ) +
                              "&records="  + make_mvt( tests[ i ].mvt_1,
                                                       tests[ i ].mvt_2,
                                                       tests[ i ].mvt_3,
                                                       tests[ i ].mvt_4,
                                                       tests[ i ].mvt_5,
                                                       tests[ i ].mvt_6 ),
                  handler, response, g_Verbose );
        //
        ok = ok && check( "ws_transfer", i+1, 1,
                     response.get_jresp_status() == tests[ i ].exp_status
                     &&
                     ( ! response.is_jresp_rejected() ||
                         ( response.get_jresp_reason() == tests[ i ].exp_reason &&
                           response.get_jresp_data()   == tests[ i ].exp_data ) ) );
        //
        ok = ok && check( "ws_transfer", i+1, 2, check_bal( { tests[ i ].exp_1,
                                                         tests[ i ].exp_2,
                                                         tests[ i ].exp_3,
                                                         tests[ i ].exp_4,
                                                         tests[ i ].exp_5,
                                                         tests[ i ].exp_6 } ) );
    }
    //
    //--------------
    if (ok) exec_url( "/ws/transfer?currency=EUR"
                          "&records=[{\"a\":\"1\",\"m\":\"-6\"},{\"a\":\"2\",\"m\":\"-10\"},{\"a\":\"3\",\"m\":\"8\"},{\"a\":\"3\",\"m\":\"8\"}]",
                      handler, response, g_Verbose );
    //
    ok = ok && check( "ws_transfer", 100, 1,
                 is_jresp_rejected( response, "ceiling", "3" ) );
    //
    ok = ok && check( "ws_transfer", 100, 2,
                 check_bal( { "0.90000000","5.05000000","-5.95000000","-1.00000000","1.00000000","" } ) );
    //
    //--------------
    if (ok) exec_url( "/ws/transfer?currency=EUR"
                          "&records=[{\"a\":\"1\",\"m\":\"-2\"},{\"a\":\"3\",\"m\":\"0.66\"},{\"a\":\"3\",\"m\":\"1.34\"}]",
                      handler, response, g_Verbose );
    //
    ok = ok && check( "ws_transfer", 101, 1,
                 response.is_jresp_success() );
    //
    ok = ok && check( "ws_transfer", 102, 2,
                 check_bal( { "-1.10000000","5.05000000","-3.95000000","-1.00000000","1.00000000","" } ) );
    //
    // Using the reservations
    //
    //--------------
    if (ok) exec_url( "/ws/transfer?currency=EUR"
                          "&records=[{\"a\":\"1\",\"m\":\"-2\",\"r\":\"1\"},{\"a\":\"2\",\"m\":\"2\"}]",
                      handler, response, g_Verbose );
    //
    ok = ok && check( "ws_transfer", 201, 1,
                 is_jresp_rejected( response, "reservation_too_low", "1" ) );
    //
    ok = ok && check( "ws_transfer", 201, 2,
                 check_bal( { "-1.10000000","5.05000000","-3.95000000","-1.00000000","1.00000000","" } ) );
    //
    //--------------
    if (ok) exec_url( "/ws/transfer?currency=EUR"
                          "&records=[{\"a\":\"1\",\"m\":\"-2\",\"r\":\"3\"},{\"a\":\"2\",\"m\":\"2\"}]",
                      handler, response, g_Verbose );
    //
    ok = ok && check( "ws_transfer", 202, 1,
                 is_jresp_rejected( response, "reservation_not_found", "3" ) );
    //
    ok = ok && check( "ws_transfer", 202, 2,
                 check_bal( { "-1.10000000","5.05000000","-3.95000000","-1.00000000","1.00000000","" } ) );
    //
    //--------------
    if (ok) exec_url( "/ws/transfer?currency=EUR"
                          "&records=[{\"a\":\"1\",\"m\":\"1\",\"r\":\"1\"},{\"a\":\"2\",\"m\":\"-1\"}]",
                      handler, response, g_Verbose );
    //
    ok = ok && check( "ws_transfer", 203, 1,
                 is_jresp_rejected( response, "reservation_credited", "1" ) );
    //
    ok = ok && check( "ws_transfer", 203, 2,
                 check_bal( { "-1.10000000","5.05000000","-3.95000000","-1.00000000","1.00000000","" } ) );
    //
    //--------------
    if (ok) exec_url( "/ws/transfer?currency=EUR"
                          "&records=[{\"a\":\"1\",\"m\":\"-1\",\"r\":\"2\"},{\"a\":\"1\",\"m\":\"-1\",\"r\":\"1\"},"
                                    "{\"a\":\"2\",\"m\":\"+2\"}]",
                      handler, response, g_Verbose );
    //
    ok = ok && check( "ws_transfer", 204, 1,
                 response.is_jresp_success() );
    //
    ok = ok && check( "ws_transfer", 204, 2,
                 check_bal( { "-3.10000000","7.05000000","-3.95000000","-1.00000000","1.00000000","" } ) );
    //
    //--------------
    if (ok) exec_url( "/ws/transfer?currency=EUR"
                          "&records=[{\"a\":\"1\",\"m\":\"-1\",\"r\":\"2\"},{\"a\":\"2\",\"m\":\"1\"}]",
                      handler, response, g_Verbose );
    //
    ok = ok && check( "ws_transfer", 205, 1,
                 response.is_jresp_success() );
    //
    ok = ok && check( "ws_transfer", 205, 2,
                 check_bal( { "-4.10000000","8.05000000","-3.95000000","-1.00000000","1.00000000","" } ) );
    //
    //--------------
    if (ok) exec_url( "/ws/transfer?currency=EUR"
                          "&records=[{\"a\":\"1\",\"m\":\"-1\",\"r\":\"2\"},{\"a\":\"2\",\"m\":\"1\"}]",
                      handler, response, g_Verbose );
    //
    ok = ok && check( "ws_transfer", 206, 1,
                 is_jresp_rejected( response, "reservation_too_low", "2" ) );
    //
    ok = ok && check( "ws_transfer", 206, 2,
                 check_bal( { "-4.10000000","8.05000000","-3.95000000","-1.00000000","1.00000000","" } ) );
    //
    // Back to well known values for following reverse tests
    //
    //--------------
    if (ok) exec_url( "/ws/transfer?currency=EUR"
                          "&records="  + make_mvt( "3.1", "-3.05", "-0.05", "", "", "" ),
                      handler, response, g_Verbose );
    //
    ok = ok && check( "ws_transfer", 301, 1,
                 response.is_jresp_success() );
    //
    ok = ok && check( "ws_transfer", 301, 2,
                 check_bal( { "-1.00000000","5.00000000","-4.00000000","-1.00000000","1.00000000","" } ) );
    //
    return ok;
}

//-------------------------------------------------------------------------
bool validate_ws_reverse ( void )
{
    std::string journal, reversal, nu;
    bool        ok = true;
    //
    AdmeteRequestHandler          handler;
    masnae::Fastcpipp_Path        document;
    masnae::ProcessingResponse    response;
    //
    handler.security_disable_uriana();
    handler.check_security( "/ws", document, response );
    //
    //--------------
    // A transaction...
    exec_url( "/ws/transfer?currency=EUR"
                          "&records="  + make_mvt( "2", "-3", "1", "", "", "" ),
                      handler, response, g_Verbose );
    //
    ok = ok && check( "ws_reverse", 1, 1,
                 is_jresp_success( response, "/data/transaction", journal ) );
    //
    ok = ok && check( "ws_reverse", 1, 2,
                 check_bal( { "1.00000000","2.00000000","-3.00000000","-1.00000000","1.00000000","" } ) );
    //
    //--------------
    // ... reversed
    if (ok) exec_url( "/ws/reverse?journal=" + journal,
                      handler, response, g_Verbose );
    //
    ok = ok && check( "ws_reverse", 2, 1,
                 is_jresp_success( response, "/data/transaction", reversal ) );
    //
    // Same as 4.2 of validate_ws_transfer
    ok = ok && check( "ws_reverse", 2, 2,
                 check_bal( { "-1.00000000","5.00000000","-4.00000000","-1.00000000","1.00000000","" } ) );
    //
    //--------------
    // Already reversed
    if (ok) exec_url( "/ws/reverse?journal=" + journal,
                      handler, response, g_Verbose );
    //
    ok = ok && check( "ws_reverse", 3, 1,
                 is_jresp_rejected( response, "already_reversed", reversal ) );
    //
    ok = ok && check( "ws_reverse", 3, 2,
                 check_bal( { "-1.00000000","5.00000000","-4.00000000","-1.00000000","1.00000000","" } ) );
    //
    //--------------
    // Overflow if reversed: rejected
    //      1      2      3
    //  =  -1      5     -4
    //            -7     +7       1st, cannot be reversed: bal 2 would be 11, >10
    //     -6     +6              2nd
    if (ok) exec_url( "/ws/transfer?currency=EUR"
                         "&records="  + make_mvt( "", "-7", "+7", "", "", "" ),
                      handler, response, g_Verbose );
    //
    ok = ok && check( "ws_reverse", 4, 1,
                 is_jresp_success( response, "/data/transaction", journal ) );
    //
    if (ok) exec_url( "/ws/transfer?currency=EUR"
                         "&records="  + make_mvt( "-6", "+6", "", "", "", "" ),
                      handler, response, g_Verbose );
    //
    ok = ok && check( "ws_reverse", 4, 2,
                 is_jresp_success( response, "", nu ) );
    //
    ok = ok && check( "ws_reverse", 4, 3,
                 check_bal( { "-7.00000000","4.00000000","3.00000000","-1.00000000","1.00000000","" } ) );
    //
    if (ok) exec_url( "/ws/reverse?journal=" + journal,
                      handler, response, g_Verbose );
    //
    ok = ok && check( "ws_reverse", 4, 5,
                 is_jresp_rejected( response, "ceiling", "2" ) );
    //
    ok = ok && check( "ws_reverse", 4, 6, check_bal( { "-7.00000000","4.00000000","3.00000000","-1.00000000","1.00000000","" } ) );
    //
    return ok;
}

//-------------------------------------------------------------------------
bool validate_account ( void )
{
    std::string a, b, c, d, j, nu;
    bool        ok = true;
    //
    AdmeteRequestHandler          handler;
    masnae::Fastcpipp_Path        document;
    masnae::ProcessingResponse    response;
    //
    handler.security_disable_uriana();
    handler.check_security( "/ws", document, response );
    //
    //--------------
    // Create A, B and C accounts
    exec_url( "/ws/account/create?currency=USD"
                                 "&overdraft=-10&ceiling=0",
                      handler, response, g_Verbose );
    //
    ok = ok && check( "account", 1, 1,
                      is_jresp_success( response, "/data/account", a ) );
    //
    if (ok) exec_url( "/ws/account/create?currency=USD"
                                 "&overdraft=0&ceiling=10",
                      handler, response, g_Verbose );
    //
    ok = ok && check( "account", 1, 2,
                      is_jresp_success( response, "/data/account", b ) );
    //
    if (ok) exec_url( "/ws/account/create?currency=USD"
                                 "&overdraft=0&ceiling=10",
                      handler, response, g_Verbose );
    //
    ok = ok && check( "account", 1, 3,
                      is_jresp_success( response, "/data/account", c ) );
    //
    //--------------
    // Transfer A:-10 B:+4 B:+6
    std::string reference = adawat::uid_ts( adawat::uid_type::medium_ts );
    //
    if (ok) exec_url( "/ws/transfer?currency=USD&type=deposit&category=test&reference=" + reference +
                          "&records=[{\"a\":\""+a+"\",\"m\":\"-10\"},{\"a\":\""+b+"\",\"m\":\"4\"},{\"a\":\""+b+"\",\"m\":\"6\"}]",
                      handler, response, g_Verbose );
    //
    ok = ok && check( "account", 2, 1,
                      is_jresp_success( response, "/data/transaction", j ) );
    //
    // Duplicate user reference, if using:
    if (ok) exec_url( "/ws/transfer?currency=USD&type=deposit&category=test&reference=" + reference +
                          "&records=[{\"a\":\""+a+"\",\"m\":\"2\"},{\"a\":\""+b+"\",\"m\":\"-1\"},{\"a\":\""+b+"\",\"m\":\"-1\"}]",
                      handler, response, g_Verbose );
    //
    ok = ok && check( "account", 2, 2,
                      is_jresp_rejected( response, "duplicate_reference", nu ) );
    //
    //--------------
    // Delete A rejected
    if (ok) exec_url( "/ws/account/delete?accounts=" + a,
                      handler, response, g_Verbose );
    //
    ok = ok && check( "account", 3, 1,
                      is_jresp_rejected( response, "not_zero", a ) );
    //
    // Delete C accepted
    if (ok) exec_url( "/ws/account/delete?accounts=" + c,
                      handler, response, g_Verbose );
    //
    ok = ok && check( "account", 3, 2,
                      response.is_jresp_success() );
    //
    //--------------
    // Balance
    if (ok) exec_url( "/ws/account/select?"
                      "fields=[\"k_account\",\"s_currency\",\"f_balance\"]&"
                      "filter={\"k_account\":\"" + a + "\"}",                  
                      handler, response, g_Verbose );
    //
    rapidjson::GenericValue< rapidjson::UTF8<> > data;
    //
    // [{"f_balance":"0.00000000","k_account":"10000000686","s_currency":"USD"}]
    //
    ok = ok && check( "account", 4, 1,
                      is_jresp_success( response, data ) &&
                      data.IsArray() &&
                      data[ 0 ][ "f_balance"  ]  == "-10.00000000" &&
                      data[ 0 ][ "k_account"  ]  == a.c_str()      &&
                      data[ 0 ][ "s_currency" ]  == "USD" );
    //
    //--------------
    // Ledger
    if (ok) exec_url( "/ws/account/ledger?"
                      "fields=[\"d_date\",\"f_amount\",\"s_type\",\"s_category\",\"s_reference\"]&"
                      "filter={\"k_account\":\"" + b + "\"}",                  
                      handler, response, g_Verbose );
    //
    // [{"d_date":"2019-04-10 07:37:41","f_amount":"4.00000000","s_category":"test","s_type":"deposit"},
    //  {"d_date":"2019-04-10 07:37:41","f_amount":"6.00000000","s_category":"test","s_type":"deposit"}]
    //
    ok = ok && check( "account", 5, 1,
                      is_jresp_success( response, data )              &&
                      data.IsArray()                                  &&
                      data[ 0 ][ "f_amount"    ] == "4.00000000"      &&
                      data[ 1 ][ "f_amount"    ] == "6.00000000"      &&
                      data[ 0 ][ "s_type"      ] == "deposit"         &&
                      data[ 1 ][ "s_category"  ] == "test"            &&
                      data[ 0 ][ "s_reference" ] == reference.c_str() &&
                      ( std::abs(time(nullptr) - masnae::mysql::datetime_utc(data[ 0 ][ "d_date" ].GetString())) < 2 )
                    );
    //
    //--------------
    // Balance back to 0
    if (ok) exec_url( "/ws/reverse?journal=" + j,
                      handler, response, g_Verbose );
    //
    ok = ok && check( "account", 6, 1,
                      response.is_jresp_success() );
    //
    // Delete A rejected still used
    if (ok) exec_url( "/ws/account/delete?accounts=" + a,
                      handler, response, g_Verbose );
    //
    ok = ok && check( "account", 7, 1,
                      is_jresp_rejected( response, "used", "" ) );
    //
    //--------------
    // Create D accounts with prefix
    exec_url( "/ws/account/create?currency=USD"
                                 "&overdraft=-10&ceiling=0&prefix=9876543210",
                      handler, response, g_Verbose );
    //
    ok = ok && check( "account", 8, 1,
                      is_jresp_rejected( response, "prefix_length", "" ) );
    //
    exec_url( "/ws/account/create?currency=USD"
                                 "&overdraft=-10&ceiling=0&prefix=429",
                      handler, response, g_Verbose );
    //
    ok = ok && check( "account", 8, 2,
                      is_jresp_success( response, "/data/account", d ) &&
                      adawat::str_starts_with( d, "429") );
    //
    // Delete D accepted
    if (ok) exec_url( "/ws/account/delete?accounts=" + d,
                      handler, response, g_Verbose );
    //
    ok = ok && check( "account", 8, 3,
                      response.is_jresp_success() );
    //
    return ok;
}

//-------------------------------------------------------------------------
bool validate_account2 ( void )
{
    std::string a, b, c, d, j;
    bool        ok = true;
    //
    AdmeteRequestHandler          handler;
    masnae::Fastcpipp_Path        document;
    masnae::ProcessingResponse    response;
    //
    handler.security_disable_uriana();
    handler.check_security( "/ws", document, response );
    //
    AccountManager              AM;
    std::vector< unsigned >     periods, p;
    unsigned                    period_max;
    AccountManager::UsagePer    u1, u2;
    std::string                 s1, s2;
    //
    //------------
    ok = ok && check( "account2", 1, 1,
                      AM.get_usage_periods( "1", periods, period_max, response ) &&
                      (p = { 1 }, periods == p) && period_max == 1
                    );
    //
    ok = ok && check( "account2", 1, 2,
                      AM.get_usage_periods( "21,7", periods, period_max, response ) &&
                      (p = { 7,21 }, periods == p) && period_max == 21
                    );
    //
    ok = ok && check( "account2", 1, 3,
                      AM.get_usage_periods( "1,7,30", periods, period_max, response ) &&
                      (p = { 1,7,30 }, periods == p) && period_max == 30
                    );
    //
    ok = ok && check( "account2", 1, 4,
                      ! AM.get_usage_periods( "1,7,99", periods, period_max, response )
                    );
    //
    //------------
    u1.clear();
    periods = { 1,7,30 };
    ok = ok && check( "account2", 2, 1,
                      AM.aggregate_periods( u1, periods, u2, response ) &&
                      u2[1].count == 0 && u2[7].count == 0 && u2[30].count == 0
                    );
    u1.clear();
    u1[ 0].amount.set( "1", "EUR"); u1[ 0].count = 1;
    periods = { 1,7,30 };
    ok = ok && check( "account2", 2, 2,
                      AM.aggregate_periods( u1, periods, u2, response ) &&
                      u2[1].count == 1 && u2[7].count == 1 && u2[30].count == 1
                    );
    //
    u1.clear();
    u1[15].amount.set( "8", "EUR"); u1[15].count = 8;
    periods = { 1,7,30 };
    ok = ok && check( "account2", 2, 3,
                      AM.aggregate_periods( u1, periods, u2, response ) &&
                      u2[1].count == 0 && u2[7].count == 0 && u2[30].count == 8
                    );
    //
    u1.clear();
    u1[ 0].amount.set( "1", "EUR"); u1[ 0].count = 1;
    u1[15].amount.set( "8", "EUR"); u1[15].count = 8;
    periods = { 1,7,30 };
    ok = ok && check( "account2", 2, 4,
                      AM.aggregate_periods( u1, periods, u2, response ) &&
                      u2[1].count == 1 && u2[7].count == 1 && u2[30].count == 9
                    );
    //
    u1.clear();
    u1[ 0].amount.set( "1", "EUR"); u1[ 0].count = 1;
    u1[ 5].amount.set( "2", "EUR"); u1[ 5].count = 2;
    u1[ 8].amount.set( "4", "EUR"); u1[ 8].count = 4;
    u1[15].amount.set( "8", "EUR"); u1[15].count = 8;
    periods = { 1,7,30 };
    ok = ok && check( "account2", 2, 5,
                      AM.aggregate_periods( u1, periods, u2, response ) &&
                      u2[1].count == 1 && u2[7].count == 3 && u2[30].count == 15
                    );
    //
    u1.clear();
    u1[ 0].amount.set( "1", "EUR");  u1[ 0].count = 1;
    u1[ 5].amount.set( "2", "EUR");  u1[ 5].count = 2;
    u1[ 6].amount.set( "4", "EUR");  u1[ 6].count = 4;
    u1[ 7].amount.set( "8", "EUR");  u1[ 7].count = 8;
    u1[15].amount.set( "16", "EUR"); u1[15].count = 16;
    periods = { 1,7,30 };
    ok = ok && check( "account2", 2, 6,
                      AM.aggregate_periods( u1, periods, u2, response ) &&
                      u2[1].count == 1 && u2[7].count == 7 && u2[30].count == 31
                    );
    //
    u1.clear();
    u1[ 0].amount.set( "1",  "EUR"); u1[ 0].count = 1;
    u1[15].amount.set( "8",  "EUR"); u1[15].count = 8;
    u1[32].amount.set( "16", "EUR"); u1[32].count = 16;
    periods = { 1,7,30 };
    ok = ok && check( "account2", 2, 7,
                      AM.aggregate_periods( u1, periods, u2, response ) &&
                      u2[1].count == 1 && u2[7].count == 1 && u2[30].count == 9
                    );
    //
    u1.clear();
    u1[ 0].amount.set( "1",  "EUR"); u1[ 0].count = 1;
    u1[15].amount.set( "8",  "EUR"); u1[15].count = 8;
    u1[32].amount.set( "16", "EUR"); u1[32].count = 16;
    periods = { 1 };
    ok = ok && check( "account2", 2, 8,
                      AM.aggregate_periods( u1, periods, u2, response ) &&
                      u2[1].count == 1 && u2[7].count == 0 && u2[30].count == 0
                    );
    //
    u1.clear();
    u1[ 0].amount.set( "1",  "EUR"); u1[ 0].count = 1;
    u1[15].amount.set( "8",  "EUR"); u1[15].count = 8;
    u1[32].amount.set( "16", "EUR"); u1[32].count = 16;
    periods = { 1,7 };
    ok = ok && check( "account2", 2, 9,
                      AM.aggregate_periods( u1, periods, u2, response ) &&
                      u2[1].count == 1 && u2[7].count == 1 && u2[30].count == 0
                    );
    //
    //------------
    ok = ok && check( "account2", 3, 1,
                      AM.get_usage_mode( "negative", s1, s2, response ) &&
                      AM.get_usage_data( s1, s2, "2", "Europe/Paris", "", "" , 7, u2, response ) &&
                      u2[0].count == 4 && u2[0].amount.value() == "14.05" && u2[0].amount.currency() == "EUR"
                    );
    //
    ok = ok && check( "account2", 3, 2,
                      AM.get_usage_mode( "negative", s1, s2, response ) &&
                      AM.get_usage_data( s1, s2, "3", "Europe/Paris", "", "" , 7, u2, response ) &&
                      u2[0].count == 3 && u2[0].amount.value() == "7.05" && u2[0].amount.currency() == "EUR"
                    );
    //
    ok = ok && check( "account2", 3, 3,
                      AM.get_usage_mode( "positive", s1, s2, response ) &&
                      AM.get_usage_data( s1, s2, "2", "Europe/Paris", "", "" , 7, u2, response ) &&
                      u2[0].count == 6 && u2[0].amount.value() == "17.05" && u2[0].amount.currency() == "EUR"
                    );
    //
    ok = ok && check( "account2", 3, 4,
                      AM.get_usage_mode( "sum", s1, s2, response ) &&
                      AM.get_usage_data( s1, s2, "2", "Europe/Paris", "", "" , 7, u2, response ) &&
                      u2[0].count == 10 && u2[0].amount.value() == "3" && u2[0].amount.currency() == "EUR"
                    );
    //
    ok = ok && check( "account2", 3, 5,
                      AM.get_usage_mode( "sum_absolute", s1, s2, response ) &&
                      AM.get_usage_data( s1, s2, "2", "Europe/Paris", "", "" , 7, u2, response ) &&
                      u2[0].count == 10 && u2[0].amount.value() == "31.1" && u2[0].amount.currency() == "EUR"
                    );
    //
    ok = ok && check( "account2", 3, 6,
                      AM.get_usage_mode( "negative", s1, s2, response ) &&
                      AM.get_usage_data( s1, s2, "2", "Europe/Paris", "transfer", "" , 7, u2, response ) &&
                      u2[0].count == 2 && u2[0].amount.value() == "4" && u2[0].amount.currency() == "EUR"
                    );
    //
    ok = ok && check( "account2", 3, 6,
                      AM.get_usage_mode( "negative", s1, s2, response ) &&
                      AM.get_usage_data( s1, s2, "2", "Europe/Paris", "transfer", "test31" , 7, u2, response ) &&
                      u2[0].count == 1 && u2[0].amount.value() == "3" && u2[0].amount.currency() == "EUR"
                    );
    //
    //------------
    rapidjson::GenericValue< rapidjson::UTF8<> > data;
    //
    if (ok) exec_url( "/ws/account/usage?mode=negative&account=2&periods=1,7&timezone=Europe/Paris&type=transfer&category=test31",
                      handler, response, g_Verbose );
    //
    ok = ok && check( "account2", 4, 1,
                      is_jresp_success( response, data ) &&
                      data.IsObject() &&
                      data["1"]["count"]    == "1"   &&
                      data["1"]["amount"]   == "3"   &&
                      data["1"]["currency"] == "EUR" &&
                      data["7"]["count"]    == "1"   &&
                      data["1"]["amount"]   == "3"   &&
                      data["1"]["currency"] == "EUR" );
    //
    return ok;
}

//-------------------------------------------------------------------------
bool validate_reservation ( void )
{
    std::string a, b, c, j;
    bool        ok = true;
    //
    AdmeteRequestHandler                         handler;
    masnae::Fastcpipp_Path                       document;
    masnae::ProcessingResponse                   response;
    rapidjson::GenericValue< rapidjson::UTF8<> > data;
    //
    handler.security_disable_uriana();
    handler.check_security( "/ws", document, response );
    //
    //--------------
    // Retrieve reservation on test account 1 (created in validate_ws_transfer)
    exec_url( "/ws/reservation/select?"
             "fields=[\"k_account\",\"k_reservation\",\"f_amount\"]&"
             "filter={\"k_account\":\"1\"}",                  
              handler, response, g_Verbose );
    //
    // [{"k_account":"1","k_reservation":"1","f_amount":"0.00000000"},
    //  {"k_account":"1","k_reservation":"2","f_amount":"0.00000000"}]
    //
    ok = ok && check( "reservation", 1, 1,
                  is_jresp_success( response, data ) &&
                  data.IsArray() &&
                  data[ 0 ][ "k_account"  ]     == "1"           &&
                  data[ 0 ][ "k_reservation"  ] == "1"           &&
                  data[ 0 ][ "f_amount" ]       == "0.00000000"  &&
                  data[ 1 ][ "k_account"  ]     == "1"           &&
                  data[ 1 ][ "k_reservation"  ] == "2"           &&
                  data[ 1 ][ "f_amount" ]       == "0.00000000");
    //
    //--------------
    // Delete reservations
    if (ok) exec_url( "/ws/reservation/delete?"
                     "filter={\"k_account\":\"1\"}",                  
                      handler, response, g_Verbose );
    //
    ok = ok && check( "reservation", 2, 1,
                 response.is_jresp_success() );
    //
    if (ok) exec_url( "/ws/reservation/select?"
                     "fields=[\"k_account\",\"k_reservation\",\"f_amount\"]&"
                     "filter={\"k_account\":\"1\"}",                  
                      handler, response, g_Verbose );
    //
    ok = ok && check( "reservation", 2, 2,
                  is_jresp_success( response, data ) &&
                  data.IsArray() && data.Empty() );
    //
    //--------------
    // Account 2 balance is 4, overdraft -10 (end of validate_ws_reverse)
    if (ok) exec_url( "/ws/reservation/create?"
                     "account=2&limit=7&lifetime=-1&narration=31",
                      handler, response, g_Verbose );
    //
    ok = ok && check( "account", 3, 1,
                 is_jresp_rejected( response, "invalid_lifetime", "" ) );
    //
    if (ok) exec_url( "/ws/reservation/create?"
                     "account=2&limit=7&lifetime=" +
                     std::to_string( constants::k_reservation_lifetime_max + 120 ) +
                     "&narration=32",
                      handler, response, g_Verbose );
    //
    ok = ok && check( "account", 3, 2,
                 is_jresp_rejected( response, "invalid_lifetime", "" ) );
    //
    // {"expiration":"2019-03-01T07:07:56","reservation":"kihL0o"}
    if (ok) exec_url( "/ws/reservation/create?"
                     "account=2&limit=7&lifetime=10&narration=33",
                      handler, response, g_Verbose );
    //
    ok = ok && check( "account", 3, 3,
                  is_jresp_success( response, data ) &&
                  data.IsObject() &&
                  data[ "reservation" ].GetStringLength() == 6 );
    //
    if (ok) exec_url( "/ws/reservation/create?"
                     "account=2&limit=8&lifetime=10&narration=34",
                      handler, response, g_Verbose );
    //
    ok = ok && check( "account", 3, 4,
                  is_jresp_rejected( response, "overdraft", "2" ) );
    //
    return ok;
}

//-------------------------------------------------------------------------
bool validate_info (void)
{
    bool                    ok = true;
    rapidjson::Document     doc;
    //
    doc.Parse< rapidjson::kParseFullPrecisionFlag | rapidjson::kParseCommentsFlag >(
        g_ReservationManager.specific_info().c_str() );
    ok &= check( "info", 1, 1, !doc.HasParseError() && doc.IsObject() );
    //
    return ok;
}

//-------------------------------------------------------------------------
bool validate_ws_exchange ( void )
{
    std::string a, b, c, j;
    bool        ok = true;
    //
    AdmeteRequestHandler                         handler;
    masnae::Fastcpipp_Path                       document;
    masnae::ProcessingResponse                   response;
    rapidjson::GenericValue< rapidjson::UTF8<> > data;
    std::string                                  journal, reversal;
    //
    handler.security_disable_uriana();
    handler.check_security( "/ws", document, response );
    //
    //--------------
    // Accounts EUR: 1 bal=-7   2 bal=4
    // Accounts USD: 4 bal=-1   5 bal=1
    exec_url( "/ws/exchange?currency_1=EUR&currency_2=USD"
                  "&type=transfer&category=test11&narration=EUR USD"
                  "&records_1=[{\"a\":\"1\",\"m\":\"-1\"}, {\"a\":\"2\",\"m\":\"1\"}]"
                  "&records_2=[{\"a\":\"4\",\"m\":\"1.3\"},{\"a\":\"5\",\"m\":\"-1.3\"}]",
              handler, response, g_Verbose );
    //
    ok = ok && check( "ws_exchange", 1, 1,
                 is_jresp_success( response, "/data/transaction", journal ) );
    //
    ok = ok && check( "ws_exchange", 1, 2,
                 check_bal( { "-8.00000000","5.00000000","3.00000000","0.30000000","-0.30000000","" } ) );
    //
    //--------------
    // ... reversed
    if (ok) exec_url( "/ws/reverse?journal=" + journal,
                      handler, response, g_Verbose );
    //
    ok = ok && check( "ws_exchange", 2, 1,
                 is_jresp_success( response, "/data/transaction", reversal ) );
    //
    ok = ok && check( "ws_exchange", 2, 2,
                 check_bal( { "-7.00000000","4.00000000","3.00000000","-1.00000000","1.00000000","" } ) );
    //
    //--------------
    if (ok) exec_url( "/ws/exchange?currency_1=EUR&currency_2=USD"
                          "&type=transfer&category=test31&narration=EUR USD"
                          "&records_1=[{\"a\":\"1\",\"m\":\"3\"},  {\"a\":\"2\",\"m\":\"-3\"}]"
                          "&records_2=[{\"a\":\"4\",\"m\":\"4.5\"},{\"a\":\"5\",\"m\":\"-4.5\"}]",
                      handler, response, g_Verbose );
    //
    ok = ok && check( "ws_exchange", 3, 1,
                 is_jresp_success( response, "/data/transaction", journal ) );
    //
    ok = ok && check( "ws_exchange", 3, 2,
                 check_bal( { "-4.00000000","1.00000000","3.00000000","3.50000000","-3.50000000","" } ) );
    //
    //--------------
    if (ok) exec_url( "/ws/exchange?currency_1=EUR&currency_2=EUR"
                          "&type=transfer&category=test41&narration=EUR EUR"
                          "&records_1=[{\"a\":\"1\",\"m\":\"3\"},  {\"a\":\"1\",\"m\":\"-3\"}]"
                          "&records_2=[{\"a\":\"1\",\"m\":\"4.5\"},{\"a\":\"1\",\"m\":\"-4.5\"}]",
                      handler, response, g_Verbose );
    //
    ok = ok && check( "ws_exchange", 4, 1,
                 is_jresp_success( response, "/data/transaction", journal ) );
    //
    ok = ok && check( "ws_exchange", 4, 2,
                 check_bal( { "-4.00000000","1.00000000","3.00000000","3.50000000","-3.50000000","" } ) );
    //
    //--------------
    if (ok) exec_url( "/ws/exchange?currency_1=EUR&currency_2=USD"
                          "&type=transfer&category=test41&narration=EUR USD"
                          "&records_1=[{\"a\":\"1\",\"m\":\"3\"},  {\"a\":\"2\",\"m\":\"-2\"}]"
                          "&records_2=[{\"a\":\"4\",\"m\":\"4.5\"},{\"a\":\"5\",\"m\":\"-4.5\"}]",
                      handler, response, g_Verbose );
    //
    ok = ok && check( "account", 4, 1,
                 is_jresp_rejected( response, "not_zero", "" ) );
    //
    if (ok) exec_url( "/ws/exchange?currency_1=EUR&currency_2=USD"
                          "&type=transfer&category=test51&narration=EUR USD"
                          "&records_1=[{\"a\":\"1\",\"m\":\"2\"},  {\"a\":\"2\",\"m\":\"-2\"}]"
                          "&records_2=[{\"a\":\"4\",\"m\":\"1.5\"},{\"a\":\"5\",\"m\":\"-4.5\"}]",
                      handler, response, g_Verbose );
    //
    ok = ok && check( "account", 5, 1,
                 is_jresp_rejected( response, "not_zero", "" ) );
    //
    return ok;
}

//-------------------------------------------------------------------------
// Self tests function
void validate ( bool *  p_succeeded )
{
    // Pre-allocate DB handles
    {
        masnae::globals::mysql_handler->get_connection( masnae::mysql::writer );
        masnae::globals::mysql_handler->get_connection( masnae::mysql::reader );
    }
    //
    *p_succeeded = true;
    //
    start ("misc       ");
    *p_succeeded = *p_succeeded && stop ("misc",           validate_misc() );
    //
    start ("ws_transfer");
    *p_succeeded = *p_succeeded && stop ("ws_transfer",    validate_ws_transfer() );
    //
    start ("ws_reverse ");
    *p_succeeded = *p_succeeded && stop ("ws_reverse",     validate_ws_reverse() );
    //
    start ("account    ");
    *p_succeeded = *p_succeeded && stop ("account",        validate_account() );
    //
    start ("reservation");
    *p_succeeded = *p_succeeded && stop ("reservation",    validate_reservation() );
    //
    start ("ws_exchange");
    *p_succeeded = *p_succeeded && stop ("ws_exchange",    validate_ws_exchange() );
    //
    start ("account2   ");
    *p_succeeded = *p_succeeded && stop ("account2",       validate_account2() );
    //
    start ("info       ");
    *p_succeeded = *p_succeeded && stop ("info",           validate_info() );
    //
    printf ("-------\n");
    printf ("validation : %s\n", *p_succeeded ? "ok" : "error");
}

//-------------------------------------------------------------------------
// Alternate validate function used to perform load testing

#include <random>

void validate_ ( bool *  p_succeeded )
{
    AdmeteRequestHandler          handler;
    masnae::Fastcpipp_Path        document;
    masnae::ProcessingResponse    response;
    //
    handler.security_disable_uriana();
    handler.check_security( "/ws", document, response );
    //
    //--------------
    std::vector< std::string > accounts;
    //
    //--------------
    /*
    {
        // Generate new accounts using prefix 99
        //
        printf( "%s creating accounts...\n",
                     adawat::time_format_rfc3339_local().c_str() );
        //
        for ( unsigned u = 0; u < 10000; u++ )
        {
            std::string a;
            //
            exec_url( "/ws/account/create?currency=EUR&prefix=99"
                                         "&overdraft=-1000000&ceiling=1000000",
                              handler, response, g_Verbose );
            //
            if ( ! check( "account", 1, u,
                           is_jresp_success( response, "/data/account", a ) ) )
            {
                puts ("error");
                return;
            }
            //
            accounts.push_back( a );
        }
    }
    */
    //
    //--------------
    /*
    {
        // Read existing accounts using prefix 99
        //
        masnae::SingleKeyValue_List  rows;
        masnae::mysql::Connection    db = masnae::globals::mysql_handler->get_connection( masnae::mysql::writer );
        //
        for ( unsigned u = 0; u == 0 || ! rows.empty(); u += 100 )
        {
            rows.clear();
            //
            db.do_select( "SELECT k_account FROM t_accounts "
                          "WHERE k_account LIKE '99%' "
                          "LIMIT "+std::to_string(u)+",100",
                          rows );
            //
            for ( auto row: rows )
                accounts.push_back( row["k_account"] );
        }
        //
        printf( "%s %lu accounts loaded...\n",
                 adawat::time_format_rfc3339_local().c_str(),
                 accounts.size() );
    }
    */
    //
    //--------------
    /*
    {
        // Generate transactions
        //
        std::default_random_engine generator;
        std::uniform_int_distribution<unsigned> distribution( 0, accounts.size() - 1);
        //
        std::string from, to, reference, trans;
        //
        for ( unsigned n = 0; n < 1000; n++ )
        {
            printf( "%s transferts loop %u...\n",
                         adawat::time_format_rfc3339_local().c_str(),
                         n );
            //
            for ( unsigned i = 0; i < 1000; i++ )
            {
                reference = "99_" + adawat::uid_ts( adawat::uid_medium_ts );
                from      = accounts[ distribution(generator) ];
                to        = accounts[ distribution(generator) ];
                //
                exec_url( "/ws/transfer?currency=EUR&type=transfer&category=test&reference=" + reference +
                                      "&records=[{\"a\":\"" + from + "\",\"m\":\"-1\"},"
                                                "{\"a\":\"" + to   + "\",\"m\":\"1\"}]",
                                  handler, response, g_Verbose );
                //
                if ( ! check( "account", n, i,
                               is_jresp_success( response, "/data/transaction", trans ) ) )
                {
                    puts ("error");
                    return;
                }
            }
        }
    }
    */
    //--------------
    /*
    {
        // Query accounts' usage
        //
        std::default_random_engine generator;
        std::uniform_int_distribution<unsigned> distribution( 0, accounts.size() - 1);
        //
        std::string from;
        //
        printf( "%s usage...\n",
                     adawat::time_format_rfc3339_local().c_str() );
        //
        for ( unsigned n = 0; n < 10000; n++ )
        {
            from      = accounts[ distribution(generator) ];
            //
            exec_url( "/ws/account/usage?mode=negative&account="+from+"&periods=1,7&timezone=Europe/Paris",
                      handler, response, g_Verbose );
            //
            if ( ! check( "usage", 2, n,
                           response.is_jresp_success() ) )
            {
                puts ("error");
                return;
            }
        }
    }
    */
    //
    printf( "%s done\n",
                 adawat::time_format_rfc3339_local().c_str() );
}

} // namespace admete
