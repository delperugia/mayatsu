// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

// Copyright 2019 Pierre del Perugia
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <adawat/adawat_money.h>
#include <adawat/adawat_string.h>

#include "masnae_global.h"
#include "masnae_misc.h"
#include "masnae_mysql.h"

#include "admete_misc.h"
#include "admete_request.h"

namespace admete {

//-------------------------------------------------------------------------
// Records a double list of operations on accounts, one per currency.
// The sum of all amounts in each list must be null, all accounts must use
// the same currency as the transaction.
// The final balance of each account must be in the overdraft / ceiling range.
// Parameter:
//  - records_1     [{"1":"-100"},{"2":"100"}]
//  - records_2     [{"3":"130"},{"4":"-130"}]
//  - currency_1    EUR
//  - currency_2    USD
//  - type
//  - category
//  - narration     'EUR / USD exchange'
// Returns:
//      success     {"date":"2019-02-07T13:54:36","sequence":"60-3","transaction":"439"}
//      failed
//      error
//      rejected    overflow                account
//                  invalid_currency
//                  not_zero
//                  reservation_credited    reservation
//                  overdraft               account
//                  ceiling                 account
//                  incompatible_currency   account
//                  not_found               account
//                  reservation_not_found   reservation
//                  reservation_too_low     reservation
//                  invalid_value           amount
//                  duplicate_reference
//
void AdmeteRequestHandler::process_ws_exchange ( const masnae::SingleKey_ValueList &  p_parameters,
                                                 masnae::ProcessingResponse &         p_result )
{
    // Check that the caller is allowed here
    if ( ! has_permission( "admete.exchange", p_result ) )
        return;
    //
    // Check parameters
    std::string  records_json_1, currency_1, records_json_2, currency_2, type, category, narration, user_reference;
    //
    if ( ! masnae::get_parameter( p_parameters, "records_1",  records_json_1, false, false, p_result ) ||
         ! masnae::get_parameter( p_parameters, "currency_1", currency_1,     false, false, p_result ) ||
         ! masnae::get_parameter( p_parameters, "records_2",  records_json_2, false, false, p_result ) ||
         ! masnae::get_parameter( p_parameters, "currency_2", currency_2,     false, false, p_result ) ||
         ! masnae::get_parameter( p_parameters, "type",       type,           false, true,  p_result ) ||
         ! masnae::get_parameter( p_parameters, "category",   category,       false, true,  p_result ) ||
         ! masnae::get_parameter( p_parameters, "narration",  narration,      false, true,  p_result ) ||
         ! masnae::get_parameter( p_parameters, "reference",  user_reference, false, true,  p_result ) )
        return;
    //
    // Transacion timestamp
    time_t      now     = time( nullptr );
    std::string period  = get_period( now );
    std::string date    = masnae::mysql::datetime_utc( now );
    //
    p_result.add_log( masnae::ProcessingResponse::regular,
                      "Exchangeing on " + date );
    //
    // Parse JSON to retrieve records, build SQL WHERE clause to retrieve
    // accounts, check that sum of records is zero. Set p_result on error
    List_Record  records_1, records_2;
    std::string  accounts_where_1, accounts_where_2;
    //
    if ( ! prepare_records( records_json_1, currency_1,
                            records_1, accounts_where_1,
                            p_result ) ||
         ! prepare_records( records_json_2, currency_2,
                            records_2, accounts_where_2,
                            p_result ) )
        return;
    //
    // Transaction
    uint64_t               next_journal, next_posting;
    SingleKey_Accounts     accounts_mem_1, accounts_mem_2;
    SingleKey_Reservation  reservation_mem_1, reservation_mem_2;
    //
    auto     db     = masnae::globals::mysql_handler->get_connection( masnae::mysql::writer );
    unsigned result = db.start_transaction();
    //
    if ( result == 0 ) {
        bool ok = true;
        //
        // Get accounts details (locking them and reservation)
        ok = ok && get_accounts( db, accounts_where_1, accounts_mem_1, p_result );
        ok = ok && get_accounts( db, accounts_where_2, accounts_mem_2, p_result );
        //
        // Get reservations (locking them and accounts)
        ok = ok && get_reservations( db, accounts_where_1, reservation_mem_1, p_result );
        ok = ok && get_reservations( db, accounts_where_2, reservation_mem_2, p_result );
        //
        // Applies records to balances, check account overflows / currencies
        ok = ok && apply_records( records_1, accounts_mem_1, reservation_mem_1, p_result );
        ok = ok && apply_records( records_2, accounts_mem_2, reservation_mem_2, p_result );
        //
        // Get next ids for journal and posting (locking)
        ok = ok && get_next_sequence( db, next_journal, next_posting, p_result );
        //
        // If everything goes well, this will be our result (before next_.. change)
        ok = ok && fill_successful_result( date, next_journal, next_posting,
                                           records_1.size() + records_2.size(),
                                           p_result );
        //
        // Write journal
        ok = ok && write_journal( db, period, next_journal, date,
                                  type, category, narration,
                                  m_security.user, user_reference,
                                  p_result );
        //
        // Write posting, update accounts' balance
        SingleKey_Accounts  accounts_mem_merged = accounts_mem_1;
        List_Record         records_merged      = records_1;
        //
        accounts_mem_merged.insert( accounts_mem_2.begin(), accounts_mem_2.end() );
        records_merged.insert( records_merged.end(), records_2.begin(), records_2.end() );
        //
        ok = ok && write_posting( db, period, next_journal, next_posting,
                                  records_merged, accounts_mem_merged,
                                  p_result );  // next_.. change here
        //
        // Update reservations
        ok = ok && write_reservations( db, reservation_mem_1, p_result );
        ok = ok && write_reservations( db, reservation_mem_2, p_result );
        //
        // Increase journal and posting ids
        ok = ok && update_next_sequence( db, next_journal, next_posting, p_result );
        //
        // Finaly, commit all
        if ( ok ) {
            result = db.commit_transaction();
            //
            if ( result != 0 ) {
                masnae::mysql::build_resp_error( result, p_result );
                ok = false;
            }
        }
        //
        if ( ! ok )
            db.rollback_transaction();
    } else {
        masnae::mysql::build_resp_error( result, p_result );
    }
}

} // namespace admete
