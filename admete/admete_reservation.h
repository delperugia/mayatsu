// Copyright 2019 Pierre del Perugia
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

//
// Manage reservations
//

#ifndef admete_reservation_h
#define admete_reservation_h

#include <random>
#include <shared_mutex>
#include <adawat/adawat_stats.h>

#include "masnae_bus.h"

namespace admete {
    
//-------------------------------------------------------------------------
class ReservationManager : public masnae::bus::Subscriber
{
    public:
        // To be called once before any execution
        bool              start            ( void );
        //
        // To be called once before stopping the program
        void              stop             ( void );
        //
        // Return module information as a JSON string
        std::string       specific_info  ( void );
        //
        // Search and return reservations
        void              select           ( const std::string &           p_fields_list,
                                             const std::string &           p_filter,
                                             const std::string &           p_order,
                                             const std::string &           p_range,
                                             masnae::ProcessingResponse &  p_result );
        //
        // Create a new reservation
        void              create           ( const std::string &           p_account,
                                             const std::string &           p_limit,
                                             int64_t                       p_lifetime,
                                             const std::string &           p_narration,
                                             masnae::ProcessingResponse &  p_result );
        //
        // Delete reservations
        void              close            ( const std::string &           p_filter,
                                             const std::string &           p_order,
                                             const std::string &           p_range,
                                             masnae::ProcessingResponse &  p_result );
        //
        // Called by the bus with a new message
        virtual
        void              bus_event        ( const masnae::bus::Message &  p_message );
        //
    private:
        // Configuration
        std::shared_mutex   m_mutex_config;
        int64_t             m_cleaning_probability = 100;  // range 1-100-10000
        //
        // Parse the configuration
        bool              update_config    ( void );
        //
        std::default_random_engine  m_generator;
        //
        // Statistics
        std::mutex                  m_stats_mutex;
        adawat::StatsValue          m_stats_cleaning;
        //
        // Check if cleaning must be done and do it
        void              maybe_clean    ( void );
        //
        // Try to apply the reservation on the account
        bool reserve( const std::string &           p_account,
                      SingleKey_Accounts &          p_accounts_mem,
                      const std::string &           p_limit,
                      masnae::ProcessingResponse &  p_result );
        //
        // Create reservation, fill p_result with reservation id
        bool create_reservation ( masnae::mysql::Connection &   p_db,
                                  const std::string &           p_account,
                                  const std::string &           p_limit,
                                  int64_t                       p_lifetime,
                                  const std::string &           p_narration,
                                  masnae::ProcessingResponse &  p_result );
};

} // namespace admete

#endif
