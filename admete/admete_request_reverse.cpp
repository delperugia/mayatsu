// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

// Copyright 2019 Pierre del Perugia
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <adawat/adawat_string.h>

#include "masnae_mysql.h"
#include "masnae_global.h"
#include "masnae_misc.h"

#include "admete_common.h"
#include "admete_misc.h"
#include "admete_request.h"

namespace admete {

//-------------------------------------------------------------------------
// Retrieve the journal and corresponding posting.
// Journal must not be already reversed
// Returns:
//      success
//      failed
//      rejected    not_found
//                  already_reversed    journal
//
bool get_journal( masnae::mysql::Connection &    p_db,
                  const std::string &            p_journal,
                  masnae::SingleKeyValue_List &  p_journal_db,
                  masnae::SingleKeyValue_List &  p_posting_db,
                  masnae::ProcessingResponse &   p_result )
{
    unsigned  db_result;
    //
    db_result = p_db.do_select( "SELECT s_type,s_category,s_narration,k_journal_reversal"
                                " FROM t_journal"
                                " WHERE k_journal='" + masnae::mysql::sql_escape( p_journal ) +"'"
                                " FOR UPDATE",
                                p_journal_db );
    //
    if ( db_result != 0 ) {
        p_result.set_jresp_failed();
        p_result.add_error( "get_journal select t_journal error " + p_journal +
                            ": " + std::to_string( db_result ) );
        return false;
    }
    //
    if ( p_journal_db.size() != 1 ) {
        p_result.set_jresp_rejected( "not_found", "null" );
        return false;
    }
    //
    // No need to lock the posting, they cannot be modified
    db_result = p_db.do_select( "SELECT k_account,f_amount,s_currency"
                                " FROM t_posting"
                                " WHERE k_journal='" + masnae::mysql::sql_escape( p_journal ) +"'",
                                p_posting_db );
    //
    if ( db_result != 0 ) {
        p_result.set_jresp_failed();
        p_result.add_error( "get_journal select t_posting error " + p_journal +
                            ": " + std::to_string( db_result ) );
        return false;
    }
    //
    if ( p_posting_db.empty() ) {
        p_result.set_jresp_failed();
        p_result.add_error( "get_journal no posting in journal " + p_journal );
        return false;
    }
    //
    // The journal must not have been reversed before
    if ( p_journal_db[ 0 ][ "k_journal_reversal" ] != "" ) {
        p_result.set_jresp_rejected(
            "already_reversed",
            masnae::json_quote_string( p_journal_db[ 0 ][ "k_journal_reversal" ] )
        );
        return false;
    }
    //
    return true;
}

//-------------------------------------------------------------------------
// From the posting of a journal, build the SQL WHERE clause
// to retrieve accounts involved
// Returns:
//      success
//      failed
bool build_where( const masnae::SingleKeyValue_List &  p_posting_db,
                  std::string &                        p_accounts_where,
                  masnae::ProcessingResponse &         p_result )
{
    try {
        // Build the WHERE clause to retrieve account (an account
        // can appear several times in records)
        std::set< std::string > accounts;
        //
        for ( const auto & p: p_posting_db )
            accounts.insert( "'" +
                             masnae::mysql::sql_escape( p.at( "k_account" ) ) +
                             "'" );
        //
        p_accounts_where = adawat::str_join( accounts );
        //
        return true;
    } catch ( const std::out_of_range & e ) {  // inexisting field
        std::string what = e.what();
        //
        p_result.set_jresp_failed();
        p_result.add_error( "build_where: " + what );
        return false;            
    }
}

//-------------------------------------------------------------------------
// From the posting of a journal, build the list of new postings
// to put in the reversal
// Returns:
//      success
//      failed
bool invert_records( const masnae::SingleKeyValue_List &  p_posting_db,
                     List_Record &                        p_records,
                     masnae::ProcessingResponse &         p_result )
{
    try {
        for ( const auto & p: p_posting_db ) {
            std::string value = p.at( "f_amount" );
            //
            if ( value[0] == '-' )
                value.erase( 0, 1 );
            else
                value.insert( 0, "-" );
            //
            Record new_record;
            //
            new_record.account = p.at( "k_account" );
            //
            auto result = new_record.amount.set( value, p.at( "s_currency" ) );
            if ( result != adawat::Amount::result::ok ) {
                p_result.set_jresp_failed();
                p_result.add_error( "invert_records f_amount invalid: " + 
                                    adawat::Amount::to_string( result ) + " on " +
                                    value );
                return false;
            }
            //
            p_records.push_back ( new_record );
        }
    } catch ( const std::out_of_range & e ) {  // inexisting field
        std::string what = e.what();
        //
        p_result.set_jresp_failed();
        p_result.add_error( "invert_records: " + what );
        return false;            
    }
    //
    // Log new calculated balances
    p_result.add_log( masnae::ProcessingResponse::verbose, "Inverted:" );
    //
    for ( const auto & mvt: p_records )
        p_result.add_log( masnae::ProcessingResponse::verbose,
                          adawat::str_sprintf( "%-12s %10s", 
                                               mvt.account.c_str(),
                                               mvt.amount.value().c_str()
                          ) );
    //
    return true;
}

//-------------------------------------------------------------------------
// Update reversed journal with reversal journal id
// Returns:
//      success
//      failed
bool update_journal( masnae::mysql::Connection &   p_db,
                     const std::string &           p_journal,
                     uint64_t                      p_next_journal,
                     masnae::ProcessingResponse &  p_result )
{
    size_t       affected;
    unsigned     db_result;
    //
    db_result = p_db.do_update(
        "UPDATE t_journal"
        " SET k_journal_reversal=" + std::to_string(p_next_journal) +
        " WHERE k_journal='" + masnae::mysql::sql_escape( p_journal ) + "'",
        affected );
    //
    if ( db_result != 0 ) {
        p_result.set_jresp_failed();
        p_result.add_error( "update_journal update error " + std::to_string( db_result ) );
        return false;
    }
    //
    if ( affected != 1 ) {
        p_result.set_jresp_failed();
        p_result.add_error( "update_journal update changed " + std::to_string( affected ) + " rows" );
        return false;
    }
    //
    return true;
}

//-------------------------------------------------------------------------
bool get_narration_length ( size_t &                      p_lenght,
                            masnae::ProcessingResponse &  p_result )
{
    // Retrieve user's details
    masnae::SingleKeyValue_List  data;
    auto                         db = masnae::globals::mysql_handler->get_connection( masnae::mysql::reader );
    //
    unsigned db_result =
        db.do_select( "SELECT character_maximum_length "
                      "FROM INFORMATION_SCHEMA.COLUMNS "
                      "WHERE table_name = 't_journal' AND COLUMN_NAME = 's_narration'",
                       data );
    //
    if ( db_result != 0 ) {
        p_result.set_jresp_failed();
        p_result.add_error( "get_narration_length select " + std::to_string( db_result ) );
        return false;
    }
    //
    if ( data.size() != 1 || data[0].count( "character_maximum_length" ) != 1 ) {
        p_result.set_jresp_failed();
        p_result.add_error( "get_narration_length data error" );
        return false;
    }
    //
    int length = atoi( data[0]["character_maximum_length"].c_str() );
    //
    if ( length < 10 || length > 255 ) {
        p_result.set_jresp_failed();
        p_result.add_error( "get_narration_length length error" );
        return false;
    }
    //
    p_lenght = static_cast< size_t >( length );
    return true;
}

//-------------------------------------------------------------------------
// Cancel a transaction by creation a new transaction
// with the same records but negated amount.
// A transaction can only be reversed once.
// Parameter:
//  - journal       journal id to reverse
// Returns:
//      success     {"date":"2019-02-07T13:54:36","sequence":"60-3","transaction":"439"}
//      failed
//      rejected    not_found
//                  already_reversed    journal
//                  overflow            account
//                  overdraft           account
//                  ceiling             account
//                  duplicate_reference
//
void AdmeteRequestHandler::process_ws_reverse ( const masnae::SingleKey_ValueList &  p_parameters,
                                                masnae::ProcessingResponse &         p_result )
{
    // Check that the caller is allowed here
    if ( ! has_permission( "admete.reverse", p_result ) )
        return;
    //
    // Check parameters
    std::string  journal, user_reference;
    //
    if ( ! masnae::get_parameter( p_parameters, "journal",   journal,        false, false, p_result ) ||
         ! masnae::get_parameter( p_parameters, "reference", user_reference, false, true,  p_result ) )
        return;
    //
    // Retrieve narration field size
    size_t narration_max_length;
    if ( ! get_narration_length( narration_max_length, p_result ) )
        return;
    //
    // Transacion timestamp
    time_t      now     = time( nullptr );
    std::string period  = get_period( now );
    std::string date    = masnae::mysql::datetime_utc( now );
    //
    p_result.add_log( masnae::ProcessingResponse::regular,
                      "Reversing on " + date +
                      " for " + journal );
    //
    // Transaction
    uint64_t                      next_journal, next_posting;
    List_Record                   records;
    masnae::SingleKeyValue_List   journal_db, posting_db;
    SingleKey_Accounts            accounts_mem;
    SingleKey_Reservation         reservation_mem;  // not used
    std::string                   accounts_where;
    //
    auto     db     = masnae::globals::mysql_handler->get_connection( masnae::mysql::writer );
    unsigned result = db.start_transaction();
    //
    if ( result == 0 ) {
        bool ok = true;
        //
        // Get journal (locking) and posting details
        ok = ok && get_journal( db, journal, journal_db, posting_db, p_result );
        //
        // Build the WHERE clause to retrieve all involved accounts in this journal
        ok = ok && build_where( posting_db, accounts_where, p_result );
        //
        // Get accounts details (locking)
        ok = ok && get_accounts( db, accounts_where, accounts_mem, p_result );
        //
        // From the posting, build the records to replay
        ok = ok && invert_records( posting_db,
                                   records,
                                   p_result );
        //
        // Applies records to balances, check account overflows / currencies
        ok = ok && apply_records( records,
                                  accounts_mem,
                                  reservation_mem,
                                  p_result );
        //
        // Add a prefix to the narration, ensure there will be no overflow in DB
        std::string new_narration = constants::k_reverse_prefix + journal_db[0]["s_narration"];
        //
        if ( new_narration.length() > narration_max_length )
            new_narration.erase( narration_max_length );
        //
        // Get next ids for journal and posting (locking)
        ok = ok && get_next_sequence( db, next_journal, next_posting, p_result );
        //
        // If everything goes well, this will be our result (before next_.. change)
        ok = ok && fill_successful_result( date, next_journal, next_posting, posting_db.size(), p_result );
        //
        // Write new journal
        ok = ok && write_journal( db,
                                  period,
                                  next_journal,
                                  date,
                                  journal_db[0]["s_type"],
                                  journal_db[0]["s_category"],
                                  new_narration,
                                  m_security.user,
                                  user_reference,
                                  p_result );
        //
        // Update reversed journal
        ok = ok && update_journal( db, journal, next_journal, p_result );  // before increasing next_journal
        //
        // Write posting, update accounts' balance
        ok = ok && write_posting( db, period, next_journal, next_posting, records, accounts_mem, p_result );  // next_.. change here
        //
        // Increase journal and posting ids
        ok = ok && update_next_sequence( db, next_journal, next_posting, p_result );
        //
        // Finaly, commit all
        if ( ok ) {
            result = db.commit_transaction();
            //
            if ( result != 0 ) {
                masnae::mysql::build_resp_error( result, p_result );
                ok = false;
            }
        }
        //
        if ( ! ok )
            db.rollback_transaction();
    } else {
        masnae::mysql::build_resp_error( result, p_result );
    }
}

} // namespace admete
