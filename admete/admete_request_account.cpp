// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

// Copyright 2019 Pierre del Perugia
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <adawat/adawat_money.h>

#include "masnae_global.h"
#include "masnae_misc.h"

#include "admete_common.h"
#include "admete_request.h"

namespace admete {

//-------------------------------------------------------------------------
bool AdmeteRequestHandler::process_ws_account ( const masnae::Fastcpipp_Path &       p_document,
                                                const masnae::SingleKey_ValueList &  p_parameters,
                                                masnae::ProcessingResponse &         p_result )
{
    if ( p_document.size() < 2 )
        return false;
    //
         if ( p_document[ 1 ] == "select" ) process_ws_account_select( p_parameters, p_result );
    else if ( p_document[ 1 ] == "ledger" ) process_ws_account_ledger( p_parameters, p_result );
    else if ( p_document[ 1 ] == "create" ) process_ws_account_create( p_parameters, p_result );
    else if ( p_document[ 1 ] == "delete" ) process_ws_account_delete( p_parameters, p_result );
    else if ( p_document[ 1 ] == "usage" )  process_ws_account_usage ( p_parameters, p_result );
    else return false;
    //
    return true;
}

//-------------------------------------------------------------------------
// Search and return accounts
// Use the same syntax as the /mgt/table/ syntax, with 
// parameters: fields
//             filter
//             order
//             range
void AdmeteRequestHandler::process_ws_account_select ( const masnae::SingleKey_ValueList &  p_parameters,
                                                       masnae::ProcessingResponse &         p_result )
{
    // Check that the caller is allowed here
    if ( ! has_permission( "admete.account_select", p_result ) )
        return;
    //
    // Retrieve parameters
    std::string command, fields, filter, order, range;
    //
    if ( ! masnae::get_parameter( p_parameters, "fields", fields,  false, true, p_result ) ||
         ! masnae::get_parameter( p_parameters, "filter", filter,  false, true, p_result ) ||
         ! masnae::get_parameter( p_parameters, "order",  order,   false, true, p_result ) ||
         ! masnae::get_parameter( p_parameters, "range",  range,   false, true, p_result ) )
        return;
    //
    g_AccountManager.select( fields, filter, order, range, p_result );
}

//-------------------------------------------------------------------------
// Search and return journal and posting entries
// Use the same syntax as the /mgt/table/ syntax, with 
// parameters: fields
//             filter
//             order
//             range
void AdmeteRequestHandler::process_ws_account_ledger ( const masnae::SingleKey_ValueList & p_parameters,
                                                       masnae::ProcessingResponse &        p_result )
{
    // Check that the caller is allowed here
    if ( ! has_permission( "admete.account_ledger", p_result ) )
        return;
    //
    // Retrieve parameters
    std::string command, fields, filter, order, range;
    //
    if ( ! masnae::get_parameter( p_parameters, "fields", fields,  false, true, p_result ) ||
         ! masnae::get_parameter( p_parameters, "filter", filter,  false, true, p_result ) ||
         ! masnae::get_parameter( p_parameters, "order",  order,   false, true, p_result ) ||
         ! masnae::get_parameter( p_parameters, "range",  range,   false, true, p_result ) )
        return;
    //
    g_AccountManager.ledger( fields, filter, order, range, p_result );
}

//-------------------------------------------------------------------------
// Create a new account
// Parameters
//  currency    3 letters currency code
//  overdraft   default 0
//  ceiling     default 10000000000
//  prefix      account's prefix
// Returns:
//      success     {"status":"success","data":{"account":"11","ceiling":"10000000000","overdraft":"0"}}
//      failed
//      error

void AdmeteRequestHandler::process_ws_account_create ( const masnae::SingleKey_ValueList & p_parameters,
                                                       masnae::ProcessingResponse &        p_result )
{
    // Check that the caller is allowed here
    if ( ! has_permission( "admete.account_create", p_result ) )
        return;
    //
    // Retrieve parameters
    std::string  currency, overdraft, ceiling, prefix;
    //
    if ( ! masnae::get_parameter( p_parameters, "currency",  currency,  false, false, p_result ) ||
         ! masnae::get_parameter( p_parameters, "overdraft", overdraft, false, true,  p_result ) ||
         ! masnae::get_parameter( p_parameters, "ceiling",   ceiling,   false, true,  p_result ) ||
         ! masnae::get_parameter( p_parameters, "prefix",    prefix,    false, true,  p_result ) )
        return;
    //
    // To validate the currency code, create a null amount
    adawat::Amount a;
    if ( a.set( "0", currency ) != adawat::Amount::result::ok ) {
        p_result.set_jresp_error( "Invalid currency ");
        return;
    }
    //
    // Set default values
    if ( overdraft.empty() ) overdraft = "0";
    if ( ceiling.empty() )   ceiling   = "10000000000";
    //
    g_AccountManager.create( currency, overdraft, ceiling, prefix, p_result );
}

//-------------------------------------------------------------------------
// Delete accounts. Their balance must be null and they must
// not be present in t_posting
// Parameter:
//  accounts    csv of accounts to delete
// Returns:
//      success
//      error
//      failed
//      rejected    not_zero                account
//      rejected    used

void AdmeteRequestHandler::process_ws_account_delete ( const masnae::SingleKey_ValueList & p_parameters,
                                                       masnae::ProcessingResponse &        p_result )
{
    // Check that the caller is allowed here
    if ( ! has_permission( "admete.account_delete", p_result ) )
        return;
    //
    // Retrieve parameters
    std::string accounts;
    //
    if ( ! masnae::get_parameter( p_parameters, "accounts", accounts, false, false, p_result ) )
        return;
    //
    g_AccountManager.close( accounts, p_result );
}

//-------------------------------------------------------------------------
// Retrieves the sum and count of transaction on an account
// aggregate by periods of time.
// Parameter:
//   mode       negative, positive, sum, sum_absolute
//   account    account number
//   periods    CSV of periods, in days (e.g.: 1,7,30)
//   timezone   User's timezone to define 'day' (e.g.: Europe/Paris)
//   type       optional, the journal type to count
//   category   optional, the journal category to count
// Returns:
//      success     data: { "1":{"amount":"3","count":"1","currency":"EUR"},
//                          "7":{"amount":"3","count":"1","currency":"EUR"}}
//      error
//      failed
//      rejected    invalid_mode

void AdmeteRequestHandler::process_ws_account_usage ( const masnae::SingleKey_ValueList & p_parameters,
                                                      masnae::ProcessingResponse &        p_result )
{
    // Check that the caller is allowed here
    if ( ! has_permission( "admete.account_usage", p_result ) )
        return;
    //
    // Retrieve parameters
    std::string mode, account, periods, timezone, type, category;
    //
    if ( ! masnae::get_parameter( p_parameters, "mode",     mode,     false, false, p_result ) ||
         ! masnae::get_parameter( p_parameters, "account",  account,  false, false, p_result ) ||
         ! masnae::get_parameter( p_parameters, "periods",  periods,  false, false, p_result ) ||
         ! masnae::get_parameter( p_parameters, "timezone", timezone, false, false, p_result ) ||
         ! masnae::get_parameter( p_parameters, "type",     type,     false, true,  p_result ) ||
         ! masnae::get_parameter( p_parameters, "category", category, false, true,  p_result ) )
        return;
    //
    g_AccountManager.usage( mode, account, periods, timezone, type, category, p_result );
}

} // namespace admete
