// Copyright 2019 Pierre del Perugia
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

//
// Manage accounts
//

#ifndef admete_account_h
#define admete_account_h

#include <shared_mutex>
#include <adawat/adawat_error_control.h>

#include "masnae_bus.h"
#include "masnae_mysql.h"

#include "admete_misc.h"

namespace admete {
    
//-------------------------------------------------------------------------
class AccountManager : public masnae::bus::Subscriber
{
    public:
        // To be called once before any execution
        bool              start            ( void );
        //
        // To be called once before stopping the program
        void              stop             ( void );
        //
        // Search and return accounts
        void              select           ( const std::string &           p_fields_list,
                                             const std::string &           p_filter,
                                             const std::string &           p_order,
                                             const std::string &           p_range,
                                             masnae::ProcessingResponse &  p_result );
        //
        // Search and return accounts
        void              ledger           ( const std::string &           p_fields_list,
                                             const std::string &           p_filter,
                                             const std::string &           p_order,
                                             const std::string &           p_range,
                                             masnae::ProcessingResponse &  p_result );
        //
        // Create a new account
        void              create           ( const std::string &           p_currency,
                                             const std::string &           p_overdraft,
                                             const std::string &           p_ceiling,
                                             const std::string &           p_prefix,
                                             masnae::ProcessingResponse &  p_result );
        //
        // Returns the count of transactions per period for an account
        void              usage            ( const std::string &           p_mode,
                                             const std::string &           p_account,
                                             const std::string &           p_periods,
                                             const std::string &           p_timezone,
                                             const std::string &           p_type,
                                             const std::string &           p_category,
                                             masnae::ProcessingResponse &  p_result );
        //
        // Delete accounts. Their balance must be null and they must
        // not be present in t_posting
        void              close            ( const std::string &           p_accounts,
                                             masnae::ProcessingResponse &  p_result );
        //
        // Called by the bus with a new message
        virtual
        void              bus_event        ( const masnae::bus::Message &  p_message );
        //
    private:
        // Configuration
        std::shared_mutex   m_mutex_config;
        bool                m_control_key_enabled = false;  // add a control key at the end of the account number
        adawat::error_control_type
                            m_control_key_type    = adawat::error_control_type::damm;   // control key algorythm to use
        size_t              m_length              = 10;     // account number length (without key)
        std::string         m_prefix              = "1";    // default prefix digits, must not start with 0
        //
        // Parse the configuration
        bool update_config ( void );
        //
        // Create functions
        //
        // Get next ids for account (locking)
        bool get_next_account ( masnae::mysql::Connection &   p_db,
                                uint64_t &                    p_next_account,
                                masnae::ProcessingResponse &  p_result );
        //
        // From the next sequence build an account number (adding prefix, padding and key)
        // Change next_account to next value
        bool build_account_from_sequence ( const std::string &           p_prefix,
                                           uint64_t &                    p_next_account,
                                           std::string &                 p_account,
                                           masnae::ProcessingResponse &  p_result );
        //
        // Write account to database
        bool write_account ( masnae::mysql::Connection &   p_db,
                             const std::string &           p_account,
                             const std::string &           p_currency,
                             const std::string &           p_overdraft,
                             const std::string &           p_ceiling,
                             masnae::ProcessingResponse &  p_result );
        //
        // Set JResp data with the result of a account creation
        bool fill_successful_result ( const std::string &           p_account,
                                      const std::string &           p_overdraft,
                                      const std::string &           p_ceiling,
                                      masnae::ProcessingResponse &  p_result );
        //
        // Write next sequence value to use
        bool update_next_account ( masnae::mysql::Connection &   p_db,
                                   uint64_t                      p_next_account,
                                   masnae::ProcessingResponse &  p_result );
        //
        // Delete functions
        //
        // Parse a CSV list of account, quote them, check
        // that it is not empty
        bool parse_account_list ( const std::string &       p_accounts,
                                  std::string &             p_accounts_where );
        //
        // Check that all balance are null
        bool check_null ( const SingleKey_Accounts &        p_accounts_mem,
                          masnae::ProcessingResponse &      p_result );
        //
        // Delete accounts
        bool delete_account ( masnae::mysql::Connection &   p_db,
                              const std::string &           p_accounts_where,
                              masnae::ProcessingResponse &  p_result );
        //
        // Usage functions
        //
        friend bool validate_account2 ( void );
        //
        struct Usage
        {
            adawat::Amount  amount;
            unsigned        count = 0;
            //
            bool add   ( const Usage &  p_usage );
        };
        //
        typedef std::map< unsigned, Usage >  UsagePer;
        //
        // Given a CSV of periods (in days), checks that all values are
        // integers in the allowed range ([1..constants::k_usage_period_max])
        // and sort the list
        bool get_usage_periods ( const std::string &           p_periods_csv,
                                 std::vector< unsigned > &     p_periods,
                                 unsigned &                    p_period_max,
                                 masnae::ProcessingResponse &  p_result );
        //
        // Depending on the mode, build the amount agregation and
        // the where clause to apply
        bool get_usage_mode    ( const std::string &           p_mode,
                                 std::string &                 p_mode_amount,
                                 std::string &                 p_mode_where,
                                 masnae::ProcessingResponse &  p_result );
        //
        // Returns the DB period(s) needed to fill the requested periods
        std::string
             get_db_periods    ( unsigned                      p_period_max );
        //
        // Extract account's data
        bool get_usage_data    ( const std::string &           p_mode_amount,
                                 const std::string &           p_mode_where,
                                 const std::string &           p_account,
                                 const std::string &           p_timezone,
                                 const std::string &           p_type,
                                 const std::string &           p_category,
                                 unsigned                      p_period_max,
                                 UsagePer &                    p_usage_per_day,   // indexed by day, from today
                                 masnae::ProcessingResponse &  p_result );
        //
        bool aggregate_periods ( const UsagePer &                 p_usage_per_day,
                                 const std::vector< unsigned > &  p_periods,
                                 UsagePer &                       p_usage_per_period,
                                 masnae::ProcessingResponse &     p_result );
};

} // namespace admete

#endif
