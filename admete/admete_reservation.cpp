// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

// Copyright 2019 Pierre del Perugia
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <adawat/adawat_string.h>
#include <adawat/adawat_uid.h>

#include "masnae_global.h"
#include "masnae_misc.h"

#include "admete_misc.h"
#include "admete_reservation.h"

namespace admete {

//-------------------------------------------------------------------------
// Use the Masnae ws_tables engine to access to the t_reservations table
// but don't expose it directly for seurity reason
class TableReservations : public masnae::ws_tables::Definition
{
    public:
        TableReservations() :
            masnae::ws_tables::Definition( "t_reservations" )
        {
            m_fields = {
                // name              type ins   upt   req   def.ins  def.upt
                { "k_account",     { 'S', true, true, true, nullptr, nullptr } },
                { "k_reservation", { 'S', true, true, true, nullptr, nullptr } },
                { "f_amount",      { 'D', true, true, true, nullptr, nullptr } },
                { "d_expiration",  { 'S', true, true, true, nullptr, nullptr } },
                { "s_narration",   { 'S', true, true, true, nullptr, nullptr } }
            };
        }
};

//-------------------------------------------------------------------------
// Called by the main to initialize the class
bool ReservationManager::start ( void )
{
    masnae::globals::publisher->subscribe( this );
    //
    update_config();
    //
    return true;
}

//-------------------------------------------------------------------------
// Call before program termination
void ReservationManager::stop ( void )
{
    if ( masnae::globals::publisher != nullptr )
        masnae::globals::publisher->unsubscribe( this );
}

//-------------------------------------------------------------------------
// Return statistics on the module
std::string ReservationManager::specific_info ( void )
{
    // Retrieve data
    masnae::SingleKey_Value  data;
    //
    {
        std::lock_guard< std::mutex > lock( m_stats_mutex );
        //
        masnae::add_stats( "cleaning", m_stats_cleaning, data );
    }
    //
    return "{\"stats\": " + masnae::json_convert( data ) +
           "}";
}

//-------------------------------------------------------------------------
// Search and return accounts
void ReservationManager::select ( const std::string &           p_fields_list,
                                  const std::string &           p_filter,
                                  const std::string &           p_order,
                                  const std::string &           p_range,
                                  masnae::ProcessingResponse &  p_result )
{
    TableReservations  table;
    //
    table.execute( "SELECT", p_fields_list, p_filter, p_order, p_range, p_result );
}

//-------------------------------------------------------------------------
// Try to apply the reservation on the account
// Returns:
//      success
//      rejected    incompatible_currency   account
//                  overdraft               account
//                  ceiling                 account
//                  not_found
//                  not_positive
//                  invalid_precision       limit
//                  invalid_value           limit
//                  overflow                limit / account
bool ReservationManager::reserve( const std::string &           p_account,
                                  SingleKey_Accounts &          p_accounts_mem,
                                  const std::string &           p_limit,
                                  masnae::ProcessingResponse &  p_result )
{
    // Check that the account was found
    auto account = p_accounts_mem.find( p_account );
    //
    if ( account == p_accounts_mem.end() ) {
        p_result.set_jresp_rejected( "not_found", "null" );
        return false;
    }
    //
    // Build the reservation to take
    adawat::Amount  reservation;
    //
    auto result = reservation.set( p_limit, account->second.balance.currency() );
    if ( result != adawat::Amount::result::ok ) {
        p_result.set_jresp_rejected(
            adawat::Amount::to_string( result ),
            masnae::json_quote_string( p_limit )
        );
        return false;
    }
    //
    // Reservation must be positive
    if ( ! reservation.is_positive() ) {
        p_result.set_jresp_rejected( "not_positive", "null" );
        return false;
    }
    //
    // Make as if this new reservation was commited in account
    result = account->second.reserved.add( reservation );
    if ( result != adawat::Amount::result::ok ) {
        p_result.set_jresp_rejected(
            adawat::Amount::to_string( result ),
            masnae::json_quote_string( p_limit )
        );
        return false;
    }
    //
    // Check that the resulting balance is within account's limits
    if ( ! account->second.check_limits( p_result ) )
        return false;        
    //
    return true;
}

//-------------------------------------------------------------------------
// Create reservation, fill p_result with reservation id
// Returns:
//      success     { "reservation": "", "expiration": "" }
//      failed
bool ReservationManager::create_reservation ( masnae::mysql::Connection &   p_db,
                                              const std::string &           p_account,
                                              const std::string &           p_limit,
                                              int64_t                       p_lifetime,
                                              const std::string &           p_narration,
                                              masnae::ProcessingResponse &  p_result )
{
    // Generate id and calculate expiration
    std::string  expiration, id;
    std::time_t  now;
    //
    time( & now );
    expiration = masnae::mysql::datetime_utc( now + p_lifetime );
    id         = adawat::uid_no_ts( 6, adawat::uid_alphabet::base62 );
    //
    if ( id.empty() || expiration.empty() ) {
        p_result.set_jresp_failed();
        p_result.add_error( "create_reservation empty id or expiration" );
        return false;
    }
    //
    // Store new reservation
    std::string nu;
    unsigned    db_result = p_db.do_insert(
        "INSERT INTO t_reservations"
        " (k_account,k_reservation,f_amount,d_expiration,s_narration) VALUES "
        " ('" + masnae::mysql::sql_escape( p_account   ) +
        "','" + masnae::mysql::sql_escape( id          ) +
        "','" + masnae::mysql::sql_escape( p_limit     ) +
        "','" + masnae::mysql::sql_escape( expiration  ) +
        "','" + masnae::mysql::sql_escape( p_narration ) + "')",
        nu );
    //
    if ( db_result != 0 ) {
        p_result.set_jresp_failed();
        p_result.add_error( "create_reservation insert error " + std::to_string( db_result ) );
        return false;
    }
    //
    p_result.set_jresp_success(
        masnae::json_convert(
            { { "reservation", id         },
              { "expiration",  expiration } }
        )
    );
    //
    return true;
}

//-------------------------------------------------------------------------
// Create a new reservation
// Returns:
//      success     { "reservation": "", "expiration": "" }
//      failed
//      rejected    incompatible_currency   account
//                  overdraft               account
//                  ceiling                 account
//                  not_found
//                  not_positive
//                  invalid_precision       limit
//                  invalid_value           limit
//                  overflow                limit / account
void ReservationManager::create ( const std::string &           p_account,
                                  const std::string &           p_limit,
                                  int64_t                       p_lifetime,
                                  const std::string &           p_narration,
                                  masnae::ProcessingResponse &  p_result )
{
    // Eventually delete expired reservation
    maybe_clean();
    //
    p_result.add_log( masnae::ProcessingResponse::regular,
                      "Reserving " + p_limit   +
                      " on "       + p_account +
                      " for "      + std::to_string( p_lifetime ) );
    //
    // Transaction
    SingleKey_Accounts  accounts_mem;
    //
    auto     db     = masnae::globals::mysql_handler->get_connection( masnae::mysql::writer );
    unsigned result = db.start_transaction();
    //
    if ( result == 0 ) {
        bool ok = true;
        //
        // Get accounts details (locking them and reservation)
        ok = ok && get_accounts( db,
                                 "'" + masnae::mysql::sql_escape( p_account ) + "'",
                                 accounts_mem,
                                 p_result );
        //
        // Try to apply the reservation on the account
        ok = ok && reserve( p_account, accounts_mem, p_limit, p_result );
        //
        // Create reservation, fill p_result with reservation id
        ok = ok && create_reservation( db, p_account, p_limit, p_lifetime, p_narration, p_result );
        //
        // Finaly, commit all
        if ( ok ) {
            result = db.commit_transaction();
            //
            if ( result != 0 ) {
                masnae::mysql::build_resp_error( result, p_result );
                ok = false;
            }
        }
        //
        if ( ! ok )
            db.rollback_transaction();
    } else {
        masnae::mysql::build_resp_error( result, p_result );
    }
}

//-------------------------------------------------------------------------
// Delete reservations
void ReservationManager::close ( const std::string &           p_filter,
                                 const std::string &           p_order,
                                 const std::string &           p_range,
                                 masnae::ProcessingResponse &  p_result )
{
    TableReservations  table;
    //
    table.execute( "DELETE", "", p_filter, p_order, p_range, p_result );
}

//-------------------------------------------------------------------------
void ReservationManager::bus_event ( const masnae::bus::Message &  p_message )
{
    switch ( p_message.type )
    {
        case masnae::bus::Message::config_updated:
            update_config();
            break;
        default:
            break;
    }
}

//-------------------------------------------------------------------------
// Reads configuration from global config object.
bool ReservationManager::update_config ( void )
{
    bool     ok = true;
    int64_t  new_cleaning_probability;
    //
    //-----
    new_cleaning_probability = 
        masnae::globals::config_handler->get_int64( "/reservation/cleaning_probability", 100 );
    //
    ok &= new_cleaning_probability >= 1 && new_cleaning_probability <= 10000;
    //
    //-----
    if ( ok ) {
        std::lock_guard<std::shared_mutex>  lock( m_mutex_config );
        //
        m_cleaning_probability = new_cleaning_probability;
    } else {
        masnae::service_error_log( "ReservationManager: invalid config" );
    }
    //
    return ok;
}

//-------------------------------------------------------------------------
// Check if cleaning must be done and do it
void ReservationManager::maybe_clean ( void )
{
    int64_t cleaning_probability;
    //
    // One chance out of m_cleaning_probability
    {
        std::shared_lock< std::shared_mutex >  lock( m_mutex_config );
        //
        std::uniform_int_distribution< int64_t > distribution( 0, m_cleaning_probability );
        //
        if ( distribution( m_generator ) != 0 )
            return;
        //
        cleaning_probability = m_cleaning_probability;
    }
    //
    // Delete a limited number of old reservations
    masnae::mysql::Connection  db = masnae::globals::mysql_handler->get_connection( masnae::mysql::writer );
    size_t                     affected;
    //
    db.do_delete( "DELETE FROM t_reservations "
                  "WHERE d_expiration < utc_timestamp() "
                  "LIMIT " + std::to_string( 2 * cleaning_probability ),
                  affected );
    //
    // Update stats
    {
        std::lock_guard< std::mutex > lock( m_stats_mutex );
        //
        m_stats_cleaning.add_value( affected );
    }
}

} // namespace admete
