// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

// Copyright 2019 Pierre del Perugia
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <adawat/adawat_string.h>
#include <rapidjson/document.h>

#include "masnae_misc.h"

#include "admete_misc.h"

namespace admete {

//-------------------------------------------------------------------------
// Check if balance is in [overdraft+reserved,ceiling]
// (reserved is always positive or null)
// Returns:
//      success
//      failed
//      rejected    overflow                account
//                  incompatible_currency   account
//                  overdraft               account
//                  ceiling                 account
bool Account::check_limits ( masnae::ProcessingResponse &  p_result ) const
{
    adawat::Amount::result  r;
    adawat::Amount          lower_limit = overdraft;
    int                     cmp;
    //
    //-------
    if ( reserved.is_negative() ) {
        p_result.set_jresp_failed();
        p_result.add_error( "check_limits negative reservation for " + account );
        return false;
    }
    //
    //-------
    r = lower_limit.add( reserved );
    if ( r != adawat::Amount::result::ok ) {
        p_result.set_jresp_rejected( adawat::Amount::to_string( r ),
                                     masnae::json_quote_string( account ) );
        return false;
    }
    //
    //-------
    r = balance.comp( lower_limit, cmp );
    if ( r != adawat::Amount::result::ok ) {
        p_result.set_jresp_rejected( adawat::Amount::to_string( r ),
                                     masnae::json_quote_string( account ) );
        return false;
    }
    if ( cmp == -1 ) {  // balance < lower_limit
        p_result.set_jresp_rejected( "overdraft",
                                     masnae::json_quote_string( account ) );
        return false;
    }
    //
    //-------
    r = balance.comp( ceiling, cmp );
    if ( r != adawat::Amount::result::ok ) {
        p_result.set_jresp_rejected( adawat::Amount::to_string( r ),
                                     masnae::json_quote_string( account ) );
        return false;
    }
    if ( cmp == +1 ) {  // balance > ceiling
        p_result.set_jresp_rejected( "ceiling",
                                     masnae::json_quote_string( account ) );
        return false;
    }
    //
    //-------
    return true;
}

//-------------------------------------------------------------------------
void log_accounts ( const std::string &           p_title,
                    const SingleKey_Accounts &    p_accounts_mem,
                    masnae::ProcessingResponse &  p_result )
{
    if ( p_accounts_mem.empty() ) {
        p_result.add_log( masnae::ProcessingResponse::verbose, p_title + " -" );
        return;
    }
    //
    p_result.add_log( masnae::ProcessingResponse::verbose,
                      adawat::str_sprintf( "%-13s    balance  overdraft    ceiling   reserved", 
                                           p_title.c_str() ) );
    //
    for ( const auto & account: p_accounts_mem )
        p_result.add_log( masnae::ProcessingResponse::verbose,
                          adawat::str_sprintf( "%-13s %10s %10s %10s %10s", 
                                               account.first.c_str(),
                                               account.second.balance.value().c_str(),
                                               account.second.overdraft.value().c_str(),
                                               account.second.ceiling.value().c_str(),
                                               account.second.reserved.value().c_str()
                          ) );
}

//-------------------------------------------------------------------------
void log_reservations ( const std::string &            p_title,
                        const SingleKey_Reservation &  p_reservation_mem,
                        masnae::ProcessingResponse &   p_result )
{
    if ( p_reservation_mem.empty() ) {
        p_result.add_log( masnae::ProcessingResponse::verbose, p_title + " -" );
        return;
    }
    //
    p_result.add_log( masnae::ProcessingResponse::verbose,
                      adawat::str_sprintf( "%-20s     id     amount",
                                           p_title.c_str() ) );
    //
    for ( const auto & reservation: p_reservation_mem )
        p_result.add_log( masnae::ProcessingResponse::verbose,
                          adawat::str_sprintf( "%-20s %6s %10s", 
                                               reservation.second.account.c_str(),
                                               reservation.second.id.c_str(),
                                               reservation.second.amount.value().c_str()
                          ) );
}

//-------------------------------------------------------------------------
// Parse a list of records as a JSON expression
// p_text is an array of objects with a single member, like:
//    [ {"a":"1000001","m":"-2"}, {"a":"1000002","m":"1"}, {"a":"1000003","m":"1"} ]
//    [ {"a":"1000001","m":"-2","r":"456781"}, {"a":"1000003","m":"2"} ]
// Returns:
//      success
//      error
//      rejected    invalid_currency
//                  invalid_value           amount
bool parse_records ( const std::string &           p_text,
                     const std::string &           p_currency,
                     List_Record &                 p_records,
                     masnae::ProcessingResponse &  p_result )
{
    p_records.clear();
    //
    // Parse it
    rapidjson::Document  document;
    //
    document.Parse< rapidjson::kParseFullPrecisionFlag |
                         rapidjson::kParseCommentsFlag >( p_text.c_str() );
    //
    if ( document.HasParseError() ) {
        p_result.set_jresp_error( "records is not a valid JSON" );
        return false;
    }
    if ( ! document.IsArray() ) {
        p_result.set_jresp_error( "records is not an array" );
        return false;
    }
    //
    // Copy records in p_records
    for ( const auto & mvt : document.GetArray() ) {
        if ( ! mvt.IsObject()                                  ||
             ! mvt.HasMember( "a" ) || ! mvt[ "a" ].IsString() ||  // account
             ! mvt.HasMember( "m" ) || ! mvt[ "m" ].IsString() )   // amount
        {
            p_result.set_jresp_error( "records' object members a and m are mandatory" );
            return false;
        }
        //
        Record new_record;
            new_record.account = mvt[ "a" ].GetString();
        //
        auto result = new_record.amount.set( mvt[ "m" ].GetString(), p_currency );
        if ( result != adawat::Amount::result::ok ) {
            p_result.set_jresp_rejected(
                adawat::Amount::to_string( result ),
                masnae::json_quote_string( mvt[ "m" ].GetString() )
            );
            return false;
        }
        //
        // If a reservation is set, copy it
        if ( mvt.HasMember( "r" ) ) {
            if ( ! mvt[ "r" ].IsString() ) {
                p_result.set_jresp_error( "records' object members r must be a string" );
                return false;
            }
            //
            new_record.reservation  = mvt[ "r" ].GetString();
        }
        //
        p_records.push_back ( new_record );
    }
    //
    if ( p_records.empty() ) {  // must not be empty
        p_result.set_jresp_error( "records must not be empty" );
        return false;
    }
    //
    return true;
}

//-------------------------------------------------------------------------
// Parse JSON to retrieve records, build SQL WHERE clause to retrieve
// accounts, check that sum of records is zero. Set p_result on error
// JSON format: array of single member objects with account : amount
//          [ {"10001":"-5"}, {"10002":"5"} ]
// Returns:
//      success
//      error
//      rejected    overflow                account
//                  invalid_currency
//                  not_zero
//                  reservation_credited    reservation
//                  invalid_value           amount
bool prepare_records( const std::string &           p_records_json,
                      const std::string &           p_currency,
                      List_Record  &                p_records,
                      std::string &                 p_accounts_where,
                      masnae::ProcessingResponse &  p_result )
{
    // Check the currency, prepare to sum records (set sum = 0)
    adawat::Amount sum;
    //
    auto r = sum.set( "0", p_currency );
    if ( r != adawat::Amount::result::ok ) {
        p_result.set_jresp_rejected( adawat::Amount::to_string( r ), "null" );
        return false;
    }
    //
    // Parse a list of records as a JSON expression
    if ( ! parse_records ( p_records_json, p_currency, p_records, p_result ) )
        return false;
    //
    // Log records
    p_result.add_log( masnae::ProcessingResponse::verbose, "Records:" );
    //
    for ( const auto & mvt: p_records )
        p_result.add_log( masnae::ProcessingResponse::verbose,
                          adawat::str_sprintf( "%-12s %10s %s", 
                                               mvt.account.c_str(),
                                               mvt.amount.value().c_str(),
                                               mvt.reservation.c_str()
                          ) );
    //
    // Build the WHERE clause to retrieve account (an account
    // can appear several times in records)
    std::set< std::string > accounts;
    //
    for ( const auto & mvt: p_records )
        accounts.insert( "'" + masnae::mysql::sql_escape( mvt.account ) + "'" );
    //
    p_accounts_where = adawat::str_join( accounts );
    //
    // Check that the sum of records is zero and does not overflow
    // Check that there is no reservation on positive number
    for ( const auto & mvt: p_records ) {
        //
        // ... add to sum
        auto result = sum.add( mvt.amount );
        //
        if ( result != adawat::Amount::result::ok ) {
            p_result.set_jresp_rejected(
                adawat::Amount::to_string( result ),
                masnae::json_quote_string( mvt.account )
            );
            return false;
        }
        //
        // ... check that reservation are not on credited operation
        if ( ! mvt.reservation.empty() && mvt.amount.is_positive() ) {
            p_result.set_jresp_rejected(
                "reservation_credited",
                masnae::json_quote_string( mvt.reservation )
            );
            return false;
        }
    }
    //
    if ( ! sum.is_zero() ) {
        p_result.set_jresp_rejected( "not_zero", "null" );
        return false;
    }
    //
    return true;
}

//-------------------------------------------------------------------------
// Period is the UTC year / month
std::string get_period ( time_t  p_now )
{
    std::tm  utc;
    char     text[ 8 ];
    //
    if ( gmtime_r( & p_now, & utc ) == nullptr )
        return "";
    //
    size_t length_text = strftime( text, 8, "%Y%m", &utc );
    if ( length_text == 0 )  // overflow
        return "";
    //
    return text;
}

//-------------------------------------------------------------------------
// Get next ids for journal and posting (locking)
// Returns:
//      success
//      failed
bool get_next_sequence( masnae::mysql::Connection &   p_db,
                        uint64_t &                    p_next_journal,
                        uint64_t &                    p_next_posting,
                        masnae::ProcessingResponse &  p_result )
{
    masnae::SingleKeyValue_List  rows;
    unsigned                     db_result;
    //
    db_result = p_db.do_select( "SELECT i_next_journal,i_next_posting"
                                " FROM t_sequencer"
                                " FOR UPDATE",
                                rows );
    //
    if ( db_result != 0 ) {
        p_result.set_jresp_failed();
        p_result.add_error( "get_next_sequence select error " + std::to_string( db_result ) );
        return false;
    }
    //
    if ( rows.size() != 1 ) {
        p_result.set_jresp_failed();
        p_result.add_error( "get_next_sequence not single row" );
        return false;
    }
    //
    char t;
    //
    if ( sscanf( rows[0]["i_next_journal"].c_str(), "%" SCNu64 "%c", & p_next_journal, & t ) != 1 ) {
        p_result.set_jresp_failed();
        p_result.add_error( "get_next_sequence invalid i_next_journal" );
        return false;
    }
    //
    if ( sscanf( rows[0]["i_next_posting"].c_str(), "%" SCNu64 "%c", & p_next_posting, & t ) != 1 ) {
        p_result.set_jresp_failed();
        p_result.add_error( "get_next_sequence invalid i_next_posting" );
        return false;
    }
    //
    p_result.add_log( masnae::ProcessingResponse::verbose,
                      "Sequence: journal #" + rows[0]["i_next_journal"] +
                              ", posting #" + rows[0]["i_next_posting"] );
    //
    return true;
}

//-------------------------------------------------------------------------
// Set JResp data with the result of a journal transaction / reversal
bool fill_successful_result ( const std::string &           p_date,
                              uint64_t                      p_next_journal,
                              uint64_t                      p_next_posting,
                              size_t                        p_nb_posting,
                              masnae::ProcessingResponse &  p_result )
{
    std::string sequence = std::to_string( p_next_posting % 1000 ) +
                           "-" +
                           std::to_string( p_nb_posting );
    //
    p_result.set_jresp_success( masnae::json_convert( {
        { "date",        p_date                           },
        { "transaction", std::to_string( p_next_journal ) },
        { "sequence",    sequence                         },
    } ) );
    //
    return true;
}

//-------------------------------------------------------------------------
// Get accounts details (locking them and reservation)
// p_accounts_where is a CSV (escaped) of accounts
// Returns:
//      success
//      failed

    // Read one of the amount fields of an account
    bool read_db_amount ( const masnae::SingleKey_Value &  p_account,
                          const std::string &              p_field,
                          adawat::Amount &                 p_amount,
                          masnae::ProcessingResponse &     p_result )
    {
        try {
            auto r = p_amount.set( p_account.at( p_field      ),    // can throw
                                   p_account.at( "s_currency" ) );  // can throw
            //
            if ( r != adawat::Amount::result::ok ) {
                p_result.set_jresp_failed();
                p_result.add_error( "db account " + p_field + " invalid: " + 
                                    adawat::Amount::to_string( r ) + " on " +
                                    p_account.at( "k_account" ) );  // can throw
                return false;
            }
            //
            return true;
        } catch ( const std::out_of_range & e ) {  // inexisting field
            std::string what = e.what();
            //
            p_result.set_jresp_failed();
            p_result.add_error( "read_db_amount: " + what );
            return false;            
        }
    }

    // Create in-memory objects for the accounts, parse amounts
    bool load_account_in_memory ( const masnae::SingleKeyValue_List &  p_accounts_db,
                                  SingleKey_Accounts &                 p_accounts_mem,
                                  masnae::ProcessingResponse &         p_result )
    {
        try {
            for ( const auto & a: p_accounts_db ) {
                Account new_account;
                //
                new_account.account = a.at( "k_account" );  // can throw
                //
                if ( ! read_db_amount( a, "f_balance",   new_account.balance,   p_result ) ||
                     ! read_db_amount( a, "f_overdraft", new_account.overdraft, p_result ) ||
                     ! read_db_amount( a, "f_ceiling",   new_account.ceiling,   p_result ) ||
                     ! read_db_amount( a, "f_reserved",  new_account.reserved,  p_result ) )
                       return false;
                //
                p_accounts_mem[ new_account.account ] = new_account;
            }
        } catch ( const std::out_of_range & e ) {  // inexisting field
            std::string what = e.what();
            //
            p_result.set_jresp_failed();
            p_result.add_error( "load_account_in_memory: " + what );
            return false;
        }
        //
        return true;
    }
    
bool get_accounts( masnae::mysql::Connection &    p_db,
                   const std::string &            p_accounts_where,
                   SingleKey_Accounts &           p_accounts_mem,
                   masnae::ProcessingResponse &   p_result )
{
    unsigned                    db_result;
    masnae::SingleKeyValue_List accounts;
    //
    db_result = p_db.do_select(
        "SELECT a.*, COALESCE( r.f_reserved, 0 ) AS f_reserved FROM ("
        " SELECT k_account,s_currency,f_balance,f_overdraft,f_ceiling"
        " FROM t_accounts"
        " WHERE k_account IN ( " + p_accounts_where +" )"
        ") a "
        "LEFT JOIN ("
        " SELECT k_account,SUM(f_amount) AS f_reserved"
        " FROM t_reservations"
        " WHERE k_account IN ( " + p_accounts_where +" )"
        "   AND d_expiration > utc_timestamp()"
        ") r "
        "ON a.k_account=r.k_account "
        "FOR UPDATE",
        accounts );
    //
    if ( db_result != 0 ) {
        p_result.set_jresp_failed();
        p_result.add_error( "get_accounts select error " + p_accounts_where +
                            ": " + std::to_string( db_result ) );
        return false;
    }
    //
    // Create in-memory objects for the accounts, parse amounts
    if ( ! load_account_in_memory( accounts, p_accounts_mem, p_result ) )
        return false;
    //
    log_accounts( "Accounts:", p_accounts_mem, p_result );
    //
    return true;
}

//-------------------------------------------------------------------------
// Get reservation details (locking them and accounts)
// p_accounts_where is a CSV (escaped) of accounts
// Returns:
//      success
//      failed

    // Create in-memory objects for the reservations, parse amounts
    bool load_reservations_in_memory ( const masnae::SingleKeyValue_List &  p_reservation_db,
                                       SingleKey_Reservation &              p_reservation_mem,
                                       masnae::ProcessingResponse &         p_result )
    {
        try {
            for ( const auto & reservation: p_reservation_db ) {
                Reservation new_reservation;
                    new_reservation.account = reservation.at( "k_account"     );  // can throw
                    new_reservation.id      = reservation.at( "k_reservation" );  // can throw
                //
                // Reservations are indexed by account:id in map
                std::string index = new_reservation.account + ":" + new_reservation.id;
                //
                auto r = new_reservation.amount.set( reservation.at( "f_amount"   ),    // can throw
                                                     reservation.at( "s_currency" ) );  // can throw
                //
                if ( r != adawat::Amount::result::ok ) {
                    p_result.set_jresp_failed();
                    p_result.add_error( "get_reservations " + index +
                                        " invalid: "  + adawat::Amount::to_string( r )  +
                                        " on amount " + reservation.at( "f_amount" ) );  // can throw
                    return false;
                }
                //
                // Reservations can be null if fully used, but not negative
                if ( new_reservation.amount.is_negative() ) {
                    p_result.set_jresp_failed();
                    p_result.add_error( "get_reservations " + index +
                                        " is negative" );
                    return false;
                }
                //
                p_reservation_mem[ index ] = new_reservation;
            }
        } catch ( const std::out_of_range & e ) {  // inexisting field
            std::string what = e.what();
            //
            p_result.set_jresp_failed();
            p_result.add_error( "load_reservations_in_memory: " + what );
            return false;
        }
        //
        return true;
    }

bool get_reservations( masnae::mysql::Connection &    p_db,
                       const std::string &            p_accounts_where,
                       SingleKey_Reservation &        p_reservation_mem,
                       masnae::ProcessingResponse &   p_result )
{
    unsigned                    db_result;
    masnae::SingleKeyValue_List rows;
    //
    db_result = p_db.do_select(
        "SELECT r.k_account,k_reservation,f_amount,s_currency" 
        " FROM t_reservations r"
        " JOIN t_accounts a ON a.k_account=r.k_account"
        " WHERE r.k_account IN (" + p_accounts_where +")"
        "   AND r.d_expiration > utc_timestamp()"
        " FOR UPDATE",
        rows );
    //
    if ( db_result != 0 ) {
        p_result.set_jresp_failed();
        p_result.add_error( "get_reservations select error " + p_accounts_where +
                            ": " + std::to_string( db_result ) );
        return false;
    }
    //
    // Create in-memory objects for the accounts, parse amounts
    if ( ! load_reservations_in_memory( rows, p_reservation_mem, p_result ) )
        return false;
    //
    // Log reservation
    log_reservations( "Reservations:", p_reservation_mem, p_result );
    //
    return true;
}

//-------------------------------------------------------------------------
// Update reservations
// Returns:
//      success
//      failed
bool write_reservations ( masnae::mysql::Connection &    p_db,
                          const SingleKey_Reservation &  p_reservation_mem,
                          masnae::ProcessingResponse &   p_result )
{
    size_t       affected;
    unsigned     db_result;
    std::string  reservations;
    //
    // Update accounts
    for ( const auto & r: p_reservation_mem ) {
        db_result = p_db.do_update(
            "UPDATE t_reservations SET"
            " f_amount = "          + masnae::mysql::sql_escape( r.second.amount.value() ) + " "
            "WHERE k_account= '"    + masnae::mysql::sql_escape( r.second.account        ) + "'"
            "  and k_reservation='" + masnae::mysql::sql_escape( r.second.id             ) + "'",
            affected );
        //
        if ( db_result != 0 ) {
            p_result.set_jresp_failed();
            p_result.add_error( "write_reservations update error " + std::to_string( db_result ) );
            return false;
        }
        if ( affected != 1 ) {
            p_result.set_jresp_failed();
            p_result.add_error( "write_reservations update changed " + std::to_string( affected ) + " rows" );
            return false;
        }
    }
    //
    return true;
}

//-------------------------------------------------------------------------
// Applies records to balances, check account overflows / currencies
// Returns:
//      success
//      failed
//      rejected    overdraft               account
//                  ceiling                 account
//                  overflow                account
//                  incompatible_currency   account
//                  not_found               account
//                  reservation_not_found   reservation
//                  reservation_too_low     reservation

    // If a reservation is set in a record, check that the record
    // amount is lower or equal to the reservation and:
    //  - decrease the reservation
    //  - decrease amount reserved in account
    bool handle_reservation ( const Record &                p_record,
                              SingleKey_Reservation &       p_reservation_mem,
                              Account &                     p_account,
                              masnae::ProcessingResponse &  p_result )
    {
        // If no reservation is specified
        if ( p_record.reservation.empty() )
            return true;
        //
        // Reservations are indexed by account:id in map
        std::string index = p_record.account + ":" + p_record.reservation;
        //
        // Find reservation
        auto reservation = p_reservation_mem.find( index );
        if ( reservation == p_reservation_mem.end() ) {
            p_result.set_jresp_rejected( "reservation_not_found",
                                         masnae::json_quote_string( p_record.reservation ) );
            return false;
        }
        //
        // p_record.amount is negative, calculate the absolute value
        auto amount_to_take = p_record.amount;
        //
        auto result = amount_to_take.mul( "-1" );
        if ( result != adawat::Amount::result::ok ) {
            p_result.set_jresp_failed();
            p_result.add_error( "handle_reservation mul invalid: " + 
                                adawat::Amount::to_string( result ) + " on " +
                                amount_to_take.value() );
        }
        //
        //---
        p_result.add_log( masnae::ProcessingResponse::verbose,
                          "Using reservation " + p_record.reservation +
                          " for " + amount_to_take.value() );
        //
        // Compare with record amount
        int  cmp;
        //
        result = amount_to_take.comp( reservation->second.amount, cmp );
        if ( result != adawat::Amount::result::ok ) {
            p_result.set_jresp_failed();
            p_result.add_error( "handle_reservation comp invalid: " + 
                                adawat::Amount::to_string( result ) + " on " +
                                p_record.account );
        }
        //
        if ( cmp == 1 ) {  // amount_to_take > reservation->second.amount
            p_result.set_jresp_rejected( "reservation_too_low",
                                         masnae::json_quote_string( p_record.reservation ) );
            return false;
        }
        //
        // Decrease reservation
        result = reservation->second.amount.sub( amount_to_take );
        if ( result != adawat::Amount::result::ok ) {
            p_result.set_jresp_failed();
            p_result.add_error( "handle_reservation sub reservation: " + 
                                adawat::Amount::to_string( result ) );
            return false;
        }
        //
        // Decrease reserved
        result = p_account.reserved.sub( amount_to_take );
        if ( result != adawat::Amount::result::ok ) {
            p_result.set_jresp_failed();
            p_result.add_error( "handle_reservation sub reservation: " + 
                                adawat::Amount::to_string( result ) );
            return false;
        }
        //
        return true;
    }

bool apply_records ( const List_Record &                  p_records,
                     SingleKey_Accounts &                 p_accounts_mem,
                     SingleKey_Reservation &              p_reservation_mem,
                     masnae::ProcessingResponse &         p_result )
{
    // For each record...
    for ( const auto & mvt: p_records ) {
        //
        // Find the corresponding account
        auto account = p_accounts_mem.find( mvt.account );
        if ( account == p_accounts_mem.end() ) {
            p_result.set_jresp_rejected( "not_found",
                                         masnae::json_quote_string( mvt.account ) );
            return false;
        }
        //
        // If a reservation is specified on this record, use it
        if ( ! handle_reservation( mvt, p_reservation_mem, account->second, p_result ) )
            return false;
        //
        // ... add the record amount to the balance of the account
        auto r = account->second.balance.add( mvt.amount );
        if ( r != adawat::Amount::result::ok ) {
            p_result.set_jresp_rejected( adawat::Amount::to_string( r ),
                                         masnae::json_quote_string( mvt.account ) );
            return false;
        }
    }
    //
    // Log new calculated balances and reservations
    log_accounts    ( "New balances:",     p_accounts_mem,    p_result );
    log_reservations( "New reservations:", p_reservation_mem, p_result );
    //
    // Check that all balances are still in [overdraft,ceiling]
    for ( const auto & account: p_accounts_mem )
        if ( ! account.second.check_limits( p_result ) )
            return false;
    //
    return true;
}

//-------------------------------------------------------------------------
// Write journal
// Returns:
//      success
//      failed
//      rejected    duplicate_reference
bool write_journal( masnae::mysql::Connection &   p_db,
                    const std::string &           p_period,
                    uint64_t                      p_next_journal,
                    const std::string &           p_date,
                    const std::string &           p_type,
                    const std::string &           p_category,
                    const std::string &           p_narration,
                    const std::string &           p_user,
                    const std::string &           p_user_reference,
                    masnae::ProcessingResponse &  p_result )
{
    std::string  id;
    unsigned     db_result;
    //
    db_result = p_db.do_insert(
        "INSERT INTO t_journal (i_period,k_journal,d_date,s_type,s_category,s_narration,s_user,s_user_reference) VALUES ("
        "'" + masnae::mysql::sql_escape( p_period       ) + "',"
            + std::to_string           ( p_next_journal ) +  ","
        "'" + masnae::mysql::sql_escape( p_date         ) + "',"
        "'" + masnae::mysql::sql_escape( p_type         ) + "',"
        "'" + masnae::mysql::sql_escape( p_category     ) + "',"
        "'" + masnae::mysql::sql_escape( p_narration    ) + "'," +
        ( p_user.empty()           ?
            "NULL" :
            ("'" + masnae::mysql::sql_escape( p_user ) + "'") ) +
          "," +
        ( p_user_reference.empty() ?
            "NULL" :
            ("'" + masnae::mysql::sql_escape( p_user_reference ) + "'") ) +
          ")",
        id );
    //
    if ( db_result != 0 ) {
        //
        // In case of Duplicate entry error on the s_user_reference column,
        // return a specific error. MySQL error message contains the index name:
        //    Duplicate entry '789456' for key 'UK_user_reference'
        if ( db_result == 1062 ) {
            std::string error = p_db.native_error();
            //
            if ( error.find( "UK_user_reference" ) != std::string::npos ) {
                p_result.set_jresp_rejected( "duplicate_reference", "null" );
                return false;
            }
        }
        //
        p_result.set_jresp_failed();
        p_result.add_error( "write_journal insert error " + std::to_string( db_result ) );
        //
        return false;
    }
    //
    return true;
}

//-------------------------------------------------------------------------
// Write posting, update accounts' balance
// Update p_next_journal / p_next_posting with the next vales to use
// Returns:
//      success
//      failed
bool write_posting( masnae::mysql::Connection &   p_db,
                    const std::string &           p_period,
                    uint64_t &                    p_next_journal,
                    uint64_t &                    p_next_posting,
                    const List_Record &           p_records,
                    const SingleKey_Accounts &    p_accounts_mem,
                    masnae::ProcessingResponse &  p_result )
{
    size_t       affected;
    unsigned     db_result;
    std::string  id, postings;
    //
    // Build posting list query
    for ( const auto & mvt: p_records )
        postings += ",('" + masnae::mysql::sql_escape( p_period              ) + "',"
                          + std::to_string           ( p_next_journal        ) + ","
                          + std::to_string           ( p_next_posting++      ) + ","  // increase posting id
                          + masnae::mysql::sql_escape( mvt.account           ) + ","
                          + masnae::mysql::sql_escape( mvt.amount.value()    ) + ",'"
                          + masnae::mysql::sql_escape( mvt.amount.currency() ) + "')";
    //
    postings[ 0 ] = ' ';  // replace leading comma by space
    p_next_journal++;
    //
    // Write posting list
    db_result = p_db.do_insert(
        "INSERT INTO t_posting (i_period,k_journal,k_post,k_account,f_amount,s_currency) VALUES " +
        postings,
        id );
    //
    if ( db_result != 0 ) {
        p_result.set_jresp_failed();
        p_result.add_error( "write_posting insert error " + std::to_string( db_result ) );
        return false;
    }
    //
    // Update accounts
    for ( const auto & a: p_accounts_mem ) {
        db_result = p_db.do_update(
            "UPDATE t_accounts SET"
            " f_balance =     " + masnae::mysql::sql_escape( a.second.balance.value() ) + " "
            "WHERE k_account='" + masnae::mysql::sql_escape( a.first                  ) + "'",
            affected );
        //
        if ( db_result != 0 ) {
            p_result.set_jresp_failed();
            p_result.add_error( "write_posting update error " + std::to_string( db_result ) );
            return false;
        }
        if ( affected != 1 ) {
            p_result.set_jresp_failed();
            p_result.add_error( "write_posting update changed " + std::to_string( affected ) + " rows" );
            return false;
        }
    }
    //
    return true;
}

//-------------------------------------------------------------------------
// Write next sequence values to use
// Returns:
//      success
//      failed
bool update_next_sequence( masnae::mysql::Connection &   p_db,
                           uint64_t                      p_next_journal,
                           uint64_t                      p_next_posting,
                           masnae::ProcessingResponse &  p_result )
{
    size_t    affected;
    unsigned  db_result;
    //
    db_result = p_db.do_update(
        "UPDATE t_sequencer SET"
        " i_next_journal = " + std::to_string( p_next_journal ) + ","
        " i_next_posting = " + std::to_string( p_next_posting ),
        affected );
    //
    if ( db_result != 0 ) {
        p_result.set_jresp_failed();
        p_result.add_error( "update_next_sequence update error " + std::to_string( db_result ) );
        return false;
    }
    //
    if ( affected != 1 ) {
        p_result.set_jresp_failed();
        p_result.add_error( "update_next_sequence update changed " + std::to_string( affected ) + " rows" );
        return false;
    }
    //
    return true;
}

} // namespace admete
